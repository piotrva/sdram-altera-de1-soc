-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "02/06/2015 21:28:32"

-- 
-- Device: Altera 5CSEMA5F31C6 Package FBGA896
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	SDRAM_TEST IS
    PORT (
	DRAM_ADDR : OUT std_logic_vector(12 DOWNTO 0);
	DRAM_DQ : INOUT std_logic_vector(15 DOWNTO 0);
	DRAM_BA : OUT std_logic_vector(1 DOWNTO 0);
	DRAM_LDQM : OUT std_logic;
	DRAM_UDQM : OUT std_logic;
	DRAM_RAS_N : OUT std_logic;
	DRAM_CAS_N : OUT std_logic;
	DRAM_CKE : OUT std_logic;
	DRAM_CLK : OUT std_logic;
	DRAM_WE_N : OUT std_logic;
	DRAM_CS_N : OUT std_logic;
	CLOCK_50 : IN std_logic;
	LEDR : OUT std_logic_vector(9 DOWNTO 0);
	HEX0 : OUT std_logic_vector(0 TO 6);
	HEX1 : OUT std_logic_vector(0 TO 6);
	HEX2 : OUT std_logic_vector(0 TO 6);
	HEX3 : OUT std_logic_vector(0 TO 6);
	HEX4 : OUT std_logic_vector(0 TO 6);
	HEX5 : OUT std_logic_vector(0 TO 6);
	SW : IN std_logic_vector(9 DOWNTO 0);
	KEY : IN std_logic_vector(3 DOWNTO 0)
	);
END SDRAM_TEST;

-- Design Ports Information
-- DRAM_ADDR[0]	=>  Location: PIN_AK14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- DRAM_ADDR[1]	=>  Location: PIN_AH14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- DRAM_ADDR[2]	=>  Location: PIN_AG15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- DRAM_ADDR[3]	=>  Location: PIN_AE14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- DRAM_ADDR[4]	=>  Location: PIN_AB15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- DRAM_ADDR[5]	=>  Location: PIN_AC14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- DRAM_ADDR[6]	=>  Location: PIN_AD14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- DRAM_ADDR[7]	=>  Location: PIN_AF15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- DRAM_ADDR[8]	=>  Location: PIN_AH15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- DRAM_ADDR[9]	=>  Location: PIN_AG13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- DRAM_ADDR[10]	=>  Location: PIN_AG12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- DRAM_ADDR[11]	=>  Location: PIN_AH13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- DRAM_ADDR[12]	=>  Location: PIN_AJ14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- DRAM_BA[0]	=>  Location: PIN_AF13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- DRAM_BA[1]	=>  Location: PIN_AJ12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- DRAM_LDQM	=>  Location: PIN_AB13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- DRAM_UDQM	=>  Location: PIN_AK12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- DRAM_RAS_N	=>  Location: PIN_AE13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- DRAM_CAS_N	=>  Location: PIN_AF11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- DRAM_CKE	=>  Location: PIN_AK13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- DRAM_CLK	=>  Location: PIN_AH12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- DRAM_WE_N	=>  Location: PIN_AA13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- DRAM_CS_N	=>  Location: PIN_AG11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- LEDR[0]	=>  Location: PIN_V16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- LEDR[1]	=>  Location: PIN_W16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- LEDR[2]	=>  Location: PIN_V17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- LEDR[3]	=>  Location: PIN_V18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- LEDR[4]	=>  Location: PIN_W17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- LEDR[5]	=>  Location: PIN_W19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- LEDR[6]	=>  Location: PIN_Y19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- LEDR[7]	=>  Location: PIN_W20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- LEDR[8]	=>  Location: PIN_W21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- LEDR[9]	=>  Location: PIN_Y21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX0[6]	=>  Location: PIN_AH28,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX0[5]	=>  Location: PIN_AG28,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX0[4]	=>  Location: PIN_AF28,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX0[3]	=>  Location: PIN_AG27,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX0[2]	=>  Location: PIN_AE28,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX0[1]	=>  Location: PIN_AE27,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX0[0]	=>  Location: PIN_AE26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX1[6]	=>  Location: PIN_AD27,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX1[5]	=>  Location: PIN_AF30,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX1[4]	=>  Location: PIN_AF29,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX1[3]	=>  Location: PIN_AG30,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX1[2]	=>  Location: PIN_AH30,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX1[1]	=>  Location: PIN_AH29,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX1[0]	=>  Location: PIN_AJ29,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX2[6]	=>  Location: PIN_AC30,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX2[5]	=>  Location: PIN_AC29,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX2[4]	=>  Location: PIN_AD30,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX2[3]	=>  Location: PIN_AC28,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX2[2]	=>  Location: PIN_AD29,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX2[1]	=>  Location: PIN_AE29,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX2[0]	=>  Location: PIN_AB23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX3[6]	=>  Location: PIN_AB22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX3[5]	=>  Location: PIN_AB25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX3[4]	=>  Location: PIN_AB28,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX3[3]	=>  Location: PIN_AC25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX3[2]	=>  Location: PIN_AD25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX3[1]	=>  Location: PIN_AC27,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX3[0]	=>  Location: PIN_AD26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX4[6]	=>  Location: PIN_W25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX4[5]	=>  Location: PIN_V23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX4[4]	=>  Location: PIN_W24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX4[3]	=>  Location: PIN_W22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX4[2]	=>  Location: PIN_Y24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX4[1]	=>  Location: PIN_Y23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX4[0]	=>  Location: PIN_AA24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX5[6]	=>  Location: PIN_AA25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX5[5]	=>  Location: PIN_AA26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX5[4]	=>  Location: PIN_AB26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX5[3]	=>  Location: PIN_AB27,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX5[2]	=>  Location: PIN_Y27,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX5[1]	=>  Location: PIN_AA28,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- HEX5[0]	=>  Location: PIN_V25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- SW[0]	=>  Location: PIN_AB12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[1]	=>  Location: PIN_AC12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[2]	=>  Location: PIN_AF9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[3]	=>  Location: PIN_AF10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[4]	=>  Location: PIN_AD11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[5]	=>  Location: PIN_AD12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[6]	=>  Location: PIN_AE11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[7]	=>  Location: PIN_AC9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[8]	=>  Location: PIN_AD10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[9]	=>  Location: PIN_AE12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- KEY[0]	=>  Location: PIN_AA14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- KEY[1]	=>  Location: PIN_AA15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- KEY[2]	=>  Location: PIN_W15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- DRAM_DQ[0]	=>  Location: PIN_AK6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- DRAM_DQ[1]	=>  Location: PIN_AJ7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- DRAM_DQ[2]	=>  Location: PIN_AK7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- DRAM_DQ[3]	=>  Location: PIN_AK8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- DRAM_DQ[4]	=>  Location: PIN_AK9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- DRAM_DQ[5]	=>  Location: PIN_AG10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- DRAM_DQ[6]	=>  Location: PIN_AK11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- DRAM_DQ[7]	=>  Location: PIN_AJ11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- DRAM_DQ[8]	=>  Location: PIN_AH10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- DRAM_DQ[9]	=>  Location: PIN_AJ10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- DRAM_DQ[10]	=>  Location: PIN_AJ9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- DRAM_DQ[11]	=>  Location: PIN_AH9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- DRAM_DQ[12]	=>  Location: PIN_AH8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- DRAM_DQ[13]	=>  Location: PIN_AH7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- DRAM_DQ[14]	=>  Location: PIN_AJ6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- DRAM_DQ[15]	=>  Location: PIN_AJ5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 4mA
-- CLOCK_50	=>  Location: PIN_AF14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- KEY[3]	=>  Location: PIN_Y16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default


ARCHITECTURE structure OF SDRAM_TEST IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_DRAM_ADDR : std_logic_vector(12 DOWNTO 0);
SIGNAL ww_DRAM_BA : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_DRAM_LDQM : std_logic;
SIGNAL ww_DRAM_UDQM : std_logic;
SIGNAL ww_DRAM_RAS_N : std_logic;
SIGNAL ww_DRAM_CAS_N : std_logic;
SIGNAL ww_DRAM_CKE : std_logic;
SIGNAL ww_DRAM_CLK : std_logic;
SIGNAL ww_DRAM_WE_N : std_logic;
SIGNAL ww_DRAM_CS_N : std_logic;
SIGNAL ww_CLOCK_50 : std_logic;
SIGNAL ww_LEDR : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_HEX0 : std_logic_vector(0 TO 6);
SIGNAL ww_HEX1 : std_logic_vector(0 TO 6);
SIGNAL ww_HEX2 : std_logic_vector(0 TO 6);
SIGNAL ww_HEX3 : std_logic_vector(0 TO 6);
SIGNAL ww_HEX4 : std_logic_vector(0 TO 6);
SIGNAL ww_HEX5 : std_logic_vector(0 TO 6);
SIGNAL ww_SW : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_KEY : std_logic_vector(3 DOWNTO 0);
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_VCOPH_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_MHI_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_REFCLK_SELECT_CLKIN_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_MHI_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_SHIFTEN_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER_VCO0PH_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[1].gpll~PLL_OUTPUT_COUNTER_VCO0PH_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \SW[0]~input_o\ : std_logic;
SIGNAL \SW[1]~input_o\ : std_logic;
SIGNAL \SW[2]~input_o\ : std_logic;
SIGNAL \SW[3]~input_o\ : std_logic;
SIGNAL \SW[4]~input_o\ : std_logic;
SIGNAL \SW[5]~input_o\ : std_logic;
SIGNAL \SW[6]~input_o\ : std_logic;
SIGNAL \SW[7]~input_o\ : std_logic;
SIGNAL \SW[8]~input_o\ : std_logic;
SIGNAL \SW[9]~input_o\ : std_logic;
SIGNAL \KEY[0]~input_o\ : std_logic;
SIGNAL \KEY[1]~input_o\ : std_logic;
SIGNAL \KEY[2]~input_o\ : std_logic;
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \CLOCK_50~input_o\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_REFCLK_SELECT_O_EXTSWITCHBUF\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_REFCLK_SELECT_O_CLKOUT\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI2\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI3\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI4\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI5\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI6\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI7\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_O_UP\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI1\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_O_SHIFTENM\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI0\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_O_SHIFT\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_CNTNEN\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIGSHIFTEN7\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_TCLK\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH0\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH1\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH2\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH3\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH4\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH5\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH6\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH7\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIGSHIFTEN6\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\ : std_logic;
SIGNAL \Add0~5_sumout\ : std_logic;
SIGNAL \KEY[3]~input_o\ : std_logic;
SIGNAL \SDRAM1|u1|state_r.INITWAIT~feeder_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add0~25_sumout\ : std_logic;
SIGNAL \Add0~6\ : std_logic;
SIGNAL \Add0~9_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|sDataDir_r~q\ : std_logic;
SIGNAL \hAddr[23]~2_combout\ : std_logic;
SIGNAL \DRAM_DQ[4]~input_o\ : std_logic;
SIGNAL \DRAM_DQ[5]~input_o\ : std_logic;
SIGNAL \DRAM_DQ[3]~input_o\ : std_logic;
SIGNAL \Equal1~5_combout\ : std_logic;
SIGNAL \DRAM_DQ[0]~input_o\ : std_logic;
SIGNAL \SDRAM1|u1|hDOut_r[0]~feeder_combout\ : std_logic;
SIGNAL \DRAM_DQ[2]~input_o\ : std_logic;
SIGNAL \DRAM_DQ[1]~input_o\ : std_logic;
SIGNAL \Equal1~4_combout\ : std_logic;
SIGNAL \DRAM_DQ[7]~input_o\ : std_logic;
SIGNAL \DRAM_DQ[8]~input_o\ : std_logic;
SIGNAL \SDRAM1|u1|hDOut_r[8]~feeder_combout\ : std_logic;
SIGNAL \DRAM_DQ[6]~input_o\ : std_logic;
SIGNAL \SDRAM1|u1|hDOut_r[6]~feeder_combout\ : std_logic;
SIGNAL \Equal1~3_combout\ : std_logic;
SIGNAL \DRAM_DQ[14]~input_o\ : std_logic;
SIGNAL \DRAM_DQ[13]~input_o\ : std_logic;
SIGNAL \DRAM_DQ[12]~input_o\ : std_logic;
SIGNAL \Equal1~1_combout\ : std_logic;
SIGNAL \DRAM_DQ[10]~input_o\ : std_logic;
SIGNAL \DRAM_DQ[9]~input_o\ : std_logic;
SIGNAL \DRAM_DQ[11]~input_o\ : std_logic;
SIGNAL \Equal1~2_combout\ : std_logic;
SIGNAL \DRAM_DQ[15]~input_o\ : std_logic;
SIGNAL \Equal1~0_combout\ : std_logic;
SIGNAL \Equal1~6_combout\ : std_logic;
SIGNAL \Selector33~0_combout\ : std_logic;
SIGNAL \Equal0~3_combout\ : std_logic;
SIGNAL \Equal0~0_combout\ : std_logic;
SIGNAL \Equal0~1_combout\ : std_logic;
SIGNAL \Equal0~4_combout\ : std_logic;
SIGNAL \hAddr[23]~4_combout\ : std_logic;
SIGNAL \FSM_autotest.A_SET_READ~q\ : std_logic;
SIGNAL \Selector0~0_combout\ : std_logic;
SIGNAL \FSM_autotest.A_IDLE~q\ : std_logic;
SIGNAL \hAddr[23]~1_combout\ : std_logic;
SIGNAL \hAddr[23]~3_combout\ : std_logic;
SIGNAL \Add0~10\ : std_logic;
SIGNAL \Add0~13_sumout\ : std_logic;
SIGNAL \Add0~14\ : std_logic;
SIGNAL \Add0~17_sumout\ : std_logic;
SIGNAL \Add0~18\ : std_logic;
SIGNAL \Add0~21_sumout\ : std_logic;
SIGNAL \Add0~22\ : std_logic;
SIGNAL \Add0~25_sumout\ : std_logic;
SIGNAL \Add0~26\ : std_logic;
SIGNAL \Add0~29_sumout\ : std_logic;
SIGNAL \Add0~30\ : std_logic;
SIGNAL \Add0~33_sumout\ : std_logic;
SIGNAL \Add0~34\ : std_logic;
SIGNAL \Add0~37_sumout\ : std_logic;
SIGNAL \Add0~38\ : std_logic;
SIGNAL \Add0~41_sumout\ : std_logic;
SIGNAL \Add0~42\ : std_logic;
SIGNAL \Add0~45_sumout\ : std_logic;
SIGNAL \Add0~46\ : std_logic;
SIGNAL \Add0~49_sumout\ : std_logic;
SIGNAL \Add0~50\ : std_logic;
SIGNAL \Add0~53_sumout\ : std_logic;
SIGNAL \Add0~54\ : std_logic;
SIGNAL \Add0~57_sumout\ : std_logic;
SIGNAL \Add0~58\ : std_logic;
SIGNAL \Add0~61_sumout\ : std_logic;
SIGNAL \Add0~62\ : std_logic;
SIGNAL \Add0~65_sumout\ : std_logic;
SIGNAL \Add0~66\ : std_logic;
SIGNAL \Add0~69_sumout\ : std_logic;
SIGNAL \Add0~70\ : std_logic;
SIGNAL \Add0~73_sumout\ : std_logic;
SIGNAL \Add0~74\ : std_logic;
SIGNAL \Add0~77_sumout\ : std_logic;
SIGNAL \Add0~78\ : std_logic;
SIGNAL \Add0~81_sumout\ : std_logic;
SIGNAL \Add0~82\ : std_logic;
SIGNAL \Add0~85_sumout\ : std_logic;
SIGNAL \Add0~86\ : std_logic;
SIGNAL \Add0~89_sumout\ : std_logic;
SIGNAL \Add0~90\ : std_logic;
SIGNAL \Add0~93_sumout\ : std_logic;
SIGNAL \Add0~94\ : std_logic;
SIGNAL \Add0~97_sumout\ : std_logic;
SIGNAL \Add0~98\ : std_logic;
SIGNAL \Add0~1_sumout\ : std_logic;
SIGNAL \Equal0~2_combout\ : std_logic;
SIGNAL \Selector2~0_combout\ : std_logic;
SIGNAL \FSM_autotest.A_WRITE~q\ : std_logic;
SIGNAL \Selector3~0_combout\ : std_logic;
SIGNAL \FSM_autotest.A_WRITE_WAIT~q\ : std_logic;
SIGNAL \Selector32~0_combout\ : std_logic;
SIGNAL \wr~q\ : std_logic;
SIGNAL \rd~q\ : std_logic;
SIGNAL \SDRAM1|u1|sAddr_x[10]~12_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add0~26\ : std_logic;
SIGNAL \SDRAM1|u1|Add0~29_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|Add0~30\ : std_logic;
SIGNAL \SDRAM1|u1|Add0~33_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|Add0~34\ : std_logic;
SIGNAL \SDRAM1|u1|Add0~37_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|Add0~38\ : std_logic;
SIGNAL \SDRAM1|u1|Add0~41_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|Add0~42\ : std_logic;
SIGNAL \SDRAM1|u1|Add0~45_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|Add0~46\ : std_logic;
SIGNAL \SDRAM1|u1|Add0~1_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|Add0~2\ : std_logic;
SIGNAL \SDRAM1|u1|Add0~17_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|Add0~18\ : std_logic;
SIGNAL \SDRAM1|u1|Add0~5_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|Add0~6\ : std_logic;
SIGNAL \SDRAM1|u1|Add0~9_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|Add0~10\ : std_logic;
SIGNAL \SDRAM1|u1|Add0~13_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|Add0~14\ : std_logic;
SIGNAL \SDRAM1|u1|Add0~21_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|doSelfRfsh~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|doSelfRfsh~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add0~22\ : std_logic;
SIGNAL \SDRAM1|u1|Add0~49_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|Add0~50\ : std_logic;
SIGNAL \SDRAM1|u1|Add0~53_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|doSelfRfsh~2_combout\ : std_logic;
SIGNAL \wr~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|doSelfRfsh~3_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Selector31~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|state_r.INITSETMODE~q\ : std_logic;
SIGNAL \SDRAM1|u1|rasTimer_r[2]~5_combout\ : std_logic;
SIGNAL \SDRAM1|u1|activeRow_r[0][3]~q\ : std_logic;
SIGNAL \SDRAM1|u1|activeRow_r[0][4]~q\ : std_logic;
SIGNAL \SDRAM1|u1|activeRow_r[0][5]~q\ : std_logic;
SIGNAL \SDRAM1|u1|doActivate~2_combout\ : std_logic;
SIGNAL \SDRAM1|u1|activeRow_r[0][8]~q\ : std_logic;
SIGNAL \SDRAM1|u1|activeRow_r[0][6]~feeder_combout\ : std_logic;
SIGNAL \SDRAM1|u1|activeRow_r[0][6]~q\ : std_logic;
SIGNAL \SDRAM1|u1|activeRow_r[0][7]~q\ : std_logic;
SIGNAL \SDRAM1|u1|doActivate~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|activeRow_r[0][12]~q\ : std_logic;
SIGNAL \SDRAM1|u1|Equal2~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Selector59~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|doSelfRfsh~4_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Selector59~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|wrTimer_r[1]~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|WideOr18~combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add4~2\ : std_logic;
SIGNAL \SDRAM1|u1|Add4~21_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|rfshCntr_r[4]~3_combout\ : std_logic;
SIGNAL \SDRAM1|u1|rfshCntr_x[1]~5_combout\ : std_logic;
SIGNAL \SDRAM1|u1|rfshCntr_r[4]~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|refTimer_r[5]~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|Add3~38\ : std_logic;
SIGNAL \SDRAM1|u1|Add3~33_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|Add3~34\ : std_logic;
SIGNAL \SDRAM1|u1|Add3~29_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|Add3~30\ : std_logic;
SIGNAL \SDRAM1|u1|Add3~25_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|Add3~26\ : std_logic;
SIGNAL \SDRAM1|u1|Add3~5_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|refTimer_x[4]~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add3~6\ : std_logic;
SIGNAL \SDRAM1|u1|Add3~2\ : std_logic;
SIGNAL \SDRAM1|u1|Add3~21_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|refTimer_x[6]~3_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add3~22\ : std_logic;
SIGNAL \SDRAM1|u1|Add3~17_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|refTimer_x[7]~2_combout\ : std_logic;
SIGNAL \SDRAM1|u1|refTimer_r[7]~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|Add3~18\ : std_logic;
SIGNAL \SDRAM1|u1|Add3~13_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|Add3~14\ : std_logic;
SIGNAL \SDRAM1|u1|Add3~9_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|Equal6~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add3~1_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|refTimer_x[5]~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add3~37_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|refTimer_x[0]~4_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Equal6~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Equal6~2_combout\ : std_logic;
SIGNAL \SDRAM1|u1|rfshCntr_r[4]~2_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add4~22\ : std_logic;
SIGNAL \SDRAM1|u1|Add4~17_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|rfshCntr_x[2]~4_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add4~18\ : std_logic;
SIGNAL \SDRAM1|u1|Add4~13_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|rfshCntr_x[3]~3_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add4~14\ : std_logic;
SIGNAL \SDRAM1|u1|Add4~9_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|rfshCntr_x[4]~2_combout\ : std_logic;
SIGNAL \SDRAM1|u1|rfshCntr_r[4]~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|rfshCntr_r[1]~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|Add4~10\ : std_logic;
SIGNAL \SDRAM1|u1|Add4~5_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|rfshCntr_x[5]~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add4~6\ : std_logic;
SIGNAL \SDRAM1|u1|Add4~53_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|rfshCntr_x[6]~13_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add4~54\ : std_logic;
SIGNAL \SDRAM1|u1|Add4~49_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|rfshCntr_x[7]~12_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add4~50\ : std_logic;
SIGNAL \SDRAM1|u1|Add4~45_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|rfshCntr_x[8]~11_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add4~46\ : std_logic;
SIGNAL \SDRAM1|u1|Add4~41_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|rfshCntr_x[9]~10_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add4~42\ : std_logic;
SIGNAL \SDRAM1|u1|Add4~37_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|rfshCntr_x[10]~9_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add4~38\ : std_logic;
SIGNAL \SDRAM1|u1|Add4~33_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|rfshCntr_x[11]~8_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add4~34\ : std_logic;
SIGNAL \SDRAM1|u1|Add4~29_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|rfshCntr_x[12]~7_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add4~30\ : std_logic;
SIGNAL \SDRAM1|u1|Add4~25_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|rfshCntr_x[13]~6_combout\ : std_logic;
SIGNAL \SDRAM1|u1|rfshCntr_r[3]~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|Equal8~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|rfshCntr_r[5]~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|rfshCntr_r[9]~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|Equal8~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Equal0~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|cmd_x[3]~5_combout\ : std_logic;
SIGNAL \SDRAM1|u1|wrTimer_r[0]~3_combout\ : std_logic;
SIGNAL \SDRAM1|u1|wrTimer_r[1]~2_combout\ : std_logic;
SIGNAL \SDRAM1|u1|combinatorial~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|rasTimer_r[0]~4_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add1~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|rasTimer_r[1]~3_combout\ : std_logic;
SIGNAL \SDRAM1|u1|rasTimer_r[2]~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|rasTimer_r[2]~2_combout\ : std_logic;
SIGNAL \SDRAM1|u1|rasTimer_r[0]~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|rasTimer_r[2]~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Selector22~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Equal9~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|state_x.RW~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Selector59~2_combout\ : std_logic;
SIGNAL \SDRAM1|u1|doActivate~3_combout\ : std_logic;
SIGNAL \SDRAM1|u1|activeRow_r[0][0]~q\ : std_logic;
SIGNAL \SDRAM1|u1|activeRow_r[0][1]~q\ : std_logic;
SIGNAL \SDRAM1|u1|activeRow_r[0][2]~q\ : std_logic;
SIGNAL \SDRAM1|u1|doActivate~4_combout\ : std_logic;
SIGNAL \SDRAM1|u1|activeRow_r[0][9]~q\ : std_logic;
SIGNAL \SDRAM1|u1|activeRow_r[0][10]~feeder_combout\ : std_logic;
SIGNAL \SDRAM1|u1|activeRow_r[0][10]~q\ : std_logic;
SIGNAL \SDRAM1|u1|activeRow_r[0][11]~q\ : std_logic;
SIGNAL \SDRAM1|u1|doActivate~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|doActivate~combout\ : std_logic;
SIGNAL \SDRAM1|u1|Selector20~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|state_r.ACTIVATE~q\ : std_logic;
SIGNAL \SDRAM1|u1|Selector21~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|state_r.REFRESHROW~q\ : std_logic;
SIGNAL \SDRAM1|u1|state_r.INITRFSH~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|state_x.INITSETMODE~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|state_x.RW~3_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Selector28~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|state_x.RW~2_combout\ : std_logic;
SIGNAL \SDRAM1|u1|state_r.INITRFSH~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|state_r.INITRFSH~q\ : std_logic;
SIGNAL \SDRAM1|u1|WideOr16~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|state_x.INITPCHG~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|state_r.INITPCHG~q\ : std_logic;
SIGNAL \SDRAM1|u1|Add4~1_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|rfshCntr_x[0]~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|state_x.INITSETMODE~2_combout\ : std_logic;
SIGNAL \SDRAM1|u1|state_r.RW~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|state_x.INITSETMODE~4_combout\ : std_logic;
SIGNAL \SDRAM1|u1|state_x.RW~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|state_r.RW~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|state_r.RW~q\ : std_logic;
SIGNAL \SDRAM1|u1|Selector22~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|state_r.SELFREFRESH~q\ : std_logic;
SIGNAL \SDRAM1|u1|timer_r[1]~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~9_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|timer_x[0]~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|timer_x[0]~2_combout\ : std_logic;
SIGNAL \SDRAM1|u1|timer_x[0]~3_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~10\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~5_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|state_x.INITSETMODE~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|cke_x~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|timer_r[1]~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~6\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~1_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|timer_x[2]~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Equal7~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|rfshCntr_r[4]~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|state_x.INITSETMODE~3_combout\ : std_logic;
SIGNAL \SDRAM1|u1|state_r.INITWAIT~q\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~2\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~57_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|timer_x[3]~4_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~58\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~53_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|timer_r[4]~12_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~54\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~49_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|timer_r[5]~11_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~50\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~45_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|timer_r[6]~10_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~46\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~42\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~37_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|timer_r[8]~8_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~38\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~33_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|timer_r[9]~7_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~34\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~29_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|timer_r[10]~6_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~30\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~25_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|timer_r[11]~5_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~26\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~21_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|timer_r[12]~4_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~22\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~17_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|timer_r[13]~3_combout\ : std_logic;
SIGNAL \SDRAM1|u1|timer_r[13]~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|timer_r[9]~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|timer_r[12]~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~18\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~13_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|timer_r[14]~2_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Equal7~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Equal7~3_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Add5~41_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|timer_r[7]~9_combout\ : std_logic;
SIGNAL \SDRAM1|u1|timer_r[7]~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|timer_r[5]~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|Equal7~2_combout\ : std_logic;
SIGNAL \SDRAM1|u1|wrTimer_r[1]~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|rdPipeline_x[4]~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|rdPipeline_r[4]~DUPLICATE_q\ : std_logic;
SIGNAL \Selector5~0_combout\ : std_logic;
SIGNAL \FSM_autotest.A_READ~q\ : std_logic;
SIGNAL \Selector6~0_combout\ : std_logic;
SIGNAL \FSM_autotest.A_READ_WAIT~q\ : std_logic;
SIGNAL \Selector37~0_combout\ : std_logic;
SIGNAL \rd~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|wrTimer_r[1]~4_combout\ : std_logic;
SIGNAL \SDRAM1|u1|sDataDir_r~DUPLICATE_q\ : std_logic;
SIGNAL \hAddr[23]~5_combout\ : std_logic;
SIGNAL \FSM_autotest.A_SET_WRITE~q\ : std_logic;
SIGNAL \hAddr[23]~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|sData_r[0]~feeder_combout\ : std_logic;
SIGNAL \hDIn[1]~feeder_combout\ : std_logic;
SIGNAL \hDIn[2]~feeder_combout\ : std_logic;
SIGNAL \hDIn[3]~feeder_combout\ : std_logic;
SIGNAL \SDRAM1|u1|sData_r[3]~feeder_combout\ : std_logic;
SIGNAL \hDIn[4]~feeder_combout\ : std_logic;
SIGNAL \SDRAM1|u1|sData_r[4]~feeder_combout\ : std_logic;
SIGNAL \hDIn[5]~feeder_combout\ : std_logic;
SIGNAL \SDRAM1|u1|sData_r[5]~feeder_combout\ : std_logic;
SIGNAL \hDIn[7]~feeder_combout\ : std_logic;
SIGNAL \SDRAM1|u1|sData_r[8]~feeder_combout\ : std_logic;
SIGNAL \hDIn[9]~feeder_combout\ : std_logic;
SIGNAL \SDRAM1|u1|sData_r[10]~feeder_combout\ : std_logic;
SIGNAL \SDRAM1|u1|sData_r[13]~feeder_combout\ : std_logic;
SIGNAL \SDRAM1|u1|sData_r[14]~feeder_combout\ : std_logic;
SIGNAL \SDRAM1|u1|sData_r[15]~feeder_combout\ : std_logic;
SIGNAL \SDRAM1|u1|sAddr_x[0]~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|sAddr_x[1]~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|sAddr_x[2]~2_combout\ : std_logic;
SIGNAL \SDRAM1|u1|sAddr_x[3]~3_combout\ : std_logic;
SIGNAL \SDRAM1|u1|sAddr_x[4]~17_combout\ : std_logic;
SIGNAL \SDRAM1|u1|sAddr_x[5]~13_combout\ : std_logic;
SIGNAL \SDRAM1|u1|sAddr_x[6]~4_combout\ : std_logic;
SIGNAL \SDRAM1|u1|sAddr_x[7]~5_combout\ : std_logic;
SIGNAL \SDRAM1|u1|sAddr_x[8]~6_combout\ : std_logic;
SIGNAL \SDRAM1|u1|sAddr_x[9]~7_combout\ : std_logic;
SIGNAL \SDRAM1|u1|sAddr_x[10]~8_combout\ : std_logic;
SIGNAL \SDRAM1|u1|sAddr_x[10]~9_combout\ : std_logic;
SIGNAL \SDRAM1|u1|sAddr_x[11]~10_combout\ : std_logic;
SIGNAL \SDRAM1|u1|sAddr_x[12]~11_combout\ : std_logic;
SIGNAL \SDRAM1|u1|cmd_x[0]~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|cmd_x[0]~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|cmd_x[0]~2_combout\ : std_logic;
SIGNAL \SDRAM1|u1|cmd_x[3]~3_combout\ : std_logic;
SIGNAL \SDRAM1|u1|cmd_x[4]~4_combout\ : std_logic;
SIGNAL \SDRAM1|u1|cmd_x[3]~6_combout\ : std_logic;
SIGNAL \SDRAM1|u1|cmd_x[3]~7_combout\ : std_logic;
SIGNAL \SDRAM1|u1|cke_r~q\ : std_logic;
SIGNAL \SDRAM1|u1|cmd_x[2]~8_combout\ : std_logic;
SIGNAL \SDRAM1|u1|cmd_x[2]~9_combout\ : std_logic;
SIGNAL \SDRAM1|u1|Selector20~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|cmd_x[2]~10_combout\ : std_logic;
SIGNAL \Selector36~0_combout\ : std_logic;
SIGNAL \LEDR[0]~reg0_q\ : std_logic;
SIGNAL \Selector35~0_combout\ : std_logic;
SIGNAL \LEDR[1]~reg0_q\ : std_logic;
SIGNAL \LEDR[2]~0_combout\ : std_logic;
SIGNAL \LEDR[2]~reg0_q\ : std_logic;
SIGNAL \LEDR[8]~1_combout\ : std_logic;
SIGNAL \LEDR[8]~reg0_q\ : std_logic;
SIGNAL \LED01|Mux6~0_combout\ : std_logic;
SIGNAL \LED01|Mux5~0_combout\ : std_logic;
SIGNAL \LED01|Mux4~0_combout\ : std_logic;
SIGNAL \LED01|Mux3~0_combout\ : std_logic;
SIGNAL \LED01|Mux2~0_combout\ : std_logic;
SIGNAL \LED01|Mux1~0_combout\ : std_logic;
SIGNAL \LED01|Mux0~0_combout\ : std_logic;
SIGNAL \LED02|Mux6~0_combout\ : std_logic;
SIGNAL \LED02|Mux5~0_combout\ : std_logic;
SIGNAL \LED02|Mux4~0_combout\ : std_logic;
SIGNAL \LED02|Mux3~0_combout\ : std_logic;
SIGNAL \LED02|Mux2~0_combout\ : std_logic;
SIGNAL \LED02|Mux1~0_combout\ : std_logic;
SIGNAL \LED02|Mux0~0_combout\ : std_logic;
SIGNAL \LED03|Mux6~0_combout\ : std_logic;
SIGNAL \LED03|Mux5~0_combout\ : std_logic;
SIGNAL \LED03|Mux4~0_combout\ : std_logic;
SIGNAL \LED03|Mux3~0_combout\ : std_logic;
SIGNAL \LED03|Mux2~0_combout\ : std_logic;
SIGNAL \LED03|Mux1~0_combout\ : std_logic;
SIGNAL \LED03|Mux0~0_combout\ : std_logic;
SIGNAL \LED04|Mux6~0_combout\ : std_logic;
SIGNAL \LED04|Mux5~0_combout\ : std_logic;
SIGNAL \LED04|Mux4~0_combout\ : std_logic;
SIGNAL \LED04|Mux3~0_combout\ : std_logic;
SIGNAL \LED04|Mux2~0_combout\ : std_logic;
SIGNAL \LED04|Mux1~0_combout\ : std_logic;
SIGNAL \LED04|Mux0~0_combout\ : std_logic;
SIGNAL \LED05|Mux6~0_combout\ : std_logic;
SIGNAL \LED05|Mux5~0_combout\ : std_logic;
SIGNAL \LED05|Mux4~0_combout\ : std_logic;
SIGNAL \LED05|Mux3~0_combout\ : std_logic;
SIGNAL \LED05|Mux2~0_combout\ : std_logic;
SIGNAL \LED05|Mux1~0_combout\ : std_logic;
SIGNAL \LED05|Mux0~0_combout\ : std_logic;
SIGNAL \LED06|Mux6~0_combout\ : std_logic;
SIGNAL \LED06|Mux5~0_combout\ : std_logic;
SIGNAL \LED06|Mux4~0_combout\ : std_logic;
SIGNAL \LED06|Mux3~0_combout\ : std_logic;
SIGNAL \LED06|Mux2~0_combout\ : std_logic;
SIGNAL \LED06|Mux1~0_combout\ : std_logic;
SIGNAL \LED06|Mux0~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|rasTimer_r\ : std_logic_vector(2 DOWNTO 0);
SIGNAL hAddr : std_logic_vector(24 DOWNTO 0);
SIGNAL \SDRAM1|u1|rfshCntr_r\ : std_logic_vector(13 DOWNTO 0);
SIGNAL \SDRAM1|u1|sAddr_r\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \SDRAM1|u1|nopCntr_r\ : std_logic_vector(13 DOWNTO 0);
SIGNAL \SDRAM1|u1|ba_r\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \SDRAM1|u1|cmd_r\ : std_logic_vector(5 DOWNTO 0);
SIGNAL \SDRAM1|u1|timer_r\ : std_logic_vector(14 DOWNTO 0);
SIGNAL \SDRAM1|u1|rdPipeline_r\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \SDRAM1|u1|wrTimer_r\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \SDRAM1|u1|activeFlag_r\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \SDRAM1|u1|activeBank_r\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \SDRAM1|u1|hDOut_r\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \SDRAM1|u1|refTimer_r\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \SDRAM1|u1|sData_r\ : std_logic_vector(15 DOWNTO 0);
SIGNAL hDIn : std_logic_vector(15 DOWNTO 0);
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|fboutclk_wire\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \SDRAM1|u1|ALT_INV_refTimer_r[7]~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_refTimer_r[5]~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_sDataDir_r~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_rd~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_wr~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_rdPipeline_r[4]~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_rasTimer_r[0]~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_timer_r[5]~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_timer_r[7]~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_timer_r[9]~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_timer_r[12]~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_timer_r[13]~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_rfshCntr_r[9]~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_rfshCntr_r[1]~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_rfshCntr_r[3]~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_rfshCntr_r[4]~DUPLICATE_q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_rfshCntr_r[5]~DUPLICATE_q\ : std_logic;
SIGNAL \PLL|pll_x2_x4_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\ : std_logic;
SIGNAL \ALT_INV_KEY[3]~input_o\ : std_logic;
SIGNAL \ALT_INV_DRAM_DQ[8]~input_o\ : std_logic;
SIGNAL \ALT_INV_DRAM_DQ[6]~input_o\ : std_logic;
SIGNAL \ALT_INV_DRAM_DQ[0]~input_o\ : std_logic;
SIGNAL ALT_INV_hDIn : std_logic_vector(15 DOWNTO 0);
SIGNAL \SDRAM1|u1|ALT_INV_state_r.INITRFSH~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_state_x.RW~3_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Selector59~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Selector59~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_rfshCntr_r[4]~3_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Equal6~2_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Equal6~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_refTimer_r\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \SDRAM1|u1|ALT_INV_Equal6~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_WideOr18~combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_wrTimer_r[1]~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_wrTimer_r[1]~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_state_x.RW~2_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_state_r.RW~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_state_x.INITSETMODE~4_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add1~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_rasTimer_r[2]~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_state_x.INITSETMODE~2_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_rfshCntr_r[4]~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_state_x.INITSETMODE~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_state_x.RW~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_timer_x[0]~2_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_timer_x[0]~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_timer_r[1]~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_state_x.INITSETMODE~0_combout\ : std_logic;
SIGNAL \ALT_INV_hAddr[23]~2_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_sDataDir_r~q\ : std_logic;
SIGNAL \ALT_INV_FSM_autotest.A_WRITE_WAIT~q\ : std_logic;
SIGNAL \ALT_INV_hAddr[23]~1_combout\ : std_logic;
SIGNAL \ALT_INV_Selector33~0_combout\ : std_logic;
SIGNAL \ALT_INV_FSM_autotest.A_SET_WRITE~q\ : std_logic;
SIGNAL \ALT_INV_Equal1~6_combout\ : std_logic;
SIGNAL \ALT_INV_Equal1~5_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_hDOut_r\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ALT_INV_Equal1~4_combout\ : std_logic;
SIGNAL \ALT_INV_Equal1~3_combout\ : std_logic;
SIGNAL \ALT_INV_Equal1~2_combout\ : std_logic;
SIGNAL \ALT_INV_Equal1~1_combout\ : std_logic;
SIGNAL \ALT_INV_Equal1~0_combout\ : std_logic;
SIGNAL \ALT_INV_FSM_autotest.A_READ_WAIT~q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_rdPipeline_r\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \ALT_INV_Equal0~4_combout\ : std_logic;
SIGNAL \ALT_INV_Equal0~3_combout\ : std_logic;
SIGNAL \ALT_INV_Equal0~2_combout\ : std_logic;
SIGNAL \ALT_INV_Equal0~1_combout\ : std_logic;
SIGNAL \ALT_INV_Equal0~0_combout\ : std_logic;
SIGNAL \ALT_INV_FSM_autotest.A_SET_READ~q\ : std_logic;
SIGNAL \ALT_INV_FSM_autotest.A_READ~q\ : std_logic;
SIGNAL \ALT_INV_FSM_autotest.A_WRITE~q\ : std_logic;
SIGNAL \ALT_INV_FSM_autotest.A_IDLE~q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_cmd_x[2]~9_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_cmd_x[2]~8_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Selector20~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_cke_x~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Selector31~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_cmd_x[3]~6_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_cmd_x[3]~5_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Equal0~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_cmd_x[3]~3_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_state_r.INITWAIT~q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Selector28~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_cmd_x[0]~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_cmd_x[0]~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_state_r.REFRESHROW~q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_state_r.INITRFSH~q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_state_r.SELFREFRESH~q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_doSelfRfsh~4_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_doActivate~combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_doActivate~4_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_activeRow_r[0][2]~q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_activeRow_r[0][1]~q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_activeRow_r[0][0]~q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_doActivate~3_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_activeBank_r\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \SDRAM1|u1|ALT_INV_activeFlag_r\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \SDRAM1|u1|ALT_INV_doActivate~2_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_activeRow_r[0][5]~q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_activeRow_r[0][4]~q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_activeRow_r[0][3]~q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_doActivate~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_activeRow_r[0][8]~q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_activeRow_r[0][7]~q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_activeRow_r[0][6]~q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_doActivate~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_activeRow_r[0][11]~q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_activeRow_r[0][10]~q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_activeRow_r[0][9]~q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Equal2~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_activeRow_r[0][12]~q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_state_x.RW~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_sAddr_x[10]~12_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_sAddr_x[10]~8_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_state_r.INITPCHG~q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_doSelfRfsh~2_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_doSelfRfsh~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_doSelfRfsh~0_combout\ : std_logic;
SIGNAL \ALT_INV_rd~q\ : std_logic;
SIGNAL \ALT_INV_wr~q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Equal9~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Equal8~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Equal8~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Selector22~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_combinatorial~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_wrTimer_r\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \SDRAM1|u1|ALT_INV_state_r.RW~q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_rasTimer_r[2]~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_rasTimer_r\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \SDRAM1|u1|ALT_INV_WideOr16~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_state_r.INITSETMODE~q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Equal7~3_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Equal7~2_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_timer_r\ : std_logic_vector(14 DOWNTO 0);
SIGNAL \SDRAM1|u1|ALT_INV_Equal7~1_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Equal7~0_combout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_state_r.ACTIVATE~q\ : std_logic;
SIGNAL \LED06|ALT_INV_Mux6~0_combout\ : std_logic;
SIGNAL \LED05|ALT_INV_Mux6~0_combout\ : std_logic;
SIGNAL \LED04|ALT_INV_Mux6~0_combout\ : std_logic;
SIGNAL \LED03|ALT_INV_Mux6~0_combout\ : std_logic;
SIGNAL \LED02|ALT_INV_Mux6~0_combout\ : std_logic;
SIGNAL \LED01|ALT_INV_Mux6~0_combout\ : std_logic;
SIGNAL \ALT_INV_LEDR[8]~reg0_q\ : std_logic;
SIGNAL \ALT_INV_LEDR[2]~reg0_q\ : std_logic;
SIGNAL \ALT_INV_LEDR[1]~reg0_q\ : std_logic;
SIGNAL \ALT_INV_LEDR[0]~reg0_q\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add3~37_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add3~21_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add3~17_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add3~5_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add3~1_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add4~53_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add4~49_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add4~45_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add4~41_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add4~37_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add4~33_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add4~29_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add4~25_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add4~21_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add4~17_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add4~13_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add4~9_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add4~5_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add4~1_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add5~57_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add5~53_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add5~49_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add5~45_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add5~41_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add5~37_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add5~33_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add5~29_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add5~25_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add5~21_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add5~17_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add5~13_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add5~9_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add5~5_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_Add5~1_sumout\ : std_logic;
SIGNAL \SDRAM1|u1|ALT_INV_nopCntr_r\ : std_logic_vector(13 DOWNTO 0);
SIGNAL \SDRAM1|u1|ALT_INV_rfshCntr_r\ : std_logic_vector(13 DOWNTO 0);
SIGNAL ALT_INV_hAddr : std_logic_vector(24 DOWNTO 0);

BEGIN

DRAM_ADDR <= ww_DRAM_ADDR;
DRAM_BA <= ww_DRAM_BA;
DRAM_LDQM <= ww_DRAM_LDQM;
DRAM_UDQM <= ww_DRAM_UDQM;
DRAM_RAS_N <= ww_DRAM_RAS_N;
DRAM_CAS_N <= ww_DRAM_CAS_N;
DRAM_CKE <= ww_DRAM_CKE;
DRAM_CLK <= ww_DRAM_CLK;
DRAM_WE_N <= ww_DRAM_WE_N;
DRAM_CS_N <= ww_DRAM_CS_N;
ww_CLOCK_50 <= CLOCK_50;
LEDR <= ww_LEDR;
HEX0 <= ww_HEX0;
HEX1 <= ww_HEX1;
HEX2 <= ww_HEX2;
HEX3 <= ww_HEX3;
HEX4 <= ww_HEX4;
HEX5 <= ww_HEX5;
ww_SW <= SW;
ww_KEY <= KEY;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH0\ <= \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_VCOPH_bus\(0);
\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH1\ <= \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_VCOPH_bus\(1);
\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH2\ <= \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_VCOPH_bus\(2);
\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH3\ <= \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_VCOPH_bus\(3);
\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH4\ <= \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_VCOPH_bus\(4);
\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH5\ <= \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_VCOPH_bus\(5);
\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH6\ <= \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_VCOPH_bus\(6);
\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH7\ <= \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_VCOPH_bus\(7);

\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI0\ <= \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_MHI_bus\(0);
\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI1\ <= \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_MHI_bus\(1);
\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI2\ <= \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_MHI_bus\(2);
\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI3\ <= \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_MHI_bus\(3);
\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI4\ <= \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_MHI_bus\(4);
\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI5\ <= \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_MHI_bus\(5);
\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI6\ <= \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_MHI_bus\(6);
\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI7\ <= \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_MHI_bus\(7);

\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_REFCLK_SELECT_CLKIN_bus\ <= (gnd & gnd & gnd & \CLOCK_50~input_o\);

\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_MHI_bus\ <= (\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI7\ & \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI6\ & 
\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI5\ & \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI4\ & \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI3\ & 
\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI2\ & \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI1\ & \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_MHI0\);

\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIGSHIFTEN6\ <= \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_SHIFTEN_bus\(6);
\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIGSHIFTEN7\ <= \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_SHIFTEN_bus\(7);

\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER_VCO0PH_bus\ <= (\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH7\ & \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH6\ & 
\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH5\ & \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH4\ & \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH3\ & 
\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH2\ & \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH1\ & \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH0\);

\PLL|pll_x2_x4_inst|altera_pll_i|general[1].gpll~PLL_OUTPUT_COUNTER_VCO0PH_bus\ <= (\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH7\ & \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH6\ & 
\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH5\ & \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH4\ & \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH3\ & 
\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH2\ & \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH1\ & \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH0\);
\SDRAM1|u1|ALT_INV_refTimer_r[7]~DUPLICATE_q\ <= NOT \SDRAM1|u1|refTimer_r[7]~DUPLICATE_q\;
\SDRAM1|u1|ALT_INV_refTimer_r[5]~DUPLICATE_q\ <= NOT \SDRAM1|u1|refTimer_r[5]~DUPLICATE_q\;
\SDRAM1|u1|ALT_INV_sDataDir_r~DUPLICATE_q\ <= NOT \SDRAM1|u1|sDataDir_r~DUPLICATE_q\;
\ALT_INV_rd~DUPLICATE_q\ <= NOT \rd~DUPLICATE_q\;
\ALT_INV_wr~DUPLICATE_q\ <= NOT \wr~DUPLICATE_q\;
\SDRAM1|u1|ALT_INV_rdPipeline_r[4]~DUPLICATE_q\ <= NOT \SDRAM1|u1|rdPipeline_r[4]~DUPLICATE_q\;
\SDRAM1|u1|ALT_INV_rasTimer_r[0]~DUPLICATE_q\ <= NOT \SDRAM1|u1|rasTimer_r[0]~DUPLICATE_q\;
\SDRAM1|u1|ALT_INV_timer_r[5]~DUPLICATE_q\ <= NOT \SDRAM1|u1|timer_r[5]~DUPLICATE_q\;
\SDRAM1|u1|ALT_INV_timer_r[7]~DUPLICATE_q\ <= NOT \SDRAM1|u1|timer_r[7]~DUPLICATE_q\;
\SDRAM1|u1|ALT_INV_timer_r[9]~DUPLICATE_q\ <= NOT \SDRAM1|u1|timer_r[9]~DUPLICATE_q\;
\SDRAM1|u1|ALT_INV_timer_r[12]~DUPLICATE_q\ <= NOT \SDRAM1|u1|timer_r[12]~DUPLICATE_q\;
\SDRAM1|u1|ALT_INV_timer_r[13]~DUPLICATE_q\ <= NOT \SDRAM1|u1|timer_r[13]~DUPLICATE_q\;
\SDRAM1|u1|ALT_INV_rfshCntr_r[9]~DUPLICATE_q\ <= NOT \SDRAM1|u1|rfshCntr_r[9]~DUPLICATE_q\;
\SDRAM1|u1|ALT_INV_rfshCntr_r[1]~DUPLICATE_q\ <= NOT \SDRAM1|u1|rfshCntr_r[1]~DUPLICATE_q\;
\SDRAM1|u1|ALT_INV_rfshCntr_r[3]~DUPLICATE_q\ <= NOT \SDRAM1|u1|rfshCntr_r[3]~DUPLICATE_q\;
\SDRAM1|u1|ALT_INV_rfshCntr_r[4]~DUPLICATE_q\ <= NOT \SDRAM1|u1|rfshCntr_r[4]~DUPLICATE_q\;
\SDRAM1|u1|ALT_INV_rfshCntr_r[5]~DUPLICATE_q\ <= NOT \SDRAM1|u1|rfshCntr_r[5]~DUPLICATE_q\;
\PLL|pll_x2_x4_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\ <= NOT \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\;
\ALT_INV_KEY[3]~input_o\ <= NOT \KEY[3]~input_o\;
\ALT_INV_DRAM_DQ[8]~input_o\ <= NOT \DRAM_DQ[8]~input_o\;
\ALT_INV_DRAM_DQ[6]~input_o\ <= NOT \DRAM_DQ[6]~input_o\;
\ALT_INV_DRAM_DQ[0]~input_o\ <= NOT \DRAM_DQ[0]~input_o\;
ALT_INV_hDIn(15) <= NOT hDIn(15);
ALT_INV_hDIn(14) <= NOT hDIn(14);
ALT_INV_hDIn(13) <= NOT hDIn(13);
ALT_INV_hDIn(10) <= NOT hDIn(10);
ALT_INV_hDIn(8) <= NOT hDIn(8);
ALT_INV_hDIn(5) <= NOT hDIn(5);
ALT_INV_hDIn(4) <= NOT hDIn(4);
ALT_INV_hDIn(3) <= NOT hDIn(3);
ALT_INV_hDIn(0) <= NOT hDIn(0);
\SDRAM1|u1|ALT_INV_state_r.INITRFSH~0_combout\ <= NOT \SDRAM1|u1|state_r.INITRFSH~0_combout\;
\SDRAM1|u1|ALT_INV_state_x.RW~3_combout\ <= NOT \SDRAM1|u1|state_x.RW~3_combout\;
\SDRAM1|u1|ALT_INV_Selector59~1_combout\ <= NOT \SDRAM1|u1|Selector59~1_combout\;
\SDRAM1|u1|ALT_INV_Selector59~0_combout\ <= NOT \SDRAM1|u1|Selector59~0_combout\;
\SDRAM1|u1|ALT_INV_rfshCntr_r[4]~3_combout\ <= NOT \SDRAM1|u1|rfshCntr_r[4]~3_combout\;
\SDRAM1|u1|ALT_INV_Equal6~2_combout\ <= NOT \SDRAM1|u1|Equal6~2_combout\;
\SDRAM1|u1|ALT_INV_Equal6~1_combout\ <= NOT \SDRAM1|u1|Equal6~1_combout\;
\SDRAM1|u1|ALT_INV_refTimer_r\(0) <= NOT \SDRAM1|u1|refTimer_r\(0);
\SDRAM1|u1|ALT_INV_refTimer_r\(1) <= NOT \SDRAM1|u1|refTimer_r\(1);
\SDRAM1|u1|ALT_INV_refTimer_r\(2) <= NOT \SDRAM1|u1|refTimer_r\(2);
\SDRAM1|u1|ALT_INV_refTimer_r\(3) <= NOT \SDRAM1|u1|refTimer_r\(3);
\SDRAM1|u1|ALT_INV_Equal6~0_combout\ <= NOT \SDRAM1|u1|Equal6~0_combout\;
\SDRAM1|u1|ALT_INV_refTimer_r\(6) <= NOT \SDRAM1|u1|refTimer_r\(6);
\SDRAM1|u1|ALT_INV_refTimer_r\(7) <= NOT \SDRAM1|u1|refTimer_r\(7);
\SDRAM1|u1|ALT_INV_refTimer_r\(8) <= NOT \SDRAM1|u1|refTimer_r\(8);
\SDRAM1|u1|ALT_INV_refTimer_r\(9) <= NOT \SDRAM1|u1|refTimer_r\(9);
\SDRAM1|u1|ALT_INV_refTimer_r\(4) <= NOT \SDRAM1|u1|refTimer_r\(4);
\SDRAM1|u1|ALT_INV_refTimer_r\(5) <= NOT \SDRAM1|u1|refTimer_r\(5);
\SDRAM1|u1|ALT_INV_WideOr18~combout\ <= NOT \SDRAM1|u1|WideOr18~combout\;
\SDRAM1|u1|ALT_INV_wrTimer_r[1]~1_combout\ <= NOT \SDRAM1|u1|wrTimer_r[1]~1_combout\;
\SDRAM1|u1|ALT_INV_wrTimer_r[1]~0_combout\ <= NOT \SDRAM1|u1|wrTimer_r[1]~0_combout\;
\SDRAM1|u1|ALT_INV_state_x.RW~2_combout\ <= NOT \SDRAM1|u1|state_x.RW~2_combout\;
\SDRAM1|u1|ALT_INV_state_r.RW~0_combout\ <= NOT \SDRAM1|u1|state_r.RW~0_combout\;
\SDRAM1|u1|ALT_INV_state_x.INITSETMODE~4_combout\ <= NOT \SDRAM1|u1|state_x.INITSETMODE~4_combout\;
\SDRAM1|u1|ALT_INV_Add1~0_combout\ <= NOT \SDRAM1|u1|Add1~0_combout\;
\SDRAM1|u1|ALT_INV_rasTimer_r[2]~1_combout\ <= NOT \SDRAM1|u1|rasTimer_r[2]~1_combout\;
\SDRAM1|u1|ALT_INV_state_x.INITSETMODE~2_combout\ <= NOT \SDRAM1|u1|state_x.INITSETMODE~2_combout\;
\SDRAM1|u1|ALT_INV_rfshCntr_r[4]~0_combout\ <= NOT \SDRAM1|u1|rfshCntr_r[4]~0_combout\;
\SDRAM1|u1|ALT_INV_state_x.INITSETMODE~1_combout\ <= NOT \SDRAM1|u1|state_x.INITSETMODE~1_combout\;
\SDRAM1|u1|ALT_INV_state_x.RW~1_combout\ <= NOT \SDRAM1|u1|state_x.RW~1_combout\;
\SDRAM1|u1|ALT_INV_timer_x[0]~2_combout\ <= NOT \SDRAM1|u1|timer_x[0]~2_combout\;
\SDRAM1|u1|ALT_INV_timer_x[0]~1_combout\ <= NOT \SDRAM1|u1|timer_x[0]~1_combout\;
\SDRAM1|u1|ALT_INV_timer_r[1]~0_combout\ <= NOT \SDRAM1|u1|timer_r[1]~0_combout\;
\SDRAM1|u1|ALT_INV_state_x.INITSETMODE~0_combout\ <= NOT \SDRAM1|u1|state_x.INITSETMODE~0_combout\;
\ALT_INV_hAddr[23]~2_combout\ <= NOT \hAddr[23]~2_combout\;
\SDRAM1|u1|ALT_INV_sDataDir_r~q\ <= NOT \SDRAM1|u1|sDataDir_r~q\;
\ALT_INV_FSM_autotest.A_WRITE_WAIT~q\ <= NOT \FSM_autotest.A_WRITE_WAIT~q\;
\ALT_INV_hAddr[23]~1_combout\ <= NOT \hAddr[23]~1_combout\;
\ALT_INV_Selector33~0_combout\ <= NOT \Selector33~0_combout\;
\ALT_INV_FSM_autotest.A_SET_WRITE~q\ <= NOT \FSM_autotest.A_SET_WRITE~q\;
\ALT_INV_Equal1~6_combout\ <= NOT \Equal1~6_combout\;
\ALT_INV_Equal1~5_combout\ <= NOT \Equal1~5_combout\;
\SDRAM1|u1|ALT_INV_hDOut_r\(5) <= NOT \SDRAM1|u1|hDOut_r\(5);
\SDRAM1|u1|ALT_INV_hDOut_r\(4) <= NOT \SDRAM1|u1|hDOut_r\(4);
\SDRAM1|u1|ALT_INV_hDOut_r\(3) <= NOT \SDRAM1|u1|hDOut_r\(3);
\ALT_INV_Equal1~4_combout\ <= NOT \Equal1~4_combout\;
\SDRAM1|u1|ALT_INV_hDOut_r\(2) <= NOT \SDRAM1|u1|hDOut_r\(2);
\SDRAM1|u1|ALT_INV_hDOut_r\(1) <= NOT \SDRAM1|u1|hDOut_r\(1);
\SDRAM1|u1|ALT_INV_hDOut_r\(0) <= NOT \SDRAM1|u1|hDOut_r\(0);
\ALT_INV_Equal1~3_combout\ <= NOT \Equal1~3_combout\;
\SDRAM1|u1|ALT_INV_hDOut_r\(8) <= NOT \SDRAM1|u1|hDOut_r\(8);
\SDRAM1|u1|ALT_INV_hDOut_r\(7) <= NOT \SDRAM1|u1|hDOut_r\(7);
\SDRAM1|u1|ALT_INV_hDOut_r\(6) <= NOT \SDRAM1|u1|hDOut_r\(6);
\ALT_INV_Equal1~2_combout\ <= NOT \Equal1~2_combout\;
\SDRAM1|u1|ALT_INV_hDOut_r\(11) <= NOT \SDRAM1|u1|hDOut_r\(11);
\SDRAM1|u1|ALT_INV_hDOut_r\(10) <= NOT \SDRAM1|u1|hDOut_r\(10);
\SDRAM1|u1|ALT_INV_hDOut_r\(9) <= NOT \SDRAM1|u1|hDOut_r\(9);
\ALT_INV_Equal1~1_combout\ <= NOT \Equal1~1_combout\;
\SDRAM1|u1|ALT_INV_hDOut_r\(14) <= NOT \SDRAM1|u1|hDOut_r\(14);
\SDRAM1|u1|ALT_INV_hDOut_r\(13) <= NOT \SDRAM1|u1|hDOut_r\(13);
\SDRAM1|u1|ALT_INV_hDOut_r\(12) <= NOT \SDRAM1|u1|hDOut_r\(12);
\ALT_INV_Equal1~0_combout\ <= NOT \Equal1~0_combout\;
\SDRAM1|u1|ALT_INV_hDOut_r\(15) <= NOT \SDRAM1|u1|hDOut_r\(15);
\ALT_INV_FSM_autotest.A_READ_WAIT~q\ <= NOT \FSM_autotest.A_READ_WAIT~q\;
\SDRAM1|u1|ALT_INV_rdPipeline_r\(0) <= NOT \SDRAM1|u1|rdPipeline_r\(0);
\ALT_INV_Equal0~4_combout\ <= NOT \Equal0~4_combout\;
\ALT_INV_Equal0~3_combout\ <= NOT \Equal0~3_combout\;
\ALT_INV_Equal0~2_combout\ <= NOT \Equal0~2_combout\;
\ALT_INV_Equal0~1_combout\ <= NOT \Equal0~1_combout\;
\ALT_INV_Equal0~0_combout\ <= NOT \Equal0~0_combout\;
\ALT_INV_FSM_autotest.A_SET_READ~q\ <= NOT \FSM_autotest.A_SET_READ~q\;
\ALT_INV_FSM_autotest.A_READ~q\ <= NOT \FSM_autotest.A_READ~q\;
\ALT_INV_FSM_autotest.A_WRITE~q\ <= NOT \FSM_autotest.A_WRITE~q\;
\ALT_INV_FSM_autotest.A_IDLE~q\ <= NOT \FSM_autotest.A_IDLE~q\;
\SDRAM1|u1|ALT_INV_cmd_x[2]~9_combout\ <= NOT \SDRAM1|u1|cmd_x[2]~9_combout\;
\SDRAM1|u1|ALT_INV_cmd_x[2]~8_combout\ <= NOT \SDRAM1|u1|cmd_x[2]~8_combout\;
\SDRAM1|u1|ALT_INV_Selector20~0_combout\ <= NOT \SDRAM1|u1|Selector20~0_combout\;
\SDRAM1|u1|ALT_INV_cke_x~0_combout\ <= NOT \SDRAM1|u1|cke_x~0_combout\;
\SDRAM1|u1|ALT_INV_Selector31~0_combout\ <= NOT \SDRAM1|u1|Selector31~0_combout\;
\SDRAM1|u1|ALT_INV_cmd_x[3]~6_combout\ <= NOT \SDRAM1|u1|cmd_x[3]~6_combout\;
\SDRAM1|u1|ALT_INV_cmd_x[3]~5_combout\ <= NOT \SDRAM1|u1|cmd_x[3]~5_combout\;
\SDRAM1|u1|ALT_INV_Equal0~0_combout\ <= NOT \SDRAM1|u1|Equal0~0_combout\;
\SDRAM1|u1|ALT_INV_cmd_x[3]~3_combout\ <= NOT \SDRAM1|u1|cmd_x[3]~3_combout\;
\SDRAM1|u1|ALT_INV_state_r.INITWAIT~q\ <= NOT \SDRAM1|u1|state_r.INITWAIT~q\;
\SDRAM1|u1|ALT_INV_Selector28~0_combout\ <= NOT \SDRAM1|u1|Selector28~0_combout\;
\SDRAM1|u1|ALT_INV_cmd_x[0]~1_combout\ <= NOT \SDRAM1|u1|cmd_x[0]~1_combout\;
\SDRAM1|u1|ALT_INV_cmd_x[0]~0_combout\ <= NOT \SDRAM1|u1|cmd_x[0]~0_combout\;
\SDRAM1|u1|ALT_INV_state_r.REFRESHROW~q\ <= NOT \SDRAM1|u1|state_r.REFRESHROW~q\;
\SDRAM1|u1|ALT_INV_state_r.INITRFSH~q\ <= NOT \SDRAM1|u1|state_r.INITRFSH~q\;
\SDRAM1|u1|ALT_INV_state_r.SELFREFRESH~q\ <= NOT \SDRAM1|u1|state_r.SELFREFRESH~q\;
\SDRAM1|u1|ALT_INV_doSelfRfsh~4_combout\ <= NOT \SDRAM1|u1|doSelfRfsh~4_combout\;
\SDRAM1|u1|ALT_INV_doActivate~combout\ <= NOT \SDRAM1|u1|doActivate~combout\;
\SDRAM1|u1|ALT_INV_doActivate~4_combout\ <= NOT \SDRAM1|u1|doActivate~4_combout\;
\SDRAM1|u1|ALT_INV_activeRow_r[0][2]~q\ <= NOT \SDRAM1|u1|activeRow_r[0][2]~q\;
\SDRAM1|u1|ALT_INV_activeRow_r[0][1]~q\ <= NOT \SDRAM1|u1|activeRow_r[0][1]~q\;
\SDRAM1|u1|ALT_INV_activeRow_r[0][0]~q\ <= NOT \SDRAM1|u1|activeRow_r[0][0]~q\;
\SDRAM1|u1|ALT_INV_doActivate~3_combout\ <= NOT \SDRAM1|u1|doActivate~3_combout\;
\SDRAM1|u1|ALT_INV_activeBank_r\(1) <= NOT \SDRAM1|u1|activeBank_r\(1);
\SDRAM1|u1|ALT_INV_activeBank_r\(0) <= NOT \SDRAM1|u1|activeBank_r\(0);
\SDRAM1|u1|ALT_INV_activeFlag_r\(0) <= NOT \SDRAM1|u1|activeFlag_r\(0);
\SDRAM1|u1|ALT_INV_doActivate~2_combout\ <= NOT \SDRAM1|u1|doActivate~2_combout\;
\SDRAM1|u1|ALT_INV_activeRow_r[0][5]~q\ <= NOT \SDRAM1|u1|activeRow_r[0][5]~q\;
\SDRAM1|u1|ALT_INV_activeRow_r[0][4]~q\ <= NOT \SDRAM1|u1|activeRow_r[0][4]~q\;
\SDRAM1|u1|ALT_INV_activeRow_r[0][3]~q\ <= NOT \SDRAM1|u1|activeRow_r[0][3]~q\;
\SDRAM1|u1|ALT_INV_doActivate~1_combout\ <= NOT \SDRAM1|u1|doActivate~1_combout\;
\SDRAM1|u1|ALT_INV_activeRow_r[0][8]~q\ <= NOT \SDRAM1|u1|activeRow_r[0][8]~q\;
\SDRAM1|u1|ALT_INV_activeRow_r[0][7]~q\ <= NOT \SDRAM1|u1|activeRow_r[0][7]~q\;
\SDRAM1|u1|ALT_INV_activeRow_r[0][6]~q\ <= NOT \SDRAM1|u1|activeRow_r[0][6]~q\;
\SDRAM1|u1|ALT_INV_doActivate~0_combout\ <= NOT \SDRAM1|u1|doActivate~0_combout\;
\SDRAM1|u1|ALT_INV_activeRow_r[0][11]~q\ <= NOT \SDRAM1|u1|activeRow_r[0][11]~q\;
\SDRAM1|u1|ALT_INV_activeRow_r[0][10]~q\ <= NOT \SDRAM1|u1|activeRow_r[0][10]~q\;
\SDRAM1|u1|ALT_INV_activeRow_r[0][9]~q\ <= NOT \SDRAM1|u1|activeRow_r[0][9]~q\;
\SDRAM1|u1|ALT_INV_Equal2~0_combout\ <= NOT \SDRAM1|u1|Equal2~0_combout\;
\SDRAM1|u1|ALT_INV_activeRow_r[0][12]~q\ <= NOT \SDRAM1|u1|activeRow_r[0][12]~q\;
\SDRAM1|u1|ALT_INV_state_x.RW~0_combout\ <= NOT \SDRAM1|u1|state_x.RW~0_combout\;
\SDRAM1|u1|ALT_INV_sAddr_x[10]~12_combout\ <= NOT \SDRAM1|u1|sAddr_x[10]~12_combout\;
\SDRAM1|u1|ALT_INV_sAddr_x[10]~8_combout\ <= NOT \SDRAM1|u1|sAddr_x[10]~8_combout\;
\SDRAM1|u1|ALT_INV_state_r.INITPCHG~q\ <= NOT \SDRAM1|u1|state_r.INITPCHG~q\;
\SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\ <= NOT \SDRAM1|u1|doSelfRfsh~3_combout\;
\SDRAM1|u1|ALT_INV_doSelfRfsh~2_combout\ <= NOT \SDRAM1|u1|doSelfRfsh~2_combout\;
\SDRAM1|u1|ALT_INV_doSelfRfsh~1_combout\ <= NOT \SDRAM1|u1|doSelfRfsh~1_combout\;
\SDRAM1|u1|ALT_INV_doSelfRfsh~0_combout\ <= NOT \SDRAM1|u1|doSelfRfsh~0_combout\;
\ALT_INV_rd~q\ <= NOT \rd~q\;
\ALT_INV_wr~q\ <= NOT \wr~q\;
\SDRAM1|u1|ALT_INV_Equal9~0_combout\ <= NOT \SDRAM1|u1|Equal9~0_combout\;
\SDRAM1|u1|ALT_INV_Equal8~1_combout\ <= NOT \SDRAM1|u1|Equal8~1_combout\;
\SDRAM1|u1|ALT_INV_Equal8~0_combout\ <= NOT \SDRAM1|u1|Equal8~0_combout\;
\SDRAM1|u1|ALT_INV_Selector22~0_combout\ <= NOT \SDRAM1|u1|Selector22~0_combout\;
\SDRAM1|u1|ALT_INV_combinatorial~0_combout\ <= NOT \SDRAM1|u1|combinatorial~0_combout\;
\SDRAM1|u1|ALT_INV_wrTimer_r\(0) <= NOT \SDRAM1|u1|wrTimer_r\(0);
\SDRAM1|u1|ALT_INV_wrTimer_r\(1) <= NOT \SDRAM1|u1|wrTimer_r\(1);
\SDRAM1|u1|ALT_INV_rdPipeline_r\(1) <= NOT \SDRAM1|u1|rdPipeline_r\(1);
\SDRAM1|u1|ALT_INV_rdPipeline_r\(2) <= NOT \SDRAM1|u1|rdPipeline_r\(2);
\SDRAM1|u1|ALT_INV_rdPipeline_r\(3) <= NOT \SDRAM1|u1|rdPipeline_r\(3);
\SDRAM1|u1|ALT_INV_rdPipeline_r\(4) <= NOT \SDRAM1|u1|rdPipeline_r\(4);
\SDRAM1|u1|ALT_INV_state_r.RW~q\ <= NOT \SDRAM1|u1|state_r.RW~q\;
\SDRAM1|u1|ALT_INV_rasTimer_r[2]~0_combout\ <= NOT \SDRAM1|u1|rasTimer_r[2]~0_combout\;
\SDRAM1|u1|ALT_INV_rasTimer_r\(0) <= NOT \SDRAM1|u1|rasTimer_r\(0);
\SDRAM1|u1|ALT_INV_rasTimer_r\(1) <= NOT \SDRAM1|u1|rasTimer_r\(1);
\SDRAM1|u1|ALT_INV_rasTimer_r\(2) <= NOT \SDRAM1|u1|rasTimer_r\(2);
\SDRAM1|u1|ALT_INV_WideOr16~0_combout\ <= NOT \SDRAM1|u1|WideOr16~0_combout\;
\SDRAM1|u1|ALT_INV_state_r.INITSETMODE~q\ <= NOT \SDRAM1|u1|state_r.INITSETMODE~q\;
\SDRAM1|u1|ALT_INV_Equal7~3_combout\ <= NOT \SDRAM1|u1|Equal7~3_combout\;
\SDRAM1|u1|ALT_INV_Equal7~2_combout\ <= NOT \SDRAM1|u1|Equal7~2_combout\;
\SDRAM1|u1|ALT_INV_timer_r\(3) <= NOT \SDRAM1|u1|timer_r\(3);
\SDRAM1|u1|ALT_INV_timer_r\(4) <= NOT \SDRAM1|u1|timer_r\(4);
\SDRAM1|u1|ALT_INV_timer_r\(5) <= NOT \SDRAM1|u1|timer_r\(5);
\SDRAM1|u1|ALT_INV_timer_r\(6) <= NOT \SDRAM1|u1|timer_r\(6);
\SDRAM1|u1|ALT_INV_timer_r\(7) <= NOT \SDRAM1|u1|timer_r\(7);
\SDRAM1|u1|ALT_INV_timer_r\(8) <= NOT \SDRAM1|u1|timer_r\(8);
\SDRAM1|u1|ALT_INV_Equal7~1_combout\ <= NOT \SDRAM1|u1|Equal7~1_combout\;
\SDRAM1|u1|ALT_INV_timer_r\(9) <= NOT \SDRAM1|u1|timer_r\(9);
\SDRAM1|u1|ALT_INV_timer_r\(10) <= NOT \SDRAM1|u1|timer_r\(10);
\SDRAM1|u1|ALT_INV_timer_r\(11) <= NOT \SDRAM1|u1|timer_r\(11);
\SDRAM1|u1|ALT_INV_timer_r\(12) <= NOT \SDRAM1|u1|timer_r\(12);
\SDRAM1|u1|ALT_INV_timer_r\(13) <= NOT \SDRAM1|u1|timer_r\(13);
\SDRAM1|u1|ALT_INV_timer_r\(14) <= NOT \SDRAM1|u1|timer_r\(14);
\SDRAM1|u1|ALT_INV_Equal7~0_combout\ <= NOT \SDRAM1|u1|Equal7~0_combout\;
\SDRAM1|u1|ALT_INV_timer_r\(0) <= NOT \SDRAM1|u1|timer_r\(0);
\SDRAM1|u1|ALT_INV_timer_r\(1) <= NOT \SDRAM1|u1|timer_r\(1);
\SDRAM1|u1|ALT_INV_timer_r\(2) <= NOT \SDRAM1|u1|timer_r\(2);
\SDRAM1|u1|ALT_INV_state_r.ACTIVATE~q\ <= NOT \SDRAM1|u1|state_r.ACTIVATE~q\;
\LED06|ALT_INV_Mux6~0_combout\ <= NOT \LED06|Mux6~0_combout\;
\LED05|ALT_INV_Mux6~0_combout\ <= NOT \LED05|Mux6~0_combout\;
\LED04|ALT_INV_Mux6~0_combout\ <= NOT \LED04|Mux6~0_combout\;
\LED03|ALT_INV_Mux6~0_combout\ <= NOT \LED03|Mux6~0_combout\;
\LED02|ALT_INV_Mux6~0_combout\ <= NOT \LED02|Mux6~0_combout\;
\LED01|ALT_INV_Mux6~0_combout\ <= NOT \LED01|Mux6~0_combout\;
\ALT_INV_LEDR[8]~reg0_q\ <= NOT \LEDR[8]~reg0_q\;
\ALT_INV_LEDR[2]~reg0_q\ <= NOT \LEDR[2]~reg0_q\;
\ALT_INV_LEDR[1]~reg0_q\ <= NOT \LEDR[1]~reg0_q\;
\ALT_INV_LEDR[0]~reg0_q\ <= NOT \LEDR[0]~reg0_q\;
\SDRAM1|u1|ALT_INV_Add3~37_sumout\ <= NOT \SDRAM1|u1|Add3~37_sumout\;
\SDRAM1|u1|ALT_INV_Add3~21_sumout\ <= NOT \SDRAM1|u1|Add3~21_sumout\;
\SDRAM1|u1|ALT_INV_Add3~17_sumout\ <= NOT \SDRAM1|u1|Add3~17_sumout\;
\SDRAM1|u1|ALT_INV_Add3~5_sumout\ <= NOT \SDRAM1|u1|Add3~5_sumout\;
\SDRAM1|u1|ALT_INV_Add3~1_sumout\ <= NOT \SDRAM1|u1|Add3~1_sumout\;
\SDRAM1|u1|ALT_INV_Add4~53_sumout\ <= NOT \SDRAM1|u1|Add4~53_sumout\;
\SDRAM1|u1|ALT_INV_Add4~49_sumout\ <= NOT \SDRAM1|u1|Add4~49_sumout\;
\SDRAM1|u1|ALT_INV_Add4~45_sumout\ <= NOT \SDRAM1|u1|Add4~45_sumout\;
\SDRAM1|u1|ALT_INV_Add4~41_sumout\ <= NOT \SDRAM1|u1|Add4~41_sumout\;
\SDRAM1|u1|ALT_INV_Add4~37_sumout\ <= NOT \SDRAM1|u1|Add4~37_sumout\;
\SDRAM1|u1|ALT_INV_Add4~33_sumout\ <= NOT \SDRAM1|u1|Add4~33_sumout\;
\SDRAM1|u1|ALT_INV_Add4~29_sumout\ <= NOT \SDRAM1|u1|Add4~29_sumout\;
\SDRAM1|u1|ALT_INV_Add4~25_sumout\ <= NOT \SDRAM1|u1|Add4~25_sumout\;
\SDRAM1|u1|ALT_INV_Add4~21_sumout\ <= NOT \SDRAM1|u1|Add4~21_sumout\;
\SDRAM1|u1|ALT_INV_Add4~17_sumout\ <= NOT \SDRAM1|u1|Add4~17_sumout\;
\SDRAM1|u1|ALT_INV_Add4~13_sumout\ <= NOT \SDRAM1|u1|Add4~13_sumout\;
\SDRAM1|u1|ALT_INV_Add4~9_sumout\ <= NOT \SDRAM1|u1|Add4~9_sumout\;
\SDRAM1|u1|ALT_INV_Add4~5_sumout\ <= NOT \SDRAM1|u1|Add4~5_sumout\;
\SDRAM1|u1|ALT_INV_Add4~1_sumout\ <= NOT \SDRAM1|u1|Add4~1_sumout\;
\SDRAM1|u1|ALT_INV_Add5~57_sumout\ <= NOT \SDRAM1|u1|Add5~57_sumout\;
\SDRAM1|u1|ALT_INV_Add5~53_sumout\ <= NOT \SDRAM1|u1|Add5~53_sumout\;
\SDRAM1|u1|ALT_INV_Add5~49_sumout\ <= NOT \SDRAM1|u1|Add5~49_sumout\;
\SDRAM1|u1|ALT_INV_Add5~45_sumout\ <= NOT \SDRAM1|u1|Add5~45_sumout\;
\SDRAM1|u1|ALT_INV_Add5~41_sumout\ <= NOT \SDRAM1|u1|Add5~41_sumout\;
\SDRAM1|u1|ALT_INV_Add5~37_sumout\ <= NOT \SDRAM1|u1|Add5~37_sumout\;
\SDRAM1|u1|ALT_INV_Add5~33_sumout\ <= NOT \SDRAM1|u1|Add5~33_sumout\;
\SDRAM1|u1|ALT_INV_Add5~29_sumout\ <= NOT \SDRAM1|u1|Add5~29_sumout\;
\SDRAM1|u1|ALT_INV_Add5~25_sumout\ <= NOT \SDRAM1|u1|Add5~25_sumout\;
\SDRAM1|u1|ALT_INV_Add5~21_sumout\ <= NOT \SDRAM1|u1|Add5~21_sumout\;
\SDRAM1|u1|ALT_INV_Add5~17_sumout\ <= NOT \SDRAM1|u1|Add5~17_sumout\;
\SDRAM1|u1|ALT_INV_Add5~13_sumout\ <= NOT \SDRAM1|u1|Add5~13_sumout\;
\SDRAM1|u1|ALT_INV_Add5~9_sumout\ <= NOT \SDRAM1|u1|Add5~9_sumout\;
\SDRAM1|u1|ALT_INV_Add5~5_sumout\ <= NOT \SDRAM1|u1|Add5~5_sumout\;
\SDRAM1|u1|ALT_INV_Add5~1_sumout\ <= NOT \SDRAM1|u1|Add5~1_sumout\;
\SDRAM1|u1|ALT_INV_nopCntr_r\(13) <= NOT \SDRAM1|u1|nopCntr_r\(13);
\SDRAM1|u1|ALT_INV_nopCntr_r\(12) <= NOT \SDRAM1|u1|nopCntr_r\(12);
\SDRAM1|u1|ALT_INV_nopCntr_r\(5) <= NOT \SDRAM1|u1|nopCntr_r\(5);
\SDRAM1|u1|ALT_INV_nopCntr_r\(4) <= NOT \SDRAM1|u1|nopCntr_r\(4);
\SDRAM1|u1|ALT_INV_nopCntr_r\(3) <= NOT \SDRAM1|u1|nopCntr_r\(3);
\SDRAM1|u1|ALT_INV_nopCntr_r\(2) <= NOT \SDRAM1|u1|nopCntr_r\(2);
\SDRAM1|u1|ALT_INV_nopCntr_r\(1) <= NOT \SDRAM1|u1|nopCntr_r\(1);
\SDRAM1|u1|ALT_INV_nopCntr_r\(0) <= NOT \SDRAM1|u1|nopCntr_r\(0);
\SDRAM1|u1|ALT_INV_nopCntr_r\(11) <= NOT \SDRAM1|u1|nopCntr_r\(11);
\SDRAM1|u1|ALT_INV_nopCntr_r\(7) <= NOT \SDRAM1|u1|nopCntr_r\(7);
\SDRAM1|u1|ALT_INV_nopCntr_r\(10) <= NOT \SDRAM1|u1|nopCntr_r\(10);
\SDRAM1|u1|ALT_INV_nopCntr_r\(9) <= NOT \SDRAM1|u1|nopCntr_r\(9);
\SDRAM1|u1|ALT_INV_nopCntr_r\(8) <= NOT \SDRAM1|u1|nopCntr_r\(8);
\SDRAM1|u1|ALT_INV_nopCntr_r\(6) <= NOT \SDRAM1|u1|nopCntr_r\(6);
\SDRAM1|u1|ALT_INV_rfshCntr_r\(6) <= NOT \SDRAM1|u1|rfshCntr_r\(6);
\SDRAM1|u1|ALT_INV_rfshCntr_r\(7) <= NOT \SDRAM1|u1|rfshCntr_r\(7);
\SDRAM1|u1|ALT_INV_rfshCntr_r\(8) <= NOT \SDRAM1|u1|rfshCntr_r\(8);
\SDRAM1|u1|ALT_INV_rfshCntr_r\(9) <= NOT \SDRAM1|u1|rfshCntr_r\(9);
\SDRAM1|u1|ALT_INV_rfshCntr_r\(10) <= NOT \SDRAM1|u1|rfshCntr_r\(10);
\SDRAM1|u1|ALT_INV_rfshCntr_r\(11) <= NOT \SDRAM1|u1|rfshCntr_r\(11);
\SDRAM1|u1|ALT_INV_rfshCntr_r\(12) <= NOT \SDRAM1|u1|rfshCntr_r\(12);
\SDRAM1|u1|ALT_INV_rfshCntr_r\(13) <= NOT \SDRAM1|u1|rfshCntr_r\(13);
\SDRAM1|u1|ALT_INV_rfshCntr_r\(1) <= NOT \SDRAM1|u1|rfshCntr_r\(1);
\SDRAM1|u1|ALT_INV_rfshCntr_r\(2) <= NOT \SDRAM1|u1|rfshCntr_r\(2);
\SDRAM1|u1|ALT_INV_rfshCntr_r\(3) <= NOT \SDRAM1|u1|rfshCntr_r\(3);
\SDRAM1|u1|ALT_INV_rfshCntr_r\(4) <= NOT \SDRAM1|u1|rfshCntr_r\(4);
\SDRAM1|u1|ALT_INV_rfshCntr_r\(5) <= NOT \SDRAM1|u1|rfshCntr_r\(5);
\SDRAM1|u1|ALT_INV_rfshCntr_r\(0) <= NOT \SDRAM1|u1|rfshCntr_r\(0);
ALT_INV_hAddr(23) <= NOT hAddr(23);
ALT_INV_hAddr(22) <= NOT hAddr(22);
ALT_INV_hAddr(21) <= NOT hAddr(21);
ALT_INV_hAddr(20) <= NOT hAddr(20);
ALT_INV_hAddr(19) <= NOT hAddr(19);
ALT_INV_hAddr(18) <= NOT hAddr(18);
ALT_INV_hAddr(17) <= NOT hAddr(17);
ALT_INV_hAddr(16) <= NOT hAddr(16);
ALT_INV_hAddr(15) <= NOT hAddr(15);
ALT_INV_hAddr(14) <= NOT hAddr(14);
ALT_INV_hAddr(13) <= NOT hAddr(13);
ALT_INV_hAddr(12) <= NOT hAddr(12);
ALT_INV_hAddr(11) <= NOT hAddr(11);
ALT_INV_hAddr(10) <= NOT hAddr(10);
ALT_INV_hAddr(9) <= NOT hAddr(9);
ALT_INV_hAddr(8) <= NOT hAddr(8);
ALT_INV_hAddr(7) <= NOT hAddr(7);
ALT_INV_hAddr(6) <= NOT hAddr(6);
ALT_INV_hAddr(5) <= NOT hAddr(5);
ALT_INV_hAddr(4) <= NOT hAddr(4);
ALT_INV_hAddr(3) <= NOT hAddr(3);
ALT_INV_hAddr(2) <= NOT hAddr(2);
ALT_INV_hAddr(1) <= NOT hAddr(1);
ALT_INV_hAddr(0) <= NOT hAddr(0);
ALT_INV_hAddr(24) <= NOT hAddr(24);

-- Location: IOOBUF_X40_Y0_N53
\DRAM_ADDR[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sAddr_r\(0),
	devoe => ww_devoe,
	o => ww_DRAM_ADDR(0));

-- Location: IOOBUF_X30_Y0_N19
\DRAM_ADDR[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sAddr_r\(1),
	devoe => ww_devoe,
	o => ww_DRAM_ADDR(1));

-- Location: IOOBUF_X38_Y0_N2
\DRAM_ADDR[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sAddr_r\(2),
	devoe => ww_devoe,
	o => ww_DRAM_ADDR(2));

-- Location: IOOBUF_X24_Y0_N19
\DRAM_ADDR[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sAddr_r\(3),
	devoe => ww_devoe,
	o => ww_DRAM_ADDR(3));

-- Location: IOOBUF_X28_Y0_N2
\DRAM_ADDR[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sAddr_r\(4),
	devoe => ww_devoe,
	o => ww_DRAM_ADDR(4));

-- Location: IOOBUF_X28_Y0_N19
\DRAM_ADDR[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sAddr_r\(5),
	devoe => ww_devoe,
	o => ww_DRAM_ADDR(5));

-- Location: IOOBUF_X24_Y0_N2
\DRAM_ADDR[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sAddr_r\(6),
	devoe => ww_devoe,
	o => ww_DRAM_ADDR(6));

-- Location: IOOBUF_X32_Y0_N19
\DRAM_ADDR[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sAddr_r\(7),
	devoe => ww_devoe,
	o => ww_DRAM_ADDR(7));

-- Location: IOOBUF_X38_Y0_N19
\DRAM_ADDR[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sAddr_r\(8),
	devoe => ww_devoe,
	o => ww_DRAM_ADDR(8));

-- Location: IOOBUF_X26_Y0_N59
\DRAM_ADDR[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sAddr_r\(9),
	devoe => ww_devoe,
	o => ww_DRAM_ADDR(9));

-- Location: IOOBUF_X26_Y0_N42
\DRAM_ADDR[10]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sAddr_r\(10),
	devoe => ww_devoe,
	o => ww_DRAM_ADDR(10));

-- Location: IOOBUF_X30_Y0_N2
\DRAM_ADDR[11]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sAddr_r\(11),
	devoe => ww_devoe,
	o => ww_DRAM_ADDR(11));

-- Location: IOOBUF_X40_Y0_N36
\DRAM_ADDR[12]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sAddr_r\(12),
	devoe => ww_devoe,
	o => ww_DRAM_ADDR(12));

-- Location: IOOBUF_X22_Y0_N19
\DRAM_BA[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|ba_r\(0),
	devoe => ww_devoe,
	o => ww_DRAM_BA(0));

-- Location: IOOBUF_X38_Y0_N53
\DRAM_BA[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|ba_r\(1),
	devoe => ww_devoe,
	o => ww_DRAM_BA(1));

-- Location: IOOBUF_X20_Y0_N19
\DRAM_LDQM~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|cmd_r\(0),
	devoe => ww_devoe,
	o => ww_DRAM_LDQM);

-- Location: IOOBUF_X36_Y0_N36
\DRAM_UDQM~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|cmd_r\(1),
	devoe => ww_devoe,
	o => ww_DRAM_UDQM);

-- Location: IOOBUF_X22_Y0_N2
\DRAM_RAS_N~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|cmd_r\(4),
	devoe => ww_devoe,
	o => ww_DRAM_RAS_N);

-- Location: IOOBUF_X18_Y0_N42
\DRAM_CAS_N~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|cmd_r\(3),
	devoe => ww_devoe,
	o => ww_DRAM_CAS_N);

-- Location: IOOBUF_X36_Y0_N53
\DRAM_CKE~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|cke_r~q\,
	devoe => ww_devoe,
	o => ww_DRAM_CKE);

-- Location: IOOBUF_X38_Y0_N36
\DRAM_CLK~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \PLL|pll_x2_x4_inst|altera_pll_i|ALT_INV_outclk_wire[0]~CLKENA0_outclk\,
	devoe => ww_devoe,
	o => ww_DRAM_CLK);

-- Location: IOOBUF_X20_Y0_N2
\DRAM_WE_N~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|cmd_r\(2),
	devoe => ww_devoe,
	o => ww_DRAM_WE_N);

-- Location: IOOBUF_X18_Y0_N59
\DRAM_CS_N~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_DRAM_CS_N);

-- Location: IOOBUF_X52_Y0_N2
\LEDR[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LEDR[0]~reg0_q\,
	devoe => ww_devoe,
	o => ww_LEDR(0));

-- Location: IOOBUF_X52_Y0_N19
\LEDR[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LEDR[1]~reg0_q\,
	devoe => ww_devoe,
	o => ww_LEDR(1));

-- Location: IOOBUF_X60_Y0_N2
\LEDR[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LEDR[2]~reg0_q\,
	devoe => ww_devoe,
	o => ww_LEDR(2));

-- Location: IOOBUF_X80_Y0_N2
\LEDR[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(3));

-- Location: IOOBUF_X60_Y0_N19
\LEDR[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(4));

-- Location: IOOBUF_X80_Y0_N19
\LEDR[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(5));

-- Location: IOOBUF_X84_Y0_N2
\LEDR[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(6));

-- Location: IOOBUF_X89_Y6_N5
\LEDR[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(7));

-- Location: IOOBUF_X89_Y8_N5
\LEDR[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LEDR[8]~reg0_q\,
	devoe => ww_devoe,
	o => ww_LEDR(8));

-- Location: IOOBUF_X89_Y6_N22
\LEDR[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => hAddr(24),
	devoe => ww_devoe,
	o => ww_LEDR(9));

-- Location: IOOBUF_X89_Y4_N96
\HEX0[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED01|ALT_INV_Mux6~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX0(6));

-- Location: IOOBUF_X89_Y13_N39
\HEX0[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED01|Mux5~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX0(5));

-- Location: IOOBUF_X89_Y13_N56
\HEX0[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED01|Mux4~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX0(4));

-- Location: IOOBUF_X89_Y4_N79
\HEX0[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED01|Mux3~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX0(3));

-- Location: IOOBUF_X89_Y11_N96
\HEX0[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED01|Mux2~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX0(2));

-- Location: IOOBUF_X89_Y11_N79
\HEX0[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED01|Mux1~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX0(1));

-- Location: IOOBUF_X89_Y8_N39
\HEX0[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED01|Mux0~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX0(0));

-- Location: IOOBUF_X89_Y8_N56
\HEX1[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED02|ALT_INV_Mux6~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX1(6));

-- Location: IOOBUF_X89_Y15_N56
\HEX1[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED02|Mux5~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX1(5));

-- Location: IOOBUF_X89_Y15_N39
\HEX1[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED02|Mux4~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX1(4));

-- Location: IOOBUF_X89_Y16_N56
\HEX1[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED02|Mux3~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX1(3));

-- Location: IOOBUF_X89_Y16_N39
\HEX1[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED02|Mux2~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX1(2));

-- Location: IOOBUF_X89_Y6_N56
\HEX1[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED02|Mux1~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX1(1));

-- Location: IOOBUF_X89_Y6_N39
\HEX1[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED02|Mux0~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX1(0));

-- Location: IOOBUF_X89_Y25_N56
\HEX2[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED03|ALT_INV_Mux6~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(6));

-- Location: IOOBUF_X89_Y20_N96
\HEX2[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED03|Mux5~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(5));

-- Location: IOOBUF_X89_Y25_N39
\HEX2[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED03|Mux4~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(4));

-- Location: IOOBUF_X89_Y20_N79
\HEX2[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED03|Mux3~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(3));

-- Location: IOOBUF_X89_Y23_N56
\HEX2[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED03|Mux2~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(2));

-- Location: IOOBUF_X89_Y23_N39
\HEX2[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED03|Mux1~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(1));

-- Location: IOOBUF_X89_Y9_N22
\HEX2[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED03|Mux0~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(0));

-- Location: IOOBUF_X89_Y9_N5
\HEX3[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED04|ALT_INV_Mux6~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX3(6));

-- Location: IOOBUF_X89_Y11_N62
\HEX3[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED04|Mux5~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX3(5));

-- Location: IOOBUF_X89_Y21_N39
\HEX3[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED04|Mux4~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX3(4));

-- Location: IOOBUF_X89_Y4_N62
\HEX3[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED04|Mux3~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX3(3));

-- Location: IOOBUF_X89_Y4_N45
\HEX3[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED04|Mux2~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX3(2));

-- Location: IOOBUF_X89_Y16_N22
\HEX3[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED04|Mux1~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX3(1));

-- Location: IOOBUF_X89_Y16_N5
\HEX3[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED04|Mux0~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX3(0));

-- Location: IOOBUF_X89_Y20_N45
\HEX4[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED05|ALT_INV_Mux6~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX4(6));

-- Location: IOOBUF_X89_Y15_N5
\HEX4[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED05|Mux5~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX4(5));

-- Location: IOOBUF_X89_Y15_N22
\HEX4[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED05|Mux4~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX4(4));

-- Location: IOOBUF_X89_Y8_N22
\HEX4[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED05|Mux3~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX4(3));

-- Location: IOOBUF_X89_Y13_N22
\HEX4[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED05|Mux2~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX4(2));

-- Location: IOOBUF_X89_Y13_N5
\HEX4[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED05|Mux1~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX4(1));

-- Location: IOOBUF_X89_Y11_N45
\HEX4[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED05|Mux0~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX4(0));

-- Location: IOOBUF_X89_Y9_N39
\HEX5[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED06|ALT_INV_Mux6~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX5(6));

-- Location: IOOBUF_X89_Y23_N5
\HEX5[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED06|Mux5~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX5(5));

-- Location: IOOBUF_X89_Y9_N56
\HEX5[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED06|Mux4~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX5(4));

-- Location: IOOBUF_X89_Y23_N22
\HEX5[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED06|Mux3~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX5(3));

-- Location: IOOBUF_X89_Y25_N22
\HEX5[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED06|Mux2~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX5(2));

-- Location: IOOBUF_X89_Y21_N56
\HEX5[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED06|Mux1~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX5(1));

-- Location: IOOBUF_X89_Y20_N62
\HEX5[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \LED06|Mux0~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX5(0));

-- Location: IOOBUF_X24_Y0_N53
\DRAM_DQ[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sData_r\(0),
	oe => \SDRAM1|u1|sDataDir_r~DUPLICATE_q\,
	devoe => ww_devoe,
	o => DRAM_DQ(0));

-- Location: IOOBUF_X26_Y0_N93
\DRAM_DQ[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sData_r\(1),
	oe => \SDRAM1|u1|sDataDir_r~DUPLICATE_q\,
	devoe => ww_devoe,
	o => DRAM_DQ(1));

-- Location: IOOBUF_X28_Y0_N36
\DRAM_DQ[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sData_r\(2),
	oe => \SDRAM1|u1|sDataDir_r~DUPLICATE_q\,
	devoe => ww_devoe,
	o => DRAM_DQ(2));

-- Location: IOOBUF_X28_Y0_N53
\DRAM_DQ[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sData_r\(3),
	oe => \SDRAM1|u1|sDataDir_r~DUPLICATE_q\,
	devoe => ww_devoe,
	o => DRAM_DQ(3));

-- Location: IOOBUF_X30_Y0_N53
\DRAM_DQ[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sData_r\(4),
	oe => \SDRAM1|u1|sDataDir_r~DUPLICATE_q\,
	devoe => ww_devoe,
	o => DRAM_DQ(4));

-- Location: IOOBUF_X18_Y0_N76
\DRAM_DQ[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sData_r\(5),
	oe => \SDRAM1|u1|sDataDir_r~DUPLICATE_q\,
	devoe => ww_devoe,
	o => DRAM_DQ(5));

-- Location: IOOBUF_X34_Y0_N59
\DRAM_DQ[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sData_r\(6),
	oe => \SDRAM1|u1|sDataDir_r~DUPLICATE_q\,
	devoe => ww_devoe,
	o => DRAM_DQ(6));

-- Location: IOOBUF_X34_Y0_N42
\DRAM_DQ[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sData_r\(7),
	oe => \SDRAM1|u1|sDataDir_r~DUPLICATE_q\,
	devoe => ww_devoe,
	o => DRAM_DQ(7));

-- Location: IOOBUF_X34_Y0_N76
\DRAM_DQ[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sData_r\(8),
	oe => \SDRAM1|u1|sDataDir_r~DUPLICATE_q\,
	devoe => ww_devoe,
	o => DRAM_DQ(8));

-- Location: IOOBUF_X34_Y0_N93
\DRAM_DQ[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sData_r\(9),
	oe => \SDRAM1|u1|sDataDir_r~DUPLICATE_q\,
	devoe => ww_devoe,
	o => DRAM_DQ(9));

-- Location: IOOBUF_X30_Y0_N36
\DRAM_DQ[10]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sData_r\(10),
	oe => \SDRAM1|u1|sDataDir_r~DUPLICATE_q\,
	devoe => ww_devoe,
	o => DRAM_DQ(10));

-- Location: IOOBUF_X18_Y0_N93
\DRAM_DQ[11]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sData_r\(11),
	oe => \SDRAM1|u1|sDataDir_r~DUPLICATE_q\,
	devoe => ww_devoe,
	o => DRAM_DQ(11));

-- Location: IOOBUF_X32_Y0_N53
\DRAM_DQ[12]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sData_r\(12),
	oe => \SDRAM1|u1|sDataDir_r~DUPLICATE_q\,
	devoe => ww_devoe,
	o => DRAM_DQ(12));

-- Location: IOOBUF_X32_Y0_N36
\DRAM_DQ[13]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sData_r\(13),
	oe => \SDRAM1|u1|sDataDir_r~DUPLICATE_q\,
	devoe => ww_devoe,
	o => DRAM_DQ(13));

-- Location: IOOBUF_X26_Y0_N76
\DRAM_DQ[14]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sData_r\(14),
	oe => \SDRAM1|u1|sDataDir_r~DUPLICATE_q\,
	devoe => ww_devoe,
	o => DRAM_DQ(14));

-- Location: IOOBUF_X24_Y0_N36
\DRAM_DQ[15]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \SDRAM1|u1|sData_r\(15),
	oe => \SDRAM1|u1|sDataDir_r~DUPLICATE_q\,
	devoe => ww_devoe,
	o => DRAM_DQ(15));

-- Location: IOIBUF_X32_Y0_N1
\CLOCK_50~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLOCK_50,
	o => \CLOCK_50~input_o\);

-- Location: PLLREFCLKSELECT_X0_Y21_N0
\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_REFCLK_SELECT\ : cyclonev_pll_refclk_select
-- pragma translate_off
GENERIC MAP (
	pll_auto_clk_sw_en => "false",
	pll_clk_loss_edge => "both_edges",
	pll_clk_loss_sw_en => "false",
	pll_clk_sw_dly => 0,
	pll_clkin_0_src => "clk_0",
	pll_clkin_1_src => "ref_clk1",
	pll_manu_clk_sw_en => "false",
	pll_sw_refclk_src => "clk_0")
-- pragma translate_on
PORT MAP (
	clkin => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_REFCLK_SELECT_CLKIN_bus\,
	clkout => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_REFCLK_SELECT_O_CLKOUT\,
	extswitchbuf => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_REFCLK_SELECT_O_EXTSWITCHBUF\);

-- Location: FRACTIONALPLL_X0_Y15_N0
\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL\ : cyclonev_fractional_pll
-- pragma translate_off
GENERIC MAP (
	dsm_accumulator_reset_value => 0,
	forcelock => "false",
	mimic_fbclk_type => "none",
	nreset_invert => "true",
	output_clock_frequency => "400.0 mhz",
	pll_atb => 0,
	pll_bwctrl => 4000,
	pll_cmp_buf_dly => "0 ps",
	pll_cp_comp => "true",
	pll_cp_current => 10,
	pll_ctrl_override_setting => "false",
	pll_dsm_dither => "disable",
	pll_dsm_out_sel => "disable",
	pll_dsm_reset => "false",
	pll_ecn_bypass => "false",
	pll_ecn_test_en => "false",
	pll_enable => "true",
	pll_fbclk_mux_1 => "glb",
	pll_fbclk_mux_2 => "m_cnt",
	pll_fractional_carry_out => 32,
	pll_fractional_division => 1,
	pll_fractional_division_string => "'0'",
	pll_fractional_value_ready => "true",
	pll_lf_testen => "false",
	pll_lock_fltr_cfg => 25,
	pll_lock_fltr_test => "false",
	pll_m_cnt_bypass_en => "false",
	pll_m_cnt_coarse_dly => "0 ps",
	pll_m_cnt_fine_dly => "0 ps",
	pll_m_cnt_hi_div => 8,
	pll_m_cnt_in_src => "ph_mux_clk",
	pll_m_cnt_lo_div => 8,
	pll_m_cnt_odd_div_duty_en => "false",
	pll_m_cnt_ph_mux_prst => 0,
	pll_m_cnt_prst => 1,
	pll_n_cnt_bypass_en => "false",
	pll_n_cnt_coarse_dly => "0 ps",
	pll_n_cnt_fine_dly => "0 ps",
	pll_n_cnt_hi_div => 1,
	pll_n_cnt_lo_div => 1,
	pll_n_cnt_odd_div_duty_en => "false",
	pll_ref_buf_dly => "0 ps",
	pll_reg_boost => 0,
	pll_regulator_bypass => "false",
	pll_ripplecap_ctrl => 0,
	pll_slf_rst => "true",
	pll_tclk_mux_en => "false",
	pll_tclk_sel => "n_src",
	pll_test_enable => "false",
	pll_testdn_enable => "false",
	pll_testup_enable => "false",
	pll_unlock_fltr_cfg => 2,
	pll_vco_div => 2,
	pll_vco_ph0_en => "true",
	pll_vco_ph1_en => "true",
	pll_vco_ph2_en => "true",
	pll_vco_ph3_en => "true",
	pll_vco_ph4_en => "true",
	pll_vco_ph5_en => "true",
	pll_vco_ph6_en => "true",
	pll_vco_ph7_en => "true",
	pll_vctrl_test_voltage => 750,
	reference_clock_frequency => "50.0 mhz",
	vccd0g_atb => "disable",
	vccd0g_output => 0,
	vccd1g_atb => "disable",
	vccd1g_output => 0,
	vccm1g_tap => 2,
	vccr_pd => "false",
	vcodiv_override => "false",
	fractional_pll_index => 0)
-- pragma translate_on
PORT MAP (
	coreclkfb => \PLL|pll_x2_x4_inst|altera_pll_i|fboutclk_wire\(0),
	ecnc1test => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_REFCLK_SELECT_O_EXTSWITCHBUF\,
	nresync => GND,
	refclkin => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_REFCLK_SELECT_O_CLKOUT\,
	shift => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_O_SHIFT\,
	shiftdonein => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_O_SHIFT\,
	shiften => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_O_SHIFTENM\,
	up => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_O_UP\,
	cntnen => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_CNTNEN\,
	fbclk => \PLL|pll_x2_x4_inst|altera_pll_i|fboutclk_wire\(0),
	tclk => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_TCLK\,
	vcoph => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_VCOPH_bus\,
	mhi => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_MHI_bus\);

-- Location: PLLRECONFIG_X0_Y19_N0
\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG\ : cyclonev_pll_reconfig
-- pragma translate_off
GENERIC MAP (
	fractional_pll_index => 0)
-- pragma translate_on
PORT MAP (
	cntnen => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_CNTNEN\,
	mhi => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_MHI_bus\,
	shift => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_O_SHIFT\,
	shiftenm => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_O_SHIFTENM\,
	up => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_O_UP\,
	shiften => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_SHIFTEN_bus\);

-- Location: PLLOUTPUTCOUNTER_X0_Y21_N1
\PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER\ : cyclonev_pll_output_counter
-- pragma translate_off
GENERIC MAP (
	c_cnt_coarse_dly => "0 ps",
	c_cnt_fine_dly => "0 ps",
	c_cnt_in_src => "ph_mux_clk",
	c_cnt_ph_mux_prst => 0,
	c_cnt_prst => 1,
	cnt_fpll_src => "fpll_0",
	dprio0_cnt_bypass_en => "false",
	dprio0_cnt_hi_div => 2,
	dprio0_cnt_lo_div => 2,
	dprio0_cnt_odd_div_even_duty_en => "false",
	duty_cycle => 50,
	output_clock_frequency => "100.0 mhz",
	phase_shift => "0 ps",
	fractional_pll_index => 0,
	output_counter_index => 7)
-- pragma translate_on
PORT MAP (
	nen0 => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_CNTNEN\,
	shift0 => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_O_SHIFT\,
	shiften => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIGSHIFTEN7\,
	tclk0 => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_TCLK\,
	up0 => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_O_UP\,
	vco0ph => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER_VCO0PH_bus\,
	divclk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire\(0));

-- Location: CLKCTRL_G3
\PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0\ : cyclonev_clkena
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	disable_mode => "low",
	ena_register_mode => "always enabled",
	ena_register_power_up => "high",
	test_syn => "high")
-- pragma translate_on
PORT MAP (
	inclk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire\(0),
	outclk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\);

-- Location: PLLOUTPUTCOUNTER_X0_Y20_N1
\PLL|pll_x2_x4_inst|altera_pll_i|general[1].gpll~PLL_OUTPUT_COUNTER\ : cyclonev_pll_output_counter
-- pragma translate_off
GENERIC MAP (
	c_cnt_coarse_dly => "0 ps",
	c_cnt_fine_dly => "0 ps",
	c_cnt_in_src => "ph_mux_clk",
	c_cnt_ph_mux_prst => 0,
	c_cnt_prst => 1,
	cnt_fpll_src => "fpll_0",
	dprio0_cnt_bypass_en => "false",
	dprio0_cnt_hi_div => 1,
	dprio0_cnt_lo_div => 1,
	dprio0_cnt_odd_div_even_duty_en => "false",
	duty_cycle => 50,
	output_clock_frequency => "200.0 mhz",
	phase_shift => "0 ps",
	fractional_pll_index => 0,
	output_counter_index => 6)
-- pragma translate_on
PORT MAP (
	nen0 => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_CNTNEN\,
	shift0 => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_O_SHIFT\,
	shiften => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIGSHIFTEN6\,
	tclk0 => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_TCLK\,
	up0 => \PLL|pll_x2_x4_inst|altera_pll_i|general[0].gpll~PLL_RECONFIG_O_UP\,
	vco0ph => \PLL|pll_x2_x4_inst|altera_pll_i|general[1].gpll~PLL_OUTPUT_COUNTER_VCO0PH_bus\,
	divclk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire\(1));

-- Location: CLKCTRL_G1
\PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0\ : cyclonev_clkena
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	disable_mode => "low",
	ena_register_mode => "always enabled",
	ena_register_power_up => "high",
	test_syn => "high")
-- pragma translate_on
PORT MAP (
	inclk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire\(1),
	outclk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\);

-- Location: LABCELL_X51_Y20_N30
\Add0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~5_sumout\ = SUM(( hAddr(0) ) + ( VCC ) + ( !VCC ))
-- \Add0~6\ = CARRY(( hAddr(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_hAddr(0),
	cin => GND,
	sumout => \Add0~5_sumout\,
	cout => \Add0~6\);

-- Location: IOIBUF_X40_Y0_N18
\KEY[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(3),
	o => \KEY[3]~input_o\);

-- Location: LABCELL_X51_Y21_N39
\SDRAM1|u1|state_r.INITWAIT~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|state_r.INITWAIT~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \SDRAM1|u1|state_r.INITWAIT~feeder_combout\);

-- Location: MLABCELL_X47_Y20_N0
\SDRAM1|u1|Add0~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add0~25_sumout\ = SUM(( \SDRAM1|u1|nopCntr_r\(0) ) + ( VCC ) + ( !VCC ))
-- \SDRAM1|u1|Add0~26\ = CARRY(( \SDRAM1|u1|nopCntr_r\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \SDRAM1|u1|ALT_INV_nopCntr_r\(0),
	cin => GND,
	sumout => \SDRAM1|u1|Add0~25_sumout\,
	cout => \SDRAM1|u1|Add0~26\);

-- Location: LABCELL_X51_Y20_N33
\Add0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~9_sumout\ = SUM(( hAddr(1) ) + ( GND ) + ( \Add0~6\ ))
-- \Add0~10\ = CARRY(( hAddr(1) ) + ( GND ) + ( \Add0~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_hAddr(1),
	cin => \Add0~6\,
	sumout => \Add0~9_sumout\,
	cout => \Add0~10\);

-- Location: FF_X48_Y20_N44
\SDRAM1|u1|sDataDir_r\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|wrTimer_r[1]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sDataDir_r~q\);

-- Location: LABCELL_X48_Y20_N9
\hAddr[23]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \hAddr[23]~2_combout\ = ( \SDRAM1|u1|rdPipeline_r\(0) & ( \FSM_autotest.A_WRITE_WAIT~q\ ) ) # ( !\SDRAM1|u1|rdPipeline_r\(0) & ( (\SDRAM1|u1|sDataDir_r~q\ & \FSM_autotest.A_WRITE_WAIT~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111000000000000111100000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \SDRAM1|u1|ALT_INV_sDataDir_r~q\,
	datad => \ALT_INV_FSM_autotest.A_WRITE_WAIT~q\,
	dataf => \SDRAM1|u1|ALT_INV_rdPipeline_r\(0),
	combout => \hAddr[23]~2_combout\);

-- Location: IOIBUF_X30_Y0_N52
\DRAM_DQ[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => DRAM_DQ(4),
	o => \DRAM_DQ[4]~input_o\);

-- Location: FF_X51_Y20_N29
\SDRAM1|u1|hDOut_r[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => \DRAM_DQ[4]~input_o\,
	sload => VCC,
	ena => \SDRAM1|u1|rdPipeline_r\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|hDOut_r\(4));

-- Location: IOIBUF_X18_Y0_N75
\DRAM_DQ[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => DRAM_DQ(5),
	o => \DRAM_DQ[5]~input_o\);

-- Location: FF_X51_Y20_N17
\SDRAM1|u1|hDOut_r[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => \DRAM_DQ[5]~input_o\,
	sload => VCC,
	ena => \SDRAM1|u1|rdPipeline_r\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|hDOut_r\(5));

-- Location: IOIBUF_X28_Y0_N52
\DRAM_DQ[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => DRAM_DQ(3),
	o => \DRAM_DQ[3]~input_o\);

-- Location: FF_X51_Y20_N22
\SDRAM1|u1|hDOut_r[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => \DRAM_DQ[3]~input_o\,
	sload => VCC,
	ena => \SDRAM1|u1|rdPipeline_r\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|hDOut_r\(3));

-- Location: LABCELL_X51_Y20_N24
\Equal1~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal1~5_combout\ = ( hAddr(5) & ( \SDRAM1|u1|hDOut_r\(3) & ( (hAddr(3) & (\SDRAM1|u1|hDOut_r\(5) & (!hAddr(4) $ (\SDRAM1|u1|hDOut_r\(4))))) ) ) ) # ( !hAddr(5) & ( \SDRAM1|u1|hDOut_r\(3) & ( (hAddr(3) & (!\SDRAM1|u1|hDOut_r\(5) & (!hAddr(4) $ 
-- (\SDRAM1|u1|hDOut_r\(4))))) ) ) ) # ( hAddr(5) & ( !\SDRAM1|u1|hDOut_r\(3) & ( (!hAddr(3) & (\SDRAM1|u1|hDOut_r\(5) & (!hAddr(4) $ (\SDRAM1|u1|hDOut_r\(4))))) ) ) ) # ( !hAddr(5) & ( !\SDRAM1|u1|hDOut_r\(3) & ( (!hAddr(3) & (!\SDRAM1|u1|hDOut_r\(5) & 
-- (!hAddr(4) $ (\SDRAM1|u1|hDOut_r\(4))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000010000000000000000001000010000100001000000000000000000100001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(4),
	datab => ALT_INV_hAddr(3),
	datac => \SDRAM1|u1|ALT_INV_hDOut_r\(4),
	datad => \SDRAM1|u1|ALT_INV_hDOut_r\(5),
	datae => ALT_INV_hAddr(5),
	dataf => \SDRAM1|u1|ALT_INV_hDOut_r\(3),
	combout => \Equal1~5_combout\);

-- Location: IOIBUF_X24_Y0_N52
\DRAM_DQ[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => DRAM_DQ(0),
	o => \DRAM_DQ[0]~input_o\);

-- Location: LABCELL_X50_Y20_N27
\SDRAM1|u1|hDOut_r[0]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|hDOut_r[0]~feeder_combout\ = ( \DRAM_DQ[0]~input_o\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_DRAM_DQ[0]~input_o\,
	combout => \SDRAM1|u1|hDOut_r[0]~feeder_combout\);

-- Location: FF_X50_Y20_N28
\SDRAM1|u1|hDOut_r[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|hDOut_r[0]~feeder_combout\,
	ena => \SDRAM1|u1|rdPipeline_r\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|hDOut_r\(0));

-- Location: IOIBUF_X28_Y0_N35
\DRAM_DQ[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => DRAM_DQ(2),
	o => \DRAM_DQ[2]~input_o\);

-- Location: FF_X51_Y20_N11
\SDRAM1|u1|hDOut_r[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => \DRAM_DQ[2]~input_o\,
	sload => VCC,
	ena => \SDRAM1|u1|rdPipeline_r\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|hDOut_r\(2));

-- Location: IOIBUF_X26_Y0_N92
\DRAM_DQ[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => DRAM_DQ(1),
	o => \DRAM_DQ[1]~input_o\);

-- Location: FF_X51_Y20_N5
\SDRAM1|u1|hDOut_r[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => \DRAM_DQ[1]~input_o\,
	sload => VCC,
	ena => \SDRAM1|u1|rdPipeline_r\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|hDOut_r\(1));

-- Location: LABCELL_X51_Y20_N12
\Equal1~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal1~4_combout\ = ( \SDRAM1|u1|hDOut_r\(1) & ( hAddr(1) & ( (!\SDRAM1|u1|hDOut_r\(0) & (!hAddr(0) & (!\SDRAM1|u1|hDOut_r\(2) $ (hAddr(2))))) # (\SDRAM1|u1|hDOut_r\(0) & (hAddr(0) & (!\SDRAM1|u1|hDOut_r\(2) $ (hAddr(2))))) ) ) ) # ( 
-- !\SDRAM1|u1|hDOut_r\(1) & ( !hAddr(1) & ( (!\SDRAM1|u1|hDOut_r\(0) & (!hAddr(0) & (!\SDRAM1|u1|hDOut_r\(2) $ (hAddr(2))))) # (\SDRAM1|u1|hDOut_r\(0) & (hAddr(0) & (!\SDRAM1|u1|hDOut_r\(2) $ (hAddr(2))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000001001000001000000000000000000000000000000001000001001000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_hDOut_r\(0),
	datab => \SDRAM1|u1|ALT_INV_hDOut_r\(2),
	datac => ALT_INV_hAddr(2),
	datad => ALT_INV_hAddr(0),
	datae => \SDRAM1|u1|ALT_INV_hDOut_r\(1),
	dataf => ALT_INV_hAddr(1),
	combout => \Equal1~4_combout\);

-- Location: IOIBUF_X34_Y0_N41
\DRAM_DQ[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => DRAM_DQ(7),
	o => \DRAM_DQ[7]~input_o\);

-- Location: FF_X52_Y19_N53
\SDRAM1|u1|hDOut_r[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => \DRAM_DQ[7]~input_o\,
	sload => VCC,
	ena => \SDRAM1|u1|rdPipeline_r\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|hDOut_r\(7));

-- Location: IOIBUF_X34_Y0_N75
\DRAM_DQ[8]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => DRAM_DQ(8),
	o => \DRAM_DQ[8]~input_o\);

-- Location: MLABCELL_X52_Y19_N57
\SDRAM1|u1|hDOut_r[8]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|hDOut_r[8]~feeder_combout\ = ( \DRAM_DQ[8]~input_o\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_DRAM_DQ[8]~input_o\,
	combout => \SDRAM1|u1|hDOut_r[8]~feeder_combout\);

-- Location: FF_X52_Y19_N59
\SDRAM1|u1|hDOut_r[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|hDOut_r[8]~feeder_combout\,
	ena => \SDRAM1|u1|rdPipeline_r\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|hDOut_r\(8));

-- Location: IOIBUF_X34_Y0_N58
\DRAM_DQ[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => DRAM_DQ(6),
	o => \DRAM_DQ[6]~input_o\);

-- Location: MLABCELL_X52_Y19_N15
\SDRAM1|u1|hDOut_r[6]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|hDOut_r[6]~feeder_combout\ = ( \DRAM_DQ[6]~input_o\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_DRAM_DQ[6]~input_o\,
	combout => \SDRAM1|u1|hDOut_r[6]~feeder_combout\);

-- Location: FF_X52_Y19_N17
\SDRAM1|u1|hDOut_r[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|hDOut_r[6]~feeder_combout\,
	ena => \SDRAM1|u1|rdPipeline_r\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|hDOut_r\(6));

-- Location: MLABCELL_X52_Y19_N0
\Equal1~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal1~3_combout\ = ( \SDRAM1|u1|hDOut_r\(8) & ( \SDRAM1|u1|hDOut_r\(6) & ( (hAddr(8) & (hAddr(6) & (!hAddr(7) $ (\SDRAM1|u1|hDOut_r\(7))))) ) ) ) # ( !\SDRAM1|u1|hDOut_r\(8) & ( \SDRAM1|u1|hDOut_r\(6) & ( (!hAddr(8) & (hAddr(6) & (!hAddr(7) $ 
-- (\SDRAM1|u1|hDOut_r\(7))))) ) ) ) # ( \SDRAM1|u1|hDOut_r\(8) & ( !\SDRAM1|u1|hDOut_r\(6) & ( (hAddr(8) & (!hAddr(6) & (!hAddr(7) $ (\SDRAM1|u1|hDOut_r\(7))))) ) ) ) # ( !\SDRAM1|u1|hDOut_r\(8) & ( !\SDRAM1|u1|hDOut_r\(6) & ( (!hAddr(8) & (!hAddr(6) & 
-- (!hAddr(7) $ (\SDRAM1|u1|hDOut_r\(7))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000010000000000001000010000000000000000100001000000000000100001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(7),
	datab => ALT_INV_hAddr(8),
	datac => \SDRAM1|u1|ALT_INV_hDOut_r\(7),
	datad => ALT_INV_hAddr(6),
	datae => \SDRAM1|u1|ALT_INV_hDOut_r\(8),
	dataf => \SDRAM1|u1|ALT_INV_hDOut_r\(6),
	combout => \Equal1~3_combout\);

-- Location: IOIBUF_X26_Y0_N75
\DRAM_DQ[14]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => DRAM_DQ(14),
	o => \DRAM_DQ[14]~input_o\);

-- Location: FF_X51_Y21_N8
\SDRAM1|u1|hDOut_r[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => \DRAM_DQ[14]~input_o\,
	sload => VCC,
	ena => \SDRAM1|u1|rdPipeline_r\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|hDOut_r\(14));

-- Location: IOIBUF_X32_Y0_N35
\DRAM_DQ[13]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => DRAM_DQ(13),
	o => \DRAM_DQ[13]~input_o\);

-- Location: FF_X51_Y21_N47
\SDRAM1|u1|hDOut_r[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => \DRAM_DQ[13]~input_o\,
	sload => VCC,
	ena => \SDRAM1|u1|rdPipeline_r\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|hDOut_r\(13));

-- Location: IOIBUF_X32_Y0_N52
\DRAM_DQ[12]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => DRAM_DQ(12),
	o => \DRAM_DQ[12]~input_o\);

-- Location: FF_X51_Y21_N11
\SDRAM1|u1|hDOut_r[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => \DRAM_DQ[12]~input_o\,
	sload => VCC,
	ena => \SDRAM1|u1|rdPipeline_r\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|hDOut_r\(12));

-- Location: LABCELL_X51_Y21_N42
\Equal1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal1~1_combout\ = ( \SDRAM1|u1|hDOut_r\(13) & ( \SDRAM1|u1|hDOut_r\(12) & ( (hAddr(13) & (hAddr(12) & (!hAddr(14) $ (\SDRAM1|u1|hDOut_r\(14))))) ) ) ) # ( !\SDRAM1|u1|hDOut_r\(13) & ( \SDRAM1|u1|hDOut_r\(12) & ( (!hAddr(13) & (hAddr(12) & (!hAddr(14) $ 
-- (\SDRAM1|u1|hDOut_r\(14))))) ) ) ) # ( \SDRAM1|u1|hDOut_r\(13) & ( !\SDRAM1|u1|hDOut_r\(12) & ( (hAddr(13) & (!hAddr(12) & (!hAddr(14) $ (\SDRAM1|u1|hDOut_r\(14))))) ) ) ) # ( !\SDRAM1|u1|hDOut_r\(13) & ( !\SDRAM1|u1|hDOut_r\(12) & ( (!hAddr(13) & 
-- (!hAddr(12) & (!hAddr(14) $ (\SDRAM1|u1|hDOut_r\(14))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000001000000001000000001000000001000000001000000001000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(14),
	datab => ALT_INV_hAddr(13),
	datac => ALT_INV_hAddr(12),
	datad => \SDRAM1|u1|ALT_INV_hDOut_r\(14),
	datae => \SDRAM1|u1|ALT_INV_hDOut_r\(13),
	dataf => \SDRAM1|u1|ALT_INV_hDOut_r\(12),
	combout => \Equal1~1_combout\);

-- Location: IOIBUF_X30_Y0_N35
\DRAM_DQ[10]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => DRAM_DQ(10),
	o => \DRAM_DQ[10]~input_o\);

-- Location: FF_X51_Y21_N56
\SDRAM1|u1|hDOut_r[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => \DRAM_DQ[10]~input_o\,
	sload => VCC,
	ena => \SDRAM1|u1|rdPipeline_r\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|hDOut_r\(10));

-- Location: IOIBUF_X34_Y0_N92
\DRAM_DQ[9]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => DRAM_DQ(9),
	o => \DRAM_DQ[9]~input_o\);

-- Location: FF_X51_Y21_N53
\SDRAM1|u1|hDOut_r[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => \DRAM_DQ[9]~input_o\,
	sload => VCC,
	ena => \SDRAM1|u1|rdPipeline_r\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|hDOut_r\(9));

-- Location: IOIBUF_X18_Y0_N92
\DRAM_DQ[11]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => DRAM_DQ(11),
	o => \DRAM_DQ[11]~input_o\);

-- Location: FF_X51_Y21_N50
\SDRAM1|u1|hDOut_r[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => \DRAM_DQ[11]~input_o\,
	sload => VCC,
	ena => \SDRAM1|u1|rdPipeline_r\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|hDOut_r\(11));

-- Location: LABCELL_X51_Y21_N48
\Equal1~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal1~2_combout\ = ( \SDRAM1|u1|hDOut_r\(11) & ( hAddr(11) & ( (!\SDRAM1|u1|hDOut_r\(10) & (!hAddr(10) & (!hAddr(9) $ (\SDRAM1|u1|hDOut_r\(9))))) # (\SDRAM1|u1|hDOut_r\(10) & (hAddr(10) & (!hAddr(9) $ (\SDRAM1|u1|hDOut_r\(9))))) ) ) ) # ( 
-- !\SDRAM1|u1|hDOut_r\(11) & ( !hAddr(11) & ( (!\SDRAM1|u1|hDOut_r\(10) & (!hAddr(10) & (!hAddr(9) $ (\SDRAM1|u1|hDOut_r\(9))))) # (\SDRAM1|u1|hDOut_r\(10) & (hAddr(10) & (!hAddr(9) $ (\SDRAM1|u1|hDOut_r\(9))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000001001000001000000000000000000000000000000001000001001000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_hDOut_r\(10),
	datab => ALT_INV_hAddr(9),
	datac => \SDRAM1|u1|ALT_INV_hDOut_r\(9),
	datad => ALT_INV_hAddr(10),
	datae => \SDRAM1|u1|ALT_INV_hDOut_r\(11),
	dataf => ALT_INV_hAddr(11),
	combout => \Equal1~2_combout\);

-- Location: IOIBUF_X24_Y0_N35
\DRAM_DQ[15]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => DRAM_DQ(15),
	o => \DRAM_DQ[15]~input_o\);

-- Location: FF_X50_Y20_N59
\SDRAM1|u1|hDOut_r[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => \DRAM_DQ[15]~input_o\,
	sload => VCC,
	ena => \SDRAM1|u1|rdPipeline_r\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|hDOut_r\(15));

-- Location: LABCELL_X50_Y20_N30
\Equal1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal1~0_combout\ = ( \SDRAM1|u1|hDOut_r\(15) & ( !hAddr(15) ) ) # ( !\SDRAM1|u1|hDOut_r\(15) & ( hAddr(15) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111111111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_hAddr(15),
	dataf => \SDRAM1|u1|ALT_INV_hDOut_r\(15),
	combout => \Equal1~0_combout\);

-- Location: LABCELL_X51_Y20_N18
\Equal1~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal1~6_combout\ = ( \Equal1~2_combout\ & ( !\Equal1~0_combout\ & ( (\Equal1~5_combout\ & (\Equal1~4_combout\ & (\Equal1~3_combout\ & \Equal1~1_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal1~5_combout\,
	datab => \ALT_INV_Equal1~4_combout\,
	datac => \ALT_INV_Equal1~3_combout\,
	datad => \ALT_INV_Equal1~1_combout\,
	datae => \ALT_INV_Equal1~2_combout\,
	dataf => \ALT_INV_Equal1~0_combout\,
	combout => \Equal1~6_combout\);

-- Location: LABCELL_X48_Y20_N33
\Selector33~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector33~0_combout\ = ( \SDRAM1|u1|rdPipeline_r\(0) & ( \FSM_autotest.A_READ_WAIT~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_FSM_autotest.A_READ_WAIT~q\,
	dataf => \SDRAM1|u1|ALT_INV_rdPipeline_r\(0),
	combout => \Selector33~0_combout\);

-- Location: MLABCELL_X52_Y20_N36
\Equal0~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal0~3_combout\ = ( hAddr(18) & ( hAddr(8) & ( (hAddr(21) & (hAddr(16) & (hAddr(22) & hAddr(17)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(21),
	datab => ALT_INV_hAddr(16),
	datac => ALT_INV_hAddr(22),
	datad => ALT_INV_hAddr(17),
	datae => ALT_INV_hAddr(18),
	dataf => ALT_INV_hAddr(8),
	combout => \Equal0~3_combout\);

-- Location: MLABCELL_X52_Y20_N30
\Equal0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal0~0_combout\ = ( hAddr(10) & ( hAddr(4) & ( (hAddr(7) & (hAddr(6) & (hAddr(5) & hAddr(11)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(7),
	datab => ALT_INV_hAddr(6),
	datac => ALT_INV_hAddr(5),
	datad => ALT_INV_hAddr(11),
	datae => ALT_INV_hAddr(10),
	dataf => ALT_INV_hAddr(4),
	combout => \Equal0~0_combout\);

-- Location: LABCELL_X51_Y19_N48
\Equal0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal0~1_combout\ = ( hAddr(9) & ( hAddr(12) & ( (hAddr(15) & (hAddr(13) & (hAddr(19) & hAddr(14)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(15),
	datab => ALT_INV_hAddr(13),
	datac => ALT_INV_hAddr(19),
	datad => ALT_INV_hAddr(14),
	datae => ALT_INV_hAddr(9),
	dataf => ALT_INV_hAddr(12),
	combout => \Equal0~1_combout\);

-- Location: MLABCELL_X52_Y20_N12
\Equal0~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal0~4_combout\ = ( \Equal0~1_combout\ & ( hAddr(0) & ( (\Equal0~3_combout\ & (\Equal0~2_combout\ & \Equal0~0_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal0~3_combout\,
	datac => \ALT_INV_Equal0~2_combout\,
	datad => \ALT_INV_Equal0~0_combout\,
	datae => \ALT_INV_Equal0~1_combout\,
	dataf => ALT_INV_hAddr(0),
	combout => \Equal0~4_combout\);

-- Location: LABCELL_X48_Y20_N30
\hAddr[23]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \hAddr[23]~4_combout\ = ( \FSM_autotest.A_READ_WAIT~q\ & ( (!\FSM_autotest.A_SET_WRITE~q\ & (\SDRAM1|u1|rdPipeline_r\(0) & ((\Equal1~6_combout\)))) # (\FSM_autotest.A_SET_WRITE~q\ & (((\SDRAM1|u1|rdPipeline_r\(0) & \Equal1~6_combout\)) # 
-- (\Equal0~4_combout\))) ) ) # ( !\FSM_autotest.A_READ_WAIT~q\ & ( (\FSM_autotest.A_SET_WRITE~q\ & \Equal0~4_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000101000001010000010100000101001101110000010100110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_FSM_autotest.A_SET_WRITE~q\,
	datab => \SDRAM1|u1|ALT_INV_rdPipeline_r\(0),
	datac => \ALT_INV_Equal0~4_combout\,
	datad => \ALT_INV_Equal1~6_combout\,
	dataf => \ALT_INV_FSM_autotest.A_READ_WAIT~q\,
	combout => \hAddr[23]~4_combout\);

-- Location: FF_X48_Y20_N31
\FSM_autotest.A_SET_READ\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \hAddr[23]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \FSM_autotest.A_SET_READ~q\);

-- Location: LABCELL_X48_Y20_N54
\Selector0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector0~0_combout\ = ( \FSM_autotest.A_IDLE~q\ & ( \FSM_autotest.A_SET_READ~q\ & ( (!\Equal0~4_combout\ & ((!\Selector33~0_combout\) # (\Equal1~6_combout\))) ) ) ) # ( !\FSM_autotest.A_IDLE~q\ & ( \FSM_autotest.A_SET_READ~q\ & ( (!\KEY[3]~input_o\ & 
-- (!\Equal0~4_combout\ & ((!\Selector33~0_combout\) # (\Equal1~6_combout\)))) ) ) ) # ( \FSM_autotest.A_IDLE~q\ & ( !\FSM_autotest.A_SET_READ~q\ & ( (!\Selector33~0_combout\) # (\Equal1~6_combout\) ) ) ) # ( !\FSM_autotest.A_IDLE~q\ & ( 
-- !\FSM_autotest.A_SET_READ~q\ & ( (!\KEY[3]~input_o\ & ((!\Selector33~0_combout\) # (\Equal1~6_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1101000011010000110111011101110111010000000000001101110100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal1~6_combout\,
	datab => \ALT_INV_Selector33~0_combout\,
	datac => \ALT_INV_KEY[3]~input_o\,
	datad => \ALT_INV_Equal0~4_combout\,
	datae => \ALT_INV_FSM_autotest.A_IDLE~q\,
	dataf => \ALT_INV_FSM_autotest.A_SET_READ~q\,
	combout => \Selector0~0_combout\);

-- Location: FF_X48_Y20_N56
\FSM_autotest.A_IDLE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Selector0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \FSM_autotest.A_IDLE~q\);

-- Location: LABCELL_X51_Y20_N6
\hAddr[23]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \hAddr[23]~1_combout\ = ( \Equal0~3_combout\ & ( \Equal0~1_combout\ & ( (\FSM_autotest.A_SET_WRITE~q\ & (hAddr(0) & (\Equal0~0_combout\ & \Equal0~2_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_FSM_autotest.A_SET_WRITE~q\,
	datab => ALT_INV_hAddr(0),
	datac => \ALT_INV_Equal0~0_combout\,
	datad => \ALT_INV_Equal0~2_combout\,
	datae => \ALT_INV_Equal0~3_combout\,
	dataf => \ALT_INV_Equal0~1_combout\,
	combout => \hAddr[23]~1_combout\);

-- Location: LABCELL_X51_Y20_N0
\hAddr[23]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \hAddr[23]~3_combout\ = ( \Selector33~0_combout\ & ( \Equal1~6_combout\ ) ) # ( !\Selector33~0_combout\ & ( \Equal1~6_combout\ & ( (((!\FSM_autotest.A_IDLE~q\ & !\KEY[3]~input_o\)) # (\hAddr[23]~1_combout\)) # (\hAddr[23]~2_combout\) ) ) ) # ( 
-- \Selector33~0_combout\ & ( !\Equal1~6_combout\ & ( (((!\FSM_autotest.A_IDLE~q\ & !\KEY[3]~input_o\)) # (\hAddr[23]~1_combout\)) # (\hAddr[23]~2_combout\) ) ) ) # ( !\Selector33~0_combout\ & ( !\Equal1~6_combout\ & ( (((!\FSM_autotest.A_IDLE~q\ & 
-- !\KEY[3]~input_o\)) # (\hAddr[23]~1_combout\)) # (\hAddr[23]~2_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1101010111111111110101011111111111010101111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_hAddr[23]~2_combout\,
	datab => \ALT_INV_FSM_autotest.A_IDLE~q\,
	datac => \ALT_INV_KEY[3]~input_o\,
	datad => \ALT_INV_hAddr[23]~1_combout\,
	datae => \ALT_INV_Selector33~0_combout\,
	dataf => \ALT_INV_Equal1~6_combout\,
	combout => \hAddr[23]~3_combout\);

-- Location: FF_X51_Y20_N35
\hAddr[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Add0~9_sumout\,
	sclr => \hAddr[23]~0_combout\,
	ena => \hAddr[23]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hAddr(1));

-- Location: LABCELL_X51_Y20_N36
\Add0~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~13_sumout\ = SUM(( hAddr(2) ) + ( GND ) + ( \Add0~10\ ))
-- \Add0~14\ = CARRY(( hAddr(2) ) + ( GND ) + ( \Add0~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_hAddr(2),
	cin => \Add0~10\,
	sumout => \Add0~13_sumout\,
	cout => \Add0~14\);

-- Location: FF_X51_Y20_N38
\hAddr[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Add0~13_sumout\,
	sclr => \hAddr[23]~0_combout\,
	ena => \hAddr[23]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hAddr(2));

-- Location: LABCELL_X51_Y20_N39
\Add0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~17_sumout\ = SUM(( hAddr(3) ) + ( GND ) + ( \Add0~14\ ))
-- \Add0~18\ = CARRY(( hAddr(3) ) + ( GND ) + ( \Add0~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_hAddr(3),
	cin => \Add0~14\,
	sumout => \Add0~17_sumout\,
	cout => \Add0~18\);

-- Location: FF_X51_Y20_N41
\hAddr[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Add0~17_sumout\,
	sclr => \hAddr[23]~0_combout\,
	ena => \hAddr[23]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hAddr(3));

-- Location: LABCELL_X51_Y20_N42
\Add0~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~21_sumout\ = SUM(( hAddr(4) ) + ( GND ) + ( \Add0~18\ ))
-- \Add0~22\ = CARRY(( hAddr(4) ) + ( GND ) + ( \Add0~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_hAddr(4),
	cin => \Add0~18\,
	sumout => \Add0~21_sumout\,
	cout => \Add0~22\);

-- Location: FF_X51_Y20_N44
\hAddr[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Add0~21_sumout\,
	sclr => \hAddr[23]~0_combout\,
	ena => \hAddr[23]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hAddr(4));

-- Location: LABCELL_X51_Y20_N45
\Add0~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~25_sumout\ = SUM(( hAddr(5) ) + ( GND ) + ( \Add0~22\ ))
-- \Add0~26\ = CARRY(( hAddr(5) ) + ( GND ) + ( \Add0~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_hAddr(5),
	cin => \Add0~22\,
	sumout => \Add0~25_sumout\,
	cout => \Add0~26\);

-- Location: FF_X51_Y20_N47
\hAddr[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Add0~25_sumout\,
	sclr => \hAddr[23]~0_combout\,
	ena => \hAddr[23]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hAddr(5));

-- Location: LABCELL_X51_Y20_N48
\Add0~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~29_sumout\ = SUM(( hAddr(6) ) + ( GND ) + ( \Add0~26\ ))
-- \Add0~30\ = CARRY(( hAddr(6) ) + ( GND ) + ( \Add0~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_hAddr(6),
	cin => \Add0~26\,
	sumout => \Add0~29_sumout\,
	cout => \Add0~30\);

-- Location: FF_X51_Y20_N50
\hAddr[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Add0~29_sumout\,
	sclr => \hAddr[23]~0_combout\,
	ena => \hAddr[23]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hAddr(6));

-- Location: LABCELL_X51_Y20_N51
\Add0~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~33_sumout\ = SUM(( hAddr(7) ) + ( GND ) + ( \Add0~30\ ))
-- \Add0~34\ = CARRY(( hAddr(7) ) + ( GND ) + ( \Add0~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_hAddr(7),
	cin => \Add0~30\,
	sumout => \Add0~33_sumout\,
	cout => \Add0~34\);

-- Location: FF_X51_Y20_N53
\hAddr[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Add0~33_sumout\,
	sclr => \hAddr[23]~0_combout\,
	ena => \hAddr[23]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hAddr(7));

-- Location: LABCELL_X51_Y20_N54
\Add0~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~37_sumout\ = SUM(( hAddr(8) ) + ( GND ) + ( \Add0~34\ ))
-- \Add0~38\ = CARRY(( hAddr(8) ) + ( GND ) + ( \Add0~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_hAddr(8),
	cin => \Add0~34\,
	sumout => \Add0~37_sumout\,
	cout => \Add0~38\);

-- Location: FF_X51_Y20_N56
\hAddr[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Add0~37_sumout\,
	sclr => \hAddr[23]~0_combout\,
	ena => \hAddr[23]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hAddr(8));

-- Location: LABCELL_X51_Y20_N57
\Add0~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~41_sumout\ = SUM(( hAddr(9) ) + ( GND ) + ( \Add0~38\ ))
-- \Add0~42\ = CARRY(( hAddr(9) ) + ( GND ) + ( \Add0~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_hAddr(9),
	cin => \Add0~38\,
	sumout => \Add0~41_sumout\,
	cout => \Add0~42\);

-- Location: FF_X51_Y20_N59
\hAddr[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Add0~41_sumout\,
	sclr => \hAddr[23]~0_combout\,
	ena => \hAddr[23]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hAddr(9));

-- Location: LABCELL_X51_Y19_N0
\Add0~45\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~45_sumout\ = SUM(( hAddr(10) ) + ( GND ) + ( \Add0~42\ ))
-- \Add0~46\ = CARRY(( hAddr(10) ) + ( GND ) + ( \Add0~42\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_hAddr(10),
	cin => \Add0~42\,
	sumout => \Add0~45_sumout\,
	cout => \Add0~46\);

-- Location: FF_X51_Y19_N2
\hAddr[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Add0~45_sumout\,
	sclr => \hAddr[23]~0_combout\,
	ena => \hAddr[23]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hAddr(10));

-- Location: LABCELL_X51_Y19_N3
\Add0~49\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~49_sumout\ = SUM(( hAddr(11) ) + ( GND ) + ( \Add0~46\ ))
-- \Add0~50\ = CARRY(( hAddr(11) ) + ( GND ) + ( \Add0~46\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_hAddr(11),
	cin => \Add0~46\,
	sumout => \Add0~49_sumout\,
	cout => \Add0~50\);

-- Location: FF_X51_Y19_N5
\hAddr[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Add0~49_sumout\,
	sclr => \hAddr[23]~0_combout\,
	ena => \hAddr[23]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hAddr(11));

-- Location: LABCELL_X51_Y19_N6
\Add0~53\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~53_sumout\ = SUM(( hAddr(12) ) + ( GND ) + ( \Add0~50\ ))
-- \Add0~54\ = CARRY(( hAddr(12) ) + ( GND ) + ( \Add0~50\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_hAddr(12),
	cin => \Add0~50\,
	sumout => \Add0~53_sumout\,
	cout => \Add0~54\);

-- Location: FF_X51_Y19_N8
\hAddr[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Add0~53_sumout\,
	sclr => \hAddr[23]~0_combout\,
	ena => \hAddr[23]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hAddr(12));

-- Location: LABCELL_X51_Y19_N9
\Add0~57\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~57_sumout\ = SUM(( hAddr(13) ) + ( GND ) + ( \Add0~54\ ))
-- \Add0~58\ = CARRY(( hAddr(13) ) + ( GND ) + ( \Add0~54\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_hAddr(13),
	cin => \Add0~54\,
	sumout => \Add0~57_sumout\,
	cout => \Add0~58\);

-- Location: FF_X51_Y19_N11
\hAddr[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Add0~57_sumout\,
	sclr => \hAddr[23]~0_combout\,
	ena => \hAddr[23]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hAddr(13));

-- Location: LABCELL_X51_Y19_N12
\Add0~61\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~61_sumout\ = SUM(( hAddr(14) ) + ( GND ) + ( \Add0~58\ ))
-- \Add0~62\ = CARRY(( hAddr(14) ) + ( GND ) + ( \Add0~58\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_hAddr(14),
	cin => \Add0~58\,
	sumout => \Add0~61_sumout\,
	cout => \Add0~62\);

-- Location: FF_X51_Y19_N14
\hAddr[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Add0~61_sumout\,
	sclr => \hAddr[23]~0_combout\,
	ena => \hAddr[23]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hAddr(14));

-- Location: LABCELL_X51_Y19_N15
\Add0~65\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~65_sumout\ = SUM(( hAddr(15) ) + ( GND ) + ( \Add0~62\ ))
-- \Add0~66\ = CARRY(( hAddr(15) ) + ( GND ) + ( \Add0~62\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_hAddr(15),
	cin => \Add0~62\,
	sumout => \Add0~65_sumout\,
	cout => \Add0~66\);

-- Location: FF_X51_Y19_N17
\hAddr[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Add0~65_sumout\,
	sclr => \hAddr[23]~0_combout\,
	ena => \hAddr[23]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hAddr(15));

-- Location: LABCELL_X51_Y19_N18
\Add0~69\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~69_sumout\ = SUM(( hAddr(16) ) + ( GND ) + ( \Add0~66\ ))
-- \Add0~70\ = CARRY(( hAddr(16) ) + ( GND ) + ( \Add0~66\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_hAddr(16),
	cin => \Add0~66\,
	sumout => \Add0~69_sumout\,
	cout => \Add0~70\);

-- Location: FF_X51_Y19_N20
\hAddr[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Add0~69_sumout\,
	sclr => \hAddr[23]~0_combout\,
	ena => \hAddr[23]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hAddr(16));

-- Location: LABCELL_X51_Y19_N21
\Add0~73\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~73_sumout\ = SUM(( hAddr(17) ) + ( GND ) + ( \Add0~70\ ))
-- \Add0~74\ = CARRY(( hAddr(17) ) + ( GND ) + ( \Add0~70\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_hAddr(17),
	cin => \Add0~70\,
	sumout => \Add0~73_sumout\,
	cout => \Add0~74\);

-- Location: FF_X51_Y19_N23
\hAddr[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Add0~73_sumout\,
	sclr => \hAddr[23]~0_combout\,
	ena => \hAddr[23]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hAddr(17));

-- Location: LABCELL_X51_Y19_N24
\Add0~77\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~77_sumout\ = SUM(( hAddr(18) ) + ( GND ) + ( \Add0~74\ ))
-- \Add0~78\ = CARRY(( hAddr(18) ) + ( GND ) + ( \Add0~74\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_hAddr(18),
	cin => \Add0~74\,
	sumout => \Add0~77_sumout\,
	cout => \Add0~78\);

-- Location: FF_X51_Y19_N26
\hAddr[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Add0~77_sumout\,
	sclr => \hAddr[23]~0_combout\,
	ena => \hAddr[23]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hAddr(18));

-- Location: LABCELL_X51_Y19_N27
\Add0~81\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~81_sumout\ = SUM(( hAddr(19) ) + ( GND ) + ( \Add0~78\ ))
-- \Add0~82\ = CARRY(( hAddr(19) ) + ( GND ) + ( \Add0~78\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_hAddr(19),
	cin => \Add0~78\,
	sumout => \Add0~81_sumout\,
	cout => \Add0~82\);

-- Location: FF_X51_Y19_N29
\hAddr[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Add0~81_sumout\,
	sclr => \hAddr[23]~0_combout\,
	ena => \hAddr[23]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hAddr(19));

-- Location: LABCELL_X51_Y19_N30
\Add0~85\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~85_sumout\ = SUM(( hAddr(20) ) + ( GND ) + ( \Add0~82\ ))
-- \Add0~86\ = CARRY(( hAddr(20) ) + ( GND ) + ( \Add0~82\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_hAddr(20),
	cin => \Add0~82\,
	sumout => \Add0~85_sumout\,
	cout => \Add0~86\);

-- Location: FF_X51_Y19_N32
\hAddr[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Add0~85_sumout\,
	sclr => \hAddr[23]~0_combout\,
	ena => \hAddr[23]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hAddr(20));

-- Location: LABCELL_X51_Y19_N33
\Add0~89\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~89_sumout\ = SUM(( hAddr(21) ) + ( GND ) + ( \Add0~86\ ))
-- \Add0~90\ = CARRY(( hAddr(21) ) + ( GND ) + ( \Add0~86\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_hAddr(21),
	cin => \Add0~86\,
	sumout => \Add0~89_sumout\,
	cout => \Add0~90\);

-- Location: FF_X51_Y19_N35
\hAddr[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Add0~89_sumout\,
	sclr => \hAddr[23]~0_combout\,
	ena => \hAddr[23]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hAddr(21));

-- Location: LABCELL_X51_Y19_N36
\Add0~93\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~93_sumout\ = SUM(( hAddr(22) ) + ( GND ) + ( \Add0~90\ ))
-- \Add0~94\ = CARRY(( hAddr(22) ) + ( GND ) + ( \Add0~90\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_hAddr(22),
	cin => \Add0~90\,
	sumout => \Add0~93_sumout\,
	cout => \Add0~94\);

-- Location: FF_X51_Y19_N38
\hAddr[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Add0~93_sumout\,
	sclr => \hAddr[23]~0_combout\,
	ena => \hAddr[23]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hAddr(22));

-- Location: LABCELL_X51_Y19_N39
\Add0~97\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~97_sumout\ = SUM(( hAddr(23) ) + ( GND ) + ( \Add0~94\ ))
-- \Add0~98\ = CARRY(( hAddr(23) ) + ( GND ) + ( \Add0~94\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_hAddr(23),
	cin => \Add0~94\,
	sumout => \Add0~97_sumout\,
	cout => \Add0~98\);

-- Location: FF_X51_Y19_N41
\hAddr[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Add0~97_sumout\,
	sclr => \hAddr[23]~0_combout\,
	ena => \hAddr[23]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hAddr(23));

-- Location: LABCELL_X51_Y19_N42
\Add0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~1_sumout\ = SUM(( hAddr(24) ) + ( GND ) + ( \Add0~98\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_hAddr(24),
	cin => \Add0~98\,
	sumout => \Add0~1_sumout\);

-- Location: FF_X51_Y19_N44
\hAddr[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Add0~1_sumout\,
	sclr => \hAddr[23]~0_combout\,
	ena => \hAddr[23]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hAddr(24));

-- Location: LABCELL_X50_Y20_N18
\Equal0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal0~2_combout\ = ( hAddr(2) & ( hAddr(23) & ( (hAddr(24) & (hAddr(20) & (hAddr(1) & hAddr(3)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(24),
	datab => ALT_INV_hAddr(20),
	datac => ALT_INV_hAddr(1),
	datad => ALT_INV_hAddr(3),
	datae => ALT_INV_hAddr(2),
	dataf => ALT_INV_hAddr(23),
	combout => \Equal0~2_combout\);

-- Location: MLABCELL_X52_Y20_N54
\Selector2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector2~0_combout\ = ( \Equal0~3_combout\ & ( \FSM_autotest.A_SET_WRITE~q\ & ( (!\Equal0~2_combout\) # ((!\Equal0~1_combout\) # ((!hAddr(0)) # (!\Equal0~0_combout\))) ) ) ) # ( !\Equal0~3_combout\ & ( \FSM_autotest.A_SET_WRITE~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal0~2_combout\,
	datab => \ALT_INV_Equal0~1_combout\,
	datac => ALT_INV_hAddr(0),
	datad => \ALT_INV_Equal0~0_combout\,
	datae => \ALT_INV_Equal0~3_combout\,
	dataf => \ALT_INV_FSM_autotest.A_SET_WRITE~q\,
	combout => \Selector2~0_combout\);

-- Location: FF_X52_Y20_N56
\FSM_autotest.A_WRITE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \FSM_autotest.A_WRITE~q\);

-- Location: MLABCELL_X47_Y21_N12
\Selector3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector3~0_combout\ = ( \FSM_autotest.A_WRITE~q\ ) # ( !\FSM_autotest.A_WRITE~q\ & ( (!\SDRAM1|u1|sDataDir_r~DUPLICATE_q\ & (!\SDRAM1|u1|rdPipeline_r\(0) & \FSM_autotest.A_WRITE_WAIT~q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011000000000000001100000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \SDRAM1|u1|ALT_INV_sDataDir_r~DUPLICATE_q\,
	datac => \SDRAM1|u1|ALT_INV_rdPipeline_r\(0),
	datad => \ALT_INV_FSM_autotest.A_WRITE_WAIT~q\,
	dataf => \ALT_INV_FSM_autotest.A_WRITE~q\,
	combout => \Selector3~0_combout\);

-- Location: FF_X47_Y21_N14
\FSM_autotest.A_WRITE_WAIT\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Selector3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \FSM_autotest.A_WRITE_WAIT~q\);

-- Location: MLABCELL_X47_Y21_N33
\Selector32~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector32~0_combout\ = ( \FSM_autotest.A_WRITE~q\ & ( (!\FSM_autotest.A_WRITE_WAIT~q\) # ((!\SDRAM1|u1|rdPipeline_r\(0) & (!\SDRAM1|u1|sDataDir_r~DUPLICATE_q\ & \wr~q\))) ) ) # ( !\FSM_autotest.A_WRITE~q\ & ( (\wr~q\ & ((!\FSM_autotest.A_WRITE_WAIT~q\) 
-- # ((!\SDRAM1|u1|rdPipeline_r\(0) & !\SDRAM1|u1|sDataDir_r~DUPLICATE_q\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111000000000001111100011110000111110001111000011111000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_rdPipeline_r\(0),
	datab => \SDRAM1|u1|ALT_INV_sDataDir_r~DUPLICATE_q\,
	datac => \ALT_INV_FSM_autotest.A_WRITE_WAIT~q\,
	datad => \ALT_INV_wr~q\,
	dataf => \ALT_INV_FSM_autotest.A_WRITE~q\,
	combout => \Selector32~0_combout\);

-- Location: FF_X47_Y21_N34
wr : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Selector32~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \wr~q\);

-- Location: FF_X47_Y21_N29
rd : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Selector37~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \rd~q\);

-- Location: LABCELL_X48_Y21_N9
\SDRAM1|u1|sAddr_x[10]~12\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|sAddr_x[10]~12_combout\ = ( \rd~q\ ) # ( !\rd~q\ & ( \wr~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_wr~q\,
	dataf => \ALT_INV_rd~q\,
	combout => \SDRAM1|u1|sAddr_x[10]~12_combout\);

-- Location: FF_X47_Y20_N2
\SDRAM1|u1|nopCntr_r[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|Add0~25_sumout\,
	sclr => \SDRAM1|u1|sAddr_x[10]~12_combout\,
	ena => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|nopCntr_r\(0));

-- Location: MLABCELL_X47_Y20_N3
\SDRAM1|u1|Add0~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add0~29_sumout\ = SUM(( \SDRAM1|u1|nopCntr_r\(1) ) + ( GND ) + ( \SDRAM1|u1|Add0~26\ ))
-- \SDRAM1|u1|Add0~30\ = CARRY(( \SDRAM1|u1|nopCntr_r\(1) ) + ( GND ) + ( \SDRAM1|u1|Add0~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \SDRAM1|u1|ALT_INV_nopCntr_r\(1),
	cin => \SDRAM1|u1|Add0~26\,
	sumout => \SDRAM1|u1|Add0~29_sumout\,
	cout => \SDRAM1|u1|Add0~30\);

-- Location: FF_X47_Y20_N5
\SDRAM1|u1|nopCntr_r[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|Add0~29_sumout\,
	sclr => \SDRAM1|u1|sAddr_x[10]~12_combout\,
	ena => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|nopCntr_r\(1));

-- Location: MLABCELL_X47_Y20_N6
\SDRAM1|u1|Add0~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add0~33_sumout\ = SUM(( \SDRAM1|u1|nopCntr_r\(2) ) + ( GND ) + ( \SDRAM1|u1|Add0~30\ ))
-- \SDRAM1|u1|Add0~34\ = CARRY(( \SDRAM1|u1|nopCntr_r\(2) ) + ( GND ) + ( \SDRAM1|u1|Add0~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \SDRAM1|u1|ALT_INV_nopCntr_r\(2),
	cin => \SDRAM1|u1|Add0~30\,
	sumout => \SDRAM1|u1|Add0~33_sumout\,
	cout => \SDRAM1|u1|Add0~34\);

-- Location: FF_X47_Y20_N8
\SDRAM1|u1|nopCntr_r[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|Add0~33_sumout\,
	sclr => \SDRAM1|u1|sAddr_x[10]~12_combout\,
	ena => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|nopCntr_r\(2));

-- Location: MLABCELL_X47_Y20_N9
\SDRAM1|u1|Add0~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add0~37_sumout\ = SUM(( \SDRAM1|u1|nopCntr_r\(3) ) + ( GND ) + ( \SDRAM1|u1|Add0~34\ ))
-- \SDRAM1|u1|Add0~38\ = CARRY(( \SDRAM1|u1|nopCntr_r\(3) ) + ( GND ) + ( \SDRAM1|u1|Add0~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \SDRAM1|u1|ALT_INV_nopCntr_r\(3),
	cin => \SDRAM1|u1|Add0~34\,
	sumout => \SDRAM1|u1|Add0~37_sumout\,
	cout => \SDRAM1|u1|Add0~38\);

-- Location: FF_X47_Y20_N10
\SDRAM1|u1|nopCntr_r[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|Add0~37_sumout\,
	sclr => \SDRAM1|u1|sAddr_x[10]~12_combout\,
	ena => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|nopCntr_r\(3));

-- Location: MLABCELL_X47_Y20_N12
\SDRAM1|u1|Add0~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add0~41_sumout\ = SUM(( \SDRAM1|u1|nopCntr_r\(4) ) + ( GND ) + ( \SDRAM1|u1|Add0~38\ ))
-- \SDRAM1|u1|Add0~42\ = CARRY(( \SDRAM1|u1|nopCntr_r\(4) ) + ( GND ) + ( \SDRAM1|u1|Add0~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \SDRAM1|u1|ALT_INV_nopCntr_r\(4),
	cin => \SDRAM1|u1|Add0~38\,
	sumout => \SDRAM1|u1|Add0~41_sumout\,
	cout => \SDRAM1|u1|Add0~42\);

-- Location: FF_X47_Y20_N14
\SDRAM1|u1|nopCntr_r[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|Add0~41_sumout\,
	sclr => \SDRAM1|u1|sAddr_x[10]~12_combout\,
	ena => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|nopCntr_r\(4));

-- Location: MLABCELL_X47_Y20_N15
\SDRAM1|u1|Add0~45\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add0~45_sumout\ = SUM(( \SDRAM1|u1|nopCntr_r\(5) ) + ( GND ) + ( \SDRAM1|u1|Add0~42\ ))
-- \SDRAM1|u1|Add0~46\ = CARRY(( \SDRAM1|u1|nopCntr_r\(5) ) + ( GND ) + ( \SDRAM1|u1|Add0~42\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \SDRAM1|u1|ALT_INV_nopCntr_r\(5),
	cin => \SDRAM1|u1|Add0~42\,
	sumout => \SDRAM1|u1|Add0~45_sumout\,
	cout => \SDRAM1|u1|Add0~46\);

-- Location: FF_X47_Y20_N17
\SDRAM1|u1|nopCntr_r[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|Add0~45_sumout\,
	sclr => \SDRAM1|u1|sAddr_x[10]~12_combout\,
	ena => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|nopCntr_r\(5));

-- Location: MLABCELL_X47_Y20_N18
\SDRAM1|u1|Add0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add0~1_sumout\ = SUM(( \SDRAM1|u1|nopCntr_r\(6) ) + ( GND ) + ( \SDRAM1|u1|Add0~46\ ))
-- \SDRAM1|u1|Add0~2\ = CARRY(( \SDRAM1|u1|nopCntr_r\(6) ) + ( GND ) + ( \SDRAM1|u1|Add0~46\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \SDRAM1|u1|ALT_INV_nopCntr_r\(6),
	cin => \SDRAM1|u1|Add0~46\,
	sumout => \SDRAM1|u1|Add0~1_sumout\,
	cout => \SDRAM1|u1|Add0~2\);

-- Location: FF_X47_Y20_N20
\SDRAM1|u1|nopCntr_r[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|Add0~1_sumout\,
	sclr => \SDRAM1|u1|sAddr_x[10]~12_combout\,
	ena => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|nopCntr_r\(6));

-- Location: MLABCELL_X47_Y20_N21
\SDRAM1|u1|Add0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add0~17_sumout\ = SUM(( \SDRAM1|u1|nopCntr_r\(7) ) + ( GND ) + ( \SDRAM1|u1|Add0~2\ ))
-- \SDRAM1|u1|Add0~18\ = CARRY(( \SDRAM1|u1|nopCntr_r\(7) ) + ( GND ) + ( \SDRAM1|u1|Add0~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \SDRAM1|u1|ALT_INV_nopCntr_r\(7),
	cin => \SDRAM1|u1|Add0~2\,
	sumout => \SDRAM1|u1|Add0~17_sumout\,
	cout => \SDRAM1|u1|Add0~18\);

-- Location: FF_X47_Y20_N23
\SDRAM1|u1|nopCntr_r[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|Add0~17_sumout\,
	sclr => \SDRAM1|u1|sAddr_x[10]~12_combout\,
	ena => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|nopCntr_r\(7));

-- Location: MLABCELL_X47_Y20_N24
\SDRAM1|u1|Add0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add0~5_sumout\ = SUM(( \SDRAM1|u1|nopCntr_r\(8) ) + ( GND ) + ( \SDRAM1|u1|Add0~18\ ))
-- \SDRAM1|u1|Add0~6\ = CARRY(( \SDRAM1|u1|nopCntr_r\(8) ) + ( GND ) + ( \SDRAM1|u1|Add0~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \SDRAM1|u1|ALT_INV_nopCntr_r\(8),
	cin => \SDRAM1|u1|Add0~18\,
	sumout => \SDRAM1|u1|Add0~5_sumout\,
	cout => \SDRAM1|u1|Add0~6\);

-- Location: FF_X47_Y20_N26
\SDRAM1|u1|nopCntr_r[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|Add0~5_sumout\,
	sclr => \SDRAM1|u1|sAddr_x[10]~12_combout\,
	ena => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|nopCntr_r\(8));

-- Location: MLABCELL_X47_Y20_N27
\SDRAM1|u1|Add0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add0~9_sumout\ = SUM(( \SDRAM1|u1|nopCntr_r\(9) ) + ( GND ) + ( \SDRAM1|u1|Add0~6\ ))
-- \SDRAM1|u1|Add0~10\ = CARRY(( \SDRAM1|u1|nopCntr_r\(9) ) + ( GND ) + ( \SDRAM1|u1|Add0~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \SDRAM1|u1|ALT_INV_nopCntr_r\(9),
	cin => \SDRAM1|u1|Add0~6\,
	sumout => \SDRAM1|u1|Add0~9_sumout\,
	cout => \SDRAM1|u1|Add0~10\);

-- Location: FF_X47_Y20_N28
\SDRAM1|u1|nopCntr_r[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|Add0~9_sumout\,
	sclr => \SDRAM1|u1|sAddr_x[10]~12_combout\,
	ena => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|nopCntr_r\(9));

-- Location: MLABCELL_X47_Y20_N30
\SDRAM1|u1|Add0~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add0~13_sumout\ = SUM(( \SDRAM1|u1|nopCntr_r\(10) ) + ( GND ) + ( \SDRAM1|u1|Add0~10\ ))
-- \SDRAM1|u1|Add0~14\ = CARRY(( \SDRAM1|u1|nopCntr_r\(10) ) + ( GND ) + ( \SDRAM1|u1|Add0~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \SDRAM1|u1|ALT_INV_nopCntr_r\(10),
	cin => \SDRAM1|u1|Add0~10\,
	sumout => \SDRAM1|u1|Add0~13_sumout\,
	cout => \SDRAM1|u1|Add0~14\);

-- Location: FF_X47_Y20_N32
\SDRAM1|u1|nopCntr_r[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|Add0~13_sumout\,
	sclr => \SDRAM1|u1|sAddr_x[10]~12_combout\,
	ena => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|nopCntr_r\(10));

-- Location: MLABCELL_X47_Y20_N33
\SDRAM1|u1|Add0~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add0~21_sumout\ = SUM(( \SDRAM1|u1|nopCntr_r\(11) ) + ( GND ) + ( \SDRAM1|u1|Add0~14\ ))
-- \SDRAM1|u1|Add0~22\ = CARRY(( \SDRAM1|u1|nopCntr_r\(11) ) + ( GND ) + ( \SDRAM1|u1|Add0~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \SDRAM1|u1|ALT_INV_nopCntr_r\(11),
	cin => \SDRAM1|u1|Add0~14\,
	sumout => \SDRAM1|u1|Add0~21_sumout\,
	cout => \SDRAM1|u1|Add0~22\);

-- Location: FF_X47_Y20_N35
\SDRAM1|u1|nopCntr_r[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|Add0~21_sumout\,
	sclr => \SDRAM1|u1|sAddr_x[10]~12_combout\,
	ena => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|nopCntr_r\(11));

-- Location: MLABCELL_X47_Y20_N48
\SDRAM1|u1|doSelfRfsh~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|doSelfRfsh~0_combout\ = ( !\SDRAM1|u1|nopCntr_r\(7) & ( !\SDRAM1|u1|nopCntr_r\(11) & ( (\SDRAM1|u1|nopCntr_r\(8) & (\SDRAM1|u1|nopCntr_r\(10) & (!\SDRAM1|u1|nopCntr_r\(6) & \SDRAM1|u1|nopCntr_r\(9)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000010000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_nopCntr_r\(8),
	datab => \SDRAM1|u1|ALT_INV_nopCntr_r\(10),
	datac => \SDRAM1|u1|ALT_INV_nopCntr_r\(6),
	datad => \SDRAM1|u1|ALT_INV_nopCntr_r\(9),
	datae => \SDRAM1|u1|ALT_INV_nopCntr_r\(7),
	dataf => \SDRAM1|u1|ALT_INV_nopCntr_r\(11),
	combout => \SDRAM1|u1|doSelfRfsh~0_combout\);

-- Location: MLABCELL_X47_Y20_N42
\SDRAM1|u1|doSelfRfsh~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|doSelfRfsh~1_combout\ = ( !\SDRAM1|u1|nopCntr_r\(0) & ( !\SDRAM1|u1|nopCntr_r\(2) & ( (!\SDRAM1|u1|nopCntr_r\(3) & (!\SDRAM1|u1|nopCntr_r\(5) & (!\SDRAM1|u1|nopCntr_r\(1) & \SDRAM1|u1|nopCntr_r\(4)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_nopCntr_r\(3),
	datab => \SDRAM1|u1|ALT_INV_nopCntr_r\(5),
	datac => \SDRAM1|u1|ALT_INV_nopCntr_r\(1),
	datad => \SDRAM1|u1|ALT_INV_nopCntr_r\(4),
	datae => \SDRAM1|u1|ALT_INV_nopCntr_r\(0),
	dataf => \SDRAM1|u1|ALT_INV_nopCntr_r\(2),
	combout => \SDRAM1|u1|doSelfRfsh~1_combout\);

-- Location: MLABCELL_X47_Y20_N36
\SDRAM1|u1|Add0~49\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add0~49_sumout\ = SUM(( \SDRAM1|u1|nopCntr_r\(12) ) + ( GND ) + ( \SDRAM1|u1|Add0~22\ ))
-- \SDRAM1|u1|Add0~50\ = CARRY(( \SDRAM1|u1|nopCntr_r\(12) ) + ( GND ) + ( \SDRAM1|u1|Add0~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \SDRAM1|u1|ALT_INV_nopCntr_r\(12),
	cin => \SDRAM1|u1|Add0~22\,
	sumout => \SDRAM1|u1|Add0~49_sumout\,
	cout => \SDRAM1|u1|Add0~50\);

-- Location: FF_X47_Y20_N38
\SDRAM1|u1|nopCntr_r[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|Add0~49_sumout\,
	sclr => \SDRAM1|u1|sAddr_x[10]~12_combout\,
	ena => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|nopCntr_r\(12));

-- Location: MLABCELL_X47_Y20_N39
\SDRAM1|u1|Add0~53\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add0~53_sumout\ = SUM(( \SDRAM1|u1|nopCntr_r\(13) ) + ( GND ) + ( \SDRAM1|u1|Add0~50\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \SDRAM1|u1|ALT_INV_nopCntr_r\(13),
	cin => \SDRAM1|u1|Add0~50\,
	sumout => \SDRAM1|u1|Add0~53_sumout\);

-- Location: FF_X47_Y20_N40
\SDRAM1|u1|nopCntr_r[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|Add0~53_sumout\,
	sclr => \SDRAM1|u1|sAddr_x[10]~12_combout\,
	ena => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|nopCntr_r\(13));

-- Location: MLABCELL_X47_Y21_N30
\SDRAM1|u1|doSelfRfsh~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|doSelfRfsh~2_combout\ = ( !\SDRAM1|u1|nopCntr_r\(12) & ( \SDRAM1|u1|nopCntr_r\(13) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \SDRAM1|u1|ALT_INV_nopCntr_r\(13),
	dataf => \SDRAM1|u1|ALT_INV_nopCntr_r\(12),
	combout => \SDRAM1|u1|doSelfRfsh~2_combout\);

-- Location: FF_X47_Y21_N35
\wr~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Selector32~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \wr~DUPLICATE_q\);

-- Location: MLABCELL_X47_Y21_N36
\SDRAM1|u1|doSelfRfsh~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|doSelfRfsh~3_combout\ = ( !\wr~DUPLICATE_q\ & ( (\SDRAM1|u1|doSelfRfsh~0_combout\ & (\SDRAM1|u1|doSelfRfsh~1_combout\ & (\SDRAM1|u1|doSelfRfsh~2_combout\ & !\rd~q\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000100000000000000010000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_doSelfRfsh~0_combout\,
	datab => \SDRAM1|u1|ALT_INV_doSelfRfsh~1_combout\,
	datac => \SDRAM1|u1|ALT_INV_doSelfRfsh~2_combout\,
	datad => \ALT_INV_rd~q\,
	dataf => \ALT_INV_wr~DUPLICATE_q\,
	combout => \SDRAM1|u1|doSelfRfsh~3_combout\);

-- Location: MLABCELL_X47_Y21_N54
\SDRAM1|u1|Selector31~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Selector31~0_combout\ = ( \SDRAM1|u1|state_r.SELFREFRESH~q\ & ( !\rd~DUPLICATE_q\ & ( (\SDRAM1|u1|doSelfRfsh~0_combout\ & (\SDRAM1|u1|doSelfRfsh~1_combout\ & (!\wr~DUPLICATE_q\ & \SDRAM1|u1|doSelfRfsh~2_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000001000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_doSelfRfsh~0_combout\,
	datab => \SDRAM1|u1|ALT_INV_doSelfRfsh~1_combout\,
	datac => \ALT_INV_wr~DUPLICATE_q\,
	datad => \SDRAM1|u1|ALT_INV_doSelfRfsh~2_combout\,
	datae => \SDRAM1|u1|ALT_INV_state_r.SELFREFRESH~q\,
	dataf => \ALT_INV_rd~DUPLICATE_q\,
	combout => \SDRAM1|u1|Selector31~0_combout\);

-- Location: FF_X50_Y21_N23
\SDRAM1|u1|state_r.INITSETMODE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => \SDRAM1|u1|state_r.INITRFSH~q\,
	sload => VCC,
	ena => \SDRAM1|u1|state_x.INITSETMODE~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|state_r.INITSETMODE~q\);

-- Location: MLABCELL_X52_Y21_N42
\SDRAM1|u1|rasTimer_r[2]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|rasTimer_r[2]~5_combout\ = ( \SDRAM1|u1|Equal7~0_combout\ & ( (\SDRAM1|u1|Equal7~1_combout\ & (\SDRAM1|u1|Equal7~2_combout\ & \SDRAM1|u1|state_r.ACTIVATE~q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000001010000000000000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal7~1_combout\,
	datac => \SDRAM1|u1|ALT_INV_Equal7~2_combout\,
	datad => \SDRAM1|u1|ALT_INV_state_r.ACTIVATE~q\,
	dataf => \SDRAM1|u1|ALT_INV_Equal7~0_combout\,
	combout => \SDRAM1|u1|rasTimer_r[2]~5_combout\);

-- Location: FF_X50_Y19_N2
\SDRAM1|u1|activeRow_r[0][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => hAddr(13),
	sload => VCC,
	ena => \SDRAM1|u1|rasTimer_r[2]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|activeRow_r[0][3]~q\);

-- Location: FF_X51_Y21_N17
\SDRAM1|u1|activeRow_r[0][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => hAddr(14),
	sload => VCC,
	ena => \SDRAM1|u1|rasTimer_r[2]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|activeRow_r[0][4]~q\);

-- Location: FF_X51_Y21_N19
\SDRAM1|u1|activeRow_r[0][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => hAddr(15),
	sload => VCC,
	ena => \SDRAM1|u1|rasTimer_r[2]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|activeRow_r[0][5]~q\);

-- Location: LABCELL_X51_Y21_N18
\SDRAM1|u1|doActivate~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|doActivate~2_combout\ = ( hAddr(15) & ( hAddr(14) & ( (\SDRAM1|u1|activeRow_r[0][4]~q\ & (\SDRAM1|u1|activeRow_r[0][5]~q\ & (!\SDRAM1|u1|activeRow_r[0][3]~q\ $ (hAddr(13))))) ) ) ) # ( !hAddr(15) & ( hAddr(14) & ( 
-- (\SDRAM1|u1|activeRow_r[0][4]~q\ & (!\SDRAM1|u1|activeRow_r[0][5]~q\ & (!\SDRAM1|u1|activeRow_r[0][3]~q\ $ (hAddr(13))))) ) ) ) # ( hAddr(15) & ( !hAddr(14) & ( (!\SDRAM1|u1|activeRow_r[0][4]~q\ & (\SDRAM1|u1|activeRow_r[0][5]~q\ & 
-- (!\SDRAM1|u1|activeRow_r[0][3]~q\ $ (hAddr(13))))) ) ) ) # ( !hAddr(15) & ( !hAddr(14) & ( (!\SDRAM1|u1|activeRow_r[0][4]~q\ & (!\SDRAM1|u1|activeRow_r[0][5]~q\ & (!\SDRAM1|u1|activeRow_r[0][3]~q\ $ (hAddr(13))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000010000000000000000001000010000100001000000000000000000100001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_activeRow_r[0][3]~q\,
	datab => \SDRAM1|u1|ALT_INV_activeRow_r[0][4]~q\,
	datac => ALT_INV_hAddr(13),
	datad => \SDRAM1|u1|ALT_INV_activeRow_r[0][5]~q\,
	datae => ALT_INV_hAddr(15),
	dataf => ALT_INV_hAddr(14),
	combout => \SDRAM1|u1|doActivate~2_combout\);

-- Location: FF_X50_Y19_N56
\SDRAM1|u1|activeRow_r[0][8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => hAddr(18),
	sload => VCC,
	ena => \SDRAM1|u1|rasTimer_r[2]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|activeRow_r[0][8]~q\);

-- Location: LABCELL_X50_Y19_N18
\SDRAM1|u1|activeRow_r[0][6]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|activeRow_r[0][6]~feeder_combout\ = hAddr(16)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_hAddr(16),
	combout => \SDRAM1|u1|activeRow_r[0][6]~feeder_combout\);

-- Location: FF_X50_Y19_N20
\SDRAM1|u1|activeRow_r[0][6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|activeRow_r[0][6]~feeder_combout\,
	ena => \SDRAM1|u1|rasTimer_r[2]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|activeRow_r[0][6]~q\);

-- Location: FF_X50_Y19_N47
\SDRAM1|u1|activeRow_r[0][7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => hAddr(17),
	sload => VCC,
	ena => \SDRAM1|u1|rasTimer_r[2]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|activeRow_r[0][7]~q\);

-- Location: LABCELL_X50_Y19_N54
\SDRAM1|u1|doActivate~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|doActivate~1_combout\ = ( hAddr(17) & ( hAddr(16) & ( (\SDRAM1|u1|activeRow_r[0][6]~q\ & (\SDRAM1|u1|activeRow_r[0][7]~q\ & (!\SDRAM1|u1|activeRow_r[0][8]~q\ $ (hAddr(18))))) ) ) ) # ( !hAddr(17) & ( hAddr(16) & ( 
-- (\SDRAM1|u1|activeRow_r[0][6]~q\ & (!\SDRAM1|u1|activeRow_r[0][7]~q\ & (!\SDRAM1|u1|activeRow_r[0][8]~q\ $ (hAddr(18))))) ) ) ) # ( hAddr(17) & ( !hAddr(16) & ( (!\SDRAM1|u1|activeRow_r[0][6]~q\ & (\SDRAM1|u1|activeRow_r[0][7]~q\ & 
-- (!\SDRAM1|u1|activeRow_r[0][8]~q\ $ (hAddr(18))))) ) ) ) # ( !hAddr(17) & ( !hAddr(16) & ( (!\SDRAM1|u1|activeRow_r[0][6]~q\ & (!\SDRAM1|u1|activeRow_r[0][7]~q\ & (!\SDRAM1|u1|activeRow_r[0][8]~q\ $ (hAddr(18))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1001000000000000000000001001000000001001000000000000000000001001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_activeRow_r[0][8]~q\,
	datab => ALT_INV_hAddr(18),
	datac => \SDRAM1|u1|ALT_INV_activeRow_r[0][6]~q\,
	datad => \SDRAM1|u1|ALT_INV_activeRow_r[0][7]~q\,
	datae => ALT_INV_hAddr(17),
	dataf => ALT_INV_hAddr(16),
	combout => \SDRAM1|u1|doActivate~1_combout\);

-- Location: FF_X52_Y21_N11
\SDRAM1|u1|activeRow_r[0][12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => hAddr(22),
	sload => VCC,
	ena => \SDRAM1|u1|rasTimer_r[2]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|activeRow_r[0][12]~q\);

-- Location: MLABCELL_X52_Y21_N9
\SDRAM1|u1|Equal2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Equal2~0_combout\ = ( \SDRAM1|u1|activeRow_r[0][12]~q\ & ( !hAddr(22) ) ) # ( !\SDRAM1|u1|activeRow_r[0][12]~q\ & ( hAddr(22) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010110101010101010101010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(22),
	dataf => \SDRAM1|u1|ALT_INV_activeRow_r[0][12]~q\,
	combout => \SDRAM1|u1|Equal2~0_combout\);

-- Location: FF_X47_Y20_N53
\SDRAM1|u1|activeBank_r[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => hAddr(24),
	sload => VCC,
	ena => \SDRAM1|u1|rasTimer_r[2]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|activeBank_r\(1));

-- Location: FF_X52_Y21_N14
\SDRAM1|u1|activeBank_r[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => hAddr(23),
	sload => VCC,
	ena => \SDRAM1|u1|rasTimer_r[2]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|activeBank_r\(0));

-- Location: MLABCELL_X52_Y21_N27
\SDRAM1|u1|Selector59~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Selector59~0_combout\ = (!\SDRAM1|u1|state_r.SELFREFRESH~q\ & (\SDRAM1|u1|state_r.ACTIVATE~q\ & !\SDRAM1|u1|state_r.RW~q\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101000000000000010100000000000001010000000000000101000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_state_r.SELFREFRESH~q\,
	datac => \SDRAM1|u1|ALT_INV_state_r.ACTIVATE~q\,
	datad => \SDRAM1|u1|ALT_INV_state_r.RW~q\,
	combout => \SDRAM1|u1|Selector59~0_combout\);

-- Location: MLABCELL_X47_Y21_N15
\SDRAM1|u1|doSelfRfsh~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|doSelfRfsh~4_combout\ = ( !\wr~DUPLICATE_q\ & ( (\SDRAM1|u1|nopCntr_r\(13) & (!\rd~DUPLICATE_q\ & !\SDRAM1|u1|nopCntr_r\(12))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000000000000010100000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_nopCntr_r\(13),
	datac => \ALT_INV_rd~DUPLICATE_q\,
	datad => \SDRAM1|u1|ALT_INV_nopCntr_r\(12),
	dataf => \ALT_INV_wr~DUPLICATE_q\,
	combout => \SDRAM1|u1|doSelfRfsh~4_combout\);

-- Location: MLABCELL_X52_Y21_N24
\SDRAM1|u1|Selector59~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Selector59~1_combout\ = ( \SDRAM1|u1|activeFlag_r\(0) & ( (!\SDRAM1|u1|state_r.SELFREFRESH~q\) # ((\SDRAM1|u1|doSelfRfsh~4_combout\ & (\SDRAM1|u1|doSelfRfsh~0_combout\ & \SDRAM1|u1|doSelfRfsh~1_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010101010101010111010101010101011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_state_r.SELFREFRESH~q\,
	datab => \SDRAM1|u1|ALT_INV_doSelfRfsh~4_combout\,
	datac => \SDRAM1|u1|ALT_INV_doSelfRfsh~0_combout\,
	datad => \SDRAM1|u1|ALT_INV_doSelfRfsh~1_combout\,
	dataf => \SDRAM1|u1|ALT_INV_activeFlag_r\(0),
	combout => \SDRAM1|u1|Selector59~1_combout\);

-- Location: LABCELL_X48_Y21_N45
\SDRAM1|u1|wrTimer_r[1]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|wrTimer_r[1]~1_combout\ = (!\rd~DUPLICATE_q\ & \wr~q\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010001000100010001000100010001000100010001000100010001000100010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rd~DUPLICATE_q\,
	datab => \ALT_INV_wr~q\,
	combout => \SDRAM1|u1|wrTimer_r[1]~1_combout\);

-- Location: LABCELL_X50_Y21_N21
\SDRAM1|u1|WideOr18\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|WideOr18~combout\ = ( \SDRAM1|u1|state_r.ACTIVATE~q\ ) # ( !\SDRAM1|u1|state_r.ACTIVATE~q\ & ( ((!\SDRAM1|u1|state_r.INITWAIT~q\) # (\SDRAM1|u1|state_r.INITSETMODE~q\)) # (\SDRAM1|u1|state_r.RW~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111010111111111111101011111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_state_r.RW~q\,
	datac => \SDRAM1|u1|ALT_INV_state_r.INITWAIT~q\,
	datad => \SDRAM1|u1|ALT_INV_state_r.INITSETMODE~q\,
	dataf => \SDRAM1|u1|ALT_INV_state_r.ACTIVATE~q\,
	combout => \SDRAM1|u1|WideOr18~combout\);

-- Location: LABCELL_X46_Y20_N0
\SDRAM1|u1|Add4~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add4~1_sumout\ = SUM(( \SDRAM1|u1|rfshCntr_r\(0) ) + ( VCC ) + ( !VCC ))
-- \SDRAM1|u1|Add4~2\ = CARRY(( \SDRAM1|u1|rfshCntr_r\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \SDRAM1|u1|ALT_INV_rfshCntr_r\(0),
	cin => GND,
	sumout => \SDRAM1|u1|Add4~1_sumout\,
	cout => \SDRAM1|u1|Add4~2\);

-- Location: LABCELL_X46_Y20_N3
\SDRAM1|u1|Add4~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add4~21_sumout\ = SUM(( \SDRAM1|u1|rfshCntr_r\(1) ) + ( (\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|WideOr18~combout\ & ((!\SDRAM1|u1|state_r.SELFREFRESH~q\) # (!\SDRAM1|u1|doSelfRfsh~3_combout\)))) ) + ( \SDRAM1|u1|Add4~2\ ))
-- \SDRAM1|u1|Add4~22\ = CARRY(( \SDRAM1|u1|rfshCntr_r\(1) ) + ( (\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|WideOr18~combout\ & ((!\SDRAM1|u1|state_r.SELFREFRESH~q\) # (!\SDRAM1|u1|doSelfRfsh~3_combout\)))) ) + ( \SDRAM1|u1|Add4~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datab => \SDRAM1|u1|ALT_INV_state_r.SELFREFRESH~q\,
	datac => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	datad => \SDRAM1|u1|ALT_INV_rfshCntr_r\(1),
	dataf => \SDRAM1|u1|ALT_INV_WideOr18~combout\,
	cin => \SDRAM1|u1|Add4~2\,
	sumout => \SDRAM1|u1|Add4~21_sumout\,
	cout => \SDRAM1|u1|Add4~22\);

-- Location: LABCELL_X45_Y20_N18
\SDRAM1|u1|rfshCntr_r[4]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|rfshCntr_r[4]~3_combout\ = ( \SDRAM1|u1|state_r.INITPCHG~q\ & ( !\SDRAM1|u1|WideOr18~combout\ & ( (!\SDRAM1|u1|Selector31~0_combout\ & (\SDRAM1|u1|Equal7~2_combout\ & (\SDRAM1|u1|Equal7~0_combout\ & \SDRAM1|u1|Equal7~1_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000001000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Selector31~0_combout\,
	datab => \SDRAM1|u1|ALT_INV_Equal7~2_combout\,
	datac => \SDRAM1|u1|ALT_INV_Equal7~0_combout\,
	datad => \SDRAM1|u1|ALT_INV_Equal7~1_combout\,
	datae => \SDRAM1|u1|ALT_INV_state_r.INITPCHG~q\,
	dataf => \SDRAM1|u1|ALT_INV_WideOr18~combout\,
	combout => \SDRAM1|u1|rfshCntr_r[4]~3_combout\);

-- Location: LABCELL_X45_Y20_N48
\SDRAM1|u1|rfshCntr_x[1]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|rfshCntr_x[1]~5_combout\ = ( !\SDRAM1|u1|rfshCntr_r[4]~3_combout\ & ( \SDRAM1|u1|Add4~21_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \SDRAM1|u1|ALT_INV_Add4~21_sumout\,
	dataf => \SDRAM1|u1|ALT_INV_rfshCntr_r[4]~3_combout\,
	combout => \SDRAM1|u1|rfshCntr_x[1]~5_combout\);

-- Location: LABCELL_X46_Y20_N51
\SDRAM1|u1|rfshCntr_r[4]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|rfshCntr_r[4]~1_combout\ = ( \SDRAM1|u1|Equal7~3_combout\ & ( (!\SDRAM1|u1|WideOr18~combout\ & (!\SDRAM1|u1|doSelfRfsh~3_combout\ & \SDRAM1|u1|state_r.SELFREFRESH~q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000101000000000000010100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_WideOr18~combout\,
	datac => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	datad => \SDRAM1|u1|ALT_INV_state_r.SELFREFRESH~q\,
	dataf => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	combout => \SDRAM1|u1|rfshCntr_r[4]~1_combout\);

-- Location: FF_X43_Y20_N31
\SDRAM1|u1|refTimer_r[5]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|refTimer_x[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|refTimer_r[5]~DUPLICATE_q\);

-- Location: LABCELL_X43_Y20_N0
\SDRAM1|u1|Add3~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add3~37_sumout\ = SUM(( \SDRAM1|u1|refTimer_r\(0) ) + ( VCC ) + ( !VCC ))
-- \SDRAM1|u1|Add3~38\ = CARRY(( \SDRAM1|u1|refTimer_r\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \SDRAM1|u1|ALT_INV_refTimer_r\(0),
	cin => GND,
	sumout => \SDRAM1|u1|Add3~37_sumout\,
	cout => \SDRAM1|u1|Add3~38\);

-- Location: LABCELL_X43_Y20_N3
\SDRAM1|u1|Add3~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add3~33_sumout\ = SUM(( \SDRAM1|u1|refTimer_r\(1) ) + ( VCC ) + ( \SDRAM1|u1|Add3~38\ ))
-- \SDRAM1|u1|Add3~34\ = CARRY(( \SDRAM1|u1|refTimer_r\(1) ) + ( VCC ) + ( \SDRAM1|u1|Add3~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \SDRAM1|u1|ALT_INV_refTimer_r\(1),
	cin => \SDRAM1|u1|Add3~38\,
	sumout => \SDRAM1|u1|Add3~33_sumout\,
	cout => \SDRAM1|u1|Add3~34\);

-- Location: FF_X43_Y20_N4
\SDRAM1|u1|refTimer_r[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|Add3~33_sumout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|refTimer_r\(1));

-- Location: LABCELL_X43_Y20_N6
\SDRAM1|u1|Add3~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add3~29_sumout\ = SUM(( \SDRAM1|u1|refTimer_r\(2) ) + ( VCC ) + ( \SDRAM1|u1|Add3~34\ ))
-- \SDRAM1|u1|Add3~30\ = CARRY(( \SDRAM1|u1|refTimer_r\(2) ) + ( VCC ) + ( \SDRAM1|u1|Add3~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \SDRAM1|u1|ALT_INV_refTimer_r\(2),
	cin => \SDRAM1|u1|Add3~34\,
	sumout => \SDRAM1|u1|Add3~29_sumout\,
	cout => \SDRAM1|u1|Add3~30\);

-- Location: FF_X43_Y20_N8
\SDRAM1|u1|refTimer_r[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|Add3~29_sumout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|refTimer_r\(2));

-- Location: LABCELL_X43_Y20_N9
\SDRAM1|u1|Add3~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add3~25_sumout\ = SUM(( \SDRAM1|u1|refTimer_r\(3) ) + ( VCC ) + ( \SDRAM1|u1|Add3~30\ ))
-- \SDRAM1|u1|Add3~26\ = CARRY(( \SDRAM1|u1|refTimer_r\(3) ) + ( VCC ) + ( \SDRAM1|u1|Add3~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \SDRAM1|u1|ALT_INV_refTimer_r\(3),
	cin => \SDRAM1|u1|Add3~30\,
	sumout => \SDRAM1|u1|Add3~25_sumout\,
	cout => \SDRAM1|u1|Add3~26\);

-- Location: FF_X43_Y20_N10
\SDRAM1|u1|refTimer_r[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|Add3~25_sumout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|refTimer_r\(3));

-- Location: LABCELL_X43_Y20_N12
\SDRAM1|u1|Add3~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add3~5_sumout\ = SUM(( \SDRAM1|u1|refTimer_r\(4) ) + ( VCC ) + ( \SDRAM1|u1|Add3~26\ ))
-- \SDRAM1|u1|Add3~6\ = CARRY(( \SDRAM1|u1|refTimer_r\(4) ) + ( VCC ) + ( \SDRAM1|u1|Add3~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \SDRAM1|u1|ALT_INV_refTimer_r\(4),
	cin => \SDRAM1|u1|Add3~26\,
	sumout => \SDRAM1|u1|Add3~5_sumout\,
	cout => \SDRAM1|u1|Add3~6\);

-- Location: LABCELL_X43_Y20_N33
\SDRAM1|u1|refTimer_x[4]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|refTimer_x[4]~1_combout\ = ( \SDRAM1|u1|refTimer_r[5]~DUPLICATE_q\ & ( \SDRAM1|u1|Add3~5_sumout\ ) ) # ( !\SDRAM1|u1|refTimer_r[5]~DUPLICATE_q\ & ( (\SDRAM1|u1|Add3~5_sumout\ & ((!\SDRAM1|u1|Equal6~1_combout\) # ((!\SDRAM1|u1|Equal6~0_combout\) 
-- # (\SDRAM1|u1|refTimer_r\(4))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111000001111000011100000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal6~1_combout\,
	datab => \SDRAM1|u1|ALT_INV_Equal6~0_combout\,
	datac => \SDRAM1|u1|ALT_INV_Add3~5_sumout\,
	datad => \SDRAM1|u1|ALT_INV_refTimer_r\(4),
	dataf => \SDRAM1|u1|ALT_INV_refTimer_r[5]~DUPLICATE_q\,
	combout => \SDRAM1|u1|refTimer_x[4]~1_combout\);

-- Location: FF_X43_Y20_N35
\SDRAM1|u1|refTimer_r[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|refTimer_x[4]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|refTimer_r\(4));

-- Location: LABCELL_X43_Y20_N15
\SDRAM1|u1|Add3~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add3~1_sumout\ = SUM(( \SDRAM1|u1|refTimer_r[5]~DUPLICATE_q\ ) + ( VCC ) + ( \SDRAM1|u1|Add3~6\ ))
-- \SDRAM1|u1|Add3~2\ = CARRY(( \SDRAM1|u1|refTimer_r[5]~DUPLICATE_q\ ) + ( VCC ) + ( \SDRAM1|u1|Add3~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_refTimer_r[5]~DUPLICATE_q\,
	cin => \SDRAM1|u1|Add3~6\,
	sumout => \SDRAM1|u1|Add3~1_sumout\,
	cout => \SDRAM1|u1|Add3~2\);

-- Location: LABCELL_X43_Y20_N18
\SDRAM1|u1|Add3~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add3~21_sumout\ = SUM(( \SDRAM1|u1|refTimer_r\(6) ) + ( VCC ) + ( \SDRAM1|u1|Add3~2\ ))
-- \SDRAM1|u1|Add3~22\ = CARRY(( \SDRAM1|u1|refTimer_r\(6) ) + ( VCC ) + ( \SDRAM1|u1|Add3~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \SDRAM1|u1|ALT_INV_refTimer_r\(6),
	cin => \SDRAM1|u1|Add3~2\,
	sumout => \SDRAM1|u1|Add3~21_sumout\,
	cout => \SDRAM1|u1|Add3~22\);

-- Location: LABCELL_X43_Y20_N45
\SDRAM1|u1|refTimer_x[6]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|refTimer_x[6]~3_combout\ = ( \SDRAM1|u1|refTimer_r\(4) & ( \SDRAM1|u1|Add3~21_sumout\ ) ) # ( !\SDRAM1|u1|refTimer_r\(4) & ( (\SDRAM1|u1|Add3~21_sumout\ & ((!\SDRAM1|u1|Equal6~1_combout\) # ((!\SDRAM1|u1|Equal6~0_combout\) # 
-- (\SDRAM1|u1|refTimer_r\(5))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111011000000001111101100000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal6~1_combout\,
	datab => \SDRAM1|u1|ALT_INV_refTimer_r\(5),
	datac => \SDRAM1|u1|ALT_INV_Equal6~0_combout\,
	datad => \SDRAM1|u1|ALT_INV_Add3~21_sumout\,
	dataf => \SDRAM1|u1|ALT_INV_refTimer_r\(4),
	combout => \SDRAM1|u1|refTimer_x[6]~3_combout\);

-- Location: FF_X43_Y20_N47
\SDRAM1|u1|refTimer_r[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|refTimer_x[6]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|refTimer_r\(6));

-- Location: LABCELL_X43_Y20_N21
\SDRAM1|u1|Add3~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add3~17_sumout\ = SUM(( \SDRAM1|u1|refTimer_r[7]~DUPLICATE_q\ ) + ( VCC ) + ( \SDRAM1|u1|Add3~22\ ))
-- \SDRAM1|u1|Add3~18\ = CARRY(( \SDRAM1|u1|refTimer_r[7]~DUPLICATE_q\ ) + ( VCC ) + ( \SDRAM1|u1|Add3~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_refTimer_r[7]~DUPLICATE_q\,
	cin => \SDRAM1|u1|Add3~22\,
	sumout => \SDRAM1|u1|Add3~17_sumout\,
	cout => \SDRAM1|u1|Add3~18\);

-- Location: LABCELL_X43_Y20_N36
\SDRAM1|u1|refTimer_x[7]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|refTimer_x[7]~2_combout\ = ( \SDRAM1|u1|refTimer_r\(4) & ( \SDRAM1|u1|Add3~17_sumout\ ) ) # ( !\SDRAM1|u1|refTimer_r\(4) & ( (\SDRAM1|u1|Add3~17_sumout\ & ((!\SDRAM1|u1|Equal6~1_combout\) # ((!\SDRAM1|u1|Equal6~0_combout\) # 
-- (\SDRAM1|u1|refTimer_r\(5))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001011000011110000101100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal6~1_combout\,
	datab => \SDRAM1|u1|ALT_INV_refTimer_r\(5),
	datac => \SDRAM1|u1|ALT_INV_Add3~17_sumout\,
	datad => \SDRAM1|u1|ALT_INV_Equal6~0_combout\,
	dataf => \SDRAM1|u1|ALT_INV_refTimer_r\(4),
	combout => \SDRAM1|u1|refTimer_x[7]~2_combout\);

-- Location: FF_X43_Y20_N38
\SDRAM1|u1|refTimer_r[7]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|refTimer_x[7]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|refTimer_r[7]~DUPLICATE_q\);

-- Location: LABCELL_X43_Y20_N24
\SDRAM1|u1|Add3~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add3~13_sumout\ = SUM(( \SDRAM1|u1|refTimer_r\(8) ) + ( VCC ) + ( \SDRAM1|u1|Add3~18\ ))
-- \SDRAM1|u1|Add3~14\ = CARRY(( \SDRAM1|u1|refTimer_r\(8) ) + ( VCC ) + ( \SDRAM1|u1|Add3~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \SDRAM1|u1|ALT_INV_refTimer_r\(8),
	cin => \SDRAM1|u1|Add3~18\,
	sumout => \SDRAM1|u1|Add3~13_sumout\,
	cout => \SDRAM1|u1|Add3~14\);

-- Location: FF_X43_Y20_N26
\SDRAM1|u1|refTimer_r[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|Add3~13_sumout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|refTimer_r\(8));

-- Location: FF_X43_Y20_N37
\SDRAM1|u1|refTimer_r[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|refTimer_x[7]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|refTimer_r\(7));

-- Location: LABCELL_X43_Y20_N27
\SDRAM1|u1|Add3~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add3~9_sumout\ = SUM(( \SDRAM1|u1|refTimer_r\(9) ) + ( VCC ) + ( \SDRAM1|u1|Add3~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \SDRAM1|u1|ALT_INV_refTimer_r\(9),
	cin => \SDRAM1|u1|Add3~14\,
	sumout => \SDRAM1|u1|Add3~9_sumout\);

-- Location: FF_X43_Y20_N29
\SDRAM1|u1|refTimer_r[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|Add3~9_sumout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|refTimer_r\(9));

-- Location: LABCELL_X43_Y20_N51
\SDRAM1|u1|Equal6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Equal6~0_combout\ = ( !\SDRAM1|u1|refTimer_r\(9) & ( (!\SDRAM1|u1|refTimer_r\(8) & (!\SDRAM1|u1|refTimer_r\(6) & !\SDRAM1|u1|refTimer_r\(7))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000000000000101000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_refTimer_r\(8),
	datac => \SDRAM1|u1|ALT_INV_refTimer_r\(6),
	datad => \SDRAM1|u1|ALT_INV_refTimer_r\(7),
	dataf => \SDRAM1|u1|ALT_INV_refTimer_r\(9),
	combout => \SDRAM1|u1|Equal6~0_combout\);

-- Location: LABCELL_X43_Y20_N30
\SDRAM1|u1|refTimer_x[5]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|refTimer_x[5]~0_combout\ = ( \SDRAM1|u1|refTimer_r\(4) & ( \SDRAM1|u1|Add3~1_sumout\ ) ) # ( !\SDRAM1|u1|refTimer_r\(4) & ( (\SDRAM1|u1|Add3~1_sumout\ & ((!\SDRAM1|u1|Equal6~1_combout\) # ((!\SDRAM1|u1|Equal6~0_combout\) # 
-- (\SDRAM1|u1|refTimer_r\(5))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111000001111000011100000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal6~1_combout\,
	datab => \SDRAM1|u1|ALT_INV_Equal6~0_combout\,
	datac => \SDRAM1|u1|ALT_INV_Add3~1_sumout\,
	datad => \SDRAM1|u1|ALT_INV_refTimer_r\(5),
	dataf => \SDRAM1|u1|ALT_INV_refTimer_r\(4),
	combout => \SDRAM1|u1|refTimer_x[5]~0_combout\);

-- Location: FF_X43_Y20_N32
\SDRAM1|u1|refTimer_r[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|refTimer_x[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|refTimer_r\(5));

-- Location: LABCELL_X43_Y20_N42
\SDRAM1|u1|refTimer_x[0]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|refTimer_x[0]~4_combout\ = ( \SDRAM1|u1|refTimer_r\(4) & ( \SDRAM1|u1|Add3~37_sumout\ ) ) # ( !\SDRAM1|u1|refTimer_r\(4) & ( (\SDRAM1|u1|Add3~37_sumout\ & ((!\SDRAM1|u1|Equal6~1_combout\) # ((!\SDRAM1|u1|Equal6~0_combout\) # 
-- (\SDRAM1|u1|refTimer_r\(5))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001011000011110000101100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal6~1_combout\,
	datab => \SDRAM1|u1|ALT_INV_refTimer_r\(5),
	datac => \SDRAM1|u1|ALT_INV_Add3~37_sumout\,
	datad => \SDRAM1|u1|ALT_INV_Equal6~0_combout\,
	dataf => \SDRAM1|u1|ALT_INV_refTimer_r\(4),
	combout => \SDRAM1|u1|refTimer_x[0]~4_combout\);

-- Location: FF_X43_Y20_N44
\SDRAM1|u1|refTimer_r[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|refTimer_x[0]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|refTimer_r\(0));

-- Location: LABCELL_X43_Y20_N48
\SDRAM1|u1|Equal6~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Equal6~1_combout\ = ( !\SDRAM1|u1|refTimer_r\(2) & ( (!\SDRAM1|u1|refTimer_r\(0) & (!\SDRAM1|u1|refTimer_r\(1) & !\SDRAM1|u1|refTimer_r\(3))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100000000000000110000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \SDRAM1|u1|ALT_INV_refTimer_r\(0),
	datac => \SDRAM1|u1|ALT_INV_refTimer_r\(1),
	datad => \SDRAM1|u1|ALT_INV_refTimer_r\(3),
	dataf => \SDRAM1|u1|ALT_INV_refTimer_r\(2),
	combout => \SDRAM1|u1|Equal6~1_combout\);

-- Location: LABCELL_X43_Y20_N39
\SDRAM1|u1|Equal6~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Equal6~2_combout\ = ( !\SDRAM1|u1|refTimer_r\(4) & ( (\SDRAM1|u1|Equal6~1_combout\ & (!\SDRAM1|u1|refTimer_r\(5) & \SDRAM1|u1|Equal6~0_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010000000000000101000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal6~1_combout\,
	datac => \SDRAM1|u1|ALT_INV_refTimer_r\(5),
	datad => \SDRAM1|u1|ALT_INV_Equal6~0_combout\,
	dataf => \SDRAM1|u1|ALT_INV_refTimer_r\(4),
	combout => \SDRAM1|u1|Equal6~2_combout\);

-- Location: LABCELL_X45_Y20_N54
\SDRAM1|u1|rfshCntr_r[4]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|rfshCntr_r[4]~2_combout\ = ((\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|WideOr18~combout\ & !\SDRAM1|u1|Selector31~0_combout\))) # (\SDRAM1|u1|Equal6~2_combout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100111100001111010011110000111101001111000011110100111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datab => \SDRAM1|u1|ALT_INV_WideOr18~combout\,
	datac => \SDRAM1|u1|ALT_INV_Equal6~2_combout\,
	datad => \SDRAM1|u1|ALT_INV_Selector31~0_combout\,
	combout => \SDRAM1|u1|rfshCntr_r[4]~2_combout\);

-- Location: FF_X45_Y20_N49
\SDRAM1|u1|rfshCntr_r[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|rfshCntr_x[1]~5_combout\,
	sclr => \SDRAM1|u1|rfshCntr_r[4]~1_combout\,
	ena => \SDRAM1|u1|rfshCntr_r[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rfshCntr_r\(1));

-- Location: LABCELL_X46_Y20_N6
\SDRAM1|u1|Add4~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add4~17_sumout\ = SUM(( (\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|WideOr18~combout\ & ((!\SDRAM1|u1|doSelfRfsh~3_combout\) # (!\SDRAM1|u1|state_r.SELFREFRESH~q\)))) ) + ( \SDRAM1|u1|rfshCntr_r\(2) ) + ( \SDRAM1|u1|Add4~22\ ))
-- \SDRAM1|u1|Add4~18\ = CARRY(( (\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|WideOr18~combout\ & ((!\SDRAM1|u1|doSelfRfsh~3_combout\) # (!\SDRAM1|u1|state_r.SELFREFRESH~q\)))) ) + ( \SDRAM1|u1|rfshCntr_r\(2) ) + ( \SDRAM1|u1|Add4~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000101010000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datab => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	datac => \SDRAM1|u1|ALT_INV_state_r.SELFREFRESH~q\,
	datad => \SDRAM1|u1|ALT_INV_WideOr18~combout\,
	dataf => \SDRAM1|u1|ALT_INV_rfshCntr_r\(2),
	cin => \SDRAM1|u1|Add4~22\,
	sumout => \SDRAM1|u1|Add4~17_sumout\,
	cout => \SDRAM1|u1|Add4~18\);

-- Location: LABCELL_X45_Y20_N51
\SDRAM1|u1|rfshCntr_x[2]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|rfshCntr_x[2]~4_combout\ = ( !\SDRAM1|u1|rfshCntr_r[4]~3_combout\ & ( \SDRAM1|u1|Add4~17_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \SDRAM1|u1|ALT_INV_Add4~17_sumout\,
	dataf => \SDRAM1|u1|ALT_INV_rfshCntr_r[4]~3_combout\,
	combout => \SDRAM1|u1|rfshCntr_x[2]~4_combout\);

-- Location: FF_X45_Y20_N53
\SDRAM1|u1|rfshCntr_r[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|rfshCntr_x[2]~4_combout\,
	sclr => \SDRAM1|u1|rfshCntr_r[4]~1_combout\,
	ena => \SDRAM1|u1|rfshCntr_r[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rfshCntr_r\(2));

-- Location: FF_X45_Y20_N46
\SDRAM1|u1|rfshCntr_r[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|rfshCntr_x[4]~2_combout\,
	sclr => \SDRAM1|u1|rfshCntr_r[4]~1_combout\,
	ena => \SDRAM1|u1|rfshCntr_r[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rfshCntr_r\(4));

-- Location: LABCELL_X46_Y20_N9
\SDRAM1|u1|Add4~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add4~13_sumout\ = SUM(( (\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|WideOr18~combout\ & ((!\SDRAM1|u1|doSelfRfsh~3_combout\) # (!\SDRAM1|u1|state_r.SELFREFRESH~q\)))) ) + ( \SDRAM1|u1|rfshCntr_r\(3) ) + ( \SDRAM1|u1|Add4~18\ ))
-- \SDRAM1|u1|Add4~14\ = CARRY(( (\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|WideOr18~combout\ & ((!\SDRAM1|u1|doSelfRfsh~3_combout\) # (!\SDRAM1|u1|state_r.SELFREFRESH~q\)))) ) + ( \SDRAM1|u1|rfshCntr_r\(3) ) + ( \SDRAM1|u1|Add4~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000101010000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datab => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	datac => \SDRAM1|u1|ALT_INV_state_r.SELFREFRESH~q\,
	datad => \SDRAM1|u1|ALT_INV_WideOr18~combout\,
	dataf => \SDRAM1|u1|ALT_INV_rfshCntr_r\(3),
	cin => \SDRAM1|u1|Add4~18\,
	sumout => \SDRAM1|u1|Add4~13_sumout\,
	cout => \SDRAM1|u1|Add4~14\);

-- Location: LABCELL_X45_Y20_N0
\SDRAM1|u1|rfshCntr_x[3]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|rfshCntr_x[3]~3_combout\ = ( \SDRAM1|u1|rfshCntr_r[4]~3_combout\ ) # ( !\SDRAM1|u1|rfshCntr_r[4]~3_combout\ & ( \SDRAM1|u1|Add4~13_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \SDRAM1|u1|ALT_INV_Add4~13_sumout\,
	dataf => \SDRAM1|u1|ALT_INV_rfshCntr_r[4]~3_combout\,
	combout => \SDRAM1|u1|rfshCntr_x[3]~3_combout\);

-- Location: FF_X45_Y20_N1
\SDRAM1|u1|rfshCntr_r[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|rfshCntr_x[3]~3_combout\,
	sclr => \SDRAM1|u1|rfshCntr_r[4]~1_combout\,
	ena => \SDRAM1|u1|rfshCntr_r[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rfshCntr_r\(3));

-- Location: LABCELL_X46_Y20_N12
\SDRAM1|u1|Add4~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add4~9_sumout\ = SUM(( (\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|WideOr18~combout\ & ((!\SDRAM1|u1|doSelfRfsh~3_combout\) # (!\SDRAM1|u1|state_r.SELFREFRESH~q\)))) ) + ( \SDRAM1|u1|rfshCntr_r\(4) ) + ( \SDRAM1|u1|Add4~14\ ))
-- \SDRAM1|u1|Add4~10\ = CARRY(( (\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|WideOr18~combout\ & ((!\SDRAM1|u1|doSelfRfsh~3_combout\) # (!\SDRAM1|u1|state_r.SELFREFRESH~q\)))) ) + ( \SDRAM1|u1|rfshCntr_r\(4) ) + ( \SDRAM1|u1|Add4~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000101010000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datab => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	datac => \SDRAM1|u1|ALT_INV_state_r.SELFREFRESH~q\,
	datad => \SDRAM1|u1|ALT_INV_WideOr18~combout\,
	dataf => \SDRAM1|u1|ALT_INV_rfshCntr_r\(4),
	cin => \SDRAM1|u1|Add4~14\,
	sumout => \SDRAM1|u1|Add4~9_sumout\,
	cout => \SDRAM1|u1|Add4~10\);

-- Location: LABCELL_X45_Y20_N45
\SDRAM1|u1|rfshCntr_x[4]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|rfshCntr_x[4]~2_combout\ = ( \SDRAM1|u1|Add4~9_sumout\ & ( !\SDRAM1|u1|rfshCntr_r[4]~3_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010101010101010101010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_rfshCntr_r[4]~3_combout\,
	dataf => \SDRAM1|u1|ALT_INV_Add4~9_sumout\,
	combout => \SDRAM1|u1|rfshCntr_x[4]~2_combout\);

-- Location: FF_X45_Y20_N47
\SDRAM1|u1|rfshCntr_r[4]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|rfshCntr_x[4]~2_combout\,
	sclr => \SDRAM1|u1|rfshCntr_r[4]~1_combout\,
	ena => \SDRAM1|u1|rfshCntr_r[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rfshCntr_r[4]~DUPLICATE_q\);

-- Location: FF_X45_Y20_N50
\SDRAM1|u1|rfshCntr_r[1]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|rfshCntr_x[1]~5_combout\,
	sclr => \SDRAM1|u1|rfshCntr_r[4]~1_combout\,
	ena => \SDRAM1|u1|rfshCntr_r[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rfshCntr_r[1]~DUPLICATE_q\);

-- Location: LABCELL_X46_Y20_N15
\SDRAM1|u1|Add4~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add4~5_sumout\ = SUM(( (\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|WideOr18~combout\ & ((!\SDRAM1|u1|doSelfRfsh~3_combout\) # (!\SDRAM1|u1|state_r.SELFREFRESH~q\)))) ) + ( \SDRAM1|u1|rfshCntr_r\(5) ) + ( \SDRAM1|u1|Add4~10\ ))
-- \SDRAM1|u1|Add4~6\ = CARRY(( (\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|WideOr18~combout\ & ((!\SDRAM1|u1|doSelfRfsh~3_combout\) # (!\SDRAM1|u1|state_r.SELFREFRESH~q\)))) ) + ( \SDRAM1|u1|rfshCntr_r\(5) ) + ( \SDRAM1|u1|Add4~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000101010000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datab => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	datac => \SDRAM1|u1|ALT_INV_state_r.SELFREFRESH~q\,
	datad => \SDRAM1|u1|ALT_INV_WideOr18~combout\,
	dataf => \SDRAM1|u1|ALT_INV_rfshCntr_r\(5),
	cin => \SDRAM1|u1|Add4~10\,
	sumout => \SDRAM1|u1|Add4~5_sumout\,
	cout => \SDRAM1|u1|Add4~6\);

-- Location: LABCELL_X45_Y20_N9
\SDRAM1|u1|rfshCntr_x[5]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|rfshCntr_x[5]~1_combout\ = ( !\SDRAM1|u1|rfshCntr_r[4]~3_combout\ & ( \SDRAM1|u1|Add4~5_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \SDRAM1|u1|ALT_INV_Add4~5_sumout\,
	dataf => \SDRAM1|u1|ALT_INV_rfshCntr_r[4]~3_combout\,
	combout => \SDRAM1|u1|rfshCntr_x[5]~1_combout\);

-- Location: FF_X45_Y20_N10
\SDRAM1|u1|rfshCntr_r[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|rfshCntr_x[5]~1_combout\,
	sclr => \SDRAM1|u1|rfshCntr_r[4]~1_combout\,
	ena => \SDRAM1|u1|rfshCntr_r[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rfshCntr_r\(5));

-- Location: LABCELL_X46_Y20_N18
\SDRAM1|u1|Add4~53\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add4~53_sumout\ = SUM(( \SDRAM1|u1|rfshCntr_r\(6) ) + ( (\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|WideOr18~combout\ & ((!\SDRAM1|u1|state_r.SELFREFRESH~q\) # (!\SDRAM1|u1|doSelfRfsh~3_combout\)))) ) + ( \SDRAM1|u1|Add4~6\ ))
-- \SDRAM1|u1|Add4~54\ = CARRY(( \SDRAM1|u1|rfshCntr_r\(6) ) + ( (\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|WideOr18~combout\ & ((!\SDRAM1|u1|state_r.SELFREFRESH~q\) # (!\SDRAM1|u1|doSelfRfsh~3_combout\)))) ) + ( \SDRAM1|u1|Add4~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100011111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_state_r.SELFREFRESH~q\,
	datab => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	datac => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datad => \SDRAM1|u1|ALT_INV_rfshCntr_r\(6),
	dataf => \SDRAM1|u1|ALT_INV_WideOr18~combout\,
	cin => \SDRAM1|u1|Add4~6\,
	sumout => \SDRAM1|u1|Add4~53_sumout\,
	cout => \SDRAM1|u1|Add4~54\);

-- Location: LABCELL_X45_Y20_N6
\SDRAM1|u1|rfshCntr_x[6]~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|rfshCntr_x[6]~13_combout\ = ( \SDRAM1|u1|Add4~53_sumout\ & ( !\SDRAM1|u1|rfshCntr_r[4]~3_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \SDRAM1|u1|ALT_INV_rfshCntr_r[4]~3_combout\,
	dataf => \SDRAM1|u1|ALT_INV_Add4~53_sumout\,
	combout => \SDRAM1|u1|rfshCntr_x[6]~13_combout\);

-- Location: FF_X45_Y20_N7
\SDRAM1|u1|rfshCntr_r[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|rfshCntr_x[6]~13_combout\,
	sclr => \SDRAM1|u1|rfshCntr_r[4]~1_combout\,
	ena => \SDRAM1|u1|rfshCntr_r[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rfshCntr_r\(6));

-- Location: LABCELL_X46_Y20_N21
\SDRAM1|u1|Add4~49\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add4~49_sumout\ = SUM(( (\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|WideOr18~combout\ & ((!\SDRAM1|u1|state_r.SELFREFRESH~q\) # (!\SDRAM1|u1|doSelfRfsh~3_combout\)))) ) + ( \SDRAM1|u1|rfshCntr_r\(7) ) + ( \SDRAM1|u1|Add4~54\ ))
-- \SDRAM1|u1|Add4~50\ = CARRY(( (\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|WideOr18~combout\ & ((!\SDRAM1|u1|state_r.SELFREFRESH~q\) # (!\SDRAM1|u1|doSelfRfsh~3_combout\)))) ) + ( \SDRAM1|u1|rfshCntr_r\(7) ) + ( \SDRAM1|u1|Add4~54\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000111000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_state_r.SELFREFRESH~q\,
	datab => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	datac => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datad => \SDRAM1|u1|ALT_INV_WideOr18~combout\,
	dataf => \SDRAM1|u1|ALT_INV_rfshCntr_r\(7),
	cin => \SDRAM1|u1|Add4~54\,
	sumout => \SDRAM1|u1|Add4~49_sumout\,
	cout => \SDRAM1|u1|Add4~50\);

-- Location: LABCELL_X45_Y20_N30
\SDRAM1|u1|rfshCntr_x[7]~12\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|rfshCntr_x[7]~12_combout\ = ( !\SDRAM1|u1|rfshCntr_r[4]~3_combout\ & ( \SDRAM1|u1|Add4~49_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \SDRAM1|u1|ALT_INV_Add4~49_sumout\,
	dataf => \SDRAM1|u1|ALT_INV_rfshCntr_r[4]~3_combout\,
	combout => \SDRAM1|u1|rfshCntr_x[7]~12_combout\);

-- Location: FF_X45_Y20_N31
\SDRAM1|u1|rfshCntr_r[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|rfshCntr_x[7]~12_combout\,
	sclr => \SDRAM1|u1|rfshCntr_r[4]~1_combout\,
	ena => \SDRAM1|u1|rfshCntr_r[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rfshCntr_r\(7));

-- Location: LABCELL_X46_Y20_N24
\SDRAM1|u1|Add4~45\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add4~45_sumout\ = SUM(( (\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|WideOr18~combout\ & ((!\SDRAM1|u1|doSelfRfsh~3_combout\) # (!\SDRAM1|u1|state_r.SELFREFRESH~q\)))) ) + ( \SDRAM1|u1|rfshCntr_r\(8) ) + ( \SDRAM1|u1|Add4~50\ ))
-- \SDRAM1|u1|Add4~46\ = CARRY(( (\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|WideOr18~combout\ & ((!\SDRAM1|u1|doSelfRfsh~3_combout\) # (!\SDRAM1|u1|state_r.SELFREFRESH~q\)))) ) + ( \SDRAM1|u1|rfshCntr_r\(8) ) + ( \SDRAM1|u1|Add4~50\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000101010000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datab => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	datac => \SDRAM1|u1|ALT_INV_state_r.SELFREFRESH~q\,
	datad => \SDRAM1|u1|ALT_INV_WideOr18~combout\,
	dataf => \SDRAM1|u1|ALT_INV_rfshCntr_r\(8),
	cin => \SDRAM1|u1|Add4~50\,
	sumout => \SDRAM1|u1|Add4~45_sumout\,
	cout => \SDRAM1|u1|Add4~46\);

-- Location: LABCELL_X45_Y20_N33
\SDRAM1|u1|rfshCntr_x[8]~11\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|rfshCntr_x[8]~11_combout\ = ( \SDRAM1|u1|Add4~45_sumout\ & ( !\SDRAM1|u1|rfshCntr_r[4]~3_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010101010101010101010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_rfshCntr_r[4]~3_combout\,
	dataf => \SDRAM1|u1|ALT_INV_Add4~45_sumout\,
	combout => \SDRAM1|u1|rfshCntr_x[8]~11_combout\);

-- Location: FF_X45_Y20_N34
\SDRAM1|u1|rfshCntr_r[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|rfshCntr_x[8]~11_combout\,
	sclr => \SDRAM1|u1|rfshCntr_r[4]~1_combout\,
	ena => \SDRAM1|u1|rfshCntr_r[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rfshCntr_r\(8));

-- Location: LABCELL_X46_Y20_N27
\SDRAM1|u1|Add4~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add4~41_sumout\ = SUM(( (\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|WideOr18~combout\ & ((!\SDRAM1|u1|doSelfRfsh~3_combout\) # (!\SDRAM1|u1|state_r.SELFREFRESH~q\)))) ) + ( \SDRAM1|u1|rfshCntr_r\(9) ) + ( \SDRAM1|u1|Add4~46\ ))
-- \SDRAM1|u1|Add4~42\ = CARRY(( (\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|WideOr18~combout\ & ((!\SDRAM1|u1|doSelfRfsh~3_combout\) # (!\SDRAM1|u1|state_r.SELFREFRESH~q\)))) ) + ( \SDRAM1|u1|rfshCntr_r\(9) ) + ( \SDRAM1|u1|Add4~46\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000101010000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datab => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	datac => \SDRAM1|u1|ALT_INV_state_r.SELFREFRESH~q\,
	datad => \SDRAM1|u1|ALT_INV_WideOr18~combout\,
	dataf => \SDRAM1|u1|ALT_INV_rfshCntr_r\(9),
	cin => \SDRAM1|u1|Add4~46\,
	sumout => \SDRAM1|u1|Add4~41_sumout\,
	cout => \SDRAM1|u1|Add4~42\);

-- Location: LABCELL_X46_Y20_N48
\SDRAM1|u1|rfshCntr_x[9]~10\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|rfshCntr_x[9]~10_combout\ = ( !\SDRAM1|u1|rfshCntr_r[4]~3_combout\ & ( \SDRAM1|u1|Add4~41_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \SDRAM1|u1|ALT_INV_Add4~41_sumout\,
	dataf => \SDRAM1|u1|ALT_INV_rfshCntr_r[4]~3_combout\,
	combout => \SDRAM1|u1|rfshCntr_x[9]~10_combout\);

-- Location: FF_X46_Y20_N50
\SDRAM1|u1|rfshCntr_r[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|rfshCntr_x[9]~10_combout\,
	sclr => \SDRAM1|u1|rfshCntr_r[4]~1_combout\,
	ena => \SDRAM1|u1|rfshCntr_r[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rfshCntr_r\(9));

-- Location: LABCELL_X46_Y20_N30
\SDRAM1|u1|Add4~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add4~37_sumout\ = SUM(( \SDRAM1|u1|rfshCntr_r\(10) ) + ( (\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|WideOr18~combout\ & ((!\SDRAM1|u1|state_r.SELFREFRESH~q\) # (!\SDRAM1|u1|doSelfRfsh~3_combout\)))) ) + ( \SDRAM1|u1|Add4~42\ ))
-- \SDRAM1|u1|Add4~38\ = CARRY(( \SDRAM1|u1|rfshCntr_r\(10) ) + ( (\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|WideOr18~combout\ & ((!\SDRAM1|u1|state_r.SELFREFRESH~q\) # (!\SDRAM1|u1|doSelfRfsh~3_combout\)))) ) + ( \SDRAM1|u1|Add4~42\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100011111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_state_r.SELFREFRESH~q\,
	datab => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	datac => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datad => \SDRAM1|u1|ALT_INV_rfshCntr_r\(10),
	dataf => \SDRAM1|u1|ALT_INV_WideOr18~combout\,
	cin => \SDRAM1|u1|Add4~42\,
	sumout => \SDRAM1|u1|Add4~37_sumout\,
	cout => \SDRAM1|u1|Add4~38\);

-- Location: LABCELL_X46_Y20_N45
\SDRAM1|u1|rfshCntr_x[10]~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|rfshCntr_x[10]~9_combout\ = ( !\SDRAM1|u1|rfshCntr_r[4]~3_combout\ & ( \SDRAM1|u1|Add4~37_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \SDRAM1|u1|ALT_INV_Add4~37_sumout\,
	dataf => \SDRAM1|u1|ALT_INV_rfshCntr_r[4]~3_combout\,
	combout => \SDRAM1|u1|rfshCntr_x[10]~9_combout\);

-- Location: FF_X46_Y20_N47
\SDRAM1|u1|rfshCntr_r[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|rfshCntr_x[10]~9_combout\,
	sclr => \SDRAM1|u1|rfshCntr_r[4]~1_combout\,
	ena => \SDRAM1|u1|rfshCntr_r[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rfshCntr_r\(10));

-- Location: LABCELL_X46_Y20_N33
\SDRAM1|u1|Add4~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add4~33_sumout\ = SUM(( (\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|WideOr18~combout\ & ((!\SDRAM1|u1|state_r.SELFREFRESH~q\) # (!\SDRAM1|u1|doSelfRfsh~3_combout\)))) ) + ( \SDRAM1|u1|rfshCntr_r\(11) ) + ( \SDRAM1|u1|Add4~38\ ))
-- \SDRAM1|u1|Add4~34\ = CARRY(( (\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|WideOr18~combout\ & ((!\SDRAM1|u1|state_r.SELFREFRESH~q\) # (!\SDRAM1|u1|doSelfRfsh~3_combout\)))) ) + ( \SDRAM1|u1|rfshCntr_r\(11) ) + ( \SDRAM1|u1|Add4~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000111000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_state_r.SELFREFRESH~q\,
	datab => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	datac => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datad => \SDRAM1|u1|ALT_INV_WideOr18~combout\,
	dataf => \SDRAM1|u1|ALT_INV_rfshCntr_r\(11),
	cin => \SDRAM1|u1|Add4~38\,
	sumout => \SDRAM1|u1|Add4~33_sumout\,
	cout => \SDRAM1|u1|Add4~34\);

-- Location: LABCELL_X45_Y20_N42
\SDRAM1|u1|rfshCntr_x[11]~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|rfshCntr_x[11]~8_combout\ = ( \SDRAM1|u1|Add4~33_sumout\ & ( ((!\SDRAM1|u1|rfshCntr_r[4]~0_combout\) # (!\SDRAM1|u1|state_r.INITPCHG~q\)) # (\SDRAM1|u1|WideOr18~combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111100111111111111110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \SDRAM1|u1|ALT_INV_WideOr18~combout\,
	datac => \SDRAM1|u1|ALT_INV_rfshCntr_r[4]~0_combout\,
	datad => \SDRAM1|u1|ALT_INV_state_r.INITPCHG~q\,
	dataf => \SDRAM1|u1|ALT_INV_Add4~33_sumout\,
	combout => \SDRAM1|u1|rfshCntr_x[11]~8_combout\);

-- Location: FF_X45_Y20_N43
\SDRAM1|u1|rfshCntr_r[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|rfshCntr_x[11]~8_combout\,
	sclr => \SDRAM1|u1|rfshCntr_r[4]~1_combout\,
	ena => \SDRAM1|u1|rfshCntr_r[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rfshCntr_r\(11));

-- Location: LABCELL_X46_Y20_N36
\SDRAM1|u1|Add4~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add4~29_sumout\ = SUM(( (\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|WideOr18~combout\ & ((!\SDRAM1|u1|doSelfRfsh~3_combout\) # (!\SDRAM1|u1|state_r.SELFREFRESH~q\)))) ) + ( \SDRAM1|u1|rfshCntr_r\(12) ) + ( \SDRAM1|u1|Add4~34\ ))
-- \SDRAM1|u1|Add4~30\ = CARRY(( (\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|WideOr18~combout\ & ((!\SDRAM1|u1|doSelfRfsh~3_combout\) # (!\SDRAM1|u1|state_r.SELFREFRESH~q\)))) ) + ( \SDRAM1|u1|rfshCntr_r\(12) ) + ( \SDRAM1|u1|Add4~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000101010000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datab => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	datac => \SDRAM1|u1|ALT_INV_state_r.SELFREFRESH~q\,
	datad => \SDRAM1|u1|ALT_INV_WideOr18~combout\,
	dataf => \SDRAM1|u1|ALT_INV_rfshCntr_r\(12),
	cin => \SDRAM1|u1|Add4~34\,
	sumout => \SDRAM1|u1|Add4~29_sumout\,
	cout => \SDRAM1|u1|Add4~30\);

-- Location: LABCELL_X46_Y20_N42
\SDRAM1|u1|rfshCntr_x[12]~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|rfshCntr_x[12]~7_combout\ = ( !\SDRAM1|u1|rfshCntr_r[4]~3_combout\ & ( \SDRAM1|u1|Add4~29_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \SDRAM1|u1|ALT_INV_Add4~29_sumout\,
	dataf => \SDRAM1|u1|ALT_INV_rfshCntr_r[4]~3_combout\,
	combout => \SDRAM1|u1|rfshCntr_x[12]~7_combout\);

-- Location: FF_X46_Y20_N43
\SDRAM1|u1|rfshCntr_r[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|rfshCntr_x[12]~7_combout\,
	sclr => \SDRAM1|u1|rfshCntr_r[4]~1_combout\,
	ena => \SDRAM1|u1|rfshCntr_r[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rfshCntr_r\(12));

-- Location: LABCELL_X46_Y20_N39
\SDRAM1|u1|Add4~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add4~25_sumout\ = SUM(( (\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|WideOr18~combout\ & ((!\SDRAM1|u1|doSelfRfsh~3_combout\) # (!\SDRAM1|u1|state_r.SELFREFRESH~q\)))) ) + ( \SDRAM1|u1|rfshCntr_r\(13) ) + ( \SDRAM1|u1|Add4~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000101010000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datab => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	datac => \SDRAM1|u1|ALT_INV_state_r.SELFREFRESH~q\,
	datad => \SDRAM1|u1|ALT_INV_WideOr18~combout\,
	dataf => \SDRAM1|u1|ALT_INV_rfshCntr_r\(13),
	cin => \SDRAM1|u1|Add4~30\,
	sumout => \SDRAM1|u1|Add4~25_sumout\);

-- Location: LABCELL_X45_Y20_N57
\SDRAM1|u1|rfshCntr_x[13]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|rfshCntr_x[13]~6_combout\ = ( !\SDRAM1|u1|rfshCntr_r[4]~3_combout\ & ( \SDRAM1|u1|Add4~25_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \SDRAM1|u1|ALT_INV_Add4~25_sumout\,
	dataf => \SDRAM1|u1|ALT_INV_rfshCntr_r[4]~3_combout\,
	combout => \SDRAM1|u1|rfshCntr_x[13]~6_combout\);

-- Location: FF_X45_Y20_N59
\SDRAM1|u1|rfshCntr_r[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|rfshCntr_x[13]~6_combout\,
	sclr => \SDRAM1|u1|rfshCntr_r[4]~1_combout\,
	ena => \SDRAM1|u1|rfshCntr_r[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rfshCntr_r\(13));

-- Location: FF_X45_Y20_N2
\SDRAM1|u1|rfshCntr_r[3]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|rfshCntr_x[3]~3_combout\,
	sclr => \SDRAM1|u1|rfshCntr_r[4]~1_combout\,
	ena => \SDRAM1|u1|rfshCntr_r[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rfshCntr_r[3]~DUPLICATE_q\);

-- Location: LABCELL_X45_Y20_N36
\SDRAM1|u1|Equal8~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Equal8~0_combout\ = ( !\SDRAM1|u1|rfshCntr_r[3]~DUPLICATE_q\ & ( !\SDRAM1|u1|rfshCntr_r\(12) & ( (!\SDRAM1|u1|rfshCntr_r\(2) & (!\SDRAM1|u1|rfshCntr_r[4]~DUPLICATE_q\ & (!\SDRAM1|u1|rfshCntr_r[1]~DUPLICATE_q\ & !\SDRAM1|u1|rfshCntr_r\(13)))) ) 
-- ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_rfshCntr_r\(2),
	datab => \SDRAM1|u1|ALT_INV_rfshCntr_r[4]~DUPLICATE_q\,
	datac => \SDRAM1|u1|ALT_INV_rfshCntr_r[1]~DUPLICATE_q\,
	datad => \SDRAM1|u1|ALT_INV_rfshCntr_r\(13),
	datae => \SDRAM1|u1|ALT_INV_rfshCntr_r[3]~DUPLICATE_q\,
	dataf => \SDRAM1|u1|ALT_INV_rfshCntr_r\(12),
	combout => \SDRAM1|u1|Equal8~0_combout\);

-- Location: FF_X45_Y20_N11
\SDRAM1|u1|rfshCntr_r[5]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|rfshCntr_x[5]~1_combout\,
	sclr => \SDRAM1|u1|rfshCntr_r[4]~1_combout\,
	ena => \SDRAM1|u1|rfshCntr_r[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rfshCntr_r[5]~DUPLICATE_q\);

-- Location: FF_X46_Y20_N49
\SDRAM1|u1|rfshCntr_r[9]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|rfshCntr_x[9]~10_combout\,
	sclr => \SDRAM1|u1|rfshCntr_r[4]~1_combout\,
	ena => \SDRAM1|u1|rfshCntr_r[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rfshCntr_r[9]~DUPLICATE_q\);

-- Location: LABCELL_X46_Y20_N54
\SDRAM1|u1|Equal8~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Equal8~1_combout\ = ( !\SDRAM1|u1|rfshCntr_r\(10) & ( !\SDRAM1|u1|rfshCntr_r\(11) & ( (!\SDRAM1|u1|rfshCntr_r\(7) & (!\SDRAM1|u1|rfshCntr_r\(8) & (!\SDRAM1|u1|rfshCntr_r\(6) & !\SDRAM1|u1|rfshCntr_r[9]~DUPLICATE_q\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_rfshCntr_r\(7),
	datab => \SDRAM1|u1|ALT_INV_rfshCntr_r\(8),
	datac => \SDRAM1|u1|ALT_INV_rfshCntr_r\(6),
	datad => \SDRAM1|u1|ALT_INV_rfshCntr_r[9]~DUPLICATE_q\,
	datae => \SDRAM1|u1|ALT_INV_rfshCntr_r\(10),
	dataf => \SDRAM1|u1|ALT_INV_rfshCntr_r\(11),
	combout => \SDRAM1|u1|Equal8~1_combout\);

-- Location: LABCELL_X48_Y21_N48
\SDRAM1|u1|Equal0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Equal0~0_combout\ = ( !\SDRAM1|u1|rdPipeline_r[4]~DUPLICATE_q\ & ( (!\SDRAM1|u1|rdPipeline_r\(1) & (!\SDRAM1|u1|rdPipeline_r\(2) & !\SDRAM1|u1|rdPipeline_r\(3))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000000000000101000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_rdPipeline_r\(1),
	datac => \SDRAM1|u1|ALT_INV_rdPipeline_r\(2),
	datad => \SDRAM1|u1|ALT_INV_rdPipeline_r\(3),
	dataf => \SDRAM1|u1|ALT_INV_rdPipeline_r[4]~DUPLICATE_q\,
	combout => \SDRAM1|u1|Equal0~0_combout\);

-- Location: LABCELL_X48_Y21_N30
\SDRAM1|u1|cmd_x[3]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|cmd_x[3]~5_combout\ = ( \SDRAM1|u1|Equal0~0_combout\ & ( (\SDRAM1|u1|Equal8~0_combout\ & (!\SDRAM1|u1|rfshCntr_r[5]~DUPLICATE_q\ & (!\SDRAM1|u1|rfshCntr_r\(0) & \SDRAM1|u1|Equal8~1_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000010000000000000001000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal8~0_combout\,
	datab => \SDRAM1|u1|ALT_INV_rfshCntr_r[5]~DUPLICATE_q\,
	datac => \SDRAM1|u1|ALT_INV_rfshCntr_r\(0),
	datad => \SDRAM1|u1|ALT_INV_Equal8~1_combout\,
	dataf => \SDRAM1|u1|ALT_INV_Equal0~0_combout\,
	combout => \SDRAM1|u1|cmd_x[3]~5_combout\);

-- Location: LABCELL_X48_Y21_N36
\SDRAM1|u1|wrTimer_r[0]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|wrTimer_r[0]~3_combout\ = ( !\SDRAM1|u1|wrTimer_r\(0) & ( \SDRAM1|u1|wrTimer_r\(1) & ( (!\SDRAM1|u1|doActivate~combout\) # ((!\SDRAM1|u1|wrTimer_r[1]~1_combout\) # ((!\SDRAM1|u1|wrTimer_r[1]~0_combout\) # (!\SDRAM1|u1|cmd_x[3]~5_combout\))) ) ) 
-- )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111100000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_doActivate~combout\,
	datab => \SDRAM1|u1|ALT_INV_wrTimer_r[1]~1_combout\,
	datac => \SDRAM1|u1|ALT_INV_wrTimer_r[1]~0_combout\,
	datad => \SDRAM1|u1|ALT_INV_cmd_x[3]~5_combout\,
	datae => \SDRAM1|u1|ALT_INV_wrTimer_r\(0),
	dataf => \SDRAM1|u1|ALT_INV_wrTimer_r\(1),
	combout => \SDRAM1|u1|wrTimer_r[0]~3_combout\);

-- Location: FF_X48_Y21_N38
\SDRAM1|u1|wrTimer_r[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|wrTimer_r[0]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|wrTimer_r\(0));

-- Location: LABCELL_X48_Y21_N0
\SDRAM1|u1|wrTimer_r[1]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|wrTimer_r[1]~2_combout\ = ( \SDRAM1|u1|wrTimer_r\(1) & ( \SDRAM1|u1|cmd_x[3]~5_combout\ & ( ((\SDRAM1|u1|wrTimer_r[1]~1_combout\ & (\SDRAM1|u1|wrTimer_r[1]~0_combout\ & \SDRAM1|u1|doActivate~combout\))) # (\SDRAM1|u1|wrTimer_r\(0)) ) ) ) # ( 
-- !\SDRAM1|u1|wrTimer_r\(1) & ( \SDRAM1|u1|cmd_x[3]~5_combout\ & ( (\SDRAM1|u1|wrTimer_r[1]~1_combout\ & (\SDRAM1|u1|wrTimer_r[1]~0_combout\ & \SDRAM1|u1|doActivate~combout\)) ) ) ) # ( \SDRAM1|u1|wrTimer_r\(1) & ( !\SDRAM1|u1|cmd_x[3]~5_combout\ & ( 
-- \SDRAM1|u1|wrTimer_r\(0) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000010101010101010100000000000000110101010101010111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_wrTimer_r\(0),
	datab => \SDRAM1|u1|ALT_INV_wrTimer_r[1]~1_combout\,
	datac => \SDRAM1|u1|ALT_INV_wrTimer_r[1]~0_combout\,
	datad => \SDRAM1|u1|ALT_INV_doActivate~combout\,
	datae => \SDRAM1|u1|ALT_INV_wrTimer_r\(1),
	dataf => \SDRAM1|u1|ALT_INV_cmd_x[3]~5_combout\,
	combout => \SDRAM1|u1|wrTimer_r[1]~2_combout\);

-- Location: FF_X48_Y21_N1
\SDRAM1|u1|wrTimer_r[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|wrTimer_r[1]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|wrTimer_r\(1));

-- Location: FF_X48_Y21_N8
\SDRAM1|u1|rdPipeline_r[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|rdPipeline_x[4]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rdPipeline_r\(4));

-- Location: LABCELL_X48_Y21_N12
\SDRAM1|u1|combinatorial~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|combinatorial~0_combout\ = ( !\SDRAM1|u1|wrTimer_r\(0) & ( !\SDRAM1|u1|rdPipeline_r\(4) & ( (!\SDRAM1|u1|rdPipeline_r\(3) & (!\SDRAM1|u1|rdPipeline_r\(2) & (!\SDRAM1|u1|rdPipeline_r\(1) & !\SDRAM1|u1|wrTimer_r\(1)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_rdPipeline_r\(3),
	datab => \SDRAM1|u1|ALT_INV_rdPipeline_r\(2),
	datac => \SDRAM1|u1|ALT_INV_rdPipeline_r\(1),
	datad => \SDRAM1|u1|ALT_INV_wrTimer_r\(1),
	datae => \SDRAM1|u1|ALT_INV_wrTimer_r\(0),
	dataf => \SDRAM1|u1|ALT_INV_rdPipeline_r\(4),
	combout => \SDRAM1|u1|combinatorial~0_combout\);

-- Location: MLABCELL_X47_Y21_N18
\SDRAM1|u1|rasTimer_r[0]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|rasTimer_r[0]~4_combout\ = ( \SDRAM1|u1|rasTimer_r\(0) & ( \SDRAM1|u1|Equal7~1_combout\ & ( (\SDRAM1|u1|Equal7~2_combout\ & (\SDRAM1|u1|state_r.ACTIVATE~q\ & \SDRAM1|u1|Equal7~0_combout\)) ) ) ) # ( !\SDRAM1|u1|rasTimer_r\(0) & ( 
-- \SDRAM1|u1|Equal7~1_combout\ & ( (!\SDRAM1|u1|rasTimer_r[2]~0_combout\) # ((\SDRAM1|u1|Equal7~2_combout\ & (\SDRAM1|u1|state_r.ACTIVATE~q\ & \SDRAM1|u1|Equal7~0_combout\))) ) ) ) # ( !\SDRAM1|u1|rasTimer_r\(0) & ( !\SDRAM1|u1|Equal7~1_combout\ & ( 
-- !\SDRAM1|u1|rasTimer_r[2]~0_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000000000000000000011111111000000010000000100000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal7~2_combout\,
	datab => \SDRAM1|u1|ALT_INV_state_r.ACTIVATE~q\,
	datac => \SDRAM1|u1|ALT_INV_Equal7~0_combout\,
	datad => \SDRAM1|u1|ALT_INV_rasTimer_r[2]~0_combout\,
	datae => \SDRAM1|u1|ALT_INV_rasTimer_r\(0),
	dataf => \SDRAM1|u1|ALT_INV_Equal7~1_combout\,
	combout => \SDRAM1|u1|rasTimer_r[0]~4_combout\);

-- Location: FF_X47_Y21_N19
\SDRAM1|u1|rasTimer_r[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|rasTimer_r[0]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rasTimer_r\(0));

-- Location: MLABCELL_X47_Y21_N39
\SDRAM1|u1|Add1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add1~0_combout\ = ( \SDRAM1|u1|rasTimer_r\(1) & ( !\SDRAM1|u1|rasTimer_r\(0) ) ) # ( !\SDRAM1|u1|rasTimer_r\(1) & ( \SDRAM1|u1|rasTimer_r\(0) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111111110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \SDRAM1|u1|ALT_INV_rasTimer_r\(0),
	dataf => \SDRAM1|u1|ALT_INV_rasTimer_r\(1),
	combout => \SDRAM1|u1|Add1~0_combout\);

-- Location: MLABCELL_X47_Y21_N42
\SDRAM1|u1|rasTimer_r[1]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|rasTimer_r[1]~3_combout\ = ( \SDRAM1|u1|Equal7~0_combout\ & ( !\SDRAM1|u1|rasTimer_r[2]~0_combout\ & ( (!\SDRAM1|u1|Add1~0_combout\ & ((!\SDRAM1|u1|Equal7~1_combout\) # ((!\SDRAM1|u1|Equal7~2_combout\) # (!\SDRAM1|u1|state_r.ACTIVATE~q\)))) ) ) 
-- ) # ( !\SDRAM1|u1|Equal7~0_combout\ & ( !\SDRAM1|u1|rasTimer_r[2]~0_combout\ & ( !\SDRAM1|u1|Add1~0_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011001100110011001100100000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal7~1_combout\,
	datab => \SDRAM1|u1|ALT_INV_Add1~0_combout\,
	datac => \SDRAM1|u1|ALT_INV_Equal7~2_combout\,
	datad => \SDRAM1|u1|ALT_INV_state_r.ACTIVATE~q\,
	datae => \SDRAM1|u1|ALT_INV_Equal7~0_combout\,
	dataf => \SDRAM1|u1|ALT_INV_rasTimer_r[2]~0_combout\,
	combout => \SDRAM1|u1|rasTimer_r[1]~3_combout\);

-- Location: FF_X47_Y21_N43
\SDRAM1|u1|rasTimer_r[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|rasTimer_r[1]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rasTimer_r\(1));

-- Location: MLABCELL_X47_Y21_N51
\SDRAM1|u1|rasTimer_r[2]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|rasTimer_r[2]~1_combout\ = ( \SDRAM1|u1|rasTimer_r\(1) & ( \SDRAM1|u1|rasTimer_r\(2) ) ) # ( !\SDRAM1|u1|rasTimer_r\(1) & ( (\SDRAM1|u1|rasTimer_r\(2) & \SDRAM1|u1|rasTimer_r\(0)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111000000000000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \SDRAM1|u1|ALT_INV_rasTimer_r\(2),
	datad => \SDRAM1|u1|ALT_INV_rasTimer_r\(0),
	dataf => \SDRAM1|u1|ALT_INV_rasTimer_r\(1),
	combout => \SDRAM1|u1|rasTimer_r[2]~1_combout\);

-- Location: MLABCELL_X47_Y21_N9
\SDRAM1|u1|rasTimer_r[2]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|rasTimer_r[2]~2_combout\ = ( \SDRAM1|u1|Equal7~1_combout\ & ( ((\SDRAM1|u1|Equal7~0_combout\ & (\SDRAM1|u1|state_r.ACTIVATE~q\ & \SDRAM1|u1|Equal7~2_combout\))) # (\SDRAM1|u1|rasTimer_r[2]~1_combout\) ) ) # ( !\SDRAM1|u1|Equal7~1_combout\ & ( 
-- \SDRAM1|u1|rasTimer_r[2]~1_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000111110000111100011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal7~0_combout\,
	datab => \SDRAM1|u1|ALT_INV_state_r.ACTIVATE~q\,
	datac => \SDRAM1|u1|ALT_INV_rasTimer_r[2]~1_combout\,
	datad => \SDRAM1|u1|ALT_INV_Equal7~2_combout\,
	dataf => \SDRAM1|u1|ALT_INV_Equal7~1_combout\,
	combout => \SDRAM1|u1|rasTimer_r[2]~2_combout\);

-- Location: FF_X47_Y21_N10
\SDRAM1|u1|rasTimer_r[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|rasTimer_r[2]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rasTimer_r\(2));

-- Location: FF_X47_Y21_N20
\SDRAM1|u1|rasTimer_r[0]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|rasTimer_r[0]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rasTimer_r[0]~DUPLICATE_q\);

-- Location: MLABCELL_X47_Y21_N24
\SDRAM1|u1|rasTimer_r[2]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|rasTimer_r[2]~0_combout\ = ( !\SDRAM1|u1|rasTimer_r[0]~DUPLICATE_q\ & ( (!\SDRAM1|u1|rasTimer_r\(2) & !\SDRAM1|u1|rasTimer_r\(1)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000000000000111100000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \SDRAM1|u1|ALT_INV_rasTimer_r\(2),
	datad => \SDRAM1|u1|ALT_INV_rasTimer_r\(1),
	dataf => \SDRAM1|u1|ALT_INV_rasTimer_r[0]~DUPLICATE_q\,
	combout => \SDRAM1|u1|rasTimer_r[2]~0_combout\);

-- Location: LABCELL_X50_Y22_N15
\SDRAM1|u1|Selector22~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Selector22~0_combout\ = ( \SDRAM1|u1|state_r.RW~q\ & ( (\SDRAM1|u1|combinatorial~0_combout\ & \SDRAM1|u1|rasTimer_r[2]~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000010101010000000001010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_combinatorial~0_combout\,
	datad => \SDRAM1|u1|ALT_INV_rasTimer_r[2]~0_combout\,
	dataf => \SDRAM1|u1|ALT_INV_state_r.RW~q\,
	combout => \SDRAM1|u1|Selector22~0_combout\);

-- Location: LABCELL_X45_Y20_N12
\SDRAM1|u1|Equal9~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Equal9~0_combout\ = ( !\SDRAM1|u1|rfshCntr_r[5]~DUPLICATE_q\ & ( (!\SDRAM1|u1|rfshCntr_r\(0) & (\SDRAM1|u1|Equal8~1_combout\ & \SDRAM1|u1|Equal8~0_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000100010000000000010001000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_rfshCntr_r\(0),
	datab => \SDRAM1|u1|ALT_INV_Equal8~1_combout\,
	datad => \SDRAM1|u1|ALT_INV_Equal8~0_combout\,
	dataf => \SDRAM1|u1|ALT_INV_rfshCntr_r[5]~DUPLICATE_q\,
	combout => \SDRAM1|u1|Equal9~0_combout\);

-- Location: MLABCELL_X47_Y20_N54
\SDRAM1|u1|state_x.RW~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|state_x.RW~0_combout\ = ( \rd~DUPLICATE_q\ & ( \wr~q\ & ( \SDRAM1|u1|Equal9~0_combout\ ) ) ) # ( !\rd~DUPLICATE_q\ & ( \wr~q\ & ( \SDRAM1|u1|Equal9~0_combout\ ) ) ) # ( \rd~DUPLICATE_q\ & ( !\wr~q\ & ( \SDRAM1|u1|Equal9~0_combout\ ) ) ) # ( 
-- !\rd~DUPLICATE_q\ & ( !\wr~q\ & ( (\SDRAM1|u1|Equal9~0_combout\ & ((!\SDRAM1|u1|doSelfRfsh~0_combout\) # ((!\SDRAM1|u1|doSelfRfsh~1_combout\) # (!\SDRAM1|u1|doSelfRfsh~2_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001110000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_doSelfRfsh~0_combout\,
	datab => \SDRAM1|u1|ALT_INV_doSelfRfsh~1_combout\,
	datac => \SDRAM1|u1|ALT_INV_Equal9~0_combout\,
	datad => \SDRAM1|u1|ALT_INV_doSelfRfsh~2_combout\,
	datae => \ALT_INV_rd~DUPLICATE_q\,
	dataf => \ALT_INV_wr~q\,
	combout => \SDRAM1|u1|state_x.RW~0_combout\);

-- Location: MLABCELL_X52_Y21_N18
\SDRAM1|u1|Selector59~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Selector59~2_combout\ = ( \SDRAM1|u1|state_x.RW~0_combout\ & ( \SDRAM1|u1|doActivate~combout\ & ( (\SDRAM1|u1|Selector59~1_combout\) # (\SDRAM1|u1|Selector59~0_combout\) ) ) ) # ( !\SDRAM1|u1|state_x.RW~0_combout\ & ( 
-- \SDRAM1|u1|doActivate~combout\ & ( ((\SDRAM1|u1|Selector59~1_combout\ & !\SDRAM1|u1|Selector22~0_combout\)) # (\SDRAM1|u1|Selector59~0_combout\) ) ) ) # ( \SDRAM1|u1|state_x.RW~0_combout\ & ( !\SDRAM1|u1|doActivate~combout\ & ( 
-- ((\SDRAM1|u1|Selector59~1_combout\ & ((!\SDRAM1|u1|sAddr_x[10]~12_combout\) # (!\SDRAM1|u1|Selector22~0_combout\)))) # (\SDRAM1|u1|Selector59~0_combout\) ) ) ) # ( !\SDRAM1|u1|state_x.RW~0_combout\ & ( !\SDRAM1|u1|doActivate~combout\ & ( 
-- ((\SDRAM1|u1|Selector59~1_combout\ & !\SDRAM1|u1|Selector22~0_combout\)) # (\SDRAM1|u1|Selector59~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101111101010101010111110101110101011111010101010101111101011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Selector59~0_combout\,
	datab => \SDRAM1|u1|ALT_INV_sAddr_x[10]~12_combout\,
	datac => \SDRAM1|u1|ALT_INV_Selector59~1_combout\,
	datad => \SDRAM1|u1|ALT_INV_Selector22~0_combout\,
	datae => \SDRAM1|u1|ALT_INV_state_x.RW~0_combout\,
	dataf => \SDRAM1|u1|ALT_INV_doActivate~combout\,
	combout => \SDRAM1|u1|Selector59~2_combout\);

-- Location: FF_X52_Y21_N20
\SDRAM1|u1|activeFlag_r[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|Selector59~2_combout\,
	ena => \SDRAM1|u1|Equal7~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|activeFlag_r\(0));

-- Location: MLABCELL_X52_Y21_N12
\SDRAM1|u1|doActivate~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|doActivate~3_combout\ = ( hAddr(24) & ( (\SDRAM1|u1|activeBank_r\(1) & (\SDRAM1|u1|activeFlag_r\(0) & (!\SDRAM1|u1|activeBank_r\(0) $ (hAddr(23))))) ) ) # ( !hAddr(24) & ( (!\SDRAM1|u1|activeBank_r\(1) & (\SDRAM1|u1|activeFlag_r\(0) & 
-- (!\SDRAM1|u1|activeBank_r\(0) $ (hAddr(23))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100000000010000010000000001000000100000000010000010000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_activeBank_r\(1),
	datab => \SDRAM1|u1|ALT_INV_activeBank_r\(0),
	datac => \SDRAM1|u1|ALT_INV_activeFlag_r\(0),
	datad => ALT_INV_hAddr(23),
	dataf => ALT_INV_hAddr(24),
	combout => \SDRAM1|u1|doActivate~3_combout\);

-- Location: FF_X51_Y21_N28
\SDRAM1|u1|activeRow_r[0][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => hAddr(10),
	sload => VCC,
	ena => \SDRAM1|u1|rasTimer_r[2]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|activeRow_r[0][0]~q\);

-- Location: FF_X51_Y21_N5
\SDRAM1|u1|activeRow_r[0][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => hAddr(11),
	sload => VCC,
	ena => \SDRAM1|u1|rasTimer_r[2]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|activeRow_r[0][1]~q\);

-- Location: FF_X51_Y21_N1
\SDRAM1|u1|activeRow_r[0][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => hAddr(12),
	sload => VCC,
	ena => \SDRAM1|u1|rasTimer_r[2]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|activeRow_r[0][2]~q\);

-- Location: LABCELL_X51_Y21_N0
\SDRAM1|u1|doActivate~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|doActivate~4_combout\ = ( \SDRAM1|u1|activeRow_r[0][2]~q\ & ( hAddr(11) & ( (hAddr(12) & (\SDRAM1|u1|activeRow_r[0][1]~q\ & (!\SDRAM1|u1|activeRow_r[0][0]~q\ $ (hAddr(10))))) ) ) ) # ( !\SDRAM1|u1|activeRow_r[0][2]~q\ & ( hAddr(11) & ( 
-- (!hAddr(12) & (\SDRAM1|u1|activeRow_r[0][1]~q\ & (!\SDRAM1|u1|activeRow_r[0][0]~q\ $ (hAddr(10))))) ) ) ) # ( \SDRAM1|u1|activeRow_r[0][2]~q\ & ( !hAddr(11) & ( (hAddr(12) & (!\SDRAM1|u1|activeRow_r[0][1]~q\ & (!\SDRAM1|u1|activeRow_r[0][0]~q\ $ 
-- (hAddr(10))))) ) ) ) # ( !\SDRAM1|u1|activeRow_r[0][2]~q\ & ( !hAddr(11) & ( (!hAddr(12) & (!\SDRAM1|u1|activeRow_r[0][1]~q\ & (!\SDRAM1|u1|activeRow_r[0][0]~q\ $ (hAddr(10))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000100000010000000001000000001000000000100000010000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(12),
	datab => \SDRAM1|u1|ALT_INV_activeRow_r[0][0]~q\,
	datac => \SDRAM1|u1|ALT_INV_activeRow_r[0][1]~q\,
	datad => ALT_INV_hAddr(10),
	datae => \SDRAM1|u1|ALT_INV_activeRow_r[0][2]~q\,
	dataf => ALT_INV_hAddr(11),
	combout => \SDRAM1|u1|doActivate~4_combout\);

-- Location: FF_X52_Y19_N4
\SDRAM1|u1|activeRow_r[0][9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => hAddr(19),
	sload => VCC,
	ena => \SDRAM1|u1|rasTimer_r[2]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|activeRow_r[0][9]~q\);

-- Location: MLABCELL_X52_Y19_N39
\SDRAM1|u1|activeRow_r[0][10]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|activeRow_r[0][10]~feeder_combout\ = hAddr(20)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(20),
	combout => \SDRAM1|u1|activeRow_r[0][10]~feeder_combout\);

-- Location: FF_X52_Y19_N40
\SDRAM1|u1|activeRow_r[0][10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|activeRow_r[0][10]~feeder_combout\,
	ena => \SDRAM1|u1|rasTimer_r[2]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|activeRow_r[0][10]~q\);

-- Location: FF_X51_Y21_N13
\SDRAM1|u1|activeRow_r[0][11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => hAddr(21),
	sload => VCC,
	ena => \SDRAM1|u1|rasTimer_r[2]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|activeRow_r[0][11]~q\);

-- Location: LABCELL_X51_Y21_N12
\SDRAM1|u1|doActivate~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|doActivate~0_combout\ = ( \SDRAM1|u1|activeRow_r[0][11]~q\ & ( hAddr(20) & ( (hAddr(21) & (\SDRAM1|u1|activeRow_r[0][10]~q\ & (!\SDRAM1|u1|activeRow_r[0][9]~q\ $ (hAddr(19))))) ) ) ) # ( !\SDRAM1|u1|activeRow_r[0][11]~q\ & ( hAddr(20) & ( 
-- (!hAddr(21) & (\SDRAM1|u1|activeRow_r[0][10]~q\ & (!\SDRAM1|u1|activeRow_r[0][9]~q\ $ (hAddr(19))))) ) ) ) # ( \SDRAM1|u1|activeRow_r[0][11]~q\ & ( !hAddr(20) & ( (hAddr(21) & (!\SDRAM1|u1|activeRow_r[0][10]~q\ & (!\SDRAM1|u1|activeRow_r[0][9]~q\ $ 
-- (hAddr(19))))) ) ) ) # ( !\SDRAM1|u1|activeRow_r[0][11]~q\ & ( !hAddr(20) & ( (!hAddr(21) & (!\SDRAM1|u1|activeRow_r[0][10]~q\ & (!\SDRAM1|u1|activeRow_r[0][9]~q\ $ (hAddr(19))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000010000000000001000010000000000000000100001000000000000100001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_activeRow_r[0][9]~q\,
	datab => ALT_INV_hAddr(21),
	datac => ALT_INV_hAddr(19),
	datad => \SDRAM1|u1|ALT_INV_activeRow_r[0][10]~q\,
	datae => \SDRAM1|u1|ALT_INV_activeRow_r[0][11]~q\,
	dataf => ALT_INV_hAddr(20),
	combout => \SDRAM1|u1|doActivate~0_combout\);

-- Location: LABCELL_X51_Y21_N24
\SDRAM1|u1|doActivate\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|doActivate~combout\ = ( \SDRAM1|u1|doActivate~4_combout\ & ( \SDRAM1|u1|doActivate~0_combout\ & ( (\SDRAM1|u1|doActivate~2_combout\ & (\SDRAM1|u1|doActivate~1_combout\ & (!\SDRAM1|u1|Equal2~0_combout\ & \SDRAM1|u1|doActivate~3_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_doActivate~2_combout\,
	datab => \SDRAM1|u1|ALT_INV_doActivate~1_combout\,
	datac => \SDRAM1|u1|ALT_INV_Equal2~0_combout\,
	datad => \SDRAM1|u1|ALT_INV_doActivate~3_combout\,
	datae => \SDRAM1|u1|ALT_INV_doActivate~4_combout\,
	dataf => \SDRAM1|u1|ALT_INV_doActivate~0_combout\,
	combout => \SDRAM1|u1|doActivate~combout\);

-- Location: LABCELL_X53_Y21_N57
\SDRAM1|u1|Selector20~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Selector20~1_combout\ = ( \wr~q\ & ( (!\SDRAM1|u1|doActivate~combout\ & (\SDRAM1|u1|Equal9~0_combout\ & \SDRAM1|u1|Selector22~0_combout\)) ) ) # ( !\wr~q\ & ( (\rd~DUPLICATE_q\ & (!\SDRAM1|u1|doActivate~combout\ & (\SDRAM1|u1|Equal9~0_combout\ 
-- & \SDRAM1|u1|Selector22~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000100000000000000010000000000000011000000000000001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rd~DUPLICATE_q\,
	datab => \SDRAM1|u1|ALT_INV_doActivate~combout\,
	datac => \SDRAM1|u1|ALT_INV_Equal9~0_combout\,
	datad => \SDRAM1|u1|ALT_INV_Selector22~0_combout\,
	dataf => \ALT_INV_wr~q\,
	combout => \SDRAM1|u1|Selector20~1_combout\);

-- Location: FF_X53_Y21_N58
\SDRAM1|u1|state_r.ACTIVATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|Selector20~1_combout\,
	ena => \SDRAM1|u1|Equal7~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|state_r.ACTIVATE~q\);

-- Location: LABCELL_X50_Y22_N12
\SDRAM1|u1|Selector21~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Selector21~0_combout\ = ( \SDRAM1|u1|state_r.RW~q\ & ( (\SDRAM1|u1|combinatorial~0_combout\ & (!\SDRAM1|u1|Equal9~0_combout\ & \SDRAM1|u1|rasTimer_r[2]~0_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000010100000000000001010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_combinatorial~0_combout\,
	datac => \SDRAM1|u1|ALT_INV_Equal9~0_combout\,
	datad => \SDRAM1|u1|ALT_INV_rasTimer_r[2]~0_combout\,
	dataf => \SDRAM1|u1|ALT_INV_state_r.RW~q\,
	combout => \SDRAM1|u1|Selector21~0_combout\);

-- Location: FF_X50_Y22_N14
\SDRAM1|u1|state_r.REFRESHROW\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|Selector21~0_combout\,
	ena => \SDRAM1|u1|Equal7~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|state_r.REFRESHROW~q\);

-- Location: LABCELL_X50_Y21_N27
\SDRAM1|u1|state_r.INITRFSH~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|state_r.INITRFSH~0_combout\ = ( \SDRAM1|u1|state_r.INITPCHG~q\ & ( (!\SDRAM1|u1|state_r.INITSETMODE~q\ & (!\SDRAM1|u1|state_r.ACTIVATE~q\ & !\SDRAM1|u1|state_r.REFRESHROW~q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010000000100000001000000010000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_state_r.INITSETMODE~q\,
	datab => \SDRAM1|u1|ALT_INV_state_r.ACTIVATE~q\,
	datac => \SDRAM1|u1|ALT_INV_state_r.REFRESHROW~q\,
	dataf => \SDRAM1|u1|ALT_INV_state_r.INITPCHG~q\,
	combout => \SDRAM1|u1|state_r.INITRFSH~0_combout\);

-- Location: LABCELL_X51_Y21_N57
\SDRAM1|u1|state_x.INITSETMODE~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|state_x.INITSETMODE~1_combout\ = ( \SDRAM1|u1|sAddr_x[10]~12_combout\ & ( \SDRAM1|u1|doActivate~combout\ & ( (!\SDRAM1|u1|doSelfRfsh~3_combout\ & (\SDRAM1|u1|state_r.RW~q\ & \SDRAM1|u1|Equal9~0_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000001000000010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	datab => \SDRAM1|u1|ALT_INV_state_r.RW~q\,
	datac => \SDRAM1|u1|ALT_INV_Equal9~0_combout\,
	datae => \SDRAM1|u1|ALT_INV_sAddr_x[10]~12_combout\,
	dataf => \SDRAM1|u1|ALT_INV_doActivate~combout\,
	combout => \SDRAM1|u1|state_x.INITSETMODE~1_combout\);

-- Location: LABCELL_X48_Y21_N18
\SDRAM1|u1|state_x.RW~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|state_x.RW~3_combout\ = ( !\SDRAM1|u1|state_r.RW~q\ & ( \SDRAM1|u1|state_r.INITRFSH~q\ & ( (!\SDRAM1|u1|Equal8~0_combout\) # ((!\SDRAM1|u1|rfshCntr_r\(0)) # ((!\SDRAM1|u1|Equal8~1_combout\) # (\SDRAM1|u1|rfshCntr_r[5]~DUPLICATE_q\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111110111111110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal8~0_combout\,
	datab => \SDRAM1|u1|ALT_INV_rfshCntr_r\(0),
	datac => \SDRAM1|u1|ALT_INV_Equal8~1_combout\,
	datad => \SDRAM1|u1|ALT_INV_rfshCntr_r[5]~DUPLICATE_q\,
	datae => \SDRAM1|u1|ALT_INV_state_r.RW~q\,
	dataf => \SDRAM1|u1|ALT_INV_state_r.INITRFSH~q\,
	combout => \SDRAM1|u1|state_x.RW~3_combout\);

-- Location: LABCELL_X50_Y22_N57
\SDRAM1|u1|Selector28~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Selector28~0_combout\ = ( \SDRAM1|u1|combinatorial~0_combout\ & ( (\SDRAM1|u1|state_r.RW~q\ & !\SDRAM1|u1|rasTimer_r[2]~0_combout\) ) ) # ( !\SDRAM1|u1|combinatorial~0_combout\ & ( \SDRAM1|u1|state_r.RW~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011000000000011001100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \SDRAM1|u1|ALT_INV_state_r.RW~q\,
	datad => \SDRAM1|u1|ALT_INV_rasTimer_r[2]~0_combout\,
	dataf => \SDRAM1|u1|ALT_INV_combinatorial~0_combout\,
	combout => \SDRAM1|u1|Selector28~0_combout\);

-- Location: LABCELL_X53_Y21_N24
\SDRAM1|u1|state_x.RW~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|state_x.RW~2_combout\ = ( \SDRAM1|u1|Selector28~0_combout\ & ( \SDRAM1|u1|doActivate~combout\ & ( (!\SDRAM1|u1|Equal9~0_combout\) # ((\SDRAM1|u1|state_r.INITRFSH~q\) # (\SDRAM1|u1|doSelfRfsh~3_combout\)) ) ) ) # ( 
-- \SDRAM1|u1|Selector28~0_combout\ & ( !\SDRAM1|u1|doActivate~combout\ & ( ((!\SDRAM1|u1|Equal9~0_combout\) # ((\SDRAM1|u1|state_r.INITRFSH~q\) # (\SDRAM1|u1|doSelfRfsh~3_combout\))) # (\SDRAM1|u1|sAddr_x[10]~12_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110111111111111100000000000000001100111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_sAddr_x[10]~12_combout\,
	datab => \SDRAM1|u1|ALT_INV_Equal9~0_combout\,
	datac => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	datad => \SDRAM1|u1|ALT_INV_state_r.INITRFSH~q\,
	datae => \SDRAM1|u1|ALT_INV_Selector28~0_combout\,
	dataf => \SDRAM1|u1|ALT_INV_doActivate~combout\,
	combout => \SDRAM1|u1|state_x.RW~2_combout\);

-- Location: LABCELL_X53_Y21_N12
\SDRAM1|u1|state_r.INITRFSH~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|state_r.INITRFSH~1_combout\ = ( \SDRAM1|u1|state_r.INITRFSH~q\ & ( \SDRAM1|u1|state_x.RW~2_combout\ ) ) # ( \SDRAM1|u1|state_r.INITRFSH~q\ & ( !\SDRAM1|u1|state_x.RW~2_combout\ & ( ((!\SDRAM1|u1|rfshCntr_r[4]~0_combout\) # 
-- ((\SDRAM1|u1|state_x.RW~3_combout\) # (\SDRAM1|u1|state_x.INITSETMODE~1_combout\))) # (\SDRAM1|u1|state_r.INITRFSH~0_combout\) ) ) ) # ( !\SDRAM1|u1|state_r.INITRFSH~q\ & ( !\SDRAM1|u1|state_x.RW~2_combout\ & ( (\SDRAM1|u1|state_r.INITRFSH~0_combout\ & 
-- (\SDRAM1|u1|rfshCntr_r[4]~0_combout\ & (!\SDRAM1|u1|state_x.INITSETMODE~1_combout\ & !\SDRAM1|u1|state_x.RW~3_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000000000000110111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_state_r.INITRFSH~0_combout\,
	datab => \SDRAM1|u1|ALT_INV_rfshCntr_r[4]~0_combout\,
	datac => \SDRAM1|u1|ALT_INV_state_x.INITSETMODE~1_combout\,
	datad => \SDRAM1|u1|ALT_INV_state_x.RW~3_combout\,
	datae => \SDRAM1|u1|ALT_INV_state_r.INITRFSH~q\,
	dataf => \SDRAM1|u1|ALT_INV_state_x.RW~2_combout\,
	combout => \SDRAM1|u1|state_r.INITRFSH~1_combout\);

-- Location: FF_X53_Y21_N13
\SDRAM1|u1|state_r.INITRFSH\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|state_r.INITRFSH~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|state_r.INITRFSH~q\);

-- Location: LABCELL_X50_Y21_N24
\SDRAM1|u1|WideOr16~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|WideOr16~0_combout\ = (!\SDRAM1|u1|state_r.INITSETMODE~q\ & !\SDRAM1|u1|state_r.ACTIVATE~q\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000100010001000100010001000100010001000100010001000100010001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_state_r.INITSETMODE~q\,
	datab => \SDRAM1|u1|ALT_INV_state_r.ACTIVATE~q\,
	combout => \SDRAM1|u1|WideOr16~0_combout\);

-- Location: LABCELL_X50_Y21_N30
\SDRAM1|u1|state_x.INITPCHG~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|state_x.INITPCHG~0_combout\ = ( !\SDRAM1|u1|state_r.INITPCHG~q\ & ( \SDRAM1|u1|WideOr16~0_combout\ & ( (!\SDRAM1|u1|state_r.INITRFSH~q\ & (!\SDRAM1|u1|state_r.REFRESHROW~q\ & (!\SDRAM1|u1|state_r.SELFREFRESH~q\ & !\SDRAM1|u1|state_r.RW~q\))) ) 
-- ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_state_r.INITRFSH~q\,
	datab => \SDRAM1|u1|ALT_INV_state_r.REFRESHROW~q\,
	datac => \SDRAM1|u1|ALT_INV_state_r.SELFREFRESH~q\,
	datad => \SDRAM1|u1|ALT_INV_state_r.RW~q\,
	datae => \SDRAM1|u1|ALT_INV_state_r.INITPCHG~q\,
	dataf => \SDRAM1|u1|ALT_INV_WideOr16~0_combout\,
	combout => \SDRAM1|u1|state_x.INITPCHG~0_combout\);

-- Location: FF_X50_Y21_N32
\SDRAM1|u1|state_r.INITPCHG\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|state_x.INITPCHG~0_combout\,
	ena => \SDRAM1|u1|state_x.INITSETMODE~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|state_r.INITPCHG~q\);

-- Location: LABCELL_X45_Y20_N3
\SDRAM1|u1|rfshCntr_x[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|rfshCntr_x[0]~0_combout\ = ( \SDRAM1|u1|Equal7~3_combout\ & ( (\SDRAM1|u1|Add4~1_sumout\ & (((!\SDRAM1|u1|state_r.INITPCHG~q\) # (\SDRAM1|u1|WideOr18~combout\)) # (\SDRAM1|u1|Selector31~0_combout\))) ) ) # ( !\SDRAM1|u1|Equal7~3_combout\ & ( 
-- \SDRAM1|u1|Add4~1_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000110111110000000011011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Selector31~0_combout\,
	datab => \SDRAM1|u1|ALT_INV_state_r.INITPCHG~q\,
	datac => \SDRAM1|u1|ALT_INV_WideOr18~combout\,
	datad => \SDRAM1|u1|ALT_INV_Add4~1_sumout\,
	dataf => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	combout => \SDRAM1|u1|rfshCntr_x[0]~0_combout\);

-- Location: FF_X45_Y20_N5
\SDRAM1|u1|rfshCntr_r[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|rfshCntr_x[0]~0_combout\,
	sclr => \SDRAM1|u1|rfshCntr_r[4]~1_combout\,
	ena => \SDRAM1|u1|rfshCntr_r[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rfshCntr_r\(0));

-- Location: LABCELL_X45_Y20_N15
\SDRAM1|u1|state_x.INITSETMODE~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|state_x.INITSETMODE~2_combout\ = ( \SDRAM1|u1|rfshCntr_r[5]~DUPLICATE_q\ & ( \SDRAM1|u1|state_r.INITRFSH~q\ ) ) # ( !\SDRAM1|u1|rfshCntr_r[5]~DUPLICATE_q\ & ( (\SDRAM1|u1|state_r.INITRFSH~q\ & ((!\SDRAM1|u1|rfshCntr_r\(0)) # 
-- ((!\SDRAM1|u1|Equal8~1_combout\) # (!\SDRAM1|u1|Equal8~0_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001110000011110000111000001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_rfshCntr_r\(0),
	datab => \SDRAM1|u1|ALT_INV_Equal8~1_combout\,
	datac => \SDRAM1|u1|ALT_INV_state_r.INITRFSH~q\,
	datad => \SDRAM1|u1|ALT_INV_Equal8~0_combout\,
	dataf => \SDRAM1|u1|ALT_INV_rfshCntr_r[5]~DUPLICATE_q\,
	combout => \SDRAM1|u1|state_x.INITSETMODE~2_combout\);

-- Location: LABCELL_X50_Y21_N42
\SDRAM1|u1|state_r.RW~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|state_r.RW~0_combout\ = ( !\SDRAM1|u1|state_r.SELFREFRESH~q\ & ( (!\SDRAM1|u1|state_r.ACTIVATE~q\ & (!\SDRAM1|u1|state_r.REFRESHROW~q\ & !\SDRAM1|u1|state_r.INITSETMODE~q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000010000000100000001000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_state_r.ACTIVATE~q\,
	datab => \SDRAM1|u1|ALT_INV_state_r.REFRESHROW~q\,
	datac => \SDRAM1|u1|ALT_INV_state_r.INITSETMODE~q\,
	dataf => \SDRAM1|u1|ALT_INV_state_r.SELFREFRESH~q\,
	combout => \SDRAM1|u1|state_r.RW~0_combout\);

-- Location: LABCELL_X53_Y21_N18
\SDRAM1|u1|state_x.INITSETMODE~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|state_x.INITSETMODE~4_combout\ = ( \SDRAM1|u1|state_r.RW~q\ & ( \SDRAM1|u1|doActivate~combout\ & ( (!\SDRAM1|u1|Selector31~0_combout\ & (\SDRAM1|u1|Equal7~3_combout\ & ((!\SDRAM1|u1|state_x.RW~0_combout\) # 
-- (!\SDRAM1|u1|sAddr_x[10]~12_combout\)))) ) ) ) # ( !\SDRAM1|u1|state_r.RW~q\ & ( \SDRAM1|u1|doActivate~combout\ & ( (!\SDRAM1|u1|Selector31~0_combout\ & \SDRAM1|u1|Equal7~3_combout\) ) ) ) # ( \SDRAM1|u1|state_r.RW~q\ & ( !\SDRAM1|u1|doActivate~combout\ & 
-- ( (!\SDRAM1|u1|Selector31~0_combout\ & \SDRAM1|u1|Equal7~3_combout\) ) ) ) # ( !\SDRAM1|u1|state_r.RW~q\ & ( !\SDRAM1|u1|doActivate~combout\ & ( (!\SDRAM1|u1|Selector31~0_combout\ & \SDRAM1|u1|Equal7~3_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011110000000000001111000000000000111100000000000011100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_state_x.RW~0_combout\,
	datab => \SDRAM1|u1|ALT_INV_sAddr_x[10]~12_combout\,
	datac => \SDRAM1|u1|ALT_INV_Selector31~0_combout\,
	datad => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datae => \SDRAM1|u1|ALT_INV_state_r.RW~q\,
	dataf => \SDRAM1|u1|ALT_INV_doActivate~combout\,
	combout => \SDRAM1|u1|state_x.INITSETMODE~4_combout\);

-- Location: LABCELL_X51_Y21_N30
\SDRAM1|u1|state_x.RW~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|state_x.RW~1_combout\ = ( \SDRAM1|u1|doActivate~combout\ & ( (\SDRAM1|u1|Equal9~0_combout\ & !\SDRAM1|u1|doSelfRfsh~3_combout\) ) ) # ( !\SDRAM1|u1|doActivate~combout\ & ( (\SDRAM1|u1|Equal9~0_combout\ & (!\SDRAM1|u1|doSelfRfsh~3_combout\ & 
-- !\SDRAM1|u1|sAddr_x[10]~12_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000000000001100000000000000110000001100000011000000110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \SDRAM1|u1|ALT_INV_Equal9~0_combout\,
	datac => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	datad => \SDRAM1|u1|ALT_INV_sAddr_x[10]~12_combout\,
	dataf => \SDRAM1|u1|ALT_INV_doActivate~combout\,
	combout => \SDRAM1|u1|state_x.RW~1_combout\);

-- Location: LABCELL_X53_Y21_N6
\SDRAM1|u1|state_r.RW~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|state_r.RW~1_combout\ = ( \SDRAM1|u1|state_r.RW~q\ & ( \SDRAM1|u1|state_x.RW~2_combout\ ) ) # ( \SDRAM1|u1|state_r.RW~q\ & ( !\SDRAM1|u1|state_x.RW~2_combout\ & ( (!\SDRAM1|u1|state_r.RW~0_combout\) # 
-- ((!\SDRAM1|u1|state_x.INITSETMODE~4_combout\) # (\SDRAM1|u1|state_x.RW~1_combout\)) ) ) ) # ( !\SDRAM1|u1|state_r.RW~q\ & ( !\SDRAM1|u1|state_x.RW~2_combout\ & ( (!\SDRAM1|u1|state_x.INITSETMODE~2_combout\ & (!\SDRAM1|u1|state_r.RW~0_combout\ & 
-- \SDRAM1|u1|state_x.INITSETMODE~4_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100000001000111111001111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_state_x.INITSETMODE~2_combout\,
	datab => \SDRAM1|u1|ALT_INV_state_r.RW~0_combout\,
	datac => \SDRAM1|u1|ALT_INV_state_x.INITSETMODE~4_combout\,
	datad => \SDRAM1|u1|ALT_INV_state_x.RW~1_combout\,
	datae => \SDRAM1|u1|ALT_INV_state_r.RW~q\,
	dataf => \SDRAM1|u1|ALT_INV_state_x.RW~2_combout\,
	combout => \SDRAM1|u1|state_r.RW~1_combout\);

-- Location: FF_X53_Y21_N7
\SDRAM1|u1|state_r.RW\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|state_r.RW~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|state_r.RW~q\);

-- Location: LABCELL_X50_Y22_N18
\SDRAM1|u1|Selector22~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Selector22~1_combout\ = ( \SDRAM1|u1|state_r.SELFREFRESH~q\ & ( \SDRAM1|u1|combinatorial~0_combout\ & ( \SDRAM1|u1|doSelfRfsh~3_combout\ ) ) ) # ( !\SDRAM1|u1|state_r.SELFREFRESH~q\ & ( \SDRAM1|u1|combinatorial~0_combout\ & ( 
-- (\SDRAM1|u1|doSelfRfsh~3_combout\ & (\SDRAM1|u1|state_r.RW~q\ & (\SDRAM1|u1|rasTimer_r[2]~0_combout\ & \SDRAM1|u1|Equal9~0_combout\))) ) ) ) # ( \SDRAM1|u1|state_r.SELFREFRESH~q\ & ( !\SDRAM1|u1|combinatorial~0_combout\ & ( 
-- \SDRAM1|u1|doSelfRfsh~3_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000010101010101010100000000000000010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	datab => \SDRAM1|u1|ALT_INV_state_r.RW~q\,
	datac => \SDRAM1|u1|ALT_INV_rasTimer_r[2]~0_combout\,
	datad => \SDRAM1|u1|ALT_INV_Equal9~0_combout\,
	datae => \SDRAM1|u1|ALT_INV_state_r.SELFREFRESH~q\,
	dataf => \SDRAM1|u1|ALT_INV_combinatorial~0_combout\,
	combout => \SDRAM1|u1|Selector22~1_combout\);

-- Location: FF_X50_Y22_N20
\SDRAM1|u1|state_r.SELFREFRESH\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|Selector22~1_combout\,
	ena => \SDRAM1|u1|Equal7~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|state_r.SELFREFRESH~q\);

-- Location: MLABCELL_X52_Y22_N45
\SDRAM1|u1|timer_r[1]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|timer_r[1]~0_combout\ = ( \SDRAM1|u1|Equal7~2_combout\ & ( \SDRAM1|u1|Equal7~0_combout\ & ( (\SDRAM1|u1|Equal7~1_combout\ & (\SDRAM1|u1|state_r.INITWAIT~q\ & !\SDRAM1|u1|state_r.SELFREFRESH~q\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000001000000010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal7~1_combout\,
	datab => \SDRAM1|u1|ALT_INV_state_r.INITWAIT~q\,
	datac => \SDRAM1|u1|ALT_INV_state_r.SELFREFRESH~q\,
	datae => \SDRAM1|u1|ALT_INV_Equal7~2_combout\,
	dataf => \SDRAM1|u1|ALT_INV_Equal7~0_combout\,
	combout => \SDRAM1|u1|timer_r[1]~0_combout\);

-- Location: LABCELL_X51_Y22_N0
\SDRAM1|u1|Add5~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add5~9_sumout\ = SUM(( \SDRAM1|u1|timer_r\(0) ) + ( VCC ) + ( !VCC ))
-- \SDRAM1|u1|Add5~10\ = CARRY(( \SDRAM1|u1|timer_r\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \SDRAM1|u1|ALT_INV_timer_r\(0),
	cin => GND,
	sumout => \SDRAM1|u1|Add5~9_sumout\,
	cout => \SDRAM1|u1|Add5~10\);

-- Location: LABCELL_X50_Y21_N45
\SDRAM1|u1|timer_x[0]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|timer_x[0]~1_combout\ = ( \SDRAM1|u1|Equal7~1_combout\ & ( (\SDRAM1|u1|Equal7~0_combout\ & (\SDRAM1|u1|Equal7~2_combout\ & ((\SDRAM1|u1|state_r.REFRESHROW~q\) # (\SDRAM1|u1|state_r.ACTIVATE~q\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000001110000000000000111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_state_r.ACTIVATE~q\,
	datab => \SDRAM1|u1|ALT_INV_state_r.REFRESHROW~q\,
	datac => \SDRAM1|u1|ALT_INV_Equal7~0_combout\,
	datad => \SDRAM1|u1|ALT_INV_Equal7~2_combout\,
	dataf => \SDRAM1|u1|ALT_INV_Equal7~1_combout\,
	combout => \SDRAM1|u1|timer_x[0]~1_combout\);

-- Location: LABCELL_X50_Y21_N48
\SDRAM1|u1|timer_x[0]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|timer_x[0]~2_combout\ = ( \SDRAM1|u1|state_r.INITWAIT~q\ & ( \SDRAM1|u1|state_r.INITRFSH~q\ & ( (!\SDRAM1|u1|Add5~9_sumout\ & (!\SDRAM1|u1|timer_x[0]~1_combout\ & !\SDRAM1|u1|Equal7~3_combout\)) ) ) ) # ( !\SDRAM1|u1|state_r.INITWAIT~q\ & ( 
-- \SDRAM1|u1|state_r.INITRFSH~q\ & ( (!\SDRAM1|u1|Add5~9_sumout\ & (!\SDRAM1|u1|timer_x[0]~1_combout\ & !\SDRAM1|u1|Equal7~3_combout\)) ) ) ) # ( \SDRAM1|u1|state_r.INITWAIT~q\ & ( !\SDRAM1|u1|state_r.INITRFSH~q\ & ( (!\SDRAM1|u1|timer_x[0]~1_combout\ & 
-- ((!\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|Add5~9_sumout\)) # (\SDRAM1|u1|Equal7~3_combout\ & ((!\SDRAM1|u1|state_r.INITPCHG~q\))))) ) ) ) # ( !\SDRAM1|u1|state_r.INITWAIT~q\ & ( !\SDRAM1|u1|state_r.INITRFSH~q\ & ( (!\SDRAM1|u1|Add5~9_sumout\ & 
-- (!\SDRAM1|u1|timer_x[0]~1_combout\ & !\SDRAM1|u1|Equal7~3_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000010000000100011001000000010000000100000001000000010000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Add5~9_sumout\,
	datab => \SDRAM1|u1|ALT_INV_timer_x[0]~1_combout\,
	datac => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datad => \SDRAM1|u1|ALT_INV_state_r.INITPCHG~q\,
	datae => \SDRAM1|u1|ALT_INV_state_r.INITWAIT~q\,
	dataf => \SDRAM1|u1|ALT_INV_state_r.INITRFSH~q\,
	combout => \SDRAM1|u1|timer_x[0]~2_combout\);

-- Location: LABCELL_X53_Y21_N30
\SDRAM1|u1|timer_x[0]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|timer_x[0]~3_combout\ = ( \SDRAM1|u1|state_x.RW~0_combout\ & ( \SDRAM1|u1|doActivate~combout\ & ( !\SDRAM1|u1|timer_x[0]~2_combout\ ) ) ) # ( !\SDRAM1|u1|state_x.RW~0_combout\ & ( \SDRAM1|u1|doActivate~combout\ & ( 
-- (!\SDRAM1|u1|timer_x[0]~2_combout\) # ((\SDRAM1|u1|Selector22~0_combout\ & \SDRAM1|u1|Equal7~3_combout\)) ) ) ) # ( \SDRAM1|u1|state_x.RW~0_combout\ & ( !\SDRAM1|u1|doActivate~combout\ & ( (!\SDRAM1|u1|timer_x[0]~2_combout\) # 
-- ((\SDRAM1|u1|sAddr_x[10]~12_combout\ & (\SDRAM1|u1|Selector22~0_combout\ & \SDRAM1|u1|Equal7~3_combout\))) ) ) ) # ( !\SDRAM1|u1|state_x.RW~0_combout\ & ( !\SDRAM1|u1|doActivate~combout\ & ( (!\SDRAM1|u1|timer_x[0]~2_combout\) # 
-- ((\SDRAM1|u1|Selector22~0_combout\ & \SDRAM1|u1|Equal7~3_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101111101010101010101110101010101011111010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_timer_x[0]~2_combout\,
	datab => \SDRAM1|u1|ALT_INV_sAddr_x[10]~12_combout\,
	datac => \SDRAM1|u1|ALT_INV_Selector22~0_combout\,
	datad => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datae => \SDRAM1|u1|ALT_INV_state_x.RW~0_combout\,
	dataf => \SDRAM1|u1|ALT_INV_doActivate~combout\,
	combout => \SDRAM1|u1|timer_x[0]~3_combout\);

-- Location: FF_X53_Y21_N31
\SDRAM1|u1|timer_r[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|timer_x[0]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|timer_r\(0));

-- Location: LABCELL_X51_Y22_N3
\SDRAM1|u1|Add5~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add5~5_sumout\ = SUM(( \SDRAM1|u1|timer_r\(1) ) + ( VCC ) + ( \SDRAM1|u1|Add5~10\ ))
-- \SDRAM1|u1|Add5~6\ = CARRY(( \SDRAM1|u1|timer_r\(1) ) + ( VCC ) + ( \SDRAM1|u1|Add5~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \SDRAM1|u1|ALT_INV_timer_r\(1),
	cin => \SDRAM1|u1|Add5~10\,
	sumout => \SDRAM1|u1|Add5~5_sumout\,
	cout => \SDRAM1|u1|Add5~6\);

-- Location: LABCELL_X50_Y22_N54
\SDRAM1|u1|state_x.INITSETMODE~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|state_x.INITSETMODE~0_combout\ = ( \SDRAM1|u1|doActivate~combout\ & ( (\SDRAM1|u1|state_r.RW~q\ & (!\SDRAM1|u1|doSelfRfsh~3_combout\ & \SDRAM1|u1|Equal9~0_combout\)) ) ) # ( !\SDRAM1|u1|doActivate~combout\ & ( 
-- (!\SDRAM1|u1|sAddr_x[10]~12_combout\ & (\SDRAM1|u1|state_r.RW~q\ & (!\SDRAM1|u1|doSelfRfsh~3_combout\ & \SDRAM1|u1|Equal9~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000100000000000000010000000000000001100000000000000110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_sAddr_x[10]~12_combout\,
	datab => \SDRAM1|u1|ALT_INV_state_r.RW~q\,
	datac => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	datad => \SDRAM1|u1|ALT_INV_Equal9~0_combout\,
	dataf => \SDRAM1|u1|ALT_INV_doActivate~combout\,
	combout => \SDRAM1|u1|state_x.INITSETMODE~0_combout\);

-- Location: MLABCELL_X52_Y22_N33
\SDRAM1|u1|cke_x~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|cke_x~0_combout\ = ( \SDRAM1|u1|Equal7~2_combout\ & ( \SDRAM1|u1|Equal7~0_combout\ & ( (!\SDRAM1|u1|Selector31~0_combout\) # (!\SDRAM1|u1|Equal7~1_combout\) ) ) ) # ( !\SDRAM1|u1|Equal7~2_combout\ & ( \SDRAM1|u1|Equal7~0_combout\ ) ) # ( 
-- \SDRAM1|u1|Equal7~2_combout\ & ( !\SDRAM1|u1|Equal7~0_combout\ ) ) # ( !\SDRAM1|u1|Equal7~2_combout\ & ( !\SDRAM1|u1|Equal7~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \SDRAM1|u1|ALT_INV_Selector31~0_combout\,
	datad => \SDRAM1|u1|ALT_INV_Equal7~1_combout\,
	datae => \SDRAM1|u1|ALT_INV_Equal7~2_combout\,
	dataf => \SDRAM1|u1|ALT_INV_Equal7~0_combout\,
	combout => \SDRAM1|u1|cke_x~0_combout\);

-- Location: LABCELL_X50_Y22_N0
\SDRAM1|u1|timer_r[1]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|timer_r[1]~1_combout\ = ( \SDRAM1|u1|state_x.INITSETMODE~0_combout\ & ( \SDRAM1|u1|cke_x~0_combout\ & ( (!\SDRAM1|u1|Equal7~3_combout\ & ((\SDRAM1|u1|Add5~5_sumout\) # (\SDRAM1|u1|timer_r[1]~0_combout\))) ) ) ) # ( 
-- !\SDRAM1|u1|state_x.INITSETMODE~0_combout\ & ( \SDRAM1|u1|cke_x~0_combout\ & ( (!\SDRAM1|u1|timer_r[1]~0_combout\ & (!\SDRAM1|u1|Equal7~3_combout\ & (\SDRAM1|u1|Add5~5_sumout\))) # (\SDRAM1|u1|timer_r[1]~0_combout\ & ((!\SDRAM1|u1|Equal7~3_combout\) # 
-- ((!\SDRAM1|u1|Selector28~0_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001011101010011000100110001001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_timer_r[1]~0_combout\,
	datab => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datac => \SDRAM1|u1|ALT_INV_Add5~5_sumout\,
	datad => \SDRAM1|u1|ALT_INV_Selector28~0_combout\,
	datae => \SDRAM1|u1|ALT_INV_state_x.INITSETMODE~0_combout\,
	dataf => \SDRAM1|u1|ALT_INV_cke_x~0_combout\,
	combout => \SDRAM1|u1|timer_r[1]~1_combout\);

-- Location: FF_X50_Y22_N1
\SDRAM1|u1|timer_r[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|timer_r[1]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|timer_r\(1));

-- Location: LABCELL_X51_Y22_N6
\SDRAM1|u1|Add5~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add5~1_sumout\ = SUM(( \SDRAM1|u1|timer_r\(2) ) + ( VCC ) + ( \SDRAM1|u1|Add5~6\ ))
-- \SDRAM1|u1|Add5~2\ = CARRY(( \SDRAM1|u1|timer_r\(2) ) + ( VCC ) + ( \SDRAM1|u1|Add5~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \SDRAM1|u1|ALT_INV_timer_r\(2),
	cin => \SDRAM1|u1|Add5~6\,
	sumout => \SDRAM1|u1|Add5~1_sumout\,
	cout => \SDRAM1|u1|Add5~2\);

-- Location: LABCELL_X50_Y21_N12
\SDRAM1|u1|timer_x[2]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|timer_x[2]~0_combout\ = ( \SDRAM1|u1|Equal7~0_combout\ & ( \SDRAM1|u1|state_r.INITRFSH~q\ & ( ((\SDRAM1|u1|Equal7~1_combout\ & \SDRAM1|u1|Equal7~2_combout\)) # (\SDRAM1|u1|Add5~1_sumout\) ) ) ) # ( !\SDRAM1|u1|Equal7~0_combout\ & ( 
-- \SDRAM1|u1|state_r.INITRFSH~q\ & ( \SDRAM1|u1|Add5~1_sumout\ ) ) ) # ( \SDRAM1|u1|Equal7~0_combout\ & ( !\SDRAM1|u1|state_r.INITRFSH~q\ & ( (!\SDRAM1|u1|Equal7~1_combout\ & (\SDRAM1|u1|Add5~1_sumout\)) # (\SDRAM1|u1|Equal7~1_combout\ & 
-- ((!\SDRAM1|u1|Equal7~2_combout\ & (\SDRAM1|u1|Add5~1_sumout\)) # (\SDRAM1|u1|Equal7~2_combout\ & ((\SDRAM1|u1|state_r.REFRESHROW~q\))))) ) ) ) # ( !\SDRAM1|u1|Equal7~0_combout\ & ( !\SDRAM1|u1|state_r.INITRFSH~q\ & ( \SDRAM1|u1|Add5~1_sumout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101000101011101010101010101010101011101010111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Add5~1_sumout\,
	datab => \SDRAM1|u1|ALT_INV_Equal7~1_combout\,
	datac => \SDRAM1|u1|ALT_INV_Equal7~2_combout\,
	datad => \SDRAM1|u1|ALT_INV_state_r.REFRESHROW~q\,
	datae => \SDRAM1|u1|ALT_INV_Equal7~0_combout\,
	dataf => \SDRAM1|u1|ALT_INV_state_r.INITRFSH~q\,
	combout => \SDRAM1|u1|timer_x[2]~0_combout\);

-- Location: FF_X50_Y21_N13
\SDRAM1|u1|timer_r[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|timer_x[2]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|timer_r\(2));

-- Location: LABCELL_X51_Y22_N48
\SDRAM1|u1|Equal7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Equal7~0_combout\ = ( !\SDRAM1|u1|timer_r\(0) & ( !\SDRAM1|u1|timer_r\(1) & ( !\SDRAM1|u1|timer_r\(2) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011001100000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \SDRAM1|u1|ALT_INV_timer_r\(2),
	datae => \SDRAM1|u1|ALT_INV_timer_r\(0),
	dataf => \SDRAM1|u1|ALT_INV_timer_r\(1),
	combout => \SDRAM1|u1|Equal7~0_combout\);

-- Location: MLABCELL_X47_Y21_N6
\SDRAM1|u1|rfshCntr_r[4]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|rfshCntr_r[4]~0_combout\ = ( \SDRAM1|u1|Equal7~1_combout\ & ( (\SDRAM1|u1|Equal7~0_combout\ & (\SDRAM1|u1|Equal7~2_combout\ & !\SDRAM1|u1|Selector31~0_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000101000000000000010100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal7~0_combout\,
	datac => \SDRAM1|u1|ALT_INV_Equal7~2_combout\,
	datad => \SDRAM1|u1|ALT_INV_Selector31~0_combout\,
	dataf => \SDRAM1|u1|ALT_INV_Equal7~1_combout\,
	combout => \SDRAM1|u1|rfshCntr_r[4]~0_combout\);

-- Location: LABCELL_X51_Y21_N36
\SDRAM1|u1|state_x.INITSETMODE~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|state_x.INITSETMODE~3_combout\ = ( \SDRAM1|u1|state_x.RW~1_combout\ & ( (\SDRAM1|u1|rfshCntr_r[4]~0_combout\ & (!\SDRAM1|u1|state_x.INITSETMODE~2_combout\ & !\SDRAM1|u1|state_x.INITSETMODE~1_combout\)) ) ) # ( !\SDRAM1|u1|state_x.RW~1_combout\ 
-- & ( (\SDRAM1|u1|rfshCntr_r[4]~0_combout\ & (!\SDRAM1|u1|Selector28~0_combout\ & (!\SDRAM1|u1|state_x.INITSETMODE~2_combout\ & !\SDRAM1|u1|state_x.INITSETMODE~1_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100000000000000010000000000000001010000000000000101000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_rfshCntr_r[4]~0_combout\,
	datab => \SDRAM1|u1|ALT_INV_Selector28~0_combout\,
	datac => \SDRAM1|u1|ALT_INV_state_x.INITSETMODE~2_combout\,
	datad => \SDRAM1|u1|ALT_INV_state_x.INITSETMODE~1_combout\,
	dataf => \SDRAM1|u1|ALT_INV_state_x.RW~1_combout\,
	combout => \SDRAM1|u1|state_x.INITSETMODE~3_combout\);

-- Location: FF_X51_Y21_N40
\SDRAM1|u1|state_r.INITWAIT\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|state_r.INITWAIT~feeder_combout\,
	ena => \SDRAM1|u1|state_x.INITSETMODE~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|state_r.INITWAIT~q\);

-- Location: FF_X50_Y19_N34
\SDRAM1|u1|timer_r[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|timer_r[7]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|timer_r\(7));

-- Location: LABCELL_X51_Y22_N9
\SDRAM1|u1|Add5~57\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add5~57_sumout\ = SUM(( \SDRAM1|u1|timer_r\(3) ) + ( VCC ) + ( \SDRAM1|u1|Add5~2\ ))
-- \SDRAM1|u1|Add5~58\ = CARRY(( \SDRAM1|u1|timer_r\(3) ) + ( VCC ) + ( \SDRAM1|u1|Add5~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \SDRAM1|u1|ALT_INV_timer_r\(3),
	cin => \SDRAM1|u1|Add5~2\,
	sumout => \SDRAM1|u1|Add5~57_sumout\,
	cout => \SDRAM1|u1|Add5~58\);

-- Location: MLABCELL_X47_Y21_N0
\SDRAM1|u1|timer_x[3]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|timer_x[3]~4_combout\ = ( \SDRAM1|u1|Equal7~0_combout\ & ( \SDRAM1|u1|doSelfRfsh~3_combout\ & ( (\SDRAM1|u1|Add5~57_sumout\ & ((!\SDRAM1|u1|Equal7~2_combout\) # (!\SDRAM1|u1|Equal7~1_combout\))) ) ) ) # ( !\SDRAM1|u1|Equal7~0_combout\ & ( 
-- \SDRAM1|u1|doSelfRfsh~3_combout\ & ( \SDRAM1|u1|Add5~57_sumout\ ) ) ) # ( \SDRAM1|u1|Equal7~0_combout\ & ( !\SDRAM1|u1|doSelfRfsh~3_combout\ & ( (!\SDRAM1|u1|Equal7~2_combout\ & (((\SDRAM1|u1|Add5~57_sumout\)))) # (\SDRAM1|u1|Equal7~2_combout\ & 
-- ((!\SDRAM1|u1|Equal7~1_combout\ & ((\SDRAM1|u1|Add5~57_sumout\))) # (\SDRAM1|u1|Equal7~1_combout\ & (\SDRAM1|u1|state_r.SELFREFRESH~q\)))) ) ) ) # ( !\SDRAM1|u1|Equal7~0_combout\ & ( !\SDRAM1|u1|doSelfRfsh~3_combout\ & ( \SDRAM1|u1|Add5~57_sumout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000011111101100000000111111110000000011111010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal7~2_combout\,
	datab => \SDRAM1|u1|ALT_INV_state_r.SELFREFRESH~q\,
	datac => \SDRAM1|u1|ALT_INV_Equal7~1_combout\,
	datad => \SDRAM1|u1|ALT_INV_Add5~57_sumout\,
	datae => \SDRAM1|u1|ALT_INV_Equal7~0_combout\,
	dataf => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	combout => \SDRAM1|u1|timer_x[3]~4_combout\);

-- Location: FF_X47_Y21_N2
\SDRAM1|u1|timer_r[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|timer_x[3]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|timer_r\(3));

-- Location: LABCELL_X51_Y22_N12
\SDRAM1|u1|Add5~53\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add5~53_sumout\ = SUM(( \SDRAM1|u1|timer_r\(4) ) + ( VCC ) + ( \SDRAM1|u1|Add5~58\ ))
-- \SDRAM1|u1|Add5~54\ = CARRY(( \SDRAM1|u1|timer_r\(4) ) + ( VCC ) + ( \SDRAM1|u1|Add5~58\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \SDRAM1|u1|ALT_INV_timer_r\(4),
	cin => \SDRAM1|u1|Add5~58\,
	sumout => \SDRAM1|u1|Add5~53_sumout\,
	cout => \SDRAM1|u1|Add5~54\);

-- Location: LABCELL_X50_Y19_N39
\SDRAM1|u1|timer_r[4]~12\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|timer_r[4]~12_combout\ = ( \SDRAM1|u1|cke_x~0_combout\ & ( (!\SDRAM1|u1|Equal7~3_combout\ & \SDRAM1|u1|Add5~53_sumout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001100000011000000110000001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datac => \SDRAM1|u1|ALT_INV_Add5~53_sumout\,
	dataf => \SDRAM1|u1|ALT_INV_cke_x~0_combout\,
	combout => \SDRAM1|u1|timer_r[4]~12_combout\);

-- Location: FF_X50_Y19_N41
\SDRAM1|u1|timer_r[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|timer_r[4]~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|timer_r\(4));

-- Location: LABCELL_X51_Y22_N15
\SDRAM1|u1|Add5~49\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add5~49_sumout\ = SUM(( \SDRAM1|u1|timer_r\(5) ) + ( VCC ) + ( \SDRAM1|u1|Add5~54\ ))
-- \SDRAM1|u1|Add5~50\ = CARRY(( \SDRAM1|u1|timer_r\(5) ) + ( VCC ) + ( \SDRAM1|u1|Add5~54\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_timer_r\(5),
	cin => \SDRAM1|u1|Add5~54\,
	sumout => \SDRAM1|u1|Add5~49_sumout\,
	cout => \SDRAM1|u1|Add5~50\);

-- Location: LABCELL_X50_Y22_N6
\SDRAM1|u1|timer_r[5]~11\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|timer_r[5]~11_combout\ = ( \SDRAM1|u1|state_x.INITSETMODE~0_combout\ & ( \SDRAM1|u1|cke_x~0_combout\ & ( (\SDRAM1|u1|Add5~49_sumout\ & !\SDRAM1|u1|Equal7~3_combout\) ) ) ) # ( !\SDRAM1|u1|state_x.INITSETMODE~0_combout\ & ( 
-- \SDRAM1|u1|cke_x~0_combout\ & ( (!\SDRAM1|u1|Equal7~3_combout\ & (\SDRAM1|u1|Add5~49_sumout\)) # (\SDRAM1|u1|Equal7~3_combout\ & (((!\SDRAM1|u1|state_r.INITWAIT~q\ & !\SDRAM1|u1|Selector28~0_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001110100010001000100010001000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Add5~49_sumout\,
	datab => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datac => \SDRAM1|u1|ALT_INV_state_r.INITWAIT~q\,
	datad => \SDRAM1|u1|ALT_INV_Selector28~0_combout\,
	datae => \SDRAM1|u1|ALT_INV_state_x.INITSETMODE~0_combout\,
	dataf => \SDRAM1|u1|ALT_INV_cke_x~0_combout\,
	combout => \SDRAM1|u1|timer_r[5]~11_combout\);

-- Location: FF_X50_Y22_N7
\SDRAM1|u1|timer_r[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|timer_r[5]~11_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|timer_r\(5));

-- Location: LABCELL_X51_Y22_N18
\SDRAM1|u1|Add5~45\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add5~45_sumout\ = SUM(( \SDRAM1|u1|timer_r\(6) ) + ( VCC ) + ( \SDRAM1|u1|Add5~50\ ))
-- \SDRAM1|u1|Add5~46\ = CARRY(( \SDRAM1|u1|timer_r\(6) ) + ( VCC ) + ( \SDRAM1|u1|Add5~50\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \SDRAM1|u1|ALT_INV_timer_r\(6),
	cin => \SDRAM1|u1|Add5~50\,
	sumout => \SDRAM1|u1|Add5~45_sumout\,
	cout => \SDRAM1|u1|Add5~46\);

-- Location: LABCELL_X50_Y19_N36
\SDRAM1|u1|timer_r[6]~10\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|timer_r[6]~10_combout\ = ( \SDRAM1|u1|cke_x~0_combout\ & ( (!\SDRAM1|u1|Equal7~3_combout\ & \SDRAM1|u1|Add5~45_sumout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001100000011000000110000001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datac => \SDRAM1|u1|ALT_INV_Add5~45_sumout\,
	dataf => \SDRAM1|u1|ALT_INV_cke_x~0_combout\,
	combout => \SDRAM1|u1|timer_r[6]~10_combout\);

-- Location: FF_X50_Y19_N38
\SDRAM1|u1|timer_r[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|timer_r[6]~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|timer_r\(6));

-- Location: LABCELL_X51_Y22_N21
\SDRAM1|u1|Add5~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add5~41_sumout\ = SUM(( \SDRAM1|u1|timer_r\(7) ) + ( VCC ) + ( \SDRAM1|u1|Add5~46\ ))
-- \SDRAM1|u1|Add5~42\ = CARRY(( \SDRAM1|u1|timer_r\(7) ) + ( VCC ) + ( \SDRAM1|u1|Add5~46\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \SDRAM1|u1|ALT_INV_timer_r\(7),
	cin => \SDRAM1|u1|Add5~46\,
	sumout => \SDRAM1|u1|Add5~41_sumout\,
	cout => \SDRAM1|u1|Add5~42\);

-- Location: LABCELL_X51_Y22_N24
\SDRAM1|u1|Add5~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add5~37_sumout\ = SUM(( \SDRAM1|u1|timer_r\(8) ) + ( VCC ) + ( \SDRAM1|u1|Add5~42\ ))
-- \SDRAM1|u1|Add5~38\ = CARRY(( \SDRAM1|u1|timer_r\(8) ) + ( VCC ) + ( \SDRAM1|u1|Add5~42\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \SDRAM1|u1|ALT_INV_timer_r\(8),
	cin => \SDRAM1|u1|Add5~42\,
	sumout => \SDRAM1|u1|Add5~37_sumout\,
	cout => \SDRAM1|u1|Add5~38\);

-- Location: LABCELL_X50_Y19_N30
\SDRAM1|u1|timer_r[8]~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|timer_r[8]~8_combout\ = ( \SDRAM1|u1|cke_x~0_combout\ & ( (!\SDRAM1|u1|Equal7~3_combout\ & \SDRAM1|u1|Add5~37_sumout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001100000011000000110000001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datac => \SDRAM1|u1|ALT_INV_Add5~37_sumout\,
	dataf => \SDRAM1|u1|ALT_INV_cke_x~0_combout\,
	combout => \SDRAM1|u1|timer_r[8]~8_combout\);

-- Location: FF_X50_Y19_N32
\SDRAM1|u1|timer_r[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|timer_r[8]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|timer_r\(8));

-- Location: LABCELL_X51_Y22_N27
\SDRAM1|u1|Add5~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add5~33_sumout\ = SUM(( \SDRAM1|u1|timer_r\(9) ) + ( VCC ) + ( \SDRAM1|u1|Add5~38\ ))
-- \SDRAM1|u1|Add5~34\ = CARRY(( \SDRAM1|u1|timer_r\(9) ) + ( VCC ) + ( \SDRAM1|u1|Add5~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_timer_r\(9),
	cin => \SDRAM1|u1|Add5~38\,
	sumout => \SDRAM1|u1|Add5~33_sumout\,
	cout => \SDRAM1|u1|Add5~34\);

-- Location: LABCELL_X50_Y22_N27
\SDRAM1|u1|timer_r[9]~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|timer_r[9]~7_combout\ = ( \SDRAM1|u1|cke_x~0_combout\ & ( \SDRAM1|u1|Add5~33_sumout\ & ( (!\SDRAM1|u1|Equal7~3_combout\) # ((!\SDRAM1|u1|state_r.INITWAIT~q\ & (!\SDRAM1|u1|Selector28~0_combout\ & !\SDRAM1|u1|state_x.INITSETMODE~0_combout\))) ) 
-- ) ) # ( \SDRAM1|u1|cke_x~0_combout\ & ( !\SDRAM1|u1|Add5~33_sumout\ & ( (!\SDRAM1|u1|state_r.INITWAIT~q\ & (!\SDRAM1|u1|Selector28~0_combout\ & (\SDRAM1|u1|Equal7~3_combout\ & !\SDRAM1|u1|state_x.INITSETMODE~0_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000010000000000000000000000000001111100011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_state_r.INITWAIT~q\,
	datab => \SDRAM1|u1|ALT_INV_Selector28~0_combout\,
	datac => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datad => \SDRAM1|u1|ALT_INV_state_x.INITSETMODE~0_combout\,
	datae => \SDRAM1|u1|ALT_INV_cke_x~0_combout\,
	dataf => \SDRAM1|u1|ALT_INV_Add5~33_sumout\,
	combout => \SDRAM1|u1|timer_r[9]~7_combout\);

-- Location: FF_X50_Y22_N28
\SDRAM1|u1|timer_r[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|timer_r[9]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|timer_r\(9));

-- Location: LABCELL_X51_Y22_N30
\SDRAM1|u1|Add5~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add5~29_sumout\ = SUM(( \SDRAM1|u1|timer_r\(10) ) + ( VCC ) + ( \SDRAM1|u1|Add5~34\ ))
-- \SDRAM1|u1|Add5~30\ = CARRY(( \SDRAM1|u1|timer_r\(10) ) + ( VCC ) + ( \SDRAM1|u1|Add5~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \SDRAM1|u1|ALT_INV_timer_r\(10),
	cin => \SDRAM1|u1|Add5~34\,
	sumout => \SDRAM1|u1|Add5~29_sumout\,
	cout => \SDRAM1|u1|Add5~30\);

-- Location: LABCELL_X50_Y22_N24
\SDRAM1|u1|timer_r[10]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|timer_r[10]~6_combout\ = ( \SDRAM1|u1|cke_x~0_combout\ & ( \SDRAM1|u1|Add5~29_sumout\ & ( (!\SDRAM1|u1|Equal7~3_combout\) # ((!\SDRAM1|u1|state_r.INITWAIT~q\ & (!\SDRAM1|u1|Selector28~0_combout\ & !\SDRAM1|u1|state_x.INITSETMODE~0_combout\))) ) 
-- ) ) # ( \SDRAM1|u1|cke_x~0_combout\ & ( !\SDRAM1|u1|Add5~29_sumout\ & ( (!\SDRAM1|u1|state_r.INITWAIT~q\ & (!\SDRAM1|u1|Selector28~0_combout\ & (!\SDRAM1|u1|state_x.INITSETMODE~0_combout\ & \SDRAM1|u1|Equal7~3_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001000000000000000000000001111111110000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_state_r.INITWAIT~q\,
	datab => \SDRAM1|u1|ALT_INV_Selector28~0_combout\,
	datac => \SDRAM1|u1|ALT_INV_state_x.INITSETMODE~0_combout\,
	datad => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datae => \SDRAM1|u1|ALT_INV_cke_x~0_combout\,
	dataf => \SDRAM1|u1|ALT_INV_Add5~29_sumout\,
	combout => \SDRAM1|u1|timer_r[10]~6_combout\);

-- Location: FF_X50_Y22_N26
\SDRAM1|u1|timer_r[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|timer_r[10]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|timer_r\(10));

-- Location: FF_X50_Y22_N43
\SDRAM1|u1|timer_r[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|timer_r[13]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|timer_r\(13));

-- Location: LABCELL_X51_Y22_N33
\SDRAM1|u1|Add5~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add5~25_sumout\ = SUM(( \SDRAM1|u1|timer_r\(11) ) + ( VCC ) + ( \SDRAM1|u1|Add5~30\ ))
-- \SDRAM1|u1|Add5~26\ = CARRY(( \SDRAM1|u1|timer_r\(11) ) + ( VCC ) + ( \SDRAM1|u1|Add5~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_timer_r\(11),
	cin => \SDRAM1|u1|Add5~30\,
	sumout => \SDRAM1|u1|Add5~25_sumout\,
	cout => \SDRAM1|u1|Add5~26\);

-- Location: LABCELL_X50_Y22_N51
\SDRAM1|u1|timer_r[11]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|timer_r[11]~5_combout\ = ( \SDRAM1|u1|state_x.INITSETMODE~0_combout\ & ( \SDRAM1|u1|cke_x~0_combout\ & ( (!\SDRAM1|u1|Equal7~3_combout\ & \SDRAM1|u1|Add5~25_sumout\) ) ) ) # ( !\SDRAM1|u1|state_x.INITSETMODE~0_combout\ & ( 
-- \SDRAM1|u1|cke_x~0_combout\ & ( (!\SDRAM1|u1|Equal7~3_combout\ & (((\SDRAM1|u1|Add5~25_sumout\)))) # (\SDRAM1|u1|Equal7~3_combout\ & (!\SDRAM1|u1|state_r.INITWAIT~q\ & (!\SDRAM1|u1|Selector28~0_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001000111110000000000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_state_r.INITWAIT~q\,
	datab => \SDRAM1|u1|ALT_INV_Selector28~0_combout\,
	datac => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datad => \SDRAM1|u1|ALT_INV_Add5~25_sumout\,
	datae => \SDRAM1|u1|ALT_INV_state_x.INITSETMODE~0_combout\,
	dataf => \SDRAM1|u1|ALT_INV_cke_x~0_combout\,
	combout => \SDRAM1|u1|timer_r[11]~5_combout\);

-- Location: FF_X50_Y22_N52
\SDRAM1|u1|timer_r[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|timer_r[11]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|timer_r\(11));

-- Location: LABCELL_X51_Y22_N36
\SDRAM1|u1|Add5~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add5~21_sumout\ = SUM(( \SDRAM1|u1|timer_r\(12) ) + ( VCC ) + ( \SDRAM1|u1|Add5~26\ ))
-- \SDRAM1|u1|Add5~22\ = CARRY(( \SDRAM1|u1|timer_r\(12) ) + ( VCC ) + ( \SDRAM1|u1|Add5~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \SDRAM1|u1|ALT_INV_timer_r\(12),
	cin => \SDRAM1|u1|Add5~26\,
	sumout => \SDRAM1|u1|Add5~21_sumout\,
	cout => \SDRAM1|u1|Add5~22\);

-- Location: LABCELL_X50_Y22_N45
\SDRAM1|u1|timer_r[12]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|timer_r[12]~4_combout\ = (!\SDRAM1|u1|Equal7~3_combout\ & (\SDRAM1|u1|Add5~21_sumout\ & \SDRAM1|u1|cke_x~0_combout\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001100000000000000110000000000000011000000000000001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datac => \SDRAM1|u1|ALT_INV_Add5~21_sumout\,
	datad => \SDRAM1|u1|ALT_INV_cke_x~0_combout\,
	combout => \SDRAM1|u1|timer_r[12]~4_combout\);

-- Location: FF_X50_Y22_N46
\SDRAM1|u1|timer_r[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|timer_r[12]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|timer_r\(12));

-- Location: LABCELL_X51_Y22_N39
\SDRAM1|u1|Add5~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add5~17_sumout\ = SUM(( \SDRAM1|u1|timer_r\(13) ) + ( VCC ) + ( \SDRAM1|u1|Add5~22\ ))
-- \SDRAM1|u1|Add5~18\ = CARRY(( \SDRAM1|u1|timer_r\(13) ) + ( VCC ) + ( \SDRAM1|u1|Add5~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \SDRAM1|u1|ALT_INV_timer_r\(13),
	cin => \SDRAM1|u1|Add5~22\,
	sumout => \SDRAM1|u1|Add5~17_sumout\,
	cout => \SDRAM1|u1|Add5~18\);

-- Location: LABCELL_X50_Y22_N42
\SDRAM1|u1|timer_r[13]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|timer_r[13]~3_combout\ = ( \SDRAM1|u1|Add5~17_sumout\ & ( (!\SDRAM1|u1|Equal7~3_combout\ & \SDRAM1|u1|cke_x~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000110011000000000011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datad => \SDRAM1|u1|ALT_INV_cke_x~0_combout\,
	dataf => \SDRAM1|u1|ALT_INV_Add5~17_sumout\,
	combout => \SDRAM1|u1|timer_r[13]~3_combout\);

-- Location: FF_X50_Y22_N44
\SDRAM1|u1|timer_r[13]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|timer_r[13]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|timer_r[13]~DUPLICATE_q\);

-- Location: FF_X50_Y22_N29
\SDRAM1|u1|timer_r[9]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|timer_r[9]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|timer_r[9]~DUPLICATE_q\);

-- Location: FF_X50_Y22_N47
\SDRAM1|u1|timer_r[12]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|timer_r[12]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|timer_r[12]~DUPLICATE_q\);

-- Location: LABCELL_X51_Y22_N42
\SDRAM1|u1|Add5~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Add5~13_sumout\ = SUM(( \SDRAM1|u1|timer_r\(14) ) + ( VCC ) + ( \SDRAM1|u1|Add5~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \SDRAM1|u1|ALT_INV_timer_r\(14),
	cin => \SDRAM1|u1|Add5~18\,
	sumout => \SDRAM1|u1|Add5~13_sumout\);

-- Location: LABCELL_X50_Y22_N36
\SDRAM1|u1|timer_r[14]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|timer_r[14]~2_combout\ = ( \SDRAM1|u1|state_x.INITSETMODE~0_combout\ & ( \SDRAM1|u1|cke_x~0_combout\ & ( (!\SDRAM1|u1|Equal7~3_combout\ & \SDRAM1|u1|Add5~13_sumout\) ) ) ) # ( !\SDRAM1|u1|state_x.INITSETMODE~0_combout\ & ( 
-- \SDRAM1|u1|cke_x~0_combout\ & ( (!\SDRAM1|u1|Equal7~3_combout\ & (\SDRAM1|u1|Add5~13_sumout\)) # (\SDRAM1|u1|Equal7~3_combout\ & (((!\SDRAM1|u1|state_r.INITWAIT~q\ & !\SDRAM1|u1|Selector28~0_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001110010001000100010001000100010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datab => \SDRAM1|u1|ALT_INV_Add5~13_sumout\,
	datac => \SDRAM1|u1|ALT_INV_state_r.INITWAIT~q\,
	datad => \SDRAM1|u1|ALT_INV_Selector28~0_combout\,
	datae => \SDRAM1|u1|ALT_INV_state_x.INITSETMODE~0_combout\,
	dataf => \SDRAM1|u1|ALT_INV_cke_x~0_combout\,
	combout => \SDRAM1|u1|timer_r[14]~2_combout\);

-- Location: FF_X50_Y22_N37
\SDRAM1|u1|timer_r[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|timer_r[14]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|timer_r\(14));

-- Location: LABCELL_X50_Y22_N30
\SDRAM1|u1|Equal7~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Equal7~1_combout\ = ( !\SDRAM1|u1|timer_r[12]~DUPLICATE_q\ & ( !\SDRAM1|u1|timer_r\(14) & ( (!\SDRAM1|u1|timer_r\(10) & (!\SDRAM1|u1|timer_r[13]~DUPLICATE_q\ & (!\SDRAM1|u1|timer_r[9]~DUPLICATE_q\ & !\SDRAM1|u1|timer_r\(11)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_timer_r\(10),
	datab => \SDRAM1|u1|ALT_INV_timer_r[13]~DUPLICATE_q\,
	datac => \SDRAM1|u1|ALT_INV_timer_r[9]~DUPLICATE_q\,
	datad => \SDRAM1|u1|ALT_INV_timer_r\(11),
	datae => \SDRAM1|u1|ALT_INV_timer_r[12]~DUPLICATE_q\,
	dataf => \SDRAM1|u1|ALT_INV_timer_r\(14),
	combout => \SDRAM1|u1|Equal7~1_combout\);

-- Location: LABCELL_X48_Y21_N33
\SDRAM1|u1|Equal7~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Equal7~3_combout\ = ( \SDRAM1|u1|Equal7~0_combout\ & ( (\SDRAM1|u1|Equal7~2_combout\ & \SDRAM1|u1|Equal7~1_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000011110000000000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \SDRAM1|u1|ALT_INV_Equal7~2_combout\,
	datad => \SDRAM1|u1|ALT_INV_Equal7~1_combout\,
	dataf => \SDRAM1|u1|ALT_INV_Equal7~0_combout\,
	combout => \SDRAM1|u1|Equal7~3_combout\);

-- Location: LABCELL_X50_Y19_N33
\SDRAM1|u1|timer_r[7]~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|timer_r[7]~9_combout\ = ( \SDRAM1|u1|cke_x~0_combout\ & ( (!\SDRAM1|u1|Equal7~3_combout\ & \SDRAM1|u1|Add5~41_sumout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001100000011000000110000001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datac => \SDRAM1|u1|ALT_INV_Add5~41_sumout\,
	dataf => \SDRAM1|u1|ALT_INV_cke_x~0_combout\,
	combout => \SDRAM1|u1|timer_r[7]~9_combout\);

-- Location: FF_X50_Y19_N35
\SDRAM1|u1|timer_r[7]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|timer_r[7]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|timer_r[7]~DUPLICATE_q\);

-- Location: FF_X50_Y22_N8
\SDRAM1|u1|timer_r[5]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|timer_r[5]~11_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|timer_r[5]~DUPLICATE_q\);

-- Location: LABCELL_X50_Y19_N42
\SDRAM1|u1|Equal7~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Equal7~2_combout\ = ( !\SDRAM1|u1|timer_r\(6) & ( !\SDRAM1|u1|timer_r[5]~DUPLICATE_q\ & ( (!\SDRAM1|u1|timer_r[7]~DUPLICATE_q\ & (!\SDRAM1|u1|timer_r\(8) & (!\SDRAM1|u1|timer_r\(3) & !\SDRAM1|u1|timer_r\(4)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_timer_r[7]~DUPLICATE_q\,
	datab => \SDRAM1|u1|ALT_INV_timer_r\(8),
	datac => \SDRAM1|u1|ALT_INV_timer_r\(3),
	datad => \SDRAM1|u1|ALT_INV_timer_r\(4),
	datae => \SDRAM1|u1|ALT_INV_timer_r\(6),
	dataf => \SDRAM1|u1|ALT_INV_timer_r[5]~DUPLICATE_q\,
	combout => \SDRAM1|u1|Equal7~2_combout\);

-- Location: LABCELL_X48_Y21_N51
\SDRAM1|u1|wrTimer_r[1]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|wrTimer_r[1]~0_combout\ = ( \SDRAM1|u1|Equal7~0_combout\ & ( (\SDRAM1|u1|Equal7~2_combout\ & (\SDRAM1|u1|Equal7~1_combout\ & \SDRAM1|u1|state_r.RW~q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000110000000000000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \SDRAM1|u1|ALT_INV_Equal7~2_combout\,
	datac => \SDRAM1|u1|ALT_INV_Equal7~1_combout\,
	datad => \SDRAM1|u1|ALT_INV_state_r.RW~q\,
	dataf => \SDRAM1|u1|ALT_INV_Equal7~0_combout\,
	combout => \SDRAM1|u1|wrTimer_r[1]~0_combout\);

-- Location: LABCELL_X48_Y21_N6
\SDRAM1|u1|rdPipeline_x[4]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|rdPipeline_x[4]~0_combout\ = ( \SDRAM1|u1|doActivate~combout\ & ( (\SDRAM1|u1|wrTimer_r[1]~0_combout\ & (\rd~DUPLICATE_q\ & (\SDRAM1|u1|Equal0~0_combout\ & \SDRAM1|u1|Equal9~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000010000000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_wrTimer_r[1]~0_combout\,
	datab => \ALT_INV_rd~DUPLICATE_q\,
	datac => \SDRAM1|u1|ALT_INV_Equal0~0_combout\,
	datad => \SDRAM1|u1|ALT_INV_Equal9~0_combout\,
	dataf => \SDRAM1|u1|ALT_INV_doActivate~combout\,
	combout => \SDRAM1|u1|rdPipeline_x[4]~0_combout\);

-- Location: FF_X48_Y21_N7
\SDRAM1|u1|rdPipeline_r[4]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|rdPipeline_x[4]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rdPipeline_r[4]~DUPLICATE_q\);

-- Location: FF_X48_Y21_N14
\SDRAM1|u1|rdPipeline_r[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => \SDRAM1|u1|rdPipeline_r[4]~DUPLICATE_q\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rdPipeline_r\(3));

-- Location: FF_X48_Y21_N50
\SDRAM1|u1|rdPipeline_r[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => \SDRAM1|u1|rdPipeline_r\(3),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rdPipeline_r\(2));

-- Location: FF_X48_Y21_N26
\SDRAM1|u1|rdPipeline_r[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => \SDRAM1|u1|rdPipeline_r\(2),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rdPipeline_r\(1));

-- Location: FF_X48_Y20_N37
\SDRAM1|u1|rdPipeline_r[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => \SDRAM1|u1|rdPipeline_r\(1),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|rdPipeline_r\(0));

-- Location: MLABCELL_X52_Y20_N57
\Selector5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector5~0_combout\ = ( \Equal0~3_combout\ & ( \FSM_autotest.A_SET_READ~q\ & ( (!\Equal0~2_combout\) # ((!\Equal0~1_combout\) # ((!\Equal0~0_combout\) # (!hAddr(0)))) ) ) ) # ( !\Equal0~3_combout\ & ( \FSM_autotest.A_SET_READ~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal0~2_combout\,
	datab => \ALT_INV_Equal0~1_combout\,
	datac => \ALT_INV_Equal0~0_combout\,
	datad => ALT_INV_hAddr(0),
	datae => \ALT_INV_Equal0~3_combout\,
	dataf => \ALT_INV_FSM_autotest.A_SET_READ~q\,
	combout => \Selector5~0_combout\);

-- Location: FF_X52_Y20_N58
\FSM_autotest.A_READ\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Selector5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \FSM_autotest.A_READ~q\);

-- Location: LABCELL_X48_Y20_N15
\Selector6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector6~0_combout\ = ( \FSM_autotest.A_READ~q\ ) # ( !\FSM_autotest.A_READ~q\ & ( (!\SDRAM1|u1|rdPipeline_r\(0) & \FSM_autotest.A_READ_WAIT~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011110000000000001111000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \SDRAM1|u1|ALT_INV_rdPipeline_r\(0),
	datad => \ALT_INV_FSM_autotest.A_READ_WAIT~q\,
	dataf => \ALT_INV_FSM_autotest.A_READ~q\,
	combout => \Selector6~0_combout\);

-- Location: FF_X48_Y20_N16
\FSM_autotest.A_READ_WAIT\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Selector6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \FSM_autotest.A_READ_WAIT~q\);

-- Location: MLABCELL_X47_Y21_N27
\Selector37~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector37~0_combout\ = (!\FSM_autotest.A_READ_WAIT~q\ & (((\rd~q\)) # (\FSM_autotest.A_READ~q\))) # (\FSM_autotest.A_READ_WAIT~q\ & (((!\SDRAM1|u1|rdPipeline_r\(0) & \rd~q\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010001011111010001000101111101000100010111110100010001011111010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_FSM_autotest.A_READ_WAIT~q\,
	datab => \ALT_INV_FSM_autotest.A_READ~q\,
	datac => \SDRAM1|u1|ALT_INV_rdPipeline_r\(0),
	datad => \ALT_INV_rd~q\,
	combout => \Selector37~0_combout\);

-- Location: FF_X47_Y21_N28
\rd~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Selector37~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \rd~DUPLICATE_q\);

-- Location: LABCELL_X48_Y20_N42
\SDRAM1|u1|wrTimer_r[1]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|wrTimer_r[1]~4_combout\ = ( \SDRAM1|u1|doActivate~combout\ & ( \SDRAM1|u1|Equal0~0_combout\ & ( (!\rd~DUPLICATE_q\ & (\wr~q\ & (\SDRAM1|u1|wrTimer_r[1]~0_combout\ & \SDRAM1|u1|Equal9~0_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rd~DUPLICATE_q\,
	datab => \ALT_INV_wr~q\,
	datac => \SDRAM1|u1|ALT_INV_wrTimer_r[1]~0_combout\,
	datad => \SDRAM1|u1|ALT_INV_Equal9~0_combout\,
	datae => \SDRAM1|u1|ALT_INV_doActivate~combout\,
	dataf => \SDRAM1|u1|ALT_INV_Equal0~0_combout\,
	combout => \SDRAM1|u1|wrTimer_r[1]~4_combout\);

-- Location: FF_X48_Y20_N43
\SDRAM1|u1|sDataDir_r~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|wrTimer_r[1]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sDataDir_r~DUPLICATE_q\);

-- Location: LABCELL_X48_Y20_N0
\hAddr[23]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \hAddr[23]~5_combout\ = ( \FSM_autotest.A_IDLE~q\ & ( (\FSM_autotest.A_WRITE_WAIT~q\ & ((\SDRAM1|u1|rdPipeline_r\(0)) # (\SDRAM1|u1|sDataDir_r~DUPLICATE_q\))) ) ) # ( !\FSM_autotest.A_IDLE~q\ & ( (!\KEY[3]~input_o\) # ((\FSM_autotest.A_WRITE_WAIT~q\ & 
-- ((\SDRAM1|u1|rdPipeline_r\(0)) # (\SDRAM1|u1|sDataDir_r~DUPLICATE_q\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010111111101010101011111100000000001111110000000000111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_KEY[3]~input_o\,
	datab => \SDRAM1|u1|ALT_INV_sDataDir_r~DUPLICATE_q\,
	datac => \SDRAM1|u1|ALT_INV_rdPipeline_r\(0),
	datad => \ALT_INV_FSM_autotest.A_WRITE_WAIT~q\,
	dataf => \ALT_INV_FSM_autotest.A_IDLE~q\,
	combout => \hAddr[23]~5_combout\);

-- Location: FF_X48_Y20_N2
\FSM_autotest.A_SET_WRITE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \hAddr[23]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \FSM_autotest.A_SET_WRITE~q\);

-- Location: LABCELL_X48_Y20_N39
\hAddr[23]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \hAddr[23]~0_combout\ = ( \FSM_autotest.A_SET_WRITE~q\ & ( \FSM_autotest.A_IDLE~q\ ) ) # ( \FSM_autotest.A_SET_WRITE~q\ & ( !\FSM_autotest.A_IDLE~q\ ) ) # ( !\FSM_autotest.A_SET_WRITE~q\ & ( !\FSM_autotest.A_IDLE~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \ALT_INV_FSM_autotest.A_SET_WRITE~q\,
	dataf => \ALT_INV_FSM_autotest.A_IDLE~q\,
	combout => \hAddr[23]~0_combout\);

-- Location: FF_X51_Y20_N32
\hAddr[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Add0~5_sumout\,
	sclr => \hAddr[23]~0_combout\,
	ena => \hAddr[23]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hAddr(0));

-- Location: FF_X52_Y20_N16
\hDIn[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	asdata => hAddr(0),
	sload => VCC,
	ena => \Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hDIn(0));

-- Location: MLABCELL_X52_Y22_N51
\SDRAM1|u1|sData_r[0]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|sData_r[0]~feeder_combout\ = hDIn(0)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_hDIn(0),
	combout => \SDRAM1|u1|sData_r[0]~feeder_combout\);

-- Location: FF_X52_Y22_N52
\SDRAM1|u1|sData_r[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|sData_r[0]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sData_r\(0));

-- Location: LABCELL_X53_Y20_N54
\hDIn[1]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \hDIn[1]~feeder_combout\ = ( hAddr(1) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => ALT_INV_hAddr(1),
	combout => \hDIn[1]~feeder_combout\);

-- Location: FF_X53_Y20_N55
\hDIn[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \hDIn[1]~feeder_combout\,
	ena => \Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hDIn(1));

-- Location: FF_X46_Y20_N58
\SDRAM1|u1|sData_r[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => hDIn(1),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sData_r\(1));

-- Location: LABCELL_X53_Y20_N0
\hDIn[2]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \hDIn[2]~feeder_combout\ = ( hAddr(2) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => ALT_INV_hAddr(2),
	combout => \hDIn[2]~feeder_combout\);

-- Location: FF_X53_Y20_N1
\hDIn[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \hDIn[2]~feeder_combout\,
	ena => \Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hDIn(2));

-- Location: FF_X53_Y21_N43
\SDRAM1|u1|sData_r[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => hDIn(2),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sData_r\(2));

-- Location: MLABCELL_X52_Y20_N48
\hDIn[3]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \hDIn[3]~feeder_combout\ = ( hAddr(3) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => ALT_INV_hAddr(3),
	combout => \hDIn[3]~feeder_combout\);

-- Location: FF_X52_Y20_N50
\hDIn[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \hDIn[3]~feeder_combout\,
	ena => \Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hDIn(3));

-- Location: MLABCELL_X52_Y22_N27
\SDRAM1|u1|sData_r[3]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|sData_r[3]~feeder_combout\ = hDIn(3)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hDIn(3),
	combout => \SDRAM1|u1|sData_r[3]~feeder_combout\);

-- Location: FF_X52_Y22_N28
\SDRAM1|u1|sData_r[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|sData_r[3]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sData_r\(3));

-- Location: MLABCELL_X52_Y20_N27
\hDIn[4]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \hDIn[4]~feeder_combout\ = ( hAddr(4) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => ALT_INV_hAddr(4),
	combout => \hDIn[4]~feeder_combout\);

-- Location: FF_X52_Y20_N28
\hDIn[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \hDIn[4]~feeder_combout\,
	ena => \Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hDIn(4));

-- Location: MLABCELL_X52_Y22_N3
\SDRAM1|u1|sData_r[4]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|sData_r[4]~feeder_combout\ = hDIn(4)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hDIn(4),
	combout => \SDRAM1|u1|sData_r[4]~feeder_combout\);

-- Location: FF_X52_Y22_N4
\SDRAM1|u1|sData_r[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|sData_r[4]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sData_r\(4));

-- Location: MLABCELL_X52_Y20_N24
\hDIn[5]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \hDIn[5]~feeder_combout\ = hAddr(5)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_hAddr(5),
	combout => \hDIn[5]~feeder_combout\);

-- Location: FF_X52_Y20_N26
\hDIn[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \hDIn[5]~feeder_combout\,
	ena => \Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hDIn(5));

-- Location: LABCELL_X42_Y6_N0
\SDRAM1|u1|sData_r[5]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|sData_r[5]~feeder_combout\ = ( hDIn(5) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => ALT_INV_hDIn(5),
	combout => \SDRAM1|u1|sData_r[5]~feeder_combout\);

-- Location: FF_X42_Y6_N1
\SDRAM1|u1|sData_r[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|sData_r[5]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sData_r\(5));

-- Location: FF_X52_Y20_N49
\hDIn[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	asdata => hAddr(6),
	sload => VCC,
	ena => \Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hDIn(6));

-- Location: FF_X52_Y22_N43
\SDRAM1|u1|sData_r[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => hDIn(6),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sData_r\(6));

-- Location: MLABCELL_X52_Y20_N51
\hDIn[7]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \hDIn[7]~feeder_combout\ = ( hAddr(7) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => ALT_INV_hAddr(7),
	combout => \hDIn[7]~feeder_combout\);

-- Location: FF_X52_Y20_N52
\hDIn[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \hDIn[7]~feeder_combout\,
	ena => \Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hDIn(7));

-- Location: FF_X53_Y21_N17
\SDRAM1|u1|sData_r[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => hDIn(7),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sData_r\(7));

-- Location: FF_X52_Y20_N40
\hDIn[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	asdata => hAddr(8),
	sload => VCC,
	ena => \Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hDIn(8));

-- Location: MLABCELL_X52_Y21_N45
\SDRAM1|u1|sData_r[8]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|sData_r[8]~feeder_combout\ = hDIn(8)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_hDIn(8),
	combout => \SDRAM1|u1|sData_r[8]~feeder_combout\);

-- Location: FF_X52_Y21_N47
\SDRAM1|u1|sData_r[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|sData_r[8]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sData_r\(8));

-- Location: LABCELL_X53_Y20_N3
\hDIn[9]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \hDIn[9]~feeder_combout\ = ( hAddr(9) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => ALT_INV_hAddr(9),
	combout => \hDIn[9]~feeder_combout\);

-- Location: FF_X53_Y20_N4
\hDIn[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \hDIn[9]~feeder_combout\,
	ena => \Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hDIn(9));

-- Location: FF_X53_Y20_N17
\SDRAM1|u1|sData_r[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => hDIn(9),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sData_r\(9));

-- Location: FF_X53_Y20_N7
\hDIn[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	asdata => hAddr(10),
	sload => VCC,
	ena => \Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hDIn(10));

-- Location: LABCELL_X53_Y20_N48
\SDRAM1|u1|sData_r[10]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|sData_r[10]~feeder_combout\ = hDIn(10)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_hDIn(10),
	combout => \SDRAM1|u1|sData_r[10]~feeder_combout\);

-- Location: FF_X53_Y20_N49
\SDRAM1|u1|sData_r[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|sData_r[10]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sData_r\(10));

-- Location: FF_X50_Y20_N22
\hDIn[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	asdata => hAddr(11),
	sload => VCC,
	ena => \Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hDIn(11));

-- Location: FF_X52_Y22_N10
\SDRAM1|u1|sData_r[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => hDIn(11),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sData_r\(11));

-- Location: FF_X50_Y20_N19
\hDIn[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	asdata => hAddr(12),
	sload => VCC,
	ena => \Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hDIn(12));

-- Location: FF_X48_Y21_N44
\SDRAM1|u1|sData_r[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => hDIn(12),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sData_r\(12));

-- Location: FF_X52_Y20_N29
\hDIn[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	asdata => hAddr(13),
	sload => VCC,
	ena => \Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hDIn(13));

-- Location: LABCELL_X42_Y6_N36
\SDRAM1|u1|sData_r[13]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|sData_r[13]~feeder_combout\ = ( hDIn(13) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => ALT_INV_hDIn(13),
	combout => \SDRAM1|u1|sData_r[13]~feeder_combout\);

-- Location: FF_X42_Y6_N37
\SDRAM1|u1|sData_r[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|sData_r[13]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sData_r\(13));

-- Location: FF_X52_Y20_N53
\hDIn[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	asdata => hAddr(14),
	sload => VCC,
	ena => \Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hDIn(14));

-- Location: LABCELL_X53_Y19_N48
\SDRAM1|u1|sData_r[14]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|sData_r[14]~feeder_combout\ = hDIn(14)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hDIn(14),
	combout => \SDRAM1|u1|sData_r[14]~feeder_combout\);

-- Location: FF_X53_Y19_N49
\SDRAM1|u1|sData_r[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|sData_r[14]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sData_r\(14));

-- Location: FF_X52_Y20_N25
\hDIn[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	asdata => hAddr(15),
	sload => VCC,
	ena => \Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => hDIn(15));

-- Location: LABCELL_X42_Y6_N15
\SDRAM1|u1|sData_r[15]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|sData_r[15]~feeder_combout\ = ( hDIn(15) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => ALT_INV_hDIn(15),
	combout => \SDRAM1|u1|sData_r[15]~feeder_combout\);

-- Location: FF_X42_Y6_N16
\SDRAM1|u1|sData_r[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|sData_r[15]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sData_r\(15));

-- Location: LABCELL_X53_Y19_N0
\SDRAM1|u1|sAddr_x[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|sAddr_x[0]~0_combout\ = ( \SDRAM1|u1|Equal7~3_combout\ & ( (!\SDRAM1|u1|state_r.ACTIVATE~q\ & (!\SDRAM1|u1|state_r.INITSETMODE~q\ & ((hAddr(0))))) # (\SDRAM1|u1|state_r.ACTIVATE~q\ & (((hAddr(10))))) ) ) # ( !\SDRAM1|u1|Equal7~3_combout\ & ( 
-- hAddr(0) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000011100010110000001110001011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_state_r.INITSETMODE~q\,
	datab => \SDRAM1|u1|ALT_INV_state_r.ACTIVATE~q\,
	datac => ALT_INV_hAddr(10),
	datad => ALT_INV_hAddr(0),
	dataf => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	combout => \SDRAM1|u1|sAddr_x[0]~0_combout\);

-- Location: FF_X53_Y19_N1
\SDRAM1|u1|sAddr_r[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|sAddr_x[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sAddr_r\(0));

-- Location: LABCELL_X53_Y19_N3
\SDRAM1|u1|sAddr_x[1]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|sAddr_x[1]~1_combout\ = ( hAddr(11) & ( (!\SDRAM1|u1|Equal7~3_combout\ & (((hAddr(1))))) # (\SDRAM1|u1|Equal7~3_combout\ & (((!\SDRAM1|u1|state_r.INITSETMODE~q\ & hAddr(1))) # (\SDRAM1|u1|state_r.ACTIVATE~q\))) ) ) # ( !hAddr(11) & ( (hAddr(1) 
-- & ((!\SDRAM1|u1|Equal7~3_combout\) # ((!\SDRAM1|u1|state_r.INITSETMODE~q\ & !\SDRAM1|u1|state_r.ACTIVATE~q\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001000000011110000100000001111001110110000111100111011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_state_r.INITSETMODE~q\,
	datab => \SDRAM1|u1|ALT_INV_state_r.ACTIVATE~q\,
	datac => ALT_INV_hAddr(1),
	datad => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	dataf => ALT_INV_hAddr(11),
	combout => \SDRAM1|u1|sAddr_x[1]~1_combout\);

-- Location: FF_X53_Y19_N4
\SDRAM1|u1|sAddr_r[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|sAddr_x[1]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sAddr_r\(1));

-- Location: LABCELL_X50_Y20_N0
\SDRAM1|u1|sAddr_x[2]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|sAddr_x[2]~2_combout\ = ( \SDRAM1|u1|state_r.ACTIVATE~q\ & ( (!\SDRAM1|u1|Equal7~3_combout\ & (hAddr(2))) # (\SDRAM1|u1|Equal7~3_combout\ & ((hAddr(12)))) ) ) # ( !\SDRAM1|u1|state_r.ACTIVATE~q\ & ( (hAddr(2) & ((!\SDRAM1|u1|Equal7~3_combout\) 
-- # (!\SDRAM1|u1|state_r.INITSETMODE~q\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010000010101010101000001010011010100110101001101010011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(2),
	datab => ALT_INV_hAddr(12),
	datac => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datad => \SDRAM1|u1|ALT_INV_state_r.INITSETMODE~q\,
	dataf => \SDRAM1|u1|ALT_INV_state_r.ACTIVATE~q\,
	combout => \SDRAM1|u1|sAddr_x[2]~2_combout\);

-- Location: FF_X50_Y20_N1
\SDRAM1|u1|sAddr_r[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|sAddr_x[2]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sAddr_r\(2));

-- Location: LABCELL_X53_Y19_N9
\SDRAM1|u1|sAddr_x[3]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|sAddr_x[3]~3_combout\ = ( hAddr(13) & ( (!\SDRAM1|u1|Equal7~3_combout\ & (((hAddr(3))))) # (\SDRAM1|u1|Equal7~3_combout\ & (((!\SDRAM1|u1|state_r.INITSETMODE~q\ & hAddr(3))) # (\SDRAM1|u1|state_r.ACTIVATE~q\))) ) ) # ( !hAddr(13) & ( (hAddr(3) 
-- & ((!\SDRAM1|u1|Equal7~3_combout\) # ((!\SDRAM1|u1|state_r.INITSETMODE~q\ & !\SDRAM1|u1|state_r.ACTIVATE~q\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111000001100000011100000110000001110001111110000111000111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_state_r.INITSETMODE~q\,
	datab => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datac => ALT_INV_hAddr(3),
	datad => \SDRAM1|u1|ALT_INV_state_r.ACTIVATE~q\,
	dataf => ALT_INV_hAddr(13),
	combout => \SDRAM1|u1|sAddr_x[3]~3_combout\);

-- Location: FF_X53_Y19_N10
\SDRAM1|u1|sAddr_r[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|sAddr_x[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sAddr_r\(3));

-- Location: LABCELL_X50_Y21_N6
\SDRAM1|u1|sAddr_x[4]~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|sAddr_x[4]~17_combout\ = ( !\SDRAM1|u1|state_r.ACTIVATE~q\ & ( ((\SDRAM1|u1|Equal7~2_combout\ & (\SDRAM1|u1|state_r.INITSETMODE~q\ & (\SDRAM1|u1|Equal7~1_combout\ & \SDRAM1|u1|Equal7~0_combout\)))) # (hAddr(4)) ) ) # ( 
-- \SDRAM1|u1|state_r.ACTIVATE~q\ & ( (!\SDRAM1|u1|Equal7~2_combout\ & (hAddr(4))) # (\SDRAM1|u1|Equal7~2_combout\ & ((!\SDRAM1|u1|Equal7~1_combout\ & (hAddr(4))) # (\SDRAM1|u1|Equal7~1_combout\ & ((!\SDRAM1|u1|Equal7~0_combout\ & (hAddr(4))) # 
-- (\SDRAM1|u1|Equal7~0_combout\ & ((hAddr(14)))))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0011001100110011001100110011001100110011001101110011001100100111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal7~2_combout\,
	datab => ALT_INV_hAddr(4),
	datac => ALT_INV_hAddr(14),
	datad => \SDRAM1|u1|ALT_INV_Equal7~1_combout\,
	datae => \SDRAM1|u1|ALT_INV_state_r.ACTIVATE~q\,
	dataf => \SDRAM1|u1|ALT_INV_Equal7~0_combout\,
	datag => \SDRAM1|u1|ALT_INV_state_r.INITSETMODE~q\,
	combout => \SDRAM1|u1|sAddr_x[4]~17_combout\);

-- Location: FF_X50_Y21_N8
\SDRAM1|u1|sAddr_r[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|sAddr_x[4]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sAddr_r\(4));

-- Location: LABCELL_X50_Y21_N0
\SDRAM1|u1|sAddr_x[5]~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|sAddr_x[5]~13_combout\ = ( !\SDRAM1|u1|state_r.ACTIVATE~q\ & ( ((\SDRAM1|u1|Equal7~2_combout\ & (\SDRAM1|u1|state_r.INITSETMODE~q\ & (\SDRAM1|u1|Equal7~0_combout\ & \SDRAM1|u1|Equal7~1_combout\)))) # (hAddr(5)) ) ) # ( 
-- \SDRAM1|u1|state_r.ACTIVATE~q\ & ( (!\SDRAM1|u1|Equal7~2_combout\ & (hAddr(5))) # (\SDRAM1|u1|Equal7~2_combout\ & ((!\SDRAM1|u1|Equal7~0_combout\ & (hAddr(5))) # (\SDRAM1|u1|Equal7~0_combout\ & ((!\SDRAM1|u1|Equal7~1_combout\ & (hAddr(5))) # 
-- (\SDRAM1|u1|Equal7~1_combout\ & ((hAddr(15)))))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0011001100110011001100110011001100110011001101110011001100100111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal7~2_combout\,
	datab => ALT_INV_hAddr(5),
	datac => ALT_INV_hAddr(15),
	datad => \SDRAM1|u1|ALT_INV_Equal7~0_combout\,
	datae => \SDRAM1|u1|ALT_INV_state_r.ACTIVATE~q\,
	dataf => \SDRAM1|u1|ALT_INV_Equal7~1_combout\,
	datag => \SDRAM1|u1|ALT_INV_state_r.INITSETMODE~q\,
	combout => \SDRAM1|u1|sAddr_x[5]~13_combout\);

-- Location: FF_X50_Y21_N1
\SDRAM1|u1|sAddr_r[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|sAddr_x[5]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sAddr_r\(5));

-- Location: MLABCELL_X52_Y20_N3
\SDRAM1|u1|sAddr_x[6]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|sAddr_x[6]~4_combout\ = ( \SDRAM1|u1|state_r.INITSETMODE~q\ & ( (!\SDRAM1|u1|Equal7~3_combout\ & (hAddr(6))) # (\SDRAM1|u1|Equal7~3_combout\ & (((hAddr(16) & \SDRAM1|u1|state_r.ACTIVATE~q\)))) ) ) # ( !\SDRAM1|u1|state_r.INITSETMODE~q\ & ( 
-- (!\SDRAM1|u1|Equal7~3_combout\ & (hAddr(6))) # (\SDRAM1|u1|Equal7~3_combout\ & ((!\SDRAM1|u1|state_r.ACTIVATE~q\ & (hAddr(6))) # (\SDRAM1|u1|state_r.ACTIVATE~q\ & ((hAddr(16)))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010011010101010101001101010000010100110101000001010011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(6),
	datab => ALT_INV_hAddr(16),
	datac => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datad => \SDRAM1|u1|ALT_INV_state_r.ACTIVATE~q\,
	dataf => \SDRAM1|u1|ALT_INV_state_r.INITSETMODE~q\,
	combout => \SDRAM1|u1|sAddr_x[6]~4_combout\);

-- Location: FF_X52_Y20_N4
\SDRAM1|u1|sAddr_r[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|sAddr_x[6]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sAddr_r\(6));

-- Location: LABCELL_X53_Y19_N6
\SDRAM1|u1|sAddr_x[7]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|sAddr_x[7]~5_combout\ = ( \SDRAM1|u1|state_r.ACTIVATE~q\ & ( (!\SDRAM1|u1|Equal7~3_combout\ & (hAddr(7))) # (\SDRAM1|u1|Equal7~3_combout\ & ((hAddr(17)))) ) ) # ( !\SDRAM1|u1|state_r.ACTIVATE~q\ & ( (hAddr(7) & 
-- ((!\SDRAM1|u1|state_r.INITSETMODE~q\) # (!\SDRAM1|u1|Equal7~3_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111000001110000011100000111000001100001111110000110000111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_state_r.INITSETMODE~q\,
	datab => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datac => ALT_INV_hAddr(7),
	datad => ALT_INV_hAddr(17),
	dataf => \SDRAM1|u1|ALT_INV_state_r.ACTIVATE~q\,
	combout => \SDRAM1|u1|sAddr_x[7]~5_combout\);

-- Location: FF_X53_Y19_N8
\SDRAM1|u1|sAddr_r[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|sAddr_x[7]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sAddr_r\(7));

-- Location: LABCELL_X53_Y19_N12
\SDRAM1|u1|sAddr_x[8]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|sAddr_x[8]~6_combout\ = ( hAddr(8) & ( (!\SDRAM1|u1|Equal7~3_combout\) # ((!\SDRAM1|u1|state_r.ACTIVATE~q\ & (!\SDRAM1|u1|state_r.INITSETMODE~q\)) # (\SDRAM1|u1|state_r.ACTIVATE~q\ & ((hAddr(18))))) ) ) # ( !hAddr(8) & ( 
-- (\SDRAM1|u1|Equal7~3_combout\ & (hAddr(18) & \SDRAM1|u1|state_r.ACTIVATE~q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000011000000000000001111101110110011111110111011001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_state_r.INITSETMODE~q\,
	datab => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datac => ALT_INV_hAddr(18),
	datad => \SDRAM1|u1|ALT_INV_state_r.ACTIVATE~q\,
	dataf => ALT_INV_hAddr(8),
	combout => \SDRAM1|u1|sAddr_x[8]~6_combout\);

-- Location: FF_X53_Y19_N13
\SDRAM1|u1|sAddr_r[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|sAddr_x[8]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sAddr_r\(8));

-- Location: LABCELL_X53_Y19_N15
\SDRAM1|u1|sAddr_x[9]~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|sAddr_x[9]~7_combout\ = ( \SDRAM1|u1|state_r.ACTIVATE~q\ & ( (!\SDRAM1|u1|Equal7~3_combout\ & (hAddr(9))) # (\SDRAM1|u1|Equal7~3_combout\ & ((hAddr(19)))) ) ) # ( !\SDRAM1|u1|state_r.ACTIVATE~q\ & ( (hAddr(9) & 
-- ((!\SDRAM1|u1|state_r.INITSETMODE~q\) # (!\SDRAM1|u1|Equal7~3_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111000001110000011100000111000001100001111110000110000111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_state_r.INITSETMODE~q\,
	datab => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datac => ALT_INV_hAddr(9),
	datad => ALT_INV_hAddr(19),
	dataf => \SDRAM1|u1|ALT_INV_state_r.ACTIVATE~q\,
	combout => \SDRAM1|u1|sAddr_x[9]~7_combout\);

-- Location: FF_X53_Y19_N17
\SDRAM1|u1|sAddr_r[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|sAddr_x[9]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sAddr_r\(9));

-- Location: MLABCELL_X52_Y21_N15
\SDRAM1|u1|sAddr_x[10]~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|sAddr_x[10]~8_combout\ = ( !\SDRAM1|u1|state_r.INITPCHG~q\ & ( (!\SDRAM1|u1|state_r.ACTIVATE~q\) # (!hAddr(20)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111110000111111111111000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \SDRAM1|u1|ALT_INV_state_r.ACTIVATE~q\,
	datad => ALT_INV_hAddr(20),
	dataf => \SDRAM1|u1|ALT_INV_state_r.INITPCHG~q\,
	combout => \SDRAM1|u1|sAddr_x[10]~8_combout\);

-- Location: MLABCELL_X52_Y21_N0
\SDRAM1|u1|sAddr_x[10]~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|sAddr_x[10]~9_combout\ = ( \SDRAM1|u1|sAddr_x[10]~8_combout\ & ( (\SDRAM1|u1|Equal7~3_combout\ & (\SDRAM1|u1|Selector22~0_combout\ & ((!\SDRAM1|u1|Equal9~0_combout\) # (\SDRAM1|u1|doSelfRfsh~3_combout\)))) ) ) # ( 
-- !\SDRAM1|u1|sAddr_x[10]~8_combout\ & ( \SDRAM1|u1|Equal7~3_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100000000001100010000000000110001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	datab => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	datac => \SDRAM1|u1|ALT_INV_Equal9~0_combout\,
	datad => \SDRAM1|u1|ALT_INV_Selector22~0_combout\,
	dataf => \SDRAM1|u1|ALT_INV_sAddr_x[10]~8_combout\,
	combout => \SDRAM1|u1|sAddr_x[10]~9_combout\);

-- Location: FF_X52_Y21_N1
\SDRAM1|u1|sAddr_r[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|sAddr_x[10]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sAddr_r\(10));

-- Location: MLABCELL_X52_Y21_N36
\SDRAM1|u1|sAddr_x[11]~10\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|sAddr_x[11]~10_combout\ = ( \SDRAM1|u1|Equal7~2_combout\ & ( (\SDRAM1|u1|Equal7~1_combout\ & (\SDRAM1|u1|Equal7~0_combout\ & (hAddr(21) & \SDRAM1|u1|state_r.ACTIVATE~q\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000010000000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal7~1_combout\,
	datab => \SDRAM1|u1|ALT_INV_Equal7~0_combout\,
	datac => ALT_INV_hAddr(21),
	datad => \SDRAM1|u1|ALT_INV_state_r.ACTIVATE~q\,
	dataf => \SDRAM1|u1|ALT_INV_Equal7~2_combout\,
	combout => \SDRAM1|u1|sAddr_x[11]~10_combout\);

-- Location: FF_X52_Y21_N38
\SDRAM1|u1|sAddr_r[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|sAddr_x[11]~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sAddr_r\(11));

-- Location: MLABCELL_X52_Y21_N39
\SDRAM1|u1|sAddr_x[12]~11\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|sAddr_x[12]~11_combout\ = ( \SDRAM1|u1|Equal7~2_combout\ & ( (\SDRAM1|u1|Equal7~1_combout\ & (\SDRAM1|u1|Equal7~0_combout\ & (hAddr(22) & \SDRAM1|u1|state_r.ACTIVATE~q\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000010000000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal7~1_combout\,
	datab => \SDRAM1|u1|ALT_INV_Equal7~0_combout\,
	datac => ALT_INV_hAddr(22),
	datad => \SDRAM1|u1|ALT_INV_state_r.ACTIVATE~q\,
	dataf => \SDRAM1|u1|ALT_INV_Equal7~2_combout\,
	combout => \SDRAM1|u1|sAddr_x[12]~11_combout\);

-- Location: FF_X52_Y21_N40
\SDRAM1|u1|sAddr_r[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|sAddr_x[12]~11_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|sAddr_r\(12));

-- Location: FF_X52_Y21_N46
\SDRAM1|u1|ba_r[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => hAddr(23),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|ba_r\(0));

-- Location: FF_X47_Y20_N47
\SDRAM1|u1|ba_r[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => hAddr(24),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|ba_r\(1));

-- Location: LABCELL_X50_Y21_N18
\SDRAM1|u1|cmd_x[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|cmd_x[0]~0_combout\ = ( !\SDRAM1|u1|state_r.INITRFSH~q\ & ( (!\SDRAM1|u1|state_r.INITPCHG~q\ & (!\SDRAM1|u1|state_r.INITSETMODE~q\ & !\SDRAM1|u1|state_r.REFRESHROW~q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100000000000000110000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \SDRAM1|u1|ALT_INV_state_r.INITPCHG~q\,
	datac => \SDRAM1|u1|ALT_INV_state_r.INITSETMODE~q\,
	datad => \SDRAM1|u1|ALT_INV_state_r.REFRESHROW~q\,
	dataf => \SDRAM1|u1|ALT_INV_state_r.INITRFSH~q\,
	combout => \SDRAM1|u1|cmd_x[0]~0_combout\);

-- Location: MLABCELL_X47_Y21_N48
\SDRAM1|u1|cmd_x[0]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|cmd_x[0]~1_combout\ = ( \SDRAM1|u1|doSelfRfsh~4_combout\ & ( (\SDRAM1|u1|cmd_x[0]~0_combout\ & ((!\SDRAM1|u1|doSelfRfsh~0_combout\) # ((!\SDRAM1|u1|doSelfRfsh~1_combout\) # (!\SDRAM1|u1|state_r.SELFREFRESH~q\)))) ) ) # ( 
-- !\SDRAM1|u1|doSelfRfsh~4_combout\ & ( \SDRAM1|u1|cmd_x[0]~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011100000111100001110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_doSelfRfsh~0_combout\,
	datab => \SDRAM1|u1|ALT_INV_doSelfRfsh~1_combout\,
	datac => \SDRAM1|u1|ALT_INV_cmd_x[0]~0_combout\,
	datad => \SDRAM1|u1|ALT_INV_state_r.SELFREFRESH~q\,
	dataf => \SDRAM1|u1|ALT_INV_doSelfRfsh~4_combout\,
	combout => \SDRAM1|u1|cmd_x[0]~1_combout\);

-- Location: LABCELL_X53_Y21_N36
\SDRAM1|u1|cmd_x[0]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|cmd_x[0]~2_combout\ = ( \SDRAM1|u1|Equal7~3_combout\ & ( \SDRAM1|u1|doActivate~combout\ & ( (!\SDRAM1|u1|cmd_x[0]~1_combout\) # ((\SDRAM1|u1|Selector22~0_combout\ & !\SDRAM1|u1|state_x.RW~0_combout\)) ) ) ) # ( \SDRAM1|u1|Equal7~3_combout\ & ( 
-- !\SDRAM1|u1|doActivate~combout\ & ( (!\SDRAM1|u1|cmd_x[0]~1_combout\) # ((\SDRAM1|u1|Selector22~0_combout\ & ((!\SDRAM1|u1|state_x.RW~0_combout\) # (\SDRAM1|u1|sAddr_x[10]~12_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111101011111000100000000000000001111010111110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Selector22~0_combout\,
	datab => \SDRAM1|u1|ALT_INV_sAddr_x[10]~12_combout\,
	datac => \SDRAM1|u1|ALT_INV_cmd_x[0]~1_combout\,
	datad => \SDRAM1|u1|ALT_INV_state_x.RW~0_combout\,
	datae => \SDRAM1|u1|ALT_INV_Equal7~3_combout\,
	dataf => \SDRAM1|u1|ALT_INV_doActivate~combout\,
	combout => \SDRAM1|u1|cmd_x[0]~2_combout\);

-- Location: FF_X53_Y21_N4
\SDRAM1|u1|cmd_r[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	asdata => \SDRAM1|u1|cmd_x[0]~2_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|cmd_r\(0));

-- Location: FF_X53_Y21_N38
\SDRAM1|u1|cmd_r[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|cmd_x[0]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|cmd_r\(1));

-- Location: MLABCELL_X52_Y22_N36
\SDRAM1|u1|cmd_x[3]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|cmd_x[3]~3_combout\ = ( \SDRAM1|u1|doSelfRfsh~3_combout\ & ( \SDRAM1|u1|Equal7~0_combout\ & ( (\SDRAM1|u1|Equal7~1_combout\ & (\SDRAM1|u1|Equal7~2_combout\ & \SDRAM1|u1|state_r.INITWAIT~q\)) ) ) ) # ( !\SDRAM1|u1|doSelfRfsh~3_combout\ & ( 
-- \SDRAM1|u1|Equal7~0_combout\ & ( (\SDRAM1|u1|Equal7~1_combout\ & (!\SDRAM1|u1|state_r.SELFREFRESH~q\ & (\SDRAM1|u1|Equal7~2_combout\ & \SDRAM1|u1|state_r.INITWAIT~q\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000001000000000000000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal7~1_combout\,
	datab => \SDRAM1|u1|ALT_INV_state_r.SELFREFRESH~q\,
	datac => \SDRAM1|u1|ALT_INV_Equal7~2_combout\,
	datad => \SDRAM1|u1|ALT_INV_state_r.INITWAIT~q\,
	datae => \SDRAM1|u1|ALT_INV_doSelfRfsh~3_combout\,
	dataf => \SDRAM1|u1|ALT_INV_Equal7~0_combout\,
	combout => \SDRAM1|u1|cmd_x[3]~3_combout\);

-- Location: LABCELL_X53_Y21_N0
\SDRAM1|u1|cmd_x[4]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|cmd_x[4]~4_combout\ = ( \SDRAM1|u1|state_x.RW~0_combout\ & ( \SDRAM1|u1|state_r.RW~q\ & ( (!\SDRAM1|u1|cmd_x[3]~3_combout\) # ((!\SDRAM1|u1|sAddr_x[10]~12_combout\) # ((\SDRAM1|u1|doActivate~combout\) # (\SDRAM1|u1|Selector28~0_combout\))) ) ) 
-- ) # ( !\SDRAM1|u1|state_x.RW~0_combout\ & ( \SDRAM1|u1|state_r.RW~q\ & ( (!\SDRAM1|u1|cmd_x[3]~3_combout\) # (\SDRAM1|u1|Selector28~0_combout\) ) ) ) # ( \SDRAM1|u1|state_x.RW~0_combout\ & ( !\SDRAM1|u1|state_r.RW~q\ & ( (!\SDRAM1|u1|cmd_x[3]~3_combout\) 
-- # (\SDRAM1|u1|Selector28~0_combout\) ) ) ) # ( !\SDRAM1|u1|state_x.RW~0_combout\ & ( !\SDRAM1|u1|state_r.RW~q\ & ( (!\SDRAM1|u1|cmd_x[3]~3_combout\) # (\SDRAM1|u1|Selector28~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010111110101111101011111010111110101111101011111110111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_cmd_x[3]~3_combout\,
	datab => \SDRAM1|u1|ALT_INV_sAddr_x[10]~12_combout\,
	datac => \SDRAM1|u1|ALT_INV_Selector28~0_combout\,
	datad => \SDRAM1|u1|ALT_INV_doActivate~combout\,
	datae => \SDRAM1|u1|ALT_INV_state_x.RW~0_combout\,
	dataf => \SDRAM1|u1|ALT_INV_state_r.RW~q\,
	combout => \SDRAM1|u1|cmd_x[4]~4_combout\);

-- Location: FF_X53_Y21_N1
\SDRAM1|u1|cmd_r[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|cmd_x[4]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|cmd_r\(4));

-- Location: LABCELL_X51_Y18_N30
\SDRAM1|u1|cmd_x[3]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|cmd_x[3]~6_combout\ = ( !\SDRAM1|u1|state_r.INITPCHG~q\ & ( !\SDRAM1|u1|state_r.ACTIVATE~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \SDRAM1|u1|ALT_INV_state_r.INITPCHG~q\,
	dataf => \SDRAM1|u1|ALT_INV_state_r.ACTIVATE~q\,
	combout => \SDRAM1|u1|cmd_x[3]~6_combout\);

-- Location: LABCELL_X53_Y21_N45
\SDRAM1|u1|cmd_x[3]~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|cmd_x[3]~7_combout\ = ( \SDRAM1|u1|cmd_x[3]~5_combout\ & ( \SDRAM1|u1|state_r.RW~q\ & ( (!\SDRAM1|u1|cmd_x[3]~3_combout\) # ((!\SDRAM1|u1|cmd_x[3]~6_combout\) # ((!\SDRAM1|u1|sAddr_x[10]~12_combout\) # (!\SDRAM1|u1|doActivate~combout\))) ) ) ) 
-- # ( !\SDRAM1|u1|cmd_x[3]~5_combout\ & ( \SDRAM1|u1|state_r.RW~q\ ) ) # ( \SDRAM1|u1|cmd_x[3]~5_combout\ & ( !\SDRAM1|u1|state_r.RW~q\ & ( (!\SDRAM1|u1|cmd_x[3]~3_combout\) # (!\SDRAM1|u1|cmd_x[3]~6_combout\) ) ) ) # ( !\SDRAM1|u1|cmd_x[3]~5_combout\ & ( 
-- !\SDRAM1|u1|state_r.RW~q\ & ( (!\SDRAM1|u1|cmd_x[3]~3_combout\) # (!\SDRAM1|u1|cmd_x[3]~6_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1110111011101110111011101110111011111111111111111111111111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_cmd_x[3]~3_combout\,
	datab => \SDRAM1|u1|ALT_INV_cmd_x[3]~6_combout\,
	datac => \SDRAM1|u1|ALT_INV_sAddr_x[10]~12_combout\,
	datad => \SDRAM1|u1|ALT_INV_doActivate~combout\,
	datae => \SDRAM1|u1|ALT_INV_cmd_x[3]~5_combout\,
	dataf => \SDRAM1|u1|ALT_INV_state_r.RW~q\,
	combout => \SDRAM1|u1|cmd_x[3]~7_combout\);

-- Location: FF_X53_Y21_N47
\SDRAM1|u1|cmd_r[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|cmd_x[3]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|cmd_r\(3));

-- Location: FF_X52_Y22_N34
\SDRAM1|u1|cke_r\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|cke_x~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|cke_r~q\);

-- Location: LABCELL_X48_Y21_N24
\SDRAM1|u1|cmd_x[2]~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|cmd_x[2]~8_combout\ = ( !\SDRAM1|u1|rdPipeline_r\(2) & ( !\SDRAM1|u1|rdPipeline_r\(1) & ( (!\SDRAM1|u1|rdPipeline_r\(3) & (\wr~q\ & (!\rd~DUPLICATE_q\ & !\SDRAM1|u1|rdPipeline_r\(4)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_rdPipeline_r\(3),
	datab => \ALT_INV_wr~q\,
	datac => \ALT_INV_rd~DUPLICATE_q\,
	datad => \SDRAM1|u1|ALT_INV_rdPipeline_r\(4),
	datae => \SDRAM1|u1|ALT_INV_rdPipeline_r\(2),
	dataf => \SDRAM1|u1|ALT_INV_rdPipeline_r\(1),
	combout => \SDRAM1|u1|cmd_x[2]~8_combout\);

-- Location: LABCELL_X50_Y21_N36
\SDRAM1|u1|cmd_x[2]~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|cmd_x[2]~9_combout\ = ( \SDRAM1|u1|state_r.INITSETMODE~q\ & ( \SDRAM1|u1|state_r.RW~q\ & ( (\SDRAM1|u1|Equal7~0_combout\ & (\SDRAM1|u1|Equal7~1_combout\ & \SDRAM1|u1|Equal7~2_combout\)) ) ) ) # ( !\SDRAM1|u1|state_r.INITSETMODE~q\ & ( 
-- \SDRAM1|u1|state_r.RW~q\ & ( (\SDRAM1|u1|Equal7~0_combout\ & (\SDRAM1|u1|Equal7~1_combout\ & \SDRAM1|u1|Equal7~2_combout\)) ) ) ) # ( \SDRAM1|u1|state_r.INITSETMODE~q\ & ( !\SDRAM1|u1|state_r.RW~q\ & ( (\SDRAM1|u1|Equal7~0_combout\ & 
-- (\SDRAM1|u1|Equal7~1_combout\ & \SDRAM1|u1|Equal7~2_combout\)) ) ) ) # ( !\SDRAM1|u1|state_r.INITSETMODE~q\ & ( !\SDRAM1|u1|state_r.RW~q\ & ( (\SDRAM1|u1|Equal7~0_combout\ & (\SDRAM1|u1|Equal7~1_combout\ & (\SDRAM1|u1|Equal7~2_combout\ & 
-- \SDRAM1|u1|state_r.INITPCHG~q\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000001000000010000000100000001000000010000000100000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Equal7~0_combout\,
	datab => \SDRAM1|u1|ALT_INV_Equal7~1_combout\,
	datac => \SDRAM1|u1|ALT_INV_Equal7~2_combout\,
	datad => \SDRAM1|u1|ALT_INV_state_r.INITPCHG~q\,
	datae => \SDRAM1|u1|ALT_INV_state_r.INITSETMODE~q\,
	dataf => \SDRAM1|u1|ALT_INV_state_r.RW~q\,
	combout => \SDRAM1|u1|cmd_x[2]~9_combout\);

-- Location: LABCELL_X53_Y21_N54
\SDRAM1|u1|Selector20~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|Selector20~0_combout\ = ( !\SDRAM1|u1|doActivate~combout\ & ( (\wr~q\) # (\rd~DUPLICATE_q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101111101011111010111110101111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rd~DUPLICATE_q\,
	datac => \ALT_INV_wr~q\,
	dataf => \SDRAM1|u1|ALT_INV_doActivate~combout\,
	combout => \SDRAM1|u1|Selector20~0_combout\);

-- Location: LABCELL_X53_Y21_N48
\SDRAM1|u1|cmd_x[2]~10\ : cyclonev_lcell_comb
-- Equation(s):
-- \SDRAM1|u1|cmd_x[2]~10_combout\ = ( \SDRAM1|u1|Selector20~0_combout\ & ( \SDRAM1|u1|state_r.RW~q\ & ( (!\SDRAM1|u1|cmd_x[2]~9_combout\) # (\SDRAM1|u1|Selector28~0_combout\) ) ) ) # ( !\SDRAM1|u1|Selector20~0_combout\ & ( \SDRAM1|u1|state_r.RW~q\ & ( 
-- (!\SDRAM1|u1|cmd_x[2]~9_combout\) # ((!\SDRAM1|u1|state_x.RW~0_combout\ & (\SDRAM1|u1|Selector28~0_combout\)) # (\SDRAM1|u1|state_x.RW~0_combout\ & ((!\SDRAM1|u1|cmd_x[2]~8_combout\)))) ) ) ) # ( \SDRAM1|u1|Selector20~0_combout\ & ( 
-- !\SDRAM1|u1|state_r.RW~q\ & ( (!\SDRAM1|u1|cmd_x[2]~9_combout\) # (\SDRAM1|u1|Selector28~0_combout\) ) ) ) # ( !\SDRAM1|u1|Selector20~0_combout\ & ( !\SDRAM1|u1|state_r.RW~q\ & ( (!\SDRAM1|u1|cmd_x[2]~9_combout\) # ((\SDRAM1|u1|Selector28~0_combout\ & 
-- !\SDRAM1|u1|state_x.RW~0_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111010111110000111101011111010111110101111111001111010111110101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_Selector28~0_combout\,
	datab => \SDRAM1|u1|ALT_INV_cmd_x[2]~8_combout\,
	datac => \SDRAM1|u1|ALT_INV_cmd_x[2]~9_combout\,
	datad => \SDRAM1|u1|ALT_INV_state_x.RW~0_combout\,
	datae => \SDRAM1|u1|ALT_INV_Selector20~0_combout\,
	dataf => \SDRAM1|u1|ALT_INV_state_r.RW~q\,
	combout => \SDRAM1|u1|cmd_x[2]~10_combout\);

-- Location: FF_X53_Y21_N49
\SDRAM1|u1|cmd_r[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[0]~CLKENA0_outclk\,
	d => \SDRAM1|u1|cmd_x[2]~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \SDRAM1|u1|cmd_r\(2));

-- Location: LABCELL_X48_Y20_N3
\Selector36~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector36~0_combout\ = ( \FSM_autotest.A_IDLE~q\ & ( (\LEDR[0]~reg0_q\) # (\FSM_autotest.A_WRITE~q\) ) ) # ( !\FSM_autotest.A_IDLE~q\ & ( (\KEY[3]~input_o\ & \LEDR[0]~reg0_q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010101000000000101010100001111111111110000111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_KEY[3]~input_o\,
	datac => \ALT_INV_FSM_autotest.A_WRITE~q\,
	datad => \ALT_INV_LEDR[0]~reg0_q\,
	dataf => \ALT_INV_FSM_autotest.A_IDLE~q\,
	combout => \Selector36~0_combout\);

-- Location: FF_X48_Y20_N5
\LEDR[0]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Selector36~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LEDR[0]~reg0_q\);

-- Location: LABCELL_X48_Y20_N6
\Selector35~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector35~0_combout\ = ( \FSM_autotest.A_IDLE~q\ & ( (\LEDR[1]~reg0_q\) # (\FSM_autotest.A_READ~q\) ) ) # ( !\FSM_autotest.A_IDLE~q\ & ( (\KEY[3]~input_o\ & \LEDR[1]~reg0_q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111000000000000111100110011111111110011001111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_FSM_autotest.A_READ~q\,
	datac => \ALT_INV_KEY[3]~input_o\,
	datad => \ALT_INV_LEDR[1]~reg0_q\,
	dataf => \ALT_INV_FSM_autotest.A_IDLE~q\,
	combout => \Selector35~0_combout\);

-- Location: FF_X48_Y20_N7
\LEDR[1]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \Selector35~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LEDR[1]~reg0_q\);

-- Location: LABCELL_X48_Y20_N12
\LEDR[2]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LEDR[2]~0_combout\ = ( \FSM_autotest.A_IDLE~q\ & ( ((\Equal0~4_combout\ & \FSM_autotest.A_SET_READ~q\)) # (\LEDR[2]~reg0_q\) ) ) # ( !\FSM_autotest.A_IDLE~q\ & ( (\KEY[3]~input_o\ & \LEDR[2]~reg0_q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010101000000000101010100000011111111110000001111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_KEY[3]~input_o\,
	datab => \ALT_INV_Equal0~4_combout\,
	datac => \ALT_INV_FSM_autotest.A_SET_READ~q\,
	datad => \ALT_INV_LEDR[2]~reg0_q\,
	dataf => \ALT_INV_FSM_autotest.A_IDLE~q\,
	combout => \LEDR[2]~0_combout\);

-- Location: FF_X48_Y20_N13
\LEDR[2]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \LEDR[2]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LEDR[2]~reg0_q\);

-- Location: LABCELL_X48_Y20_N18
\LEDR[8]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \LEDR[8]~1_combout\ = ( \LEDR[8]~reg0_q\ & ( \FSM_autotest.A_IDLE~q\ ) ) # ( !\LEDR[8]~reg0_q\ & ( \FSM_autotest.A_IDLE~q\ & ( (\SDRAM1|u1|rdPipeline_r\(0) & (\FSM_autotest.A_READ_WAIT~q\ & !\Equal1~6_combout\)) ) ) ) # ( \LEDR[8]~reg0_q\ & ( 
-- !\FSM_autotest.A_IDLE~q\ & ( \KEY[3]~input_o\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100010000000100001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \SDRAM1|u1|ALT_INV_rdPipeline_r\(0),
	datab => \ALT_INV_FSM_autotest.A_READ_WAIT~q\,
	datac => \ALT_INV_Equal1~6_combout\,
	datad => \ALT_INV_KEY[3]~input_o\,
	datae => \ALT_INV_LEDR[8]~reg0_q\,
	dataf => \ALT_INV_FSM_autotest.A_IDLE~q\,
	combout => \LEDR[8]~1_combout\);

-- Location: FF_X48_Y20_N19
\LEDR[8]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \PLL|pll_x2_x4_inst|altera_pll_i|outclk_wire[1]~CLKENA0_outclk\,
	d => \LEDR[8]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LEDR[8]~reg0_q\);

-- Location: LABCELL_X50_Y20_N3
\LED01|Mux6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED01|Mux6~0_combout\ = ( hAddr(0) & ( (!hAddr(2) $ (!hAddr(1))) # (hAddr(3)) ) ) # ( !hAddr(0) & ( (!hAddr(2) $ (!hAddr(3))) # (hAddr(1)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101111110101111010111111010111101011010111111110101101011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(2),
	datac => ALT_INV_hAddr(1),
	datad => ALT_INV_hAddr(3),
	dataf => ALT_INV_hAddr(0),
	combout => \LED01|Mux6~0_combout\);

-- Location: LABCELL_X50_Y20_N6
\LED01|Mux5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED01|Mux5~0_combout\ = ( hAddr(0) & ( !hAddr(3) $ (((hAddr(2) & !hAddr(1)))) ) ) # ( !hAddr(0) & ( (!hAddr(3) & (!hAddr(2) & hAddr(1))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100000001000000010000000100010011010100110101001101010011010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(3),
	datab => ALT_INV_hAddr(2),
	datac => ALT_INV_hAddr(1),
	dataf => ALT_INV_hAddr(0),
	combout => \LED01|Mux5~0_combout\);

-- Location: LABCELL_X50_Y20_N9
\LED01|Mux4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED01|Mux4~0_combout\ = ( hAddr(0) & ( (!hAddr(3)) # ((!hAddr(2) & !hAddr(1))) ) ) # ( !hAddr(0) & ( (!hAddr(3) & (hAddr(2) & !hAddr(1))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000000100000001000000010000011101010111010101110101011101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(3),
	datab => ALT_INV_hAddr(2),
	datac => ALT_INV_hAddr(1),
	dataf => ALT_INV_hAddr(0),
	combout => \LED01|Mux4~0_combout\);

-- Location: LABCELL_X50_Y20_N12
\LED01|Mux3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED01|Mux3~0_combout\ = ( hAddr(0) & ( (!hAddr(2) & (!hAddr(3) & !hAddr(1))) # (hAddr(2) & ((hAddr(1)))) ) ) # ( !hAddr(0) & ( (!hAddr(2) & (hAddr(3) & hAddr(1))) # (hAddr(2) & (!hAddr(3) & !hAddr(1))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000001100001100000000110011000000001100111100000000110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_hAddr(2),
	datac => ALT_INV_hAddr(3),
	datad => ALT_INV_hAddr(1),
	dataf => ALT_INV_hAddr(0),
	combout => \LED01|Mux3~0_combout\);

-- Location: LABCELL_X50_Y20_N15
\LED01|Mux2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED01|Mux2~0_combout\ = ( hAddr(0) & ( (hAddr(3) & (hAddr(2) & hAddr(1))) ) ) # ( !hAddr(0) & ( (!hAddr(3) & (!hAddr(2) & hAddr(1))) # (hAddr(3) & (hAddr(2))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001100100011001000110010001100100000001000000010000000100000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(3),
	datab => ALT_INV_hAddr(2),
	datac => ALT_INV_hAddr(1),
	dataf => ALT_INV_hAddr(0),
	combout => \LED01|Mux2~0_combout\);

-- Location: LABCELL_X50_Y20_N48
\LED01|Mux1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED01|Mux1~0_combout\ = ( hAddr(0) & ( (!hAddr(3) & (hAddr(2) & !hAddr(1))) # (hAddr(3) & ((hAddr(1)))) ) ) # ( !hAddr(0) & ( (hAddr(2) & ((hAddr(1)) # (hAddr(3)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001001100010011000100110001001100100101001001010010010100100101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(3),
	datab => ALT_INV_hAddr(2),
	datac => ALT_INV_hAddr(1),
	dataf => ALT_INV_hAddr(0),
	combout => \LED01|Mux1~0_combout\);

-- Location: LABCELL_X50_Y20_N51
\LED01|Mux0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED01|Mux0~0_combout\ = ( hAddr(0) & ( (!hAddr(3) & (!hAddr(2) & !hAddr(1))) # (hAddr(3) & (!hAddr(2) $ (!hAddr(1)))) ) ) # ( !hAddr(0) & ( (!hAddr(3) & (hAddr(2) & !hAddr(1))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000000100000001000000010000010010100100101001001010010010100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(3),
	datab => ALT_INV_hAddr(2),
	datac => ALT_INV_hAddr(1),
	dataf => ALT_INV_hAddr(0),
	combout => \LED01|Mux0~0_combout\);

-- Location: MLABCELL_X52_Y20_N6
\LED02|Mux6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED02|Mux6~0_combout\ = ( hAddr(4) & ( (!hAddr(5) $ (!hAddr(6))) # (hAddr(7)) ) ) # ( !hAddr(4) & ( (!hAddr(7) $ (!hAddr(6))) # (hAddr(5)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111110101111101011111010111110101111011011110110111101101111011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(5),
	datab => ALT_INV_hAddr(7),
	datac => ALT_INV_hAddr(6),
	dataf => ALT_INV_hAddr(4),
	combout => \LED02|Mux6~0_combout\);

-- Location: MLABCELL_X52_Y20_N9
\LED02|Mux5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED02|Mux5~0_combout\ = ( hAddr(4) & ( !hAddr(7) $ (((!hAddr(5) & hAddr(6)))) ) ) # ( !hAddr(4) & ( (hAddr(5) & (!hAddr(7) & !hAddr(6))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100010000000000010001000000000011001100011001101100110001100110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(5),
	datab => ALT_INV_hAddr(7),
	datad => ALT_INV_hAddr(6),
	dataf => ALT_INV_hAddr(4),
	combout => \LED02|Mux5~0_combout\);

-- Location: MLABCELL_X52_Y20_N42
\LED02|Mux4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED02|Mux4~0_combout\ = ( hAddr(4) & ( (!hAddr(7)) # ((!hAddr(5) & !hAddr(6))) ) ) # ( !hAddr(4) & ( (!hAddr(7) & (!hAddr(5) & hAddr(6))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011000000000000001100000011111100110011001111110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_hAddr(7),
	datac => ALT_INV_hAddr(5),
	datad => ALT_INV_hAddr(6),
	dataf => ALT_INV_hAddr(4),
	combout => \LED02|Mux4~0_combout\);

-- Location: MLABCELL_X52_Y20_N45
\LED02|Mux3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED02|Mux3~0_combout\ = ( hAddr(4) & ( (!hAddr(5) & (!hAddr(7) & !hAddr(6))) # (hAddr(5) & ((hAddr(6)))) ) ) # ( !hAddr(4) & ( (!hAddr(5) & (!hAddr(7) & hAddr(6))) # (hAddr(5) & (hAddr(7) & !hAddr(6))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000110001000000100011000100010001000010101011000100001010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(5),
	datab => ALT_INV_hAddr(7),
	datad => ALT_INV_hAddr(6),
	dataf => ALT_INV_hAddr(4),
	combout => \LED02|Mux3~0_combout\);

-- Location: MLABCELL_X52_Y20_N0
\LED02|Mux2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED02|Mux2~0_combout\ = ( hAddr(4) & ( (hAddr(6) & (hAddr(5) & hAddr(7))) ) ) # ( !hAddr(4) & ( (!hAddr(6) & (hAddr(5) & !hAddr(7))) # (hAddr(6) & ((hAddr(7)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101001010101000010100101010100000000000001010000000000000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(6),
	datac => ALT_INV_hAddr(5),
	datad => ALT_INV_hAddr(7),
	dataf => ALT_INV_hAddr(4),
	combout => \LED02|Mux2~0_combout\);

-- Location: MLABCELL_X52_Y20_N21
\LED02|Mux1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED02|Mux1~0_combout\ = ( hAddr(4) & ( (!hAddr(5) & (!hAddr(7) & hAddr(6))) # (hAddr(5) & (hAddr(7))) ) ) # ( !hAddr(4) & ( (hAddr(6) & ((hAddr(7)) # (hAddr(5)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001110111000000000111011100010001100110010001000110011001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(5),
	datab => ALT_INV_hAddr(7),
	datad => ALT_INV_hAddr(6),
	dataf => ALT_INV_hAddr(4),
	combout => \LED02|Mux1~0_combout\);

-- Location: MLABCELL_X52_Y20_N18
\LED02|Mux0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED02|Mux0~0_combout\ = ( hAddr(4) & ( (!hAddr(5) & (!hAddr(7) $ (hAddr(6)))) # (hAddr(5) & (hAddr(7) & !hAddr(6))) ) ) # ( !hAddr(4) & ( (!hAddr(5) & (!hAddr(7) & hAddr(6))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010100000000000001010000010100101000010101010010100001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(5),
	datac => ALT_INV_hAddr(7),
	datad => ALT_INV_hAddr(6),
	dataf => ALT_INV_hAddr(4),
	combout => \LED02|Mux0~0_combout\);

-- Location: LABCELL_X51_Y21_N33
\LED03|Mux6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED03|Mux6~0_combout\ = ( hAddr(9) & ( (!hAddr(8)) # ((!hAddr(10)) # (hAddr(11))) ) ) # ( !hAddr(9) & ( (!hAddr(11) & ((hAddr(10)))) # (hAddr(11) & ((!hAddr(10)) # (hAddr(8)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111111110101000011111111010111111111101011111111111110101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(8),
	datac => ALT_INV_hAddr(11),
	datad => ALT_INV_hAddr(10),
	dataf => ALT_INV_hAddr(9),
	combout => \LED03|Mux6~0_combout\);

-- Location: LABCELL_X53_Y20_N30
\LED03|Mux5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED03|Mux5~0_combout\ = ( hAddr(9) & ( (!hAddr(11) & ((!hAddr(10)) # (hAddr(8)))) ) ) # ( !hAddr(9) & ( (hAddr(8) & (!hAddr(10) $ (hAddr(11)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000000011000011000000001111001111000000001100111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_hAddr(10),
	datac => ALT_INV_hAddr(8),
	datad => ALT_INV_hAddr(11),
	dataf => ALT_INV_hAddr(9),
	combout => \LED03|Mux5~0_combout\);

-- Location: LABCELL_X53_Y20_N33
\LED03|Mux4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED03|Mux4~0_combout\ = ( hAddr(9) & ( (hAddr(8) & !hAddr(11)) ) ) # ( !hAddr(9) & ( (!hAddr(10) & (hAddr(8))) # (hAddr(10) & ((!hAddr(11)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111011101000100011101110100010001010101000000000101010100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(8),
	datab => ALT_INV_hAddr(10),
	datad => ALT_INV_hAddr(11),
	dataf => ALT_INV_hAddr(9),
	combout => \LED03|Mux4~0_combout\);

-- Location: LABCELL_X53_Y20_N36
\LED03|Mux3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED03|Mux3~0_combout\ = ( hAddr(9) & ( (!hAddr(8) & (!hAddr(10) & hAddr(11))) # (hAddr(8) & (hAddr(10))) ) ) # ( !hAddr(9) & ( (!hAddr(11) & (!hAddr(8) $ (!hAddr(10)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0110000001100000011000000110000000011001000110010001100100011001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(8),
	datab => ALT_INV_hAddr(10),
	datac => ALT_INV_hAddr(11),
	dataf => ALT_INV_hAddr(9),
	combout => \LED03|Mux3~0_combout\);

-- Location: LABCELL_X51_Y21_N6
\LED03|Mux2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED03|Mux2~0_combout\ = ( hAddr(11) & ( (hAddr(10) & ((!hAddr(8)) # (hAddr(9)))) ) ) # ( !hAddr(11) & ( (!hAddr(8) & (!hAddr(10) & hAddr(9))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100000001000000010000000100000100011001000110010001100100011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(8),
	datab => ALT_INV_hAddr(10),
	datac => ALT_INV_hAddr(9),
	dataf => ALT_INV_hAddr(11),
	combout => \LED03|Mux2~0_combout\);

-- Location: LABCELL_X53_Y20_N39
\LED03|Mux1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED03|Mux1~0_combout\ = ( hAddr(9) & ( (!hAddr(8) & (hAddr(10))) # (hAddr(8) & ((hAddr(11)))) ) ) # ( !hAddr(9) & ( (hAddr(10) & (!hAddr(8) $ (!hAddr(11)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000100100010000100010010001000100010011101110010001001110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(8),
	datab => ALT_INV_hAddr(10),
	datad => ALT_INV_hAddr(11),
	dataf => ALT_INV_hAddr(9),
	combout => \LED03|Mux1~0_combout\);

-- Location: LABCELL_X51_Y21_N9
\LED03|Mux0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED03|Mux0~0_combout\ = ( hAddr(11) & ( (hAddr(8) & (!hAddr(10) $ (!hAddr(9)))) ) ) # ( !hAddr(11) & ( (!hAddr(9) & (!hAddr(8) $ (!hAddr(10)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0110000001100000011000000110000000010100000101000001010000010100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(8),
	datab => ALT_INV_hAddr(10),
	datac => ALT_INV_hAddr(9),
	dataf => ALT_INV_hAddr(11),
	combout => \LED03|Mux0~0_combout\);

-- Location: LABCELL_X50_Y20_N54
\LED04|Mux6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED04|Mux6~0_combout\ = (!hAddr(12) & ((!hAddr(15) $ (!hAddr(14))) # (hAddr(13)))) # (hAddr(12) & ((!hAddr(14) $ (!hAddr(13))) # (hAddr(15))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0110111101111101011011110111110101101111011111010110111101111101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(15),
	datab => ALT_INV_hAddr(14),
	datac => ALT_INV_hAddr(13),
	datad => ALT_INV_hAddr(12),
	combout => \LED04|Mux6~0_combout\);

-- Location: LABCELL_X50_Y20_N33
\LED04|Mux5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED04|Mux5~0_combout\ = ( hAddr(12) & ( !hAddr(15) $ (((hAddr(14) & !hAddr(13)))) ) ) # ( !hAddr(12) & ( (!hAddr(15) & (!hAddr(14) & hAddr(13))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010100000000000001010000010100101101010101010010110101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(15),
	datac => ALT_INV_hAddr(14),
	datad => ALT_INV_hAddr(13),
	dataf => ALT_INV_hAddr(12),
	combout => \LED04|Mux5~0_combout\);

-- Location: LABCELL_X50_Y20_N36
\LED04|Mux4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED04|Mux4~0_combout\ = ( hAddr(12) & ( (!hAddr(15)) # ((!hAddr(14) & !hAddr(13))) ) ) # ( !hAddr(12) & ( (!hAddr(15) & (hAddr(14) & !hAddr(13))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010001000000000001000100000000011101110101010101110111010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(15),
	datab => ALT_INV_hAddr(14),
	datad => ALT_INV_hAddr(13),
	dataf => ALT_INV_hAddr(12),
	combout => \LED04|Mux4~0_combout\);

-- Location: LABCELL_X50_Y20_N39
\LED04|Mux3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED04|Mux3~0_combout\ = ( hAddr(12) & ( (!hAddr(14) & (!hAddr(15) & !hAddr(13))) # (hAddr(14) & ((hAddr(13)))) ) ) # ( !hAddr(12) & ( (!hAddr(15) & (hAddr(14) & !hAddr(13))) # (hAddr(15) & (!hAddr(14) & hAddr(13))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101001010000000010100101000010100000000011111010000000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(15),
	datac => ALT_INV_hAddr(14),
	datad => ALT_INV_hAddr(13),
	dataf => ALT_INV_hAddr(12),
	combout => \LED04|Mux3~0_combout\);

-- Location: LABCELL_X50_Y20_N42
\LED04|Mux2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED04|Mux2~0_combout\ = ( hAddr(12) & ( (hAddr(15) & (hAddr(14) & hAddr(13))) ) ) # ( !hAddr(12) & ( (!hAddr(15) & (!hAddr(14) & hAddr(13))) # (hAddr(15) & (hAddr(14))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001100100011001000110010001100100000001000000010000000100000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(15),
	datab => ALT_INV_hAddr(14),
	datac => ALT_INV_hAddr(13),
	dataf => ALT_INV_hAddr(12),
	combout => \LED04|Mux2~0_combout\);

-- Location: LABCELL_X50_Y20_N45
\LED04|Mux1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED04|Mux1~0_combout\ = ( hAddr(12) & ( (!hAddr(15) & (hAddr(14) & !hAddr(13))) # (hAddr(15) & ((hAddr(13)))) ) ) # ( !hAddr(12) & ( (hAddr(14) & ((hAddr(13)) # (hAddr(15)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100001111000001010000111100001010010101010000101001010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(15),
	datac => ALT_INV_hAddr(14),
	datad => ALT_INV_hAddr(13),
	dataf => ALT_INV_hAddr(12),
	combout => \LED04|Mux1~0_combout\);

-- Location: LABCELL_X50_Y20_N57
\LED04|Mux0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED04|Mux0~0_combout\ = ( hAddr(12) & ( (!hAddr(15) & (!hAddr(14) & !hAddr(13))) # (hAddr(15) & (!hAddr(14) $ (!hAddr(13)))) ) ) # ( !hAddr(12) & ( (!hAddr(15) & (hAddr(14) & !hAddr(13))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101000000000000010100000000010100101010100001010010101010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(15),
	datac => ALT_INV_hAddr(14),
	datad => ALT_INV_hAddr(13),
	dataf => ALT_INV_hAddr(12),
	combout => \LED04|Mux0~0_combout\);

-- Location: LABCELL_X85_Y17_N3
\LED05|Mux6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED05|Mux6~0_combout\ = ( hAddr(19) & ( (!hAddr(18)) # ((hAddr(16)) # (hAddr(17))) ) ) # ( !hAddr(19) & ( (!hAddr(18) & (hAddr(17))) # (hAddr(18) & ((!hAddr(17)) # (!hAddr(16)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111011101100110011101110110011010111011111111111011101111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(18),
	datab => ALT_INV_hAddr(17),
	datad => ALT_INV_hAddr(16),
	dataf => ALT_INV_hAddr(19),
	combout => \LED05|Mux6~0_combout\);

-- Location: LABCELL_X85_Y17_N6
\LED05|Mux5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED05|Mux5~0_combout\ = (!hAddr(18) & (!hAddr(19) & ((hAddr(16)) # (hAddr(17))))) # (hAddr(18) & (hAddr(16) & (!hAddr(17) $ (!hAddr(19)))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000010110100001000001011010000100000101101000010000010110100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(18),
	datab => ALT_INV_hAddr(17),
	datac => ALT_INV_hAddr(19),
	datad => ALT_INV_hAddr(16),
	combout => \LED05|Mux5~0_combout\);

-- Location: LABCELL_X85_Y17_N9
\LED05|Mux4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED05|Mux4~0_combout\ = ( hAddr(19) & ( (!hAddr(18) & (!hAddr(17) & hAddr(16))) ) ) # ( !hAddr(19) & ( ((hAddr(18) & !hAddr(17))) # (hAddr(16)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100010011111111010001001111111100000000100010000000000010001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(18),
	datab => ALT_INV_hAddr(17),
	datad => ALT_INV_hAddr(16),
	dataf => ALT_INV_hAddr(19),
	combout => \LED05|Mux4~0_combout\);

-- Location: LABCELL_X85_Y17_N12
\LED05|Mux3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED05|Mux3~0_combout\ = ( hAddr(19) & ( (hAddr(17) & (!hAddr(18) $ (hAddr(16)))) ) ) # ( !hAddr(19) & ( (!hAddr(17) & (!hAddr(18) $ (!hAddr(16)))) # (hAddr(17) & (hAddr(18) & hAddr(16))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110011000011000011001100001100110000000000110011000000000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_hAddr(17),
	datac => ALT_INV_hAddr(18),
	datad => ALT_INV_hAddr(16),
	dataf => ALT_INV_hAddr(19),
	combout => \LED05|Mux3~0_combout\);

-- Location: LABCELL_X85_Y17_N15
\LED05|Mux2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED05|Mux2~0_combout\ = ( hAddr(19) & ( (hAddr(18) & ((!hAddr(16)) # (hAddr(17)))) ) ) # ( !hAddr(19) & ( (!hAddr(18) & (hAddr(17) & !hAddr(16))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010001000000000001000100000000001010101000100010101010100010001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(18),
	datab => ALT_INV_hAddr(17),
	datad => ALT_INV_hAddr(16),
	dataf => ALT_INV_hAddr(19),
	combout => \LED05|Mux2~0_combout\);

-- Location: LABCELL_X85_Y17_N48
\LED05|Mux1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED05|Mux1~0_combout\ = ( hAddr(16) & ( (!hAddr(17) & (hAddr(18) & !hAddr(19))) # (hAddr(17) & ((hAddr(19)))) ) ) # ( !hAddr(16) & ( (hAddr(18) & ((hAddr(19)) # (hAddr(17)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001010100010101010000110100001100010101000101010100001101000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(18),
	datab => ALT_INV_hAddr(17),
	datac => ALT_INV_hAddr(19),
	datae => ALT_INV_hAddr(16),
	combout => \LED05|Mux1~0_combout\);

-- Location: LABCELL_X85_Y17_N0
\LED05|Mux0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED05|Mux0~0_combout\ = ( hAddr(19) & ( (hAddr(16) & (!hAddr(17) $ (!hAddr(18)))) ) ) # ( !hAddr(19) & ( (!hAddr(17) & (!hAddr(18) $ (!hAddr(16)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110011000000000011001100000000000000001111000000000000111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_hAddr(17),
	datac => ALT_INV_hAddr(18),
	datad => ALT_INV_hAddr(16),
	dataf => ALT_INV_hAddr(19),
	combout => \LED05|Mux0~0_combout\);

-- Location: MLABCELL_X52_Y21_N48
\LED06|Mux6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED06|Mux6~0_combout\ = ( hAddr(21) & ( (!hAddr(20)) # ((!hAddr(22)) # (hAddr(23))) ) ) # ( !hAddr(21) & ( (!hAddr(23) & ((hAddr(22)))) # (hAddr(23) & ((!hAddr(22)) # (hAddr(20)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011110100111101001111010011110111111011111110111111101111111011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(20),
	datab => ALT_INV_hAddr(23),
	datac => ALT_INV_hAddr(22),
	dataf => ALT_INV_hAddr(21),
	combout => \LED06|Mux6~0_combout\);

-- Location: MLABCELL_X52_Y21_N51
\LED06|Mux5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED06|Mux5~0_combout\ = ( hAddr(21) & ( (!hAddr(23) & ((!hAddr(22)) # (hAddr(20)))) ) ) # ( !hAddr(21) & ( (hAddr(20) & (!hAddr(23) $ (hAddr(22)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100010000010001010001000001000111001100010001001100110001000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(20),
	datab => ALT_INV_hAddr(23),
	datad => ALT_INV_hAddr(22),
	dataf => ALT_INV_hAddr(21),
	combout => \LED06|Mux5~0_combout\);

-- Location: MLABCELL_X52_Y21_N54
\LED06|Mux4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED06|Mux4~0_combout\ = ( hAddr(21) & ( (hAddr(20) & !hAddr(23)) ) ) # ( !hAddr(21) & ( (!hAddr(22) & (hAddr(20))) # (hAddr(22) & ((!hAddr(23)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101110001011100010111000101110001000100010001000100010001000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(20),
	datab => ALT_INV_hAddr(23),
	datac => ALT_INV_hAddr(22),
	dataf => ALT_INV_hAddr(21),
	combout => \LED06|Mux4~0_combout\);

-- Location: MLABCELL_X52_Y21_N57
\LED06|Mux3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED06|Mux3~0_combout\ = ( hAddr(21) & ( (!hAddr(20) & (hAddr(23) & !hAddr(22))) # (hAddr(20) & ((hAddr(22)))) ) ) # ( !hAddr(21) & ( (!hAddr(23) & (!hAddr(20) $ (!hAddr(22)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100010010001000010001001000100000100010010101010010001001010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(20),
	datab => ALT_INV_hAddr(23),
	datad => ALT_INV_hAddr(22),
	dataf => ALT_INV_hAddr(21),
	combout => \LED06|Mux3~0_combout\);

-- Location: MLABCELL_X52_Y21_N30
\LED06|Mux2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED06|Mux2~0_combout\ = ( hAddr(21) & ( (!hAddr(23) & (!hAddr(20) & !hAddr(22))) # (hAddr(23) & ((hAddr(22)))) ) ) # ( !hAddr(21) & ( (!hAddr(20) & (hAddr(23) & hAddr(22))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001000000010000000100000001010000011100000111000001110000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(20),
	datab => ALT_INV_hAddr(23),
	datac => ALT_INV_hAddr(22),
	dataf => ALT_INV_hAddr(21),
	combout => \LED06|Mux2~0_combout\);

-- Location: MLABCELL_X52_Y21_N33
\LED06|Mux1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED06|Mux1~0_combout\ = ( hAddr(21) & ( (!hAddr(20) & ((hAddr(22)))) # (hAddr(20) & (hAddr(23))) ) ) # ( !hAddr(21) & ( (hAddr(22) & (!hAddr(20) $ (!hAddr(23)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000011000000110000001100000011000011011000110110001101100011011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(20),
	datab => ALT_INV_hAddr(23),
	datac => ALT_INV_hAddr(22),
	dataf => ALT_INV_hAddr(21),
	combout => \LED06|Mux1~0_combout\);

-- Location: MLABCELL_X52_Y21_N6
\LED06|Mux0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LED06|Mux0~0_combout\ = ( hAddr(21) & ( (!hAddr(22) & (hAddr(20) & hAddr(23))) ) ) # ( !hAddr(21) & ( (!hAddr(22) & (hAddr(20) & !hAddr(23))) # (hAddr(22) & (!hAddr(20) $ (hAddr(23)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101101000000101010110100000010100000000000010100000000000001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_hAddr(22),
	datac => ALT_INV_hAddr(20),
	datad => ALT_INV_hAddr(23),
	dataf => ALT_INV_hAddr(21),
	combout => \LED06|Mux0~0_combout\);

-- Location: IOIBUF_X12_Y0_N18
\SW[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(0),
	o => \SW[0]~input_o\);

-- Location: IOIBUF_X16_Y0_N1
\SW[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(1),
	o => \SW[1]~input_o\);

-- Location: IOIBUF_X8_Y0_N35
\SW[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(2),
	o => \SW[2]~input_o\);

-- Location: IOIBUF_X4_Y0_N52
\SW[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(3),
	o => \SW[3]~input_o\);

-- Location: IOIBUF_X2_Y0_N41
\SW[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(4),
	o => \SW[4]~input_o\);

-- Location: IOIBUF_X16_Y0_N18
\SW[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(5),
	o => \SW[5]~input_o\);

-- Location: IOIBUF_X4_Y0_N35
\SW[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(6),
	o => \SW[6]~input_o\);

-- Location: IOIBUF_X4_Y0_N1
\SW[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(7),
	o => \SW[7]~input_o\);

-- Location: IOIBUF_X4_Y0_N18
\SW[8]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(8),
	o => \SW[8]~input_o\);

-- Location: IOIBUF_X2_Y0_N58
\SW[9]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(9),
	o => \SW[9]~input_o\);

-- Location: IOIBUF_X36_Y0_N1
\KEY[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(0),
	o => \KEY[0]~input_o\);

-- Location: IOIBUF_X36_Y0_N18
\KEY[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(1),
	o => \KEY[1]~input_o\);

-- Location: IOIBUF_X40_Y0_N1
\KEY[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(2),
	o => \KEY[2]~input_o\);

-- Location: LABCELL_X36_Y23_N0
\~QUARTUS_CREATED_GND~I\ : cyclonev_lcell_comb
-- Equation(s):

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
;


pll_reconfig_inst_tasks : altera_pll_reconfig_tasks
-- pragma translate_off
GENERIC MAP (
		number_of_fplls => 1);
-- pragma translate_on
END structure;



library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;
use WORK.common.all;
use WORK.sdram.all;
use WORK.XSASDRAM.all;
use work.decoder_7seg.all;

entity SDRAM_TEST is
	port(
		--SDRAM connections
		DRAM_ADDR	: out std_logic_vector(12 downto 0);
		DRAM_DQ		: inout std_logic_vector(15 downto 0);
		DRAM_BA		: out std_logic_vector(1 downto 0);
		DRAM_LDQM 	: out std_logic;
		DRAM_UDQM	: out std_logic;
		DRAM_RAS_N	: out std_logic;
		DRAM_CAS_N	: out std_logic;
		DRAM_CKE		: out std_logic;
		DRAM_CLK		: out std_logic;
		DRAM_WE_N	: out std_logic;
		DRAM_CS_N	: out std_logic;
		--CLOCK inputs
		CLOCK_50		: in std_logic;
		--OUTPUT indicatosr
		LEDR			: out std_logic_vector(9 downto 0) := "0000000000";
		HEX0			: out std_logic_vector(0 to 6);
		HEX1			: out std_logic_vector(0 to 6);
		HEX2			: out std_logic_vector(0 to 6);
		HEX3			: out std_logic_vector(0 to 6);
		HEX4			: out std_logic_vector(0 to 6);
		HEX5			: out std_logic_vector(0 to 6);
		--INPUT controls
		SW				: in std_logic_vector(9 downto 0);
		KEY			: in std_logic_vector(3 downto 0)
	);
end SDRAM_TEST;

architecture basic of SDRAM_TEST is
	signal rd : std_logic := '0';
	signal wr : std_logic := '0';
	signal done : std_logic;
	signal rdDone : std_logic;
	signal hAddr : std_logic_vector(24 downto 0) := (others => '0');
	signal hDIn : std_logic_vector(15 downto 0) := (others => '0');
	signal hDOut : std_logic_vector(15 downto 0) := (others => '0');
	
	type FSM_inout_type is (IDLE, ADDR_LD, DATA_RD, DATA_RD_WAIT, DATA_WR, DATA_WR_WAIT);
	signal FSM_inout : FSM_inout_type := IDLE;
	
	type FSM_autotest_type is (A_IDLE, A_SET_WRITE, A_WRITE, A_WRITE_WAIT, A_SET_READ, A_READ, A_READ_WAIT);
	signal FSM_autotest : FSM_autotest_type := A_IDLE;
	
	--signal clk_25 : std_logic;
	signal CLOCK_100 : std_logic;
	signal CLOCK_200 : std_logic;
	signal CLOCK_150 : std_logic;
	signal CLOCK_300 : std_logic;
	
	component PLL_x2_x4 is
		port (
			refclk   : in  std_logic := '0'; --  refclk.clk 50
			rst      : in  std_logic := '0'; --   reset.reset
			outclk_0 : out std_logic;        -- outclk0.clk 100
			outclk_1 : out std_logic;        -- outclk1.clk 200
			outclk_2 : out std_logic;        -- outclk2.clk 150
			outclk_3 : out std_logic;        -- outclk3.clk 300
			locked   : out std_logic         --  locked.export
		);
	end component PLL_x2_x4;
begin
	
	PLL : PLL_x2_x4 port map(
		refclk => CLOCK_50,
		rst => '0',
		outclk_0 => CLOCK_100,
		outclk_1 => CLOCK_200,
		outclk_2 => CLOCK_150,
		outclk_3 => CLOCK_300,
		locked => open);
	
	
	LED01 : seg7 port map(s => HEX0, q => hAddr(3 downto 0));
	LED02 : seg7 port map(s => HEX1, q => hAddr(7 downto 4));
	LED03 : seg7 port map(s => HEX2, q => hAddr(11 downto 8));
	LED04 : seg7 port map(s => HEX3, q => hAddr(15 downto 12));
	LED05 : seg7 port map(s => HEX4, q => hAddr(19 downto 16));
	LED06 : seg7 port map(s => HEX5, q => hAddr(23 downto 20));
	LEDR(9) <= hAddr(24);
	
--	process(CLOCK_50)
--	begin
--		if(rising_edge(CLOCK_50)) then
--			clk_25 <= not clk_25;
--		end if;
--	end process;
	
	
	
	process(CLOCK_200)
	begin
		if(rising_edge(CLOCK_200))then
			case FSM_autotest is
				when A_SET_WRITE =>
					if(hAddr = "1111111111111111111111111") then
						FSM_autotest <= A_SET_READ;
						hAddr <= (others => '0');
					else
						FSM_autotest <= A_WRITE;
						hDIn <= hAddr(15 downto 0);
					end if;
				when A_WRITE =>
					wr <= '1';
					FSM_autotest <= A_WRITE_WAIT;
					LEDR(0) <= '1';
				when A_WRITE_WAIT =>
					if(done = '1') then
						wr <= '0';
						hAddr <= hAddr + 1;
						FSM_autotest <= A_SET_WRITE;
					end if;
					
				when A_SET_READ =>
					if(hAddr = "1111111111111111111111111") then
						LEDR(2) <= '1';
						FSM_autotest <= A_IDLE;
					else
						FSM_autotest <= A_READ;
					end if;
				
				when A_READ =>
					rd <= '1';
					FSM_autotest <= A_READ_WAIT;
					LEDR(1) <= '1';
					
				when A_READ_WAIT =>
					if(rdDone = '1') then
						rd <= '0';
						if(hDOut /= hAddr(15 downto 0)) then
							LEDR(8) <= '1';
							FSM_autotest <= A_IDLE;
						else
							hAddr <= hAddr + 1;
							FSM_autotest <= A_SET_READ;
						end if;
					end if;
					
				when A_IDLE =>
					if(KEY(3)='0')then
						hAddr <= (others => '0');
						LEDR(8 downto 0) <= (others => '0');
						FSM_autotest <= A_SET_WRITE;
					end if;
				
				when others =>
					FSM_autotest <= A_IDLE;
			end case;
		end if;
	end process;
	
--	process(CLOCK_50)
--	begin
--		if(rising_edge(CLOCK_50)) then
--			case FSM_inout is
--				when IDLE =>
--					if(KEY(3)='0') then
--						FSM_inout <= ADDR_LD;
--					elsif(KEY(2)='0') then
--						FSM_inout <= DATA_RD;
--					elsif(KEY(1)='0') then
--						FSM_inout <= DATA_WR;
--					end if;
--					
--				when ADDR_LD =>
--					hAddr(9 downto 0) <= SW;
--					FSM_inout <= IDLE;
--					
--				when DATA_RD =>
--					rd <= '1';
--					FSM_inout <= DATA_RD_WAIT;
--					
--				when DATA_RD_WAIT =>
--					if(rdDone='1') then
--						rd <= '0';
--						FSM_inout <= IDLE;
--						LEDR <= hDOut(9 downto 0);
--					end if;
--					
--				when DATA_WR =>
--					hDIn(9 downto 0) <= SW;
--					wr <= '1';
--					FSM_inout <= DATA_WR_WAIT;
--					
--				when DATA_WR_WAIT =>
--					if(done='1') then
--						LEDR <= hDIn(9 downto 0);
--						wr <= '0';
--						FSM_inout <= IDLE;
--					end if;
--					
--				when others =>
--					FSM_inout <= IDLE;
--			end case;
--		end if;
--	end process;

	

	SDRAM1: XSASDRAMCntl
    generic map(
      FREQ                 => 100_000,  -- operating frequency in KHz
      CLK_DIV              => 1.0,  -- divisor for FREQ (can only be 1.0, 1.5, 2.0, 2.5, 3.0, 4.0, 5.0, 8.0 or 16.0)
      PIPE_EN              => false,  -- if true, enable pipelined read operations
      MAX_NOP              => 10000,  -- number of NOPs before entering self-refresh
      MULTIPLE_ACTIVE_ROWS => false,  -- if true, allow an active row in each bank
      DATA_WIDTH           => 16,  -- host & SDRAM data width
      NROWS                => 8192,  -- number of rows in SDRAM array
      NCOLS                => 1024,  -- number of columns in SDRAM array
      HADDR_WIDTH          => 25,  -- host-side address width
      SADDR_WIDTH          => 13  -- SDRAM-side address width
      )
    port map(
      -- host side
      clk                  => CLOCK_100,--: in  std_logic;  -- master clock 
      bufclk               => open,--: out std_logic;  -- buffered master clock
      clk1x                => open,--: out std_logic;  -- host clock sync'ed to master clock (and divided if CLK_DIV>1)
      clk2x                => open,--: out std_logic;  -- double-speed host clock
      lock                 => open,--: out std_logic;  -- true when host clock is locked to master clock
      rst                  => '0',--: in  std_logic;  -- reset
      rd                   => rd,--: in  std_logic;  -- initiate read operation
      wr                   => wr,--: in  std_logic;  -- initiate write operation
      earlyOpBegun         => open,--: out std_logic;  -- read/write/self-refresh op begun     (async)
      opBegun              => open,--: out std_logic;  -- read/write/self-refresh op begun (clocked)
      rdPending            => open,--: out std_logic;  -- read operation(s) are still in the pipeline
      done                 => done,--: out std_logic;  -- read or write operation is done
      rdDone               => rdDone,--: out std_logic;  -- read done and data is available
      hAddr                => hAddr,--: in  std_logic_vector(HADDR_WIDTH-1 downto 0);  -- address from host
      hDIn                 => hDIn,--: in  std_logic_vector(DATA_WIDTH-1 downto 0);  -- data from host
      hDOut                => HDOut,--: out std_logic_vector(DATA_WIDTH-1 downto 0);  -- data to host
      status               => open,--: out std_logic_vector(3 downto 0);  -- diagnostic status of the FSM         

      -- SDRAM side
      sclkfb => CLOCK_100,--: in    std_logic;         -- clock from SDRAM after PCB delays
      sclk   => DRAM_CLK,--: out   std_logic;         -- SDRAM clock sync'ed to master clock
      cke    => DRAM_CKE,--: out   std_logic;         -- clock-enable to SDRAM
      cs_n   => DRAM_CS_N,--: out   std_logic;         -- chip-select to SDRAM
      ras_n  => DRAM_RAS_N,--: out   std_logic;         -- SDRAM row address strobe
      cas_n  => DRAM_CAS_N,--: out   std_logic;         -- SDRAM column address strobe
      we_n   => DRAM_WE_N,--: out   std_logic;         -- SDRAM write enable
      ba     => DRAM_BA,--: out   std_logic_vector(1 downto 0);  -- SDRAM bank address bits
      sAddr  => DRAM_ADDR,--: out   std_logic_vector(SADDR_WIDTH-1 downto 0);  -- SDRAM row/column address
      sData  => DRAM_DQ,--: inout std_logic_vector(DATA_WIDTH-1 downto 0);  -- SDRAM in/out databus
      dqmh   => DRAM_UDQM,--: out   std_logic;         -- high databits I/O mask
      dqml   => DRAM_LDQM--: out   std_logic          -- low databits I/O mask
      );
		
		
end basic;